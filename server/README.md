==========================================
This server is responsible for two things:
==========================================
1. Running the NukemNetBot for game room advertisements.
2. Leasing UDP Relays for players, as an alternative to port-forwarding.

==============
Helpful Utils:
==============
1. DOS_ARG_EXTRACTOR.EXE (when you want to figure out which arguments are passed to an executable by another program)
