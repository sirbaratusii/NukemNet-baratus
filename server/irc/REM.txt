
# register users (run after changing to desired nick, do it with aaBlueDragon or your own admin name, and most importantly, for NukemNetBot)
/NS REGISTER {pass}

# login users
/NS IDENTIFY aaBlueDragon {pass}
/NS IDENTIFY NukemNetBot {pass}

# join reserved channels
/join #Lobby
/join #NukemNetMeta

# register reserved channel
/CS REGISTER #NukemNetMeta # do this for aaBlueDragon and NukemNetBot
/CS REGISTER #Lobby # do this for aaBlueDragon and NukemNetBot

# add owner permission on reserved channels
/CS AMODE #lobby +q NukemNetBot
/CS AMODE #lobby +q aaBlueDragon
/CS AMODE #NukemNetMeta +q NukemNetBot
/CS AMODE #NukemNetMeta +q aaBlueDragon

# only moderators can send messages, others are muted
/MODE #NukemNetMeta +m 


# edit irc.yaml
accounts.default-user-modes: (remove all, no +i)
fakelag.enabled = false # for everyone, commands have to be set ASAP
ip-limits.max-concurrent-connections: at least 16 (so multiple players can connect from same LAN for lan games)
channels.registration.operator-only true # only allow chan registration to ops
enforce-utf8: false # allow arbitrary bytes, useful because ProtocolBuffer encoded with yEnc is sent as raw bytes.
casemapping: ascii # must be set, otherwise matrix-org-irc lib receives undefined value in nickname for all events, such as 'nick' (change nick), 'part', 'quit' and it causes exceptions on the code.

# (optional) IRC to Discord Bridge 
* Register Bot Name in IRC so it cannot be kicked, give Operator permission in #lobby, and auto-op (AMODE)
* json config file for bridge bot (https://github.com/reactiflux/discord-irc)
[{
    "nickname": "DiscordBridge",
    "server": "irc.nukemnet.com",
    "discordToken": "PutTokenHere",
    "channelMapping": {
      "#lobby": "#lobby"
    }
}]