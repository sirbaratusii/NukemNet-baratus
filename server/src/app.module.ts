import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { IrcNukemBotService } from './irc-nukem-bot/irc-nukem-bot.service';
import { RoomMgrService } from './room-mgr/room-mgr.service';
import { IrcService } from './irc/irc.service';
import { RelayService } from './relay/relay.service';
import { DiscordService } from './discord/discord.service';
import { RelayController } from './relay/relay.controller';
import { ConfigModule } from '@nestjs/config';
import { DatabaseService } from './database/database.service';
import { JwtModule } from '@nestjs/jwt';
import { PortCheckerController } from './port-checker/port-checker.controller';

@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.register({
      secret: 'afdsfdsfafafdsgdgf',
      signOptions: { expiresIn: '30d' },
    }),
  ],
  controllers: [
    RelayController,
    PortCheckerController,
    // LoginController // enable one I add support to session and db
  ],
  providers: [IrcNukemBotService, RoomMgrService, IrcService, RelayService, DiscordService, DatabaseService],
})
export class AppModule implements OnModuleInit {
  private readonly logger = new Logger(AppModule.name);

  constructor(private ircNukemBot: IrcNukemBotService, private discordService:DiscordService) {}

  async onModuleInit() {
    await this.discordService.init();
    this.ircNukemBot.start();

    process.on('uncaughtException', (e)=>{
      this.logger.error('uncaughtException: \r\n' + e.stack);
      // Handle this event to prevent the process from crashing.
      // DiscordJS has a bug where it crashes the ENTIRE process when it can't connect to the Discord API (not just a localized exception)
    });
  }

}
