import { Injectable } from '@nestjs/common';
import { Client } from 'matrix-org-irc';
import { BotMetaChannel, BotName } from '../common/constants';
import { IrcClientExtra } from '../common/ircExtend';

@Injectable()
export class IrcService {

    public readonly client = new Client(process.env.IRC_SERVER, BotName, {
        channels:[BotMetaChannel],
        retryCount: 0, // disable matrix-org-irc reconnection logic, using a manual reconnetion logic instead. see "connectWithRetry()"
        autoConnect:false,
        selfSigned:true,
        autoRejoin:true,
        userName:BotName,
        messageSplit: 400, // Without it, the library sometimes loses some characters between the splits (when sender is sending a long string that is being spitted) once it has to split, verified that behaviour in my multi-part support
        encoding: 'binary', // Must put that to prevent the default 'utf8' from being set, it would break Binary messages (such as ProtoBuff with yEnc) on secured TLS connections.
        stripColors: false, // Don't strip colors, might edit cmd messages from protocol-buffers and yEnc, and break multi-part message support.
        sasl: true
    });

    constructor() {
        IrcClientExtra.fixMatrixOrgIrcBug(this.client);
    }
    
}
