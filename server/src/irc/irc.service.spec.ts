import { Test, TestingModule } from '@nestjs/testing';
import { IrcService } from './irc.service';

describe('IrcService', () => {
  let service: IrcService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IrcService],
    }).compile();

    service = module.get<IrcService>(IrcService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
