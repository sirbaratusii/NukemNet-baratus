import { Test, TestingModule } from '@nestjs/testing';
import { IrcNukemBotService } from './irc-nukem-bot.service';

describe('IrcNukemBotService', () => {
  let service: IrcNukemBotService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IrcNukemBotService],
    }).compile();

    service = module.get<IrcNukemBotService>(IrcNukemBotService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
