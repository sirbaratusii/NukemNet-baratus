import { Body, Controller, Get, Post, Res } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IsNotEmpty, IsString } from 'class-validator';
import { Response } from 'express';
import * as net from 'net';
import { IrcClientExtra } from '../common/ircExtend';
import { DatabaseService } from '../database/database.service';

async function loginIrc(nick:string, password:string):Promise<boolean> {

    return new Promise((res,rej)=>{

        let ended = false;
        function end(err:any, validCreds?:boolean) {
            if(ended) return;
            ended = true;

            clearTimeout(timer);
            try {client.destroy()} catch(e) {}
            client.removeAllListeners();

            if(err) {
                return rej(err)
            }
            return res(validCreds);
        }

        const timer = setTimeout(()=>{
            end(new Error('Login Timeout'));
        },4000);

        const client = new net.Socket();
        client.connect(6667, '127.0.0.1', function() {
            IrcClientExtra.writeSaslPlainAuth(nick, password, client);
        });
        
        client.on('data', function(data) {
            const str = data.toString();
            if(str.includes('Authentication successful')) {
                end(null,true);
            } else if(str.includes('SASL authentication failed')) {
                end(null,false);
            }
        });
        
        client.on('error', (e)=>{
            end(e);
        })
    });
}

class LoginDto {
    @IsNotEmpty()
    @IsString()
    nick:string

    @IsString()
    @IsNotEmpty()
    password:string
}

@Controller('login')
export class LoginController {

    constructor(private jwtService: JwtService, private db:DatabaseService) {}


    // TODO rate limiting
    @Post()
    async doLogin(@Body() body: LoginDto, @Res() res:Response) {
        const valid = await loginIrc(body.nick, body.password);
        if(!valid) return res.status(401).send();

        let row = (await this.db.sql`SELECT * FROM public."Account" WHERE username = ${body.nick}`)?.[0];
        if(!row) {
            // Create user in DB if not exists
            row = (await this.db.sql`INSERT INTO public."Account" (username) VALUES(${body.nick.toLowerCase()}) RETURNING *`)[0];
        }
        
        const jwt = this.jwtService.sign({
            id: row.id,
            username: row.username
        });

        return res.send({access_token: jwt});
    }

    // TODO add a GET method to test if it's still valid (if need to login)
}
