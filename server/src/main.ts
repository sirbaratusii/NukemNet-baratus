require('dotenv').config();
(function verifyEnv() {
  const envVars = ['BOT_PASSWORD','IRC_SERVER']
  for(const e of envVars) {
    if(!process?.env?.[e]?.length) {
      console.error('Missing env var ' + e);
      process.exit(1);
    }
  }
})();

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  const port = parseInt(process.env.HTTP_PORT);
  app.listen(port, ()=>{
    console.log(`Listening on port ${port}`)
  });
}
bootstrap();
