import { Injectable, OnModuleInit } from '@nestjs/common';
import axios from 'axios';

interface RelayInfo {
    id:string,
    label:string
    host:string
    port:number
    SECRET_TOKEN:string
}


interface NewRelayResponse {
    id:string
    endOfLifeTS: number
    ipv6: boolean
    port:number
}

export interface RelayInstanceInfo extends NewRelayResponse {
    type:'udp'
    def:RelayInfo
    timerId:NodeJS.Timer
}

@Injectable()
export class RelayService implements OnModuleInit {

    relayDefinitions:{[id:string]: RelayInfo}={}

    relayInstances:{[id:string]: RelayInstanceInfo}={}

    async onModuleInit() {
        for(let i=0;;i++) {
            const relayKey = 'RELAY_'+i;
            const env = process.env[relayKey];
            if(!env) break;

            try {
                const relayInfo = JSON.parse(env) as RelayInfo;
                if(!relayInfo.id || !relayInfo.SECRET_TOKEN || !relayInfo.host || !relayInfo.label || !relayInfo.port) throw 'relay missing props'
                this.relayDefinitions[relayInfo.id] = relayInfo;
            } catch(e) {}
        }
    }

    public async stopLease(id:string) {
        const relayInstance = this.relayInstances[id];
        if(!relayInstance) return;
        delete this.relayInstances[id];

        clearTimeout(relayInstance.timerId);

        const relayDef = relayInstance.def;

        // retries
        for(let i=0;i<3;i++) {
            try {
                await axios.delete(`http://${relayDef.host}:${relayDef.port}/${relayInstance.type}/${id}`,{
                    headers: {
                        "authorization": `Bearer ${relayDef.SECRET_TOKEN}`
                    }
                });
                break; // if success, don't do retries
            } catch(e) {}
        }
    }

    public async leaseNewRelay(defId:string, type:'udp') {
        if(type != 'udp') {
            throw new Error("relay type can only be udp");
        }

        const relayDef = this.relayDefinitions[defId];
        if(!relayDef) throw new Error("No such relay id " + defId);

        try {
            const response = await axios.post(`http://${relayDef.host}:${relayDef.port}/${type}`, {
                ipv6: false // TODO remove later after next NukemNet release and deploying all relays, and nukemnet server
            }, {
                headers: {
                    "authorization": `Bearer ${relayDef.SECRET_TOKEN}`
                }
            });
            const instance = response.data as NewRelayResponse;

            const ttl = instance.endOfLifeTS-Date.now();
            const timerId = setTimeout(async ()=>{
                try {
                    await this.stopLease(instance.id);
                } catch(e) {}
            },ttl);

            const fullObj = {timerId, def:relayDef, type, ...instance} as RelayInstanceInfo;
            this.relayInstances[instance.id] = fullObj;
            return fullObj;
        } catch(e) {
            throw e;
        }
    }

}
