export function removeNonAlphaNumChars(str:string) {
    return str.replace(/\W/g, '')
}

export function truncateWithEllipses(text:string, max:number) 
{
    return text.substr(0,max-1)+(text.length>max?'...':''); 
}