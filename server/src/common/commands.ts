import { Room_Advertisement, Room_CreateRequest, Room_UpdatePatchFromBot, Room_UpdatePatchFromHost } from "./room"

// NukemNet Commands
export const attrs = {
    prefix: 'cmd:',
    startSign: '<%!%cmd%!%>',
    endSign: '</%!%cmd%!%>'
}

export interface Cmd_Base {op:string, id?:string, resToReqId?:string, data?:any}

export interface Cmd_ErrorResponse extends Cmd_Base {
    op:'nnerror'
    error:string
    resToReqId:string
}

export interface Cmd_CreateRoom extends Cmd_Base {
    op: 'createRoom',
    data: Room_CreateRequest
}

export interface Cmd_CreateRoomResponse extends Cmd_Base {
    op:'roomCreated',
    resToReqId:string
    data: {
        channel: string
    }
}

export interface Cmd_GetAllRooms extends Cmd_Base {
    op:'getAllRooms'
}

export interface Cmd_GetAllRoomsResponse extends Cmd_Base {
    op:'getAllRoomsResponse',
    data: {
        rooms:Room_Advertisement[]
    }
}

export interface Cmd_RequestUpdateRoom extends Cmd_Base {
    op: 'requestUpdateRoom',
    data: {
        channel: string,
        patch: Room_UpdatePatchFromHost
    }
}

export interface Cmd_ResponseUpdateRoom extends Cmd_Base {
    op: 'updateRoomResponse',
    data: {
        foundChanges:boolean
    }
}

export interface Cmd_DeleteRoom extends Cmd_Base {
    op:'deleteRoom',
    data: {channel:string}
}

export interface Cmd_ManageRelayRequest_Base extends Cmd_Base {
    op:'manageRelayRequest',
    data: {
        action:'fetch'|'destroy'
    }
}

export interface Cmd_ManageRelayRequestDestroy extends Cmd_ManageRelayRequest_Base {
    op:'manageRelayRequest',
    data: {
        action:'destroy'
    }
}

export interface Cmd_ManageRelayRequestFetch extends Cmd_ManageRelayRequest_Base {
    op:'manageRelayRequest',
    data: {
        action:'fetch',
        type: 'udp'
        id:string // relay definition id
    }
}

export interface LeasedDedicatedServerDetails {
    ipOrHostname:string,
    port:number,
    type:'udp',
    terminationTimestamp: number
    forced?:boolean
}


export interface Cmd_ManageRelayCreated extends Cmd_Base {
    op:'spawnRelayCreated',
    data: {
        conn?:LeasedDedicatedServerDetails
        error?:string // (1) cooldown left. (2) all ports taken (maybe have 2000 in reserve)
    }
}

export interface Cmd_RelayClosed extends Cmd_Base {
    op:'relayClosed',
    data: {}
}