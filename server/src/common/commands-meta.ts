import { Room_Advertisement, Room_CreateRequest, Room_UpdatePatchFromBot } from "./room"
import {Cmd_Base} from './commands'
import { ElementType } from "./type-utils"

// Commands from bot to meta channel

const CmdMeta_NewRoom_op = 'newRoom'
export interface CmdMeta_NewRoom extends Cmd_Base {
    op:typeof CmdMeta_NewRoom_op,
    data: Room_Advertisement
}

const CmdMeta_RoomDeleted_op = 'roomsDeleted'
export interface CmdMeta_RoomDeleted extends Cmd_Base {
    op:typeof CmdMeta_RoomDeleted_op,
    data: {
        channels: string[]
    }
}

const CmdMeta_RoomDetailsUpdate_op = 'roomDetailsUpdate'
export interface CmdMeta_RoomDetailsUpdate extends Cmd_Base {
    op:typeof CmdMeta_RoomDetailsUpdate_op,
    data: {
        channel: string
        patch: Room_UpdatePatchFromBot
    }
}

export const CmdMeta_AllOps = [CmdMeta_RoomDetailsUpdate_op, CmdMeta_RoomDeleted_op,CmdMeta_NewRoom_op] as const;
export type CmdMeta_AllTypes = CmdMeta_RoomDetailsUpdate|CmdMeta_RoomDeleted|CmdMeta_NewRoom;
export type CmdMeta_AllOpTypes = ElementType<typeof CmdMeta_AllOps>