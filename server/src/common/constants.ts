export const maxChannelNameLength = 31;
export const BotName = 'NukemNetBot';
export const BotName_lowercase = BotName.toLowerCase();
export const BotMetaChannel = '#NukemNetMeta';
export const BotMetaChannel_lowercase = BotMetaChannel.toLowerCase();
export const NukemNetLobby = '#Lobby';
export const NukemNetLobby_lowercase = '#lobby';
export const MIrc_MaxBytesLength = 403; // this is specific to the "mIRC" client.
export const MatrixIrcClient_MaxBytesLength = 454 // this is specific to the node's "matrix-org-irc" npm lib client.
export const ClientAutojoinChannels = [NukemNetLobby_lowercase, BotMetaChannel_lowercase];

export function isNukemNetBot(nick: string) {
    return nick.toLowerCase() === BotName_lowercase;
}

export function isMetaChannel(chan: string) {
    return chan.toLowerCase() === BotMetaChannel_lowercase;
}

export type IrcModSigns = `~`|`&`|`@`|`%`|`+`;
export type IrcModSignCmd = `q`|`a`|`o`|`h`|'v';
export type IrcModSignOrNone = IrcModSigns | '';

export const IrcModSign_ToNum:{[sign:string]: number} = {}
IrcModSign_ToNum['~' as IrcModSigns] = 5;
IrcModSign_ToNum['&' as IrcModSigns] = 4;
IrcModSign_ToNum['@' as IrcModSigns] = 3;
IrcModSign_ToNum['%' as IrcModSigns] = 2;
IrcModSign_ToNum['+' as IrcModSigns] = 1;

export const IrcUserMod = {
    owner:      {sign:`~`,  cmd:`q`},
    admin:      {sign:`&`,  cmd:'a'},
    operator:   {sign:'@',  cmd:'o'},
    halfOp:     {sign:'%',  cmd:'h'},
    voice:      {sign:'+',  cmd:'v'}
} as {[permName:string]: {sign:IrcModSigns, cmd:IrcModSignCmd}};

export const IrcUserModSigns_High = new Set<IrcModSignOrNone>([IrcUserMod.owner.sign, IrcUserMod.admin.sign, IrcUserMod.operator.sign]);

export const IrcUserMod_SignToCmd:{[str:string]:IrcModSignCmd} = {}
export const IrcUserMod_CmdToSign:{[str:string]:IrcModSigns} = {}

for(const name in IrcUserMod) {
    const obj = IrcUserMod[name];

    const sign = obj.sign;
    const cmd = obj.cmd;

    IrcUserMod_SignToCmd[sign] = cmd;
    IrcUserMod_CmdToSign[cmd] = sign;
}