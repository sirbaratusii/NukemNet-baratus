/**
 * This module was automatically generated by `ts-interface-builder`
 */
import * as t from "ts-interface-checker";
// tslint:disable:object-literal-key-quotes

export const CmdMeta_NewRoom = t.iface(["Cmd_Base"], {
  "op": t.lit('newRoom'),
  "data": "Room_Advertisement",
});

export const CmdMeta_RoomDeleted = t.iface(["Cmd_Base"], {
  "op": t.lit('roomsDeleted'),
  "data": t.iface([], {
    "channels": t.array("string"),
  }),
});

export const CmdMeta_RoomDetailsUpdate = t.iface(["Cmd_Base"], {
  "op": t.lit('roomDetailsUpdate'),
  "data": t.iface([], {
    "channel": "string",
    "patch": "Room_UpdatePatchFromBot",
  }),
});

const exportedTypeSuite: t.ITypeSuite = {
  CmdMeta_NewRoom,
  CmdMeta_RoomDeleted,
  CmdMeta_RoomDetailsUpdate,
};
export default exportedTypeSuite;
