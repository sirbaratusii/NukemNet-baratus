/* eslint-disable */
import Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'com.nukemnet';

export interface LaunchArgType {
  id: string;
  label?: string | undefined;
  valueLabel?: string | undefined;
  args: string[];
  order?: number | undefined;
  for?: string | undefined;
  syncOnly?: boolean | undefined;
}

export interface RoomAdvertisement {
  name: string;
  gameId: string;
  gameName: string;
  execId: string;
  execName: string;
  maxPlayers: number;
  playersIn: number;
  isStarted: boolean;
  hasPassword: boolean;
  owner: string;
  createdAt: number;
  channel: string;
}

export interface LeasedDedicatedServerDetails {
  ipOrHostname: string;
  port: string;
  terminationTimestamp: number;
}

export interface ParticipantDetails {
  isExecRunning: boolean;
  isExecMissing: boolean;
  ipsAndPorts?: IpDetailsAndPorts;
}

/** FullGameRoomDetails */
export interface CmdGameRoomSyncSendAllDetails {
  publicDetails?: RoomAdvertisement;
  params: LaunchArgType[];
  dedicatedServer?: LeasedDedicatedServerDetails | undefined;
  participantNicks: { [key: string]: ParticipantDetails };
}

export interface CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry {
  key: string;
  value?: ParticipantDetails;
}

export interface IpDetailsAndPorts {
  localIpV6: string[];
  localIpV4: string[];
  externalIpV4?: string | undefined;
  externalIpV6?: string | undefined;
  gamePort: number;
  httpPort: number;
  ipv4mode?: boolean | undefined;
  ipv6tp: number;
}

export interface CmdGameRoomAskToParticipate {
  channel: string;
  ipsAndPorts?: IpDetailsAndPorts;
}

export interface CmdGameRoomPingToHost {
  ping: number;
}

export interface RootCommand {
  data?:
    | {
        $case: 'cmdAskToParticipate';
        cmdAskToParticipate: CmdGameRoomAskToParticipate;
      }
    | {
        $case: 'cmdSyncSendAllDetails';
        cmdSyncSendAllDetails: CmdGameRoomSyncSendAllDetails;
      }
    | { $case: 'cmdPingToHost'; cmdPingToHost: CmdGameRoomPingToHost };
}

function createBaseLaunchArgType(): LaunchArgType {
  return {
    id: '',
    label: undefined,
    valueLabel: undefined,
    args: [],
    order: undefined,
    for: undefined,
    syncOnly: undefined,
  };
}

export const LaunchArgType = {
  encode(
    message: LaunchArgType,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.id !== '') {
      writer.uint32(10).string(message.id);
    }
    if (message.label !== undefined) {
      writer.uint32(18).string(message.label);
    }
    if (message.valueLabel !== undefined) {
      writer.uint32(26).string(message.valueLabel);
    }
    for (const v of message.args) {
      writer.uint32(34).string(v!);
    }
    if (message.order !== undefined) {
      writer.uint32(40).int64(message.order);
    }
    if (message.for !== undefined) {
      writer.uint32(50).string(message.for);
    }
    if (message.syncOnly !== undefined) {
      writer.uint32(56).bool(message.syncOnly);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LaunchArgType {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLaunchArgType();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.label = reader.string();
          break;
        case 3:
          message.valueLabel = reader.string();
          break;
        case 4:
          message.args.push(reader.string());
          break;
        case 5:
          message.order = longToNumber(reader.int64() as Long);
          break;
        case 6:
          message.for = reader.string();
          break;
        case 7:
          message.syncOnly = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LaunchArgType {
    return {
      id: isSet(object.id) ? String(object.id) : '',
      label: isSet(object.label) ? String(object.label) : undefined,
      valueLabel: isSet(object.valueLabel)
        ? String(object.valueLabel)
        : undefined,
      args: Array.isArray(object?.args)
        ? object.args.map((e: any) => String(e))
        : [],
      order: isSet(object.order) ? Number(object.order) : undefined,
      for: isSet(object.for) ? String(object.for) : undefined,
      syncOnly: isSet(object.syncOnly) ? Boolean(object.syncOnly) : undefined,
    };
  },

  toJSON(message: LaunchArgType): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.label !== undefined && (obj.label = message.label);
    message.valueLabel !== undefined && (obj.valueLabel = message.valueLabel);
    if (message.args) {
      obj.args = message.args.map((e) => e);
    } else {
      obj.args = [];
    }
    message.order !== undefined && (obj.order = Math.round(message.order));
    message.for !== undefined && (obj.for = message.for);
    message.syncOnly !== undefined && (obj.syncOnly = message.syncOnly);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<LaunchArgType>, I>>(
    object: I,
  ): LaunchArgType {
    const message = createBaseLaunchArgType();
    message.id = object.id ?? '';
    message.label = object.label ?? undefined;
    message.valueLabel = object.valueLabel ?? undefined;
    message.args = object.args?.map((e) => e) || [];
    message.order = object.order ?? undefined;
    message.for = object.for ?? undefined;
    message.syncOnly = object.syncOnly ?? undefined;
    return message;
  },
};

function createBaseRoomAdvertisement(): RoomAdvertisement {
  return {
    name: '',
    gameId: '',
    gameName: '',
    execId: '',
    execName: '',
    maxPlayers: 0,
    playersIn: 0,
    isStarted: false,
    hasPassword: false,
    owner: '',
    createdAt: 0,
    channel: '',
  };
}

export const RoomAdvertisement = {
  encode(
    message: RoomAdvertisement,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.name !== '') {
      writer.uint32(10).string(message.name);
    }
    if (message.gameId !== '') {
      writer.uint32(18).string(message.gameId);
    }
    if (message.gameName !== '') {
      writer.uint32(26).string(message.gameName);
    }
    if (message.execId !== '') {
      writer.uint32(34).string(message.execId);
    }
    if (message.execName !== '') {
      writer.uint32(42).string(message.execName);
    }
    if (message.maxPlayers !== 0) {
      writer.uint32(48).uint32(message.maxPlayers);
    }
    if (message.playersIn !== 0) {
      writer.uint32(56).uint32(message.playersIn);
    }
    if (message.isStarted === true) {
      writer.uint32(64).bool(message.isStarted);
    }
    if (message.hasPassword === true) {
      writer.uint32(72).bool(message.hasPassword);
    }
    if (message.owner !== '') {
      writer.uint32(82).string(message.owner);
    }
    if (message.createdAt !== 0) {
      writer.uint32(88).uint64(message.createdAt);
    }
    if (message.channel !== '') {
      writer.uint32(98).string(message.channel);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RoomAdvertisement {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRoomAdvertisement();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.string();
          break;
        case 2:
          message.gameId = reader.string();
          break;
        case 3:
          message.gameName = reader.string();
          break;
        case 4:
          message.execId = reader.string();
          break;
        case 5:
          message.execName = reader.string();
          break;
        case 6:
          message.maxPlayers = reader.uint32();
          break;
        case 7:
          message.playersIn = reader.uint32();
          break;
        case 8:
          message.isStarted = reader.bool();
          break;
        case 9:
          message.hasPassword = reader.bool();
          break;
        case 10:
          message.owner = reader.string();
          break;
        case 11:
          message.createdAt = longToNumber(reader.uint64() as Long);
          break;
        case 12:
          message.channel = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): RoomAdvertisement {
    return {
      name: isSet(object.name) ? String(object.name) : '',
      gameId: isSet(object.gameId) ? String(object.gameId) : '',
      gameName: isSet(object.gameName) ? String(object.gameName) : '',
      execId: isSet(object.execId) ? String(object.execId) : '',
      execName: isSet(object.execName) ? String(object.execName) : '',
      maxPlayers: isSet(object.maxPlayers) ? Number(object.maxPlayers) : 0,
      playersIn: isSet(object.playersIn) ? Number(object.playersIn) : 0,
      isStarted: isSet(object.isStarted) ? Boolean(object.isStarted) : false,
      hasPassword: isSet(object.hasPassword)
        ? Boolean(object.hasPassword)
        : false,
      owner: isSet(object.owner) ? String(object.owner) : '',
      createdAt: isSet(object.createdAt) ? Number(object.createdAt) : 0,
      channel: isSet(object.channel) ? String(object.channel) : '',
    };
  },

  toJSON(message: RoomAdvertisement): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.gameId !== undefined && (obj.gameId = message.gameId);
    message.gameName !== undefined && (obj.gameName = message.gameName);
    message.execId !== undefined && (obj.execId = message.execId);
    message.execName !== undefined && (obj.execName = message.execName);
    message.maxPlayers !== undefined &&
      (obj.maxPlayers = Math.round(message.maxPlayers));
    message.playersIn !== undefined &&
      (obj.playersIn = Math.round(message.playersIn));
    message.isStarted !== undefined && (obj.isStarted = message.isStarted);
    message.hasPassword !== undefined &&
      (obj.hasPassword = message.hasPassword);
    message.owner !== undefined && (obj.owner = message.owner);
    message.createdAt !== undefined &&
      (obj.createdAt = Math.round(message.createdAt));
    message.channel !== undefined && (obj.channel = message.channel);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<RoomAdvertisement>, I>>(
    object: I,
  ): RoomAdvertisement {
    const message = createBaseRoomAdvertisement();
    message.name = object.name ?? '';
    message.gameId = object.gameId ?? '';
    message.gameName = object.gameName ?? '';
    message.execId = object.execId ?? '';
    message.execName = object.execName ?? '';
    message.maxPlayers = object.maxPlayers ?? 0;
    message.playersIn = object.playersIn ?? 0;
    message.isStarted = object.isStarted ?? false;
    message.hasPassword = object.hasPassword ?? false;
    message.owner = object.owner ?? '';
    message.createdAt = object.createdAt ?? 0;
    message.channel = object.channel ?? '';
    return message;
  },
};

function createBaseLeasedDedicatedServerDetails(): LeasedDedicatedServerDetails {
  return { ipOrHostname: '', port: '', terminationTimestamp: 0 };
}

export const LeasedDedicatedServerDetails = {
  encode(
    message: LeasedDedicatedServerDetails,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.ipOrHostname !== '') {
      writer.uint32(10).string(message.ipOrHostname);
    }
    if (message.port !== '') {
      writer.uint32(18).string(message.port);
    }
    if (message.terminationTimestamp !== 0) {
      writer.uint32(24).uint64(message.terminationTimestamp);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number,
  ): LeasedDedicatedServerDetails {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLeasedDedicatedServerDetails();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ipOrHostname = reader.string();
          break;
        case 2:
          message.port = reader.string();
          break;
        case 3:
          message.terminationTimestamp = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LeasedDedicatedServerDetails {
    return {
      ipOrHostname: isSet(object.ipOrHostname)
        ? String(object.ipOrHostname)
        : '',
      port: isSet(object.port) ? String(object.port) : '',
      terminationTimestamp: isSet(object.terminationTimestamp)
        ? Number(object.terminationTimestamp)
        : 0,
    };
  },

  toJSON(message: LeasedDedicatedServerDetails): unknown {
    const obj: any = {};
    message.ipOrHostname !== undefined &&
      (obj.ipOrHostname = message.ipOrHostname);
    message.port !== undefined && (obj.port = message.port);
    message.terminationTimestamp !== undefined &&
      (obj.terminationTimestamp = Math.round(message.terminationTimestamp));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<LeasedDedicatedServerDetails>, I>>(
    object: I,
  ): LeasedDedicatedServerDetails {
    const message = createBaseLeasedDedicatedServerDetails();
    message.ipOrHostname = object.ipOrHostname ?? '';
    message.port = object.port ?? '';
    message.terminationTimestamp = object.terminationTimestamp ?? 0;
    return message;
  },
};

function createBaseParticipantDetails(): ParticipantDetails {
  return { isExecRunning: false, isExecMissing: false, ipsAndPorts: undefined };
}

export const ParticipantDetails = {
  encode(
    message: ParticipantDetails,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.isExecRunning === true) {
      writer.uint32(8).bool(message.isExecRunning);
    }
    if (message.isExecMissing === true) {
      writer.uint32(16).bool(message.isExecMissing);
    }
    if (message.ipsAndPorts !== undefined) {
      IpDetailsAndPorts.encode(
        message.ipsAndPorts,
        writer.uint32(26).fork(),
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ParticipantDetails {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseParticipantDetails();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.isExecRunning = reader.bool();
          break;
        case 2:
          message.isExecMissing = reader.bool();
          break;
        case 3:
          message.ipsAndPorts = IpDetailsAndPorts.decode(
            reader,
            reader.uint32(),
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ParticipantDetails {
    return {
      isExecRunning: isSet(object.isExecRunning)
        ? Boolean(object.isExecRunning)
        : false,
      isExecMissing: isSet(object.isExecMissing)
        ? Boolean(object.isExecMissing)
        : false,
      ipsAndPorts: isSet(object.ipsAndPorts)
        ? IpDetailsAndPorts.fromJSON(object.ipsAndPorts)
        : undefined,
    };
  },

  toJSON(message: ParticipantDetails): unknown {
    const obj: any = {};
    message.isExecRunning !== undefined &&
      (obj.isExecRunning = message.isExecRunning);
    message.isExecMissing !== undefined &&
      (obj.isExecMissing = message.isExecMissing);
    message.ipsAndPorts !== undefined &&
      (obj.ipsAndPorts = message.ipsAndPorts
        ? IpDetailsAndPorts.toJSON(message.ipsAndPorts)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ParticipantDetails>, I>>(
    object: I,
  ): ParticipantDetails {
    const message = createBaseParticipantDetails();
    message.isExecRunning = object.isExecRunning ?? false;
    message.isExecMissing = object.isExecMissing ?? false;
    message.ipsAndPorts =
      object.ipsAndPorts !== undefined && object.ipsAndPorts !== null
        ? IpDetailsAndPorts.fromPartial(object.ipsAndPorts)
        : undefined;
    return message;
  },
};

function createBaseCmdGameRoomSyncSendAllDetails(): CmdGameRoomSyncSendAllDetails {
  return {
    publicDetails: undefined,
    params: [],
    dedicatedServer: undefined,
    participantNicks: {},
  };
}

export const CmdGameRoomSyncSendAllDetails = {
  encode(
    message: CmdGameRoomSyncSendAllDetails,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.publicDetails !== undefined) {
      RoomAdvertisement.encode(
        message.publicDetails,
        writer.uint32(10).fork(),
      ).ldelim();
    }
    for (const v of message.params) {
      LaunchArgType.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.dedicatedServer !== undefined) {
      LeasedDedicatedServerDetails.encode(
        message.dedicatedServer,
        writer.uint32(26).fork(),
      ).ldelim();
    }
    Object.entries(message.participantNicks).forEach(([key, value]) => {
      CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry.encode(
        { key: key as any, value },
        writer.uint32(34).fork(),
      ).ldelim();
    });
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number,
  ): CmdGameRoomSyncSendAllDetails {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCmdGameRoomSyncSendAllDetails();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.publicDetails = RoomAdvertisement.decode(
            reader,
            reader.uint32(),
          );
          break;
        case 2:
          message.params.push(LaunchArgType.decode(reader, reader.uint32()));
          break;
        case 3:
          message.dedicatedServer = LeasedDedicatedServerDetails.decode(
            reader,
            reader.uint32(),
          );
          break;
        case 4:
          const entry4 =
            CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry.decode(
              reader,
              reader.uint32(),
            );
          if (entry4.value !== undefined) {
            message.participantNicks[entry4.key] = entry4.value;
          }
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CmdGameRoomSyncSendAllDetails {
    return {
      publicDetails: isSet(object.publicDetails)
        ? RoomAdvertisement.fromJSON(object.publicDetails)
        : undefined,
      params: Array.isArray(object?.params)
        ? object.params.map((e: any) => LaunchArgType.fromJSON(e))
        : [],
      dedicatedServer: isSet(object.dedicatedServer)
        ? LeasedDedicatedServerDetails.fromJSON(object.dedicatedServer)
        : undefined,
      participantNicks: isObject(object.participantNicks)
        ? Object.entries(object.participantNicks).reduce<{
            [key: string]: ParticipantDetails;
          }>((acc, [key, value]) => {
            acc[key] = ParticipantDetails.fromJSON(value);
            return acc;
          }, {})
        : {},
    };
  },

  toJSON(message: CmdGameRoomSyncSendAllDetails): unknown {
    const obj: any = {};
    message.publicDetails !== undefined &&
      (obj.publicDetails = message.publicDetails
        ? RoomAdvertisement.toJSON(message.publicDetails)
        : undefined);
    if (message.params) {
      obj.params = message.params.map((e) =>
        e ? LaunchArgType.toJSON(e) : undefined,
      );
    } else {
      obj.params = [];
    }
    message.dedicatedServer !== undefined &&
      (obj.dedicatedServer = message.dedicatedServer
        ? LeasedDedicatedServerDetails.toJSON(message.dedicatedServer)
        : undefined);
    obj.participantNicks = {};
    if (message.participantNicks) {
      Object.entries(message.participantNicks).forEach(([k, v]) => {
        obj.participantNicks[k] = ParticipantDetails.toJSON(v);
      });
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CmdGameRoomSyncSendAllDetails>, I>>(
    object: I,
  ): CmdGameRoomSyncSendAllDetails {
    const message = createBaseCmdGameRoomSyncSendAllDetails();
    message.publicDetails =
      object.publicDetails !== undefined && object.publicDetails !== null
        ? RoomAdvertisement.fromPartial(object.publicDetails)
        : undefined;
    message.params =
      object.params?.map((e) => LaunchArgType.fromPartial(e)) || [];
    message.dedicatedServer =
      object.dedicatedServer !== undefined && object.dedicatedServer !== null
        ? LeasedDedicatedServerDetails.fromPartial(object.dedicatedServer)
        : undefined;
    message.participantNicks = Object.entries(
      object.participantNicks ?? {},
    ).reduce<{ [key: string]: ParticipantDetails }>((acc, [key, value]) => {
      if (value !== undefined) {
        acc[key] = ParticipantDetails.fromPartial(value);
      }
      return acc;
    }, {});
    return message;
  },
};

function createBaseCmdGameRoomSyncSendAllDetails_ParticipantNicksEntry(): CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry {
  return { key: '', value: undefined };
}

export const CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry = {
  encode(
    message: CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.key !== '') {
      writer.uint32(10).string(message.key);
    }
    if (message.value !== undefined) {
      ParticipantDetails.encode(
        message.value,
        writer.uint32(18).fork(),
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number,
  ): CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message =
      createBaseCmdGameRoomSyncSendAllDetails_ParticipantNicksEntry();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.key = reader.string();
          break;
        case 2:
          message.value = ParticipantDetails.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry {
    return {
      key: isSet(object.key) ? String(object.key) : '',
      value: isSet(object.value)
        ? ParticipantDetails.fromJSON(object.value)
        : undefined,
    };
  },

  toJSON(
    message: CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry,
  ): unknown {
    const obj: any = {};
    message.key !== undefined && (obj.key = message.key);
    message.value !== undefined &&
      (obj.value = message.value
        ? ParticipantDetails.toJSON(message.value)
        : undefined);
    return obj;
  },

  fromPartial<
    I extends Exact<
      DeepPartial<CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry>,
      I
    >,
  >(object: I): CmdGameRoomSyncSendAllDetails_ParticipantNicksEntry {
    const message =
      createBaseCmdGameRoomSyncSendAllDetails_ParticipantNicksEntry();
    message.key = object.key ?? '';
    message.value =
      object.value !== undefined && object.value !== null
        ? ParticipantDetails.fromPartial(object.value)
        : undefined;
    return message;
  },
};

function createBaseIpDetailsAndPorts(): IpDetailsAndPorts {
  return {
    localIpV6: [],
    localIpV4: [],
    externalIpV4: undefined,
    externalIpV6: undefined,
    gamePort: 0,
    httpPort: 0,
    ipv4mode: undefined,
    ipv6tp: 0,
  };
}

export const IpDetailsAndPorts = {
  encode(
    message: IpDetailsAndPorts,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    for (const v of message.localIpV6) {
      writer.uint32(10).string(v!);
    }
    for (const v of message.localIpV4) {
      writer.uint32(18).string(v!);
    }
    if (message.externalIpV4 !== undefined) {
      writer.uint32(26).string(message.externalIpV4);
    }
    if (message.externalIpV6 !== undefined) {
      writer.uint32(34).string(message.externalIpV6);
    }
    if (message.gamePort !== 0) {
      writer.uint32(40).uint32(message.gamePort);
    }
    if (message.httpPort !== 0) {
      writer.uint32(48).uint32(message.httpPort);
    }
    if (message.ipv4mode !== undefined) {
      writer.uint32(56).bool(message.ipv4mode);
    }
    if (message.ipv6tp !== 0) {
      writer.uint32(64).uint32(message.ipv6tp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): IpDetailsAndPorts {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseIpDetailsAndPorts();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.localIpV6.push(reader.string());
          break;
        case 2:
          message.localIpV4.push(reader.string());
          break;
        case 3:
          message.externalIpV4 = reader.string();
          break;
        case 4:
          message.externalIpV6 = reader.string();
          break;
        case 5:
          message.gamePort = reader.uint32();
          break;
        case 6:
          message.httpPort = reader.uint32();
          break;
        case 7:
          message.ipv4mode = reader.bool();
          break;
        case 8:
          message.ipv6tp = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): IpDetailsAndPorts {
    return {
      localIpV6: Array.isArray(object?.localIpV6)
        ? object.localIpV6.map((e: any) => String(e))
        : [],
      localIpV4: Array.isArray(object?.localIpV4)
        ? object.localIpV4.map((e: any) => String(e))
        : [],
      externalIpV4: isSet(object.externalIpV4)
        ? String(object.externalIpV4)
        : undefined,
      externalIpV6: isSet(object.externalIpV6)
        ? String(object.externalIpV6)
        : undefined,
      gamePort: isSet(object.gamePort) ? Number(object.gamePort) : 0,
      httpPort: isSet(object.httpPort) ? Number(object.httpPort) : 0,
      ipv4mode: isSet(object.ipv4mode) ? Boolean(object.ipv4mode) : undefined,
      ipv6tp: isSet(object.ipv6tp) ? Number(object.ipv6tp) : 0,
    };
  },

  toJSON(message: IpDetailsAndPorts): unknown {
    const obj: any = {};
    if (message.localIpV6) {
      obj.localIpV6 = message.localIpV6.map((e) => e);
    } else {
      obj.localIpV6 = [];
    }
    if (message.localIpV4) {
      obj.localIpV4 = message.localIpV4.map((e) => e);
    } else {
      obj.localIpV4 = [];
    }
    message.externalIpV4 !== undefined &&
      (obj.externalIpV4 = message.externalIpV4);
    message.externalIpV6 !== undefined &&
      (obj.externalIpV6 = message.externalIpV6);
    message.gamePort !== undefined &&
      (obj.gamePort = Math.round(message.gamePort));
    message.httpPort !== undefined && (obj.httpPort = Math.round(message.httpPort));
    message.ipv4mode !== undefined && (obj.ipv4mode = message.ipv4mode);
    message.ipv6tp !== undefined && (obj.ipv6tp = Math.round(message.ipv6tp));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<IpDetailsAndPorts>, I>>(
    object: I,
  ): IpDetailsAndPorts {
    const message = createBaseIpDetailsAndPorts();
    message.localIpV6 = object.localIpV6?.map((e) => e) || [];
    message.localIpV4 = object.localIpV4?.map((e) => e) || [];
    message.externalIpV4 = object.externalIpV4 ?? undefined;
    message.externalIpV6 = object.externalIpV6 ?? undefined;
    message.gamePort = object.gamePort ?? 0;
    message.httpPort = object.httpPort ?? 0;
    message.ipv4mode = object.ipv4mode ?? undefined;
    message.ipv6tp = object.ipv6tp ?? 0;
    return message;
  },
};

function createBaseCmdGameRoomAskToParticipate(): CmdGameRoomAskToParticipate {
  return { channel: '', ipsAndPorts: undefined };
}

export const CmdGameRoomAskToParticipate = {
  encode(
    message: CmdGameRoomAskToParticipate,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.channel !== '') {
      writer.uint32(10).string(message.channel);
    }
    if (message.ipsAndPorts !== undefined) {
      IpDetailsAndPorts.encode(
        message.ipsAndPorts,
        writer.uint32(18).fork(),
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number,
  ): CmdGameRoomAskToParticipate {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCmdGameRoomAskToParticipate();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.channel = reader.string();
          break;
        case 2:
          message.ipsAndPorts = IpDetailsAndPorts.decode(
            reader,
            reader.uint32(),
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CmdGameRoomAskToParticipate {
    return {
      channel: isSet(object.channel) ? String(object.channel) : '',
      ipsAndPorts: isSet(object.ipsAndPorts)
        ? IpDetailsAndPorts.fromJSON(object.ipsAndPorts)
        : undefined,
    };
  },

  toJSON(message: CmdGameRoomAskToParticipate): unknown {
    const obj: any = {};
    message.channel !== undefined && (obj.channel = message.channel);
    message.ipsAndPorts !== undefined &&
      (obj.ipsAndPorts = message.ipsAndPorts
        ? IpDetailsAndPorts.toJSON(message.ipsAndPorts)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CmdGameRoomAskToParticipate>, I>>(
    object: I,
  ): CmdGameRoomAskToParticipate {
    const message = createBaseCmdGameRoomAskToParticipate();
    message.channel = object.channel ?? '';
    message.ipsAndPorts =
      object.ipsAndPorts !== undefined && object.ipsAndPorts !== null
        ? IpDetailsAndPorts.fromPartial(object.ipsAndPorts)
        : undefined;
    return message;
  },
};

function createBaseCmdGameRoomPingToHost(): CmdGameRoomPingToHost {
  return { ping: 0 };
}

export const CmdGameRoomPingToHost = {
  encode(
    message: CmdGameRoomPingToHost,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.ping !== 0) {
      writer.uint32(8).uint32(message.ping);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number,
  ): CmdGameRoomPingToHost {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCmdGameRoomPingToHost();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ping = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CmdGameRoomPingToHost {
    return {
      ping: isSet(object.ping) ? Number(object.ping) : 0,
    };
  },

  toJSON(message: CmdGameRoomPingToHost): unknown {
    const obj: any = {};
    message.ping !== undefined && (obj.ping = Math.round(message.ping));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CmdGameRoomPingToHost>, I>>(
    object: I,
  ): CmdGameRoomPingToHost {
    const message = createBaseCmdGameRoomPingToHost();
    message.ping = object.ping ?? 0;
    return message;
  },
};

function createBaseRootCommand(): RootCommand {
  return { data: undefined };
}

export const RootCommand = {
  encode(
    message: RootCommand,
    writer: _m0.Writer = _m0.Writer.create(),
  ): _m0.Writer {
    if (message.data?.$case === 'cmdAskToParticipate') {
      CmdGameRoomAskToParticipate.encode(
        message.data.cmdAskToParticipate,
        writer.uint32(10).fork(),
      ).ldelim();
    }
    if (message.data?.$case === 'cmdSyncSendAllDetails') {
      CmdGameRoomSyncSendAllDetails.encode(
        message.data.cmdSyncSendAllDetails,
        writer.uint32(18).fork(),
      ).ldelim();
    }
    if (message.data?.$case === 'cmdPingToHost') {
      CmdGameRoomPingToHost.encode(
        message.data.cmdPingToHost,
        writer.uint32(26).fork(),
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RootCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRootCommand();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.data = {
            $case: 'cmdAskToParticipate',
            cmdAskToParticipate: CmdGameRoomAskToParticipate.decode(
              reader,
              reader.uint32(),
            ),
          };
          break;
        case 2:
          message.data = {
            $case: 'cmdSyncSendAllDetails',
            cmdSyncSendAllDetails: CmdGameRoomSyncSendAllDetails.decode(
              reader,
              reader.uint32(),
            ),
          };
          break;
        case 3:
          message.data = {
            $case: 'cmdPingToHost',
            cmdPingToHost: CmdGameRoomPingToHost.decode(
              reader,
              reader.uint32(),
            ),
          };
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): RootCommand {
    return {
      data: isSet(object.cmdAskToParticipate)
        ? {
            $case: 'cmdAskToParticipate',
            cmdAskToParticipate: CmdGameRoomAskToParticipate.fromJSON(
              object.cmdAskToParticipate,
            ),
          }
        : isSet(object.cmdSyncSendAllDetails)
        ? {
            $case: 'cmdSyncSendAllDetails',
            cmdSyncSendAllDetails: CmdGameRoomSyncSendAllDetails.fromJSON(
              object.cmdSyncSendAllDetails,
            ),
          }
        : isSet(object.cmdPingToHost)
        ? {
            $case: 'cmdPingToHost',
            cmdPingToHost: CmdGameRoomPingToHost.fromJSON(object.cmdPingToHost),
          }
        : undefined,
    };
  },

  toJSON(message: RootCommand): unknown {
    const obj: any = {};
    message.data?.$case === 'cmdAskToParticipate' &&
      (obj.cmdAskToParticipate = message.data?.cmdAskToParticipate
        ? CmdGameRoomAskToParticipate.toJSON(message.data?.cmdAskToParticipate)
        : undefined);
    message.data?.$case === 'cmdSyncSendAllDetails' &&
      (obj.cmdSyncSendAllDetails = message.data?.cmdSyncSendAllDetails
        ? CmdGameRoomSyncSendAllDetails.toJSON(
            message.data?.cmdSyncSendAllDetails,
          )
        : undefined);
    message.data?.$case === 'cmdPingToHost' &&
      (obj.cmdPingToHost = message.data?.cmdPingToHost
        ? CmdGameRoomPingToHost.toJSON(message.data?.cmdPingToHost)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<RootCommand>, I>>(
    object: I,
  ): RootCommand {
    const message = createBaseRootCommand();
    if (
      object.data?.$case === 'cmdAskToParticipate' &&
      object.data?.cmdAskToParticipate !== undefined &&
      object.data?.cmdAskToParticipate !== null
    ) {
      message.data = {
        $case: 'cmdAskToParticipate',
        cmdAskToParticipate: CmdGameRoomAskToParticipate.fromPartial(
          object.data.cmdAskToParticipate,
        ),
      };
    }
    if (
      object.data?.$case === 'cmdSyncSendAllDetails' &&
      object.data?.cmdSyncSendAllDetails !== undefined &&
      object.data?.cmdSyncSendAllDetails !== null
    ) {
      message.data = {
        $case: 'cmdSyncSendAllDetails',
        cmdSyncSendAllDetails: CmdGameRoomSyncSendAllDetails.fromPartial(
          object.data.cmdSyncSendAllDetails,
        ),
      };
    }
    if (
      object.data?.$case === 'cmdPingToHost' &&
      object.data?.cmdPingToHost !== undefined &&
      object.data?.cmdPingToHost !== null
    ) {
      message.data = {
        $case: 'cmdPingToHost',
        cmdPingToHost: CmdGameRoomPingToHost.fromPartial(
          object.data.cmdPingToHost,
        ),
      };
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, '$case'>]?: DeepPartial<T[K]> } & {
      $case: T['$case'];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isObject(value: any): boolean {
  return typeof value === 'object' && value !== null;
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
