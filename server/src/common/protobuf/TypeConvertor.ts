import { NNPB } from "./export";

// Overcoming missing "extend" (inheritance) in ProtocolBuffers3
// export function ProtoBuf_ExtendResolve<T>(obj:T) {
//     let o = obj as any;
//     if(o.nnextend!=null) {
//         const vals = ProtoBuf_ExtendResolve(o.nnextend);
//         o = {...o, ...vals};
//         delete o.nnextend;
//         return o;
//     } else {
//         for(const prop in o) {
//             const val = (o as any)[prop];
//             if(Array.isArray(val)) {
//                 for(let i=0;i<val.length;i++) {
//                     val[i] = ProtoBuf_ExtendResolve(val[i]);
//                 }
//             } 
//             if(typeof val === 'object') {
//                 o[prop] = ProtoBuf_ExtendResolve(o[prop]);
//             }
//         }
//     }
//     return o;
// }


// export namespace NNPBConv {
//     export namespace toPB {
//     }
//     export namespace fromPB {
//         ipDetails: (obj:NNPB.IpDetailsAndPorts)=>{}
//     }
// }
