


import { Component } from 'solid-js';
const About: Component = () => {
  return (
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        
          <div>
            NukemNet is an open source multiplayer game launcher, aimed for retro games, based on IRC<br />
          </div>

          <div>You may join our IRC server either by simply using NukemNet, or connect to our irc server (irc.nukemnet.com) with any compatible IRC client</div><br/>
          
          <div>Join our <a target="_blank" href="https://discord.gg/AmJXc8PuNS">Discord</a></div><br/>

          <div>
            NukemNet is brought to you by aaBlueDragon ( Alon Amir - אלון אמיר ), author of a previous multiplayer launcher (Dukonnector).<br/>
          </div>
          <br/>
          <h5>Credits go to</h5>
          <div>
            <span style="font-weight:bold">3DRealms Apogee IDSoftware</span> and others, for making such revolutionary iconic games and engines.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">ZNukem</span> for helping me test NukemNet a whole lot, finding bugs, and gave me motivation to come up with a new launcher, during our first dukematch on (7/May/2022), and for the cool welcome sound.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">NY00123</span> For all the years of YANG maintainance (along with Turrican and Striker), another cross-platform multiplayer launcher. also technical consultation about linux support, irc discord bridge.<br/>
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">Snake Plissken</span> For testing with me and ZNukem, having fun matches.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">Ergo IRC Team</span> for developing an awesome IRC daemon.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">AlinaFK ❤️</span> for not kicking me in the face for being way too much on my laptop all these months, working on NukemNet.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">sudolinux</span> for the logo, and helping me test NukemNet.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">THEBaratusII</span> for the top-left logo, and helping me test NukemNet.
          </div>
          <br/>
          <div>
              <span style="font-weight:bold">IP2Location</span>: This site or product includes IP2Location LITE data available from http://www.ip2location.com.<br/>
              IP2Location is a registered trademark of Hexasoft Development Sdn Bhd. All other trademarks are the property of their respective owners.
          </div>
          <br/>
          <div>
              <span style="font-weight:bold">FreakFlags</span>: https://www.freakflagsprite.com Spritesheet containing all country flags.
          </div>
      </div>
  )
};

export default About;
