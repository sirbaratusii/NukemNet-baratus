
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css';

/* @refresh reload */
import { render } from 'solid-js/web';
import App from './App';
// import { hashIntegration, Router } from '@solidjs/router';


render(() => <App/>, document.getElementById('root') as HTMLElement);
