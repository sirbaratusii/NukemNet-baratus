

import { Component, For } from 'solid-js';

type PropType = {version:string,timestamp:number};
const DownloadLinks: Component<PropType> = (props:PropType) => {

  type ExecDetails = {os:string, arch:string, text:string}

  const supportedExecs: ExecDetails[] = [{
    os:'win32',
    arch:'x64',
    text: "Windows 64 Bit"
  },{
    os:'win32',
    arch:'ia32',
    text: "Windows 32 Bit"
  },{
    os:'linux',
    arch:'x64',
    text: "Linux 64 Bit"
  },{
    os:'linux',
    arch:'ia32',
    text: "Linux 32 Bit"
  },{
    os:'linux',
    arch:'arm64',
    text: "Linux ARM 64"
  },{
    os:'darwin',
    arch:'x64',
    text: "macOS 64 Bit"
  }]

  const fullFile = (execDetails:ExecDetails)=>{
    return `NukemNet-${execDetails.os}-${execDetails.arch}-${props.version}.zip`;
  }

  const v = props.version;
  return (<div style={{"text-align":"center"}}>
    <For each={supportedExecs}>
      {(data:ExecDetails)=>{
        return <div style={{"font-size":"20pt"}}><a href={'https://nukemnet.blob.core.windows.net/releases/'+fullFile(data)}>{fullFile(data)} (<span class="release-text">{data.text}</span>)</a></div>
      }}
    </For>
    If your platform is missing, please let me know.
  </div>)
};

export default DownloadLinks;
