
import { Component } from 'solid-js';

const Donations: Component = () => {
  return <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
  While I do have a lot of fun developing NukemNet, I got server bills to pay (Linux Servers, IRC Daemon, Database, Relays, MS Azure Storage for file upload, etc..)<br/>
  And an endless amount of coding, supporting new games, fixing bugs, adding features.<br/>
  NukemNet will always be free, but If you enjoy it, and like to see it improve, then:<br/><br/>

  Consider buying me a coffee on<br/><a target="_blank" href="https://www.patreon.com/alonamir">Patreon: https://www.patreon.com/alonamir</a> or <a target="_blank" href="https://www.paypal.com/donate/?business=6X6AADL3TVMZL&no_recurring=0&currency_code=ILS">PayPal: (aabluedragon@gmail.com)</a> or <a target="_blank" href="https://Ko-fi.com/alonamir">Ko-fi: (alonamir)</a>
  <br/><br/>

  <h5>Affiliate Links</h5>
  <p>
    Another form of donation is to use the GOG/Zoom-Platform affiliate links in the Home tab.
  </p>

  <h5>My crypto addresses:</h5>
  <p>
    Ethereum Address: 0x07491956AA0B7D512516CEd1c87c112fAADA7c10 <br/>
    Solana Address: JByvm31RnEaxMRoaUMvYqWy3SdPQw6yoBD3kfrejyk8Z
  </p>
  
</div>
};

export default Donations;
