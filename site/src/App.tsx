
import { Component} from 'solid-js';
import Home from './Home';
import About from './About';
import { Tab, Tabs } from 'solid-bootstrap';
import Downloads from './Downloads';
import ChangeLog from './ChangeLog';
import HowTo from './HowTo';
import Donations from './Donations';
import Extensions from './Extensions';
import OpenSource from './OpenSource';

const App: Component = () => {
  return (
    <>
    <div class="blink_me">If you find NukemNet useful, please help spread the word 🙌</div>
    <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" class="mb-3" >
      <Tab eventKey="home" title="Home">
        <Home />
      </Tab>
      <Tab eventKey="about" title="About">
        <About />
      </Tab>
      <Tab eventKey="downloads" title="Downloads">
        <Downloads />
      </Tab>
      <Tab eventKey="changelog" title="Change Log">
        <ChangeLog />
      </Tab>
      <Tab eventKey="howto" title="How To">
        <HowTo />
      </Tab>
      <Tab eventKey="extensions" title="Extensions">
        <Extensions />
      </Tab>
      <Tab eventKey="donations" title="Donations">
        <Donations />
      </Tab>
      <Tab eventKey="opensource" title="Open Source">
        <OpenSource />
      </Tab>
    </Tabs></>)
};

export default App;
