import gogImg from "../../app/src/assets/gog.png"
import zoomPlatformImg from "../../app/src/assets/zoomplatform.webp"
import {generateLinkForAffiliate} from "../../app/src/affiliate"
import {GameDefsType} from '../../app/src/GameDefsTypes'

import {Table} from 'solid-bootstrap'

import { Component, createSignal, For } from 'solid-js';

const SupportedGames: Component = () => {

  const [SupportedGamesJson, SET_SupportedGamesJson] = createSignal({} as {
    [gameName:string]: {
      get: (GameDefsType.GetGameAfilliate_GOG|GameDefsType.GetGameAfilliate_ZoomPlatform)[]
      e: string[]
    }
  });
  const [totalGames, SET_totalGames] = createSignal(0);
  const [totalExecutables, SET_totalExecutables] = createSignal(0);
  (async()=>{
    const res = await fetch(`./SupportedGames.json?uncache=${Date.now()}`);
    const json = await res.json();
    SET_SupportedGamesJson(json);
    SET_totalGames(Object.keys(SupportedGamesJson()).length);
    SET_totalExecutables(Object.values(SupportedGamesJson()).reduce((a,b)=>a+b.e.length,0));
  })();

  return (<>
  <Table striped bordered hover variant="dark" class="supported-games-table">
    
    <thead>
      <tr><td colSpan={2}>Supported Games</td></tr>
      <tr>
        <td>Games ({totalGames()})</td><td>Executables ({totalExecutables()})</td>
      </tr>
    </thead>

    <tbody>
    <For each={Object.keys(SupportedGamesJson())}>
        {(gameName:string)=>{
          if(gameName==='default') return null;
          return (<For each={SupportedGamesJson()[gameName].e}>
              {(execName:string)=>{
                const isFirst = SupportedGamesJson()[gameName].e.indexOf(execName)===0;
                return <tr><td class='game-name'>{
                  isFirst?<>
                  {gameName}&nbsp;
                  <For each={(SupportedGamesJson()[gameName]?.get || [])}>
                    {(item)=><>
                      <a target="_blank" href={generateLinkForAffiliate(item.type, item.nameId)}><img style={{border: "3px solid #2287c9", height: "28px"}} src={item.type==='gog'?gogImg:zoomPlatformImg} /></a>&nbsp;
                    </>}
                  </For>
                  </>:''
                  }</td><td class="exec-name">{execName}</td></tr>
              }}
          </For>)
        }}
    </For>
    </tbody>
  </Table>

  <div style={{"text-align":"center"}}>
    <h4>Generic affiliate links (for unlisted games):</h4>
    <p>
      GOG: <a href="https://track.adtraction.com/t/t?a=1578845458&as=1827400461&t=2&tk=1" target="_blank">https://track.adtraction.com/t/t?a=1578845458&as=1827400461&t=2&tk=1</a><br/>
      Zoom-Platform: <a href="https://www.zoom-platform.com?affiliate=ec7ead78-709f-4eb9-b5fd-7c69815d5e09" target="_blank">https://www.zoom-platform.com?affiliate=ec7ead78-709f-4eb9-b5fd-7c69815d5e09</a>
    </p>
  </div>
  
  
  </>
  )
};

export default SupportedGames;
