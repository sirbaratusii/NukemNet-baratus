import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { SecretKeyGuard } from '../secret-key.guard';
import { UdpRelayService, CreateUdpRelayDto } from './udp-relay.service';

@Controller('udp')
export class UdpRelayController {

constructor(private udpRelayService: UdpRelayService) {}

  @Get()
  @UseGuards(SecretKeyGuard)
  async getAll() {
    return this.udpRelayService.getAll();
  }

  @Post()
  @UseGuards(SecretKeyGuard)
  async createUdpRelay(@Body() createUdpRelayDto: CreateUdpRelayDto) {
    return await this.udpRelayService.createUdpAndBind();
  }

  @Delete(":id")
  @UseGuards(SecretKeyGuard)
  async deleteUdpRelay(@Param('id') id: string, @Res() res: Response) {
    const existed = this.udpRelayService.removeById(id);
    return res.status(existed?HttpStatus.OK:HttpStatus.NOT_FOUND).json();
  }

  @Delete()
  @UseGuards(SecretKeyGuard)
  async deleteAllUdpRelays() {
    this.udpRelayService.delAll();
  }

  @Get(":id")
  @UseGuards(SecretKeyGuard)
  async getUdpRelay(@Param('id') id: string, @Res() res: Response) {
    const obj = this.udpRelayService.get(id);
    if(!obj) return res.status(HttpStatus.NOT_FOUND).json();
    return res.status(HttpStatus.OK).json(obj);
  }
}
