import { Test, TestingModule } from '@nestjs/testing';
import { UdpRelayController } from './udp-relay.controller';

describe('UdpRelayController', () => {
  let controller: UdpRelayController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UdpRelayController],
    }).compile();

    controller = module.get<UdpRelayController>(UdpRelayController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
