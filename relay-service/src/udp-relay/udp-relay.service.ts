import { Injectable } from '@nestjs/common';
import { AppService } from '../app.service';
import { Relay } from './relay';
import * as uuid from 'uuid';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateUdpRelayDto {}

export class UdpRelayDtoResponse extends CreateUdpRelayDto {
    @IsNotEmpty()
    id:string

    @IsNotEmpty()
    @IsNumber()
    port:number

    endOfLifeTS:number
}

@Injectable()
export class UdpRelayService {

    constructor(private appService:AppService) {}
    
    relays: {[id:string]: {relay:Relay, timer:NodeJS.Timeout, endOfLifeTS:number, port:number}} = {}

    getAll() {
        const map = {};
        for(const id in this.relays) {
            const data = this.relays[id];
            map[id] = {endOfLifeTS: data.endOfLifeTS, port:data.port};
        }
        return map;
    }

    delAll() {
        const allRelays = this.getAll();
        for(const id in allRelays) {
            this.removeById(id);
        }
    }

    async createUdpAndBind(): Promise<UdpRelayDtoResponse> {
        const socket = await this.appService.getUdpRelayAndBind();
        const id = uuid.v4();
        const relay = new Relay(socket);

        const timer = setTimeout(()=>{
            relay.destroy();
            delete this.relays[id];
        }, this.appService.maxRelayLife);

        const endOfLifeTS = Date.now()+this.appService.maxRelayLife
        this.relays[id] = {relay, timer, endOfLifeTS, port:socket.address().port};

        return {id, port:socket.address().port, endOfLifeTS};
    }

    get(id:string):false|{endOfLifeTS:number, port:number} {
        const relayObj = this.relays[id];
        if(relayObj == null) return false;

        return {endOfLifeTS: relayObj.endOfLifeTS, port:relayObj.port};
    }

    removeById(id:string) {
        const obj = this.relays[id];
        if(!obj) return false;
        obj.relay.destroy();
        clearTimeout(this.relays[id].timer);
        delete this.relays[id];
        return true;
    }

}
