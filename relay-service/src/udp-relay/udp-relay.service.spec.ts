import { Test, TestingModule } from '@nestjs/testing';
import { UdpRelayService } from './udp-relay.service';

describe('UdpRelayService', () => {
  let service: UdpRelayService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UdpRelayService],
    }).compile();

    service = module.get<UdpRelayService>(UdpRelayService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
