import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { IncomingMessage } from 'http';
import { Observable } from 'rxjs';

@Injectable()
export class SecretKeyGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    try {
      const req = context.switchToHttp().getRequest() as IncomingMessage;
      const authHeader = req.headers['authorization'] as string;
      if(!authHeader) return false;
      const Bearer = 'Bearer ';
      const token = authHeader.substring(authHeader.indexOf(Bearer)+Bearer.length);
      return token === process.env.SECRET_TOKEN;
    } catch(e) {}
    return false;
  }
}
