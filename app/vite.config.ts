import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
import inject from '@rollup/plugin-inject'

export default defineConfig({
  base: '',
  server: {
    port: 3001,
    hmr: false
  },
  plugins: [
    inject({
      $: 'jquery',
      jQuery: 'jquery',
      include: '**/*.js'
    }),
    solidPlugin()
  ],
  optimizeDeps: {
    esbuildOptions: {
      target: 'es2020'
    }
  }
});
