import { RemoteInfo } from "dgram";

const udpI = '.nukem.net.pack.'

export const KcpCommon = {
    rinfoToKey(rinfo:RemoteInfo) {
        return `${rinfo.address}_${rinfo.port}`;
    },
    SyncInterval: 1000,
    udpIndicator: udpI,
    udpIndicatorBuffer: Buffer.from(udpI),
    keepAliveCounts:5,
}
