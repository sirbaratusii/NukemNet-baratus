import { RemoteInfo, Socket as UDPSocket } from "dgram";
import { Socket as TCPSocket } from "net";
import { KCP } from "node-kcp-x";
import { dgram, kcp, net } from "../../NodeLibs";
import { udp6a } from "../utils";
import { KcpCommon } from "./common";

const udpIndicator = KcpCommon.udpIndicator
const udpIndicatorBuffer = KcpCommon.udpIndicatorBuffer


async function createUdpAndBind(from:number, to:number, exceptPortsSet:Set<number> = new Set()) {
    const allowedPorts = [];
    for(let i=from;i<=to;i++) {
        if(!exceptPortsSet.has(i)) {
            allowedPorts.push(i);
        }
    }

    const newClient = dgram.createSocket('udp6');

    let boundPort = null;
    while(allowedPorts.length) {
        const randIndex = Math.floor(Math.random()*allowedPorts.length);
        const port = allowedPorts[randIndex];
        allowedPorts.splice(randIndex,1);

        try {
            newClient.bind(port,'::');
            await new Promise((res,rej)=>{
                newClient.once('listening', ()=>{
                    newClient.removeAllListeners();
                    res(null);
                })
                newClient.once('error', (err)=>{
                    newClient.removeAllListeners();
                    rej(err)
                })
            });
            boundPort = port;
            break
        } catch(e) {}
    }
    if(!boundPort) {
        try {newClient.close()} catch(e) {}
        throw new Error('Could not bind to any port');
    }

    return newClient;
}

export function createUDPTunnel_server(tunnelListenPort:number, destTcpIp?:string, destTcpPort?:number, destUdpIp?:string, destUdpPort?:number, _forbiddenBindPorts:number[]=[]) {

    if(destTcpIp == null || destTcpPort == null) {
        console.log('Error: Missing TCP ip/port');
        return;
    }

    const forbiddenBindPorts = new Set([..._forbiddenBindPorts, tunnelListenPort, destTcpPort].concat(destUdpPort?[destUdpPort]:[]));

    var server = dgram.createSocket('udp6');

    type ClientInfo = {rinfo:RemoteInfo, udp?:UDPSocket|null, tcp?:TCPSocket|null, kcpobj?:KCP, kaInterval?:any, kaCountLeft?:number}
    var clients = {} as {[key:string]:ClientInfo};
    var interval = 20;

    var output = function(data:Buffer, size:number, context:any) {
        server.send(data, 0, size, context.port, udp6a(context.address));
    };

    server.on('error', (err) => {
        console.log(`server error:\n${err.stack}`);
        server.close();
    });


    function rinfoToKey(rinfo:RemoteInfo) {
        return `${rinfo.address}_${rinfo.port}`;
    }

    function initIntervalKeepAliveChecks(client:ClientInfo) {
        if(client.kaCountLeft == null) {
            client.kaCountLeft = KcpCommon.keepAliveCounts;
        }
        client.kaInterval = setInterval(()=>{
            if(destroyed) {clearInterval(client.kaInterval); return;}

            if(client.kcpobj) {
                client.kcpobj.send(Buffer.from(JSON.stringify({i:'nnpacket', type:'keepalive'})));
            }

            client.kaCountLeft!--;
            if(client.kaCountLeft!<=0) {
                
                clearInterval(client.kaInterval);

                try {client.tcp?.destroy()} catch(e) {}
                try {client.tcp?.removeAllListeners()} catch(e) {}
                client.tcp = null;
                try {client.udp?.close()} catch(e) {}
                try {client.udp?.removeAllListeners()} catch(e) {}
                client.udp = null;

                const k = rinfoToKey(client.rinfo);
                if(k) delete clients[k];
            }
        },1000);
    }

    server.on('message', async (msg, rinfo) => {
        
        try {
            // packet starts with nukemnetpack, which means it's UDP, not TCP, so forward this packet to the UDP game port.
            const starter = msg.subarray(0,udpIndicatorBuffer.length).toString();
            if(starter===udpIndicator) {
                const k = rinfoToKey(rinfo);
                let clientObj = clients[k];
                if(!clientObj) {
                    clientObj = clients[k] = {rinfo};
                    initIntervalKeepAliveChecks(clientObj);
                }

                // if udp tunnelling enabled
                if(destUdpIp && destUdpPort) {
                    if(!clientObj.udp) {
                        clientObj.udp = await createUdpAndBind(10000, 60000, forbiddenBindPorts);
                        clientObj.udp.on('message', (cmsg)=>{
                            const wrappedPacket = Buffer.from([...udpIndicatorBuffer, ...cmsg]);
                            server.send(wrappedPacket, rinfo.port, udp6a(rinfo.address));
                        });
                    }
                    // TODO when do I remove a client udp obj, or object at all? maybe when player leaves game?
                    const actualPacket = msg.subarray(udpIndicatorBuffer.length);
                    clientObj.udp.send(actualPacket, destUdpPort, udp6a(destUdpIp));
                }
                return;
            }
        } catch(e) {}

        // If we got here, it's a TCP over KCP packet or nnpacket command, process it using KCP.
        const k = rinfoToKey(rinfo);
        if(!clients[k]) {
            clients[k] = {rinfo}
            initIntervalKeepAliveChecks(clients[k]);
        };
        if(!clients[k].kcpobj) {
            var context = {
                address : rinfo.address,
                port : rinfo.port
            };
            const kcpobj = new kcp.KCP(123, context);
            kcpobj.nodelay(0, interval, 0, 0);
            kcpobj.output(output);

            clients[k].kcpobj = kcpobj;
        }

        const {kcpobj} = clients[k];
        kcpobj?.input(msg);
    });

    function onServerReceivedKcpPacket(data:Buffer, client:ClientInfo) {
        try {
            const metaPacket = data.toString();
            const metaObj = JSON.parse(metaPacket);
            if(!metaObj || !metaObj.type || metaObj.i!=='nnpacket') throw ''; // not a meta packet
            if(metaObj.type==='connect') {
                if(client.tcp) {
                    try {client.tcp.destroy()} catch(e) {}
                    client.tcp = null;
                }

                // if tcp tunneling enabled
                if(destTcpIp && destTcpPort) {
                    client.tcp = new net.Socket();
                    client.tcp.connect(destTcpPort, destTcpIp, ()=>{
                        client.kcpobj?.send(Buffer.from(JSON.stringify({i:'nnpacket', type:'connected'})));
                    });


                    function destroyTcpSocket() {
                        try {client.tcp?.destroy()} catch(e) {}
                        client.tcp = null;
                        client.kcpobj?.send(Buffer.from(JSON.stringify({i:'nnpacket', type:'closed'})));
                    }

                    client.tcp.on('error', ()=>{
                        destroyTcpSocket()
                    });
                    client.tcp.on('close', function() {
                        destroyTcpSocket()
                    });

                    client.tcp.on('data', function(data) {
                        client.kcpobj?.send(data);
                    });
                }
            } else if(metaObj.type==='close') {
                try {client.tcp?.destroy()} catch(e) {}
                client.tcp = null;
            } else if(metaObj.type==='keepalive') {
                client.kaCountLeft=KcpCommon.keepAliveCounts;
            }
            return;
        } catch(e) {}

        if(client.tcp) {
            client.tcp.write(data);
        }
    }

    let intervalId:any;
    server.on('listening', () => {
        var address = server.address();
        intervalId = setInterval(() => {
            if(destroyed) {clearInterval(intervalId); return;}
            for (var k in clients) {
                const client = clients[k];
                const kcpobj = client.kcpobj;
                if(!kcpobj) continue;
                
                kcpobj.update(Date.now());
                do {
                    var recv = kcpobj.recv();
                    if (recv) {onServerReceivedKcpPacket(recv, client)}
                } while (recv);
            }
        }, interval);
    });

    server.bind(tunnelListenPort);

    const udpPart = (destUdpIp!=null&&destUdpPort!=null)?` and UDP:${destUdpIp}:${destUdpPort}`:'';
    console.log(`Listening on UDP:${tunnelListenPort}, forwarding to TCP:${destTcpIp}:${destTcpPort}${udpPart}`)

    let destroyed = false;
    return {
        gamePort: destTcpPort,
        close: function() {
            clearInterval(intervalId);
            destroyed = true;
            try {server.close()} catch(e) {}
            for (var k in clients) {
                const client = clients[k];
                try {client.tcp?.destroy()} catch(e) {}
                try {client.tcp?.removeAllListeners()} catch(e) {}
                try {client.udp?.close()} catch(e) {}
                try {client.udp?.removeAllListeners()} catch(e) {}
                clearInterval(client.kaInterval)
            }
            server.removeAllListeners();
        }
    }
}

