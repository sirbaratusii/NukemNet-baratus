import { Socket as UDPSocket } from "dgram";
import { Socket as TCPSocket, Server as TCPServer } from "net";
import { axios, dgram, net } from '../NodeLibs';
import { serverHost, serverHttpPort } from "../store/store";
import { resolveHostIpv4 } from "./utils";


export async function checkPortConnectivityTcpUdp_Throw(port:number, ip?:string, timeoutMS?:number) {
    const res = await checkPortConnectivityTcpUdp(port, ip, timeoutMS);
    if(!res.tcp) throw res.tcpErr;
    if(!res.udp) throw res.udpErr;
    return res;
}

export async function checkPortConnectivityTcpUdp(port:number, ip?:string, timeoutMS?:number) {

    let requestSentAlready = false;

    // This function is here to avoid sending two requests to NN Server (one for TCP and one for UDP).
    async function sendRequestToServer() {
        if(requestSentAlready) return;
        requestSentAlready = true;
        try {
            const serverIpv4 = await resolveHostIpv4(serverHost); // we want to test IPv4 connectivity
            await axios.put(`http://${serverIpv4}:${serverHttpPort}/port-checker/tcpudp/port/${port}`);
        } catch(e) {}
    }

    const promises = [checkPortConnectivityTcp(port, ip, timeoutMS, sendRequestToServer), checkPortConnectivityUdp(port, ip, timeoutMS, sendRequestToServer)]
        .map(p => p.catch(e => e instanceof Error ? e : new Error(e)));

    const [tcp,udp] = await Promise.all(promises);
    return {
        tcp: tcp===true,
        tcpErr: tcp instanceof Error ? tcp : undefined,
        udp: udp===true,
        udpErr: udp instanceof Error ? udp : undefined,
    }
}

export async function checkPortConnectivityTcp(port:number, ip?:string, timeoutMS:number=4000, sendRequestToServer?:()=>any) {
    return new Promise(async (res,rej)=>{

        const checkBindOnly = !ip?.length;
        if(checkBindOnly) {
            let conn:TCPServer|null = null;
            function cleanup(err?:any) {
                try {conn?.close()} catch(e) {}
                if(err) rej(err);
                else res(true);
            }
            try {
                conn = net.createServer();
                conn.once('error',cleanup);
                conn.once('listening',()=>cleanup());
                conn.listen(port, '0.0.0.0');
            } catch(e) {
                cleanup(e);
            }
            return;
        }

        let tcpServer:TCPServer;
        let s:TCPSocket;
        let askRemoteCheckerTimer:any;
        let done = false;

        function finish(err?:any) {
            if(done) return
            done = true;
            clearTimeout(askRemoteCheckerTimer);
            clearTimeout(timer);
            try {s.destroy()} catch(e) {}
            try {tcpServer.close()} catch(e) {}
            if(err) rej(err);
            else res(true);
        }

        const timer = setTimeout(()=>{
            finish(new Error('timeout'));
        },timeoutMS)

        try {
            tcpServer = net.createServer();
            tcpServer.listen(port, '0.0.0.0');
            tcpServer.once('connection',()=>{finish();});

            askRemoteCheckerTimer = setTimeout(async ()=>{
                if(done) return;
                if(sendRequestToServer) sendRequestToServer();
                else {
                    try {
                        const serverIpv4 = await resolveHostIpv4(serverHost); // we want to test IPv4 connectivity
                        await axios.put(`http://${serverIpv4}:${serverHttpPort}/port-checker/tcp/port/${port}`);
                    } catch(e) {}
                }
            },1); // was 700 when below code was not commented out

            // Disabled to below code, might cause false positives (saying port is ready, even if it's not)
            // // Try to connect to ip once server is listening
            // s = net.createConnection({host:ip, port:port, family:4});
            // s.once('error', (e)=>finish(e));
            // s.once('close', ()=>finish('close'))
            // s.once('timeout', ()=>finish(new Error('timeout')));
            // s.once('connect', ()=>{
            //     finish();
            // });

        } catch(e) {}
    })
    
}

export async function checkPortConnectivityUdp(port:number, ip?:string, timeoutMS:number=4000, sendRequestToServer?:()=>any) {
    return new Promise(async (res,rej)=>{
        let ss:UDPSocket;

        const checkBindOnly = !ip?.length;
        if(checkBindOnly) {
            const tmr = setTimeout(()=>{
                rej(new Error('timeout'));
                ss.removeAllListeners();
                ss.close();
            },2000);
            ss = dgram.createSocket('udp4');
            ss.once('error',err=>{
                clearTimeout(tmr);
                ss.removeAllListeners();
                ss.close();
                rej(err);
            });
            ss.once('message',()=>{
                clearTimeout(tmr);
                ss.removeAllListeners();
                ss.close();
                res(true);
            });
            ss.bind(port, '0.0.0.0');
            setTimeout(()=>{
                const client = dgram.createSocket('udp4');
                try {client.send(Buffer.from('stub'), port, ip, function(error){});} catch(e) {}
                setTimeout(()=>{
                    try {client.close();} catch(e) {}
                },1000);
            },100);
            return;
        }

        let done = false;

        function closeServerSocket() {
            try {
                ss?.removeAllListeners();
                ss!.close();
            } catch(e) {}
        }

        const askRemoteCheckerTimer = setTimeout(async ()=>{
            if(done) return;
            if(sendRequestToServer) sendRequestToServer();
            else {
                try {
                    const serverIpv4 = await resolveHostIpv4(serverHost); // we want to test IPv4 connectivity
                    await axios.put(`http://${serverIpv4}:${serverHttpPort}/port-checker/udp/port/${port}`);
                } catch(e) {}
            }
        },1); // was 700 when below code was not commented out

        const timer = setTimeout(()=>{
            clearTimeout(askRemoteCheckerTimer);
            if(done) return
            done = true;
            closeServerSocket();
            rej(new Error('timeout'))
        },timeoutMS)

        try {
            ss = dgram.createSocket('udp4');

            function incomeToServer() {
                clearTimeout(timer);
                clearTimeout(askRemoteCheckerTimer);
                if(done) return
                done = true;
                closeServerSocket();
                res(true)
            }

            ss.on('message', incomeToServer)

            ss.bind(port, '0.0.0.0')
            ss.on('error', (err)=>{
                clearTimeout(timer);
                clearTimeout(askRemoteCheckerTimer);
                if(done) return
                done = true;
                closeServerSocket();
                rej(err);
            });

            // Disabled to below code, might cause false positives (saying port is ready, even if it's not)
            // ss.once('listening',()=>{
            //     const client = dgram.createSocket('udp4');
            //     try {client.send(Buffer.from('stub'), port, ip, function(error){});} catch(e) {}
            //     setTimeout(()=>{
            //         try {client.close();} catch(e) {}
            //     },1000);
            // });

        } catch(e) {}
    })
}