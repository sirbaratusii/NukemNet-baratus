import { render } from "solid-js/web";
import { BotMetaChannel_lowercase, isNukemNetBot } from "../../../server/src/common/constants";
import { IrcEventCmp_Base } from "../irc-events/components/base";
import { eventTypeToComponentMapping } from "../irc-events/eventToComponent";
import { IRCEvent_BaseEvent } from "../irc-events/ircEvents";
import { fs, path } from "../NodeLibs";
import { pathOfIrcLogs, pathOfIrcLogs_backlogFile, pathOfIrcLogs_channels, pathOfIrcLogs_private, Settings } from "../Settings";
import { EventBaseType } from "../store/store";

export async function IrcEventTextLogger(origin:EventBaseType, data:{event:IRCEvent_BaseEvent,dest:string}) {
    if(Settings.read().disableLogging) return;

    const ldest = data.dest.toLowerCase();
    if(ldest == BotMetaChannel_lowercase || isNukemNetBot(ldest)) return;

    // Create a fake element to take it's innerText content
    const stub = document.createElement('span') as HTMLSpanElement;
    const ComponentTypeOfEvent = eventTypeToComponentMapping[data.event.type] as typeof IrcEventCmp_Base;
    if(!ComponentTypeOfEvent) return '';
    render(() => <div><ComponentTypeOfEvent {...data.event}/></div>, stub)

    const el = stub.firstElementChild! as HTMLDivElement;
    if(el == null) return;

    const logText = el.innerText+'\n';
    if(!logText.length) return;
    
    try {
        try {
            await fs.access(pathOfIrcLogs)
        } catch(e) {
            await fs.mkdir(pathOfIrcLogs)
        }
        
        if(origin === 'backlog') {
            await fs.appendFile(pathOfIrcLogs_backlogFile, logText);
        } else if(origin === 'private') {
            try {
                await fs.access(pathOfIrcLogs_private)
            } catch(e) {
                await fs.mkdir(pathOfIrcLogs_private)
            }
            const fullPath = path.resolve(pathOfIrcLogs_private, data.dest) + '.log'
            await fs.appendFile(fullPath, logText);
        } else if(origin === 'channel') {
            try {
                await fs.access(pathOfIrcLogs_channels)
            } catch(e) {
                await fs.mkdir(pathOfIrcLogs_channels)
            }
            const fullPath = path.resolve(pathOfIrcLogs_channels, data.dest) + '.log'
            await fs.appendFile(fullPath, logText);
        }
    } catch(e) {}
}