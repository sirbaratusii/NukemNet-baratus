
import { Client } from 'matrix-org-irc';
import { fs, path } from '../NodeLibs'
import { DialogMgr } from '../Popups';
import { backlogTabL } from '../store/store';
import { AzureUpload } from './AzureBlobUpload';

// https://stackoverflow.com/a/19696443/230637
export const REGEX_WEB_URL_PATTERN_RFC1738 = /((?:(http|https|Http|Https|rtsp|Rtsp):\/\/(?:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,64}(?:\:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,25})?\@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\:\d{1,5})?)(\/(?:(?:[a-zA-Z0-9\;\/\?\:\@\&\=\#\~\-\.\+\!\*\'\(\)\,\_])|(?:\%[a-fA-F0-9]{2}))*)?(?:\b|$)/gi;

export function isImgExtension(url:string) {
    const ext = path.extname(url).toLowerCase();
    return ['.jpg','.jpeg','.png','.gif','.tiff','.webp','.heic'].includes(ext);
}

export async function uploadImage(fpathOrBUf:string|Buffer, filename:string, target:string, ircClient:Client) {
  if(target.toLowerCase() === backlogTabL) return

  const buffer = typeof fpathOrBUf==='string' ? await fs.readFile(fpathOrBUf):fpathOrBUf as Buffer;
  const ext = path.extname(filename).substring(1);
  const objUrl = URL.createObjectURL(new Blob([buffer], { type: 'image/'+ext }));

  setTimeout(()=>{
    // Release
    try {URL.revokeObjectURL(objUrl)} catch {};
  },120*1000)

  const res = await DialogMgr.addPopup({
    size:'xl',
    title: "Upload Image?",
    body:()=><img src={objUrl} style={{"max-width":"100%"}} />,
    buttons:[{body:"Upload", variant:"primary"},{body:"Cancel", variant:"secondary"}]
  })

  if(res.buttonIndex !== 0) return;

  await AzureUpload.fullUpload(fpathOrBUf, filename, target, ircClient, true);
}