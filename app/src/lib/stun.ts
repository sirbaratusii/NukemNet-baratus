import {stun, dgram as udp, dgram} from '../NodeLibs'
import * as UDP_Type from 'dgram' // just for types
import { resolveHostIpv4, resolveHostIpv6, udp6a } from './utils';
import { is_ipv4, is_ipv6 } from './GetIp';
import { deepClone } from './deepClone';

let nnglobal:any;
try {
  nnglobal = (window as any).nnglobal || {};
} catch(e) {
  nnglobal = {};
}

interface StunData {
  address: string // '193.43.148.37'
  family: string // 'IPv4'
  port: number // 3478
}
interface HandShakeCompressed {
  a: string // '193.43.148.37' // IP Detected by STUN server
  // no need for family, can get it by address format
  p: number // 3478, // Port Detected by STUN server
  r?:1 // instead of boolean, to save bytes
}

export interface StunDetails {
  socket: UDP_Type.Socket,
  stunAddr: StunData & { localPort: number },
  stunSrv: StunData
}

// echo hello | nc -u -p 7777 84.109.51.30 5556


export interface StunServer {
  address: string,
  port: number
}

export interface StunConnEndpoint {
  remoteAddr:string
  remotePort:number
  forwardTo?:{addr:string, port:number},
  receivedEndpoint?:{remoteAddr:string, remotePort:number},
  tunnel:UDP_Type.Socket,
  connector?:UDP_Type.RemoteInfo,
  lastHandshakeReceived:number
  otherReceivedMyHandshake?:boolean,
  timeoutInterval?:any
}

const TunnelTimeoutSeconds = 6000;

export class StunController {

  public static async createUdpAndBind(from:number, to:number, except:number) {
    const newClient = udp.createSocket('udp6');

    let portsToTry = [] as number[];

    let boundPort:number|null = null;
    for(let port=from;port<to;port++) {
      if(port === except) continue;
      portsToTry.push(port);
    }

    portsToTry = portsToTry.sort(() => Math.random() - 0.5); // TODO once you fix STUN initiation logic, do random.
    while(portsToTry.length) {
      const port = portsToTry.shift() as number;
      try {
        newClient.bind(port, '::');
        await new Promise((res,rej)=>{
          newClient.once('listening', ()=>{
            newClient.removeAllListeners();
            res(null);
          })
          newClient.once('error', (err)=>{
            newClient.removeAllListeners();
            rej(err)
          })
        });
        boundPort = port;
        break
      } catch(e) {}
    }

    if(!boundPort) {
      try {newClient.close()} catch(e) {}
      throw new Error('Could not bind to any port');
    }

    return newClient;
  }

  // * important! before adding new entries to the list, make sure the new entry works with udp6 mode (ipv6) ping it and see if the resoled ip is IPv6, otherwise, don't add it.
  static stunServersIpv6:StunServer[]= [
    // { // This server returns localhost ip for some reason
    //   "address": "stun.stunprotocol.org",
    //   "port": 3478
    // },
    {
      "address": "stun.antisip.com",
      "port": 3478
    },
    {
      "address": "stun4.l.google.com",
      "port": 19302
    },
    {
      "address": "stun3.l.google.com",
      "port": 19302
    },
    {
      "address": "stun.l.google.com",
      "port": 19302
    },
    {
      "address": "stun1.l.google.com",
      "port": 19302
    },
    {
      "address": "stun.ipfire.org",
      "port": 3478
    },
    {
      "address": "stun2.l.google.com",
      "port": 19302
    }
  ]

  static stunServersIpv4:StunServer[]= [
    {
      "address": "stun.epygi.com",
      "port": 3478
    },
    {
      "address": "stun.easycall.pl",
      "port": 3478
    },
    {
      "address": "stun.it1.hr",
      "port": 3478
    },
    {
      "address": "stun.zadarma.com",
      "port": 3478
    },
    {
      "address": "stun.voipgate.com",
      "port": 3478
    },
    {
      "address": "stun.voipcheap.com",
      "port": 3478
    },
    {
      "address": "stun.voip.blackberry.com",
      "port": 3478
    },
    {
      "address": "stun.internetcalls.com",
      "port": 3478
    },
    {
      "address": "stun.gmx.net",
      "port": 3478
    },
    {
      "address": "stun.netgsm.com.tr",
      "port": 3478
    },
    {
      "address": "stun.intervoip.com",
      "port": 3478
    },
    {
      "address": "stun.gmx.de",
      "port": 3478
    },
    {
      "address": "stun.hosteurope.de",
      "port": 3478
    },
    {
      "address": "stun.freecall.com",
      "port": 3478
    },
    {
      "address": "stun.voipwise.com",
      "port": 3478
    },
    {
      "address": "stun.voipbusterpro.com",
      "port": 3478
    },
    {
      "address": "stun.voipbuster.com",
      "port": 3478
    },
    {
      "address": "stun.1und1.de",
      "port": 3478
    },
    {
      "address": "stun.tng.de",
      "port": 3478
    },
    {
      "address": "stun.netappel.com",
      "port": 3478
    },
    {
      "address": "stun.usfamily.net",
      "port": 3478
    },
    {
      "address": "stun.voipstunt.com",
      "port": 3478
    },
    {
      "address": "stun.rockenstein.de",
      "port": 3478
    },
    {
      "address": "stun.hoiio.com",
      "port": 3478
    },
    {
      "address": "stun.dcalling.de",
      "port": 3478
    },
    {
      "address": "stun.ippi.fr",
      "port": 3478
    },
    {
      "address": "stun.solcon.nl",
      "port": 3478
    },
    {
      "address": "stun.1und1.de",
      "port": 3478
    },
    {
      "address": "stun.sippeer.dk",
      "port": 3478
    },
    {
      "address": "stun.linphone.org",
      "port": 3478
    },
    {
      "address": "stun.ozekiphone.com",
      "port": 3478
    },
    {
      "address": "stun.cope.es",
      "port": 3478
    },
    {
      "address": "stun.freevoipdeal.com",
      "port": 3478
    },
    {
      "address": "stun.halonet.pl",
      "port": 3478
    },
    {
      "address": "stun.twt.it",
      "port": 3478
    },
    {
      "address": "stun.sipdiscount.com",
      "port": 3478
    },
    {
      "address": "stun.cheapvoip.com",
      "port": 3478
    },
    {
      "address": "stun.voipplanet.nl",
      "port": 3478
    },
    {
      "address": "stun.12voip.com",
      "port": 3478
    },
    {
      "address": "stun.solnet.ch",
      "port": 3478
    },
    {
      "address": "stun.sip.us",
      "port": 3478
    },
    {
      "address": "stun.myvoiptraffic.com",
      "port": 3478
    },
    {
      "address": "stun.avigora.fr",
      "port": 3478
    },
    {
      "address": "stun.miwifi.com",
      "port": 3478
    },
    {
      "address": "stun.cablenet-as.net",
      "port": 3478
    },
    {
      "address": "stun.voippro.com",
      "port": 3478
    },
    {
      "address": "stun.justvoip.com",
      "port": 3478
    },
    {
      "address": "stun.siptraffic.com",
      "port": 3478
    },
    {
      "address": "stun.mywatson.it",
      "port": 3478
    },
    {
      "address": "stun.mit.de",
      "port": 3478
    },
    {
      "address": "stun.voipblast.com",
      "port": 3478
    },
    {
      "address": "stun.easyvoip.com",
      "port": 3478
    },
    {
      "address": "stun.smsdiscount.com",
      "port": 3478
    },
    {
      "address": "iphone-stun.strato-iphone.de",
      "port": 3478
    },
    {
      "address": "stun.acrobits.cz",
      "port": 3478
    },
    {
      "address": "stun.dus.net",
      "port": 3478
    },
    {
      "address": "stun.qq.com",
      "port": 3478
    },
    {
      "address": "stun.voipcheap.co.uk",
      "port": 3478
    },
    {
      "address": "stun.pjsip.org",
      "port": 3478
    },
    {
      "address": "stun.sigmavoip.com",
      "port": 3478
    },
    {
      "address": "stun.schlund.de",
      "port": 3478
    },
    {
      "address": "stun.t-online.de",
      "port": 3478
    },
    {
      "address": "stun.powervoip.com",
      "port": 3478
    },
    {
      "address": "stun.counterpath.com",
      "port": 3478
    },
    {
      "address": "stun.uls.co.za",
      "port": 3478
    },
    {
      "address": "stun.voip.aebc.com",
      "port": 3478
    },
    {
      "address": "stun.gmx.net",
      "port": 3478
    },
    {
      "address": "stun.rynga.com",
      "port": 3478
    },
    {
      "address": "stun.lundimatin.fr",
      "port": 3478
    },
    {
      "address": "stun.srce.hr",
      "port": 3478
    },
    {
      "address": "stun.sonetel.net",
      "port": 3478
    },
    {
      "address": "stun.voztele.com",
      "port": 3478
    },
    {
      "address": "stun.12connect.com",
      "port": 3478
    },
    {
      "address": "stun.poivy.com",
      "port": 3478
    },
    {
      "address": "stun.actionvoip.com",
      "port": 3478
    },
    {
      "address": "stun.rolmail.net",
      "port": 3478
    },
    {
      "address": "stun.telbo.com",
      "port": 3478
    },
    {
      "address": "stun.aa.net.uk",
      "port": 3478
    },
    {
      "address": "stun.callromania.ro",
      "port": 3478
    },
    {
      "address": "stun.sonetel.com",
      "port": 3478
    },
    {
      "address": "stun.ekiga.net",
      "port": 3478
    },
    {
      "address": "stun.callwithus.com",
      "port": 3478
    },
    {
      "address": "stun.jumblo.com",
      "port": 3478
    },
    {
      "address": "stun.vo.lu",
      "port": 3478
    },
    {
      "address": "stun.smartvoip.com",
      "port": 3478
    },
    {
      "address": "stun.voip.eutelia.it",
      "port": 3478
    },
    {
      "address": "stun.counterpath.net",
      "port": 3478
    },
    {
      "address": "stun.sipgate.net",
      "port": 10000
    },
    {
      "address": "stun.commpeak.com",
      "port": 3478
    },
    {
      "address": "stun.ipshka.com",
      "port": 3478
    },
    {
      "address": "stun.nfon.net",
      "port": 3478
    },
    {
      "address": "stun.voys.nl",
      "port": 3478
    },
    {
      "address": "stun.wifirst.net",
      "port": 3478
    },
    {
      "address": "stun.ppdi.com",
      "port": 3478
    },
    {
      "address": "stun.vivox.com",
      "port": 3478
    },
    {
      "address": "stun.ooma.com",
      "port": 3478
    },
    {
      "address": "stun.voipgain.com",
      "port": 3478
    },
    {
      "address": "stun.annatel.net",
      "port": 3478
    },
    {
      "address": "stun.nonoh.net",
      "port": 3478
    },
    {
      "address": "stun.voipzoom.com",
      "port": 3478
    },
    {
      "address": "stun.lowratevoip.com",
      "port": 3478
    },
    {
      "address": "stun.webcalldirect.com",
      "port": 3478
    },
    {
      "address": "stun.infra.net",
      "port": 3478
    },
    {
      "address": "stun.tel.lu",
      "port": 3478
    },
    {
      "address": "stun.bluesip.net",
      "port": 3478
    },
    {
      "address": "stun.liveo.fr",
      "port": 3478
    },
    {
      "address": "stun.siplogin.de",
      "port": 3478
    },
    {
      "address": "stun.sipgate.net",
      "port": 3478
    },
    {
      "address": "stun.voicetrading.com",
      "port": 3478
    },
    {
      "address": "stun.voipinfocenter.com",
      "port": 3478
    },
    {
      "address": "stun.freeswitch.org",
      "port": 3478
    },
    {
      "address": "stun.voipraider.com",
      "port": 3478
    }
  ]

  static allStunServers = this.stunServersIpv4.concat(this.stunServersIpv6);
  static allStunUrls = this.allStunServers.map(srv=>`stun:${srv.address}:${srv.port}`);

  static async getIpv6Address(stunServer:StunServer, timeout?:number, abortSignal?:AbortSignal): Promise<string|null> {
    let udp6:UDP_Type.Socket;
    try {
      udp6 = dgram.createSocket({type: 'udp6', ipv6Only:true, signal:abortSignal});
      udp6.on('error', ()=>{}) // Ignore errors
      const srv = deepClone(StunController.stunServersIpv6[6]);
      srv.address = is_ipv6(stunServer.address)? stunServer.address : await resolveHostIpv6(srv.address)
      const res = await StunController.tryStunServer(udp6, srv, timeout);
      return res.stunAddr.address;
    } catch(e) {} finally {
      (abortSignal as any)['closed'] = true;
      try {udp6!.close();} catch(e) {}
    }
    return null;
  }

  static async tryStunServer(srvClient:UDP_Type.Socket, stunServer:StunServer, timeout=2500): Promise<StunDetails> {
    return await new Promise((res,rej)=>{

      function cleanup() {
        srvClient.removeListener('message', onStunResponse);
        clearTimeout(stunTimeoutId);
      }

      function onStunResponse(msg:any, srvInfo:any) {
        cleanup();
        try {
          const decodedStun = stun.decode(msg);
          const addr = decodedStun.getXorAddress() || decodedStun.getAddress();
          addr.localPort = srvClient.address().port;
          res({socket:srvClient, stunAddr: addr, stunSrv: srvInfo});
        } catch(e) {
          rej(e);
        }
      }
  
      srvClient.once('message', onStunResponse);
      const stunTimeoutId = setTimeout(()=>{
        cleanup();
        rej(new Error("STUN response timed out"));
      }, timeout);

      (srvClient.send as any)(stun.createMessage(stun.constants.STUN_BINDING_REQUEST).toBuffer(), stunServer.port, stunServer.address, function(error:any){
        if(error) {
          cleanup();
          rej(error);
        }
      });

    });
  }

  public static async create(from:number, to:number, except:number, onDestroyed?:()=>void): Promise<{symmetricNat:boolean, ctl:StunController}> {

    const srvClient = await StunController.createUdpAndBind(from,to,except);

    const shuffledAvailableServers = structuredClone(StunController.allStunServers).sort(() => Math.random() - 0.5);

    let symmetricNat = false;
    let firstPort:null|number = null;
    let stunDetails:null|StunDetails = null;
    for(let i=0;i<shuffledAvailableServers.length;i++) {
      const srv = shuffledAvailableServers[i];
      const addr = await resolveHostIpv4(srv.address);
      if(!is_ipv4(addr)) {
        continue;
      }
      srv.address = udp6a(addr);
      try {
        const stunResponse = await StunController.tryStunServer(srvClient, srv);
        if(firstPort==null) {
          firstPort = stunResponse.stunAddr.port;
        } else {
          symmetricNat = firstPort != stunResponse.stunAddr.port;
          stunDetails = stunResponse
          break;
        }
      } catch(e) {
        /*
        If it's this error, ipv6 is not supported
        code: "ENETUNREACH"
        errno: -4062
        port: 19302
        */
      }
    }

    if(!stunDetails) {
      try {srvClient.close()} catch(e) {}
      throw new Error('Failed to initiate STUN');
    }

    const newInstance = new StunController(from, to, except, stunDetails, onDestroyed);
    return {symmetricNat, ctl: newInstance}
  }
  
  keepAliveIntervalId:any
  constructor(
    private fromPort:number,
    private untilPort:number,
    private exceptPort:number,
    public conn:StunDetails,
    private onDestroyed?:()=>void) {
      
      this.keepAliveIntervalId = setInterval(()=>{
        this.signalKeepAlive();
      },1500);

      this.conn.socket.on('message', (buffer, remoteInfo)=>{this.onMessage(buffer, remoteInfo);})
    }

    get isDestroyed() {
      return this.#isDestroyed;
    }

    #isDestroyed = false;
    destroy() {
      if(this.#isDestroyed) return;
      this.#isDestroyed = true;
      try {this.onDestroyed?.();} catch(e) {}
      this.conn.socket.removeAllListeners();
      try {this.conn.socket.close()} catch(e) {};
      try {this.conn.socket.disconnect()} catch(e) {};
      for(const k in this.endPoints) {
        const ep = this.endPoints[k];
        this.removeEndpoint(ep.remoteAddr, ep.remotePort);
      }
      clearInterval(this.keepAliveIntervalId);
    }
    
    // endPoints and resolvedMapping are correlated, must remember manage both.
    endPoints: {
      [ip_port:string]: StunConnEndpoint
    } = {}
    // points to endPoints
    resolvedMapping: {[ip_port:string]:string} = {}

    getKey(ip:string, port:number) {return `${ip}_${port}`}

    getEndpoint(ip:string, port:number):StunConnEndpoint {
      const key = this.getKey(ip, port);
      const resolved = this.resolvedMapping[key];
      if(resolved) {
        return this.endPoints[resolved];
      }
      return this.endPoints[key];
    }

    async addEndpoint(remoteAddr:string, remotePort:number, forwardTo?:{addr:string, port:number}):Promise<StunConnEndpoint> {
      const existingEp = this.getEndpoint(remoteAddr, remotePort);
      if(existingEp) return existingEp; // Do nothing if already exists

      const tunnel = await StunController.createUdpAndBind(this.fromPort, this.untilPort, this.exceptPort);
      
      const key = this.getKey(remoteAddr, remotePort);
      const ep = this.endPoints[key] = {
        tunnel, remoteAddr, remotePort, forwardTo,
        lastHandshakeReceived: 0
      } as StunConnEndpoint;

      tunnel.on('message', (msg, rinfo)=>{
        const ep = this.getActualEndpoint(remoteAddr, remotePort);
        if(!ep) return;

        // if(!this.endPoints[key].connector) { this IF statement is commented out, becauase it causes a bug where the connector is not up to date
          this.endPoints[key].connector = rinfo;
        // }

        if(nnglobal?.stun?.log?.all || nnglobal?.stun?.log?.sent?.raw) {
          console.log(`STUN Sent Raw to (${ep.remoteAddr}:${ep.remotePort}) ${JSON.stringify(msg)}`);
        }
        this.conn.socket.send(msg, ep.remotePort, udp6a(ep.remoteAddr));
      });

      ep.timeoutInterval = setInterval(()=>{
        if(this.#isDestroyed) {
          clearInterval(ep.timeoutInterval);
          return;
        }
        if(ep.lastHandshakeReceived + TunnelTimeoutSeconds < Date.now()) {
          ep.otherReceivedMyHandshake = false;
        }
      },3000);

      // Send initial handshake immediately
      this.sendHandshakeData(remoteAddr, remotePort);

      return this.endPoints[key];
    }

    removeEndpointObj(ep:StunConnEndpoint) {
      this.removeEndpoint(ep.remoteAddr, ep.remotePort)
    }

    removeEndpoint(remoteAddr:string, remotePort:number) {
      const data = this.getEndpoint(remoteAddr, remotePort);
      if(data) {
        clearInterval(data.timeoutInterval);

        data.tunnel.removeAllListeners();
        try {data.tunnel.close();} catch(e) {}
        try {data.tunnel.disconnect();} catch(e) {}

        const key = this.getKey(remoteAddr, remotePort);
        if(this.endPoints[key]) delete this.endPoints[key];
        if(this.resolvedMapping[key]) delete this.resolvedMapping[key];

        if(data.receivedEndpoint) {
          const rKey = this.getKey(data.receivedEndpoint.remoteAddr, data.receivedEndpoint.remotePort);
          if(this.resolvedMapping[rKey]) delete this.resolvedMapping[rKey];
        }
      }
    }

    getActualEndpoint(ip:string, port:number) {
      const ep = this.getEndpoint(ip, port);
      if(ep?.receivedEndpoint) {
        return ep.receivedEndpoint;
      } else {
        return ep;
      }
    }


    onMessage(buffer:Buffer, remoteInfo:UDP_Type.RemoteInfo) {
      try {
        const objCompressed = JSON.parse(buffer.toString()) as HandShakeCompressed;
        const obj:StunData = {address: objCompressed.a, port: objCompressed.p, family:'' }; // Family doesn't really matter
        if(!obj.address || !obj.port) throw "Not a metadata message" // if not a metadata package, just continue with packet as normal.

        const stunKey = this.getKey(obj.address, obj.port);
        const resolvedEP = this.getEndpoint(obj.address,obj.port);
        if(!resolvedEP) {
          // message from unknown endpoint
          return;
        }

        resolvedEP.lastHandshakeReceived = Date.now();
        if(objCompressed.r) {
          resolvedEP.otherReceivedMyHandshake = true;
        } else {
          delete resolvedEP.otherReceivedMyHandshake;
        }

        // if obj contains different details than remoteInfo, set up "resolvedMapping" and endPoints[key].receivedEndpoint
        const trueKey = this.getKey(remoteInfo.address, remoteInfo.port);
        if(stunKey != trueKey) {
          if(!this.resolvedMapping[trueKey]) {
            this.resolvedMapping[trueKey] = stunKey;
            this.endPoints[stunKey].receivedEndpoint = {remoteAddr: remoteInfo.address, remotePort: remoteInfo.port};
          }
        }
        if(nnglobal?.stun?.log?.all || nnglobal?.stun?.log?.received?.handshake) {
          console.log(`STUN Received Handshake from (${remoteInfo.address}:${remoteInfo.port}): ${buffer.toString()}`);
        }
        return; // Don't pass buffer if it's a handshake
      } catch(e) {}

      const endPoint = this.getEndpoint(remoteInfo.address, remoteInfo.port);
      if(!endPoint) {
        if(nnglobal?.stun?.log?.all || nnglobal?.stun?.log?.warnings) {
          console.warn(`Unknown endpoint buffer ${JSON.stringify(remoteInfo)}`)
        }
        return; // If endpoint not found do nothing
      }

      if(nnglobal?.stun?.log?.all || nnglobal?.stun?.log?.received?.raw) {
        console.log(`STUN Received Raw: ${JSON.stringify(buffer)}`);
      }

      if(endPoint.forwardTo) {
        endPoint.tunnel.send(buffer, endPoint.forwardTo.port, udp6a(endPoint.forwardTo.addr));
      } else if(endPoint.connector) {
        endPoint.tunnel.send(buffer, endPoint.connector.port, udp6a(endPoint.connector.address));
      }
    }

    sendHandshakeData(remoteAddr:string, remotePort:number) {
      const {address,port} = this.conn.stunAddr;
      const sendTo = this.getActualEndpoint(remoteAddr, remotePort);
      const obj = {a:address, p:port} as HandShakeCompressed;

      const epData = this.getEndpoint(remoteAddr, remotePort);
      if(epData.lastHandshakeReceived > (Date.now() - TunnelTimeoutSeconds)) {
        obj.r = 1;
      }

      const str = JSON.stringify(obj);
      if(nnglobal?.stun?.log?.all || nnglobal?.stun?.log?.sent?.handshake) {
        console.log(`STUN Sent Handshake to (${sendTo.remoteAddr}:${sendTo.remotePort})`)
      }
      
      this.conn.socket.send(str, sendTo.remotePort, udp6a(sendTo.remoteAddr));
    }

    signalKeepAlive() {
      // Indication to STUN server to keep port mapping alive
      this.conn.socket.send(stun.createMessage(stun.constants.STUN_BINDING_INDICATION).toBuffer(), this.conn.stunSrv.port, udp6a(this.conn.stunSrv.address));

      for(const ip_port in this.endPoints) {
        const entry = this.endPoints[ip_port];
        const endpoint = this.getActualEndpoint(entry.remoteAddr, entry.remotePort);
        this.sendHandshakeData(endpoint.remoteAddr,endpoint.remotePort);
      }
    }
}


// Usage example
// -------------
// try {
//   const ctl = await StunController.create(30000,40000,23513, {
//     port: 3478,
//     address: 'stun.halonet.pl'
//   });
//   const epClient = await ctl.addEndpoint('putOtherPeerIpHere', 'putOtherPeerStunPortHere', {addr:'localhost', port:23513});
//   console.log("STUN port: " + ctl.conn.stunAddr.port); // This is the port you give to the other peer, so he can connect to your external IP with this port.
//   console.log(`Listening on port ${epClient?.tunnel.address().port}`); // This is the port you connect to on your localhost to send/receive data to/from the other peer.
// } catch(e) {}