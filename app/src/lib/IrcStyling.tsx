
const IRCColorCodeToColor: {[colorCode:string]: string} = {
    "00":"#ffffff", // WHITE
    "0":"#ffffff", // WHITE

    "01":"#000000", // BLACK
    "1":"#000000", // BLACK

    "02":"#00007f", // BLUE
    "2":"#00007f", // BLUE

    "03":"#009300", // GREEN
    "3":"#009300", // GREEN

    "04":"#ff0000", // RED
    "4":"#ff0000", // RED

    "05":"#7f0000", // BROWN
    "5":"#7f0000", // BROWN

    "06":"#9c009c", // MAGENTA
    "6":"#9c009c", // MAGENTA

    "07":"#fc7f00", // ORANGE
    "7":"#fc7f00", // ORANGE

    "08":"#ffff00", // YELLOW
    "8":"#ffff00", // YELLOW

    "09":"#00fc00", // LIGHT CYAN
    "9":"#00fc00", // LIGHT CYAN

    "10":"#009393", // CYAN
    "11":"#00ffff", // LIGHT CYAN
    "12":"#0000fc", // LIGHT BLUE
    "13":"#ff00ff", // PINK
    "14":"#7f7f7f", // GREY
    "15":"#d2d2d2", // LIGHT GREY

    "16":"#470000",
    "17":"#472100",
    "18":"#474700",
    "19":"#324700",
    "20":"#004700",
    "21":"#00472c",
    "22":"#004747",
    "23":"#002747",
    "24":"#000047",
    "25":"#2e0047",
    "26":"#470047",
    "27":"#47002a",
    "28":"#740000",
    "29":"#743a00",
    "30":"#747400",
    "31":"#517400",
    "32":"#007400",
    "33":"#007449",
    "34":"#007474",
    "35":"#004074",
    "36":"#000074",
    "37":"#4b0074",
    "38":"#740074",
    "39":"#740045",
    "40":"#b50000",
    "41":"#b56300",
    "42":"#b5b500",
    "43":"#7db500",
    "44":"#00b500",
    "45":"#00b571",
    "46":"#00b5b5",
    "47":"#0063b5",
    "48":"#0000b5",
    "49":"#7500b5",
    "50":"#b500b5",
    "51":"#b5006b",
    "52":"#ff0000",
    "53":"#ff8c00",
    "54":"#ffff00",
    "55":"#b2ff00",
    "56":"#00ff00",
    "57":"#00ffa0",
    "58":"#00ffff",
    "59":"#008cff",
    "60":"#0000ff",
    "61":"#a500ff",
    "62":"#ff00ff",
    "63":"#ff0098",
    "64":"#ff5959",
    "65":"#ffb459",
    "66":"#ffff71",
    "67":"#cfff60",
    "68":"#6fff6f",
    "69":"#65ffc9",
    "70":"#6dffff",
    "71":"#59b4ff",
    "72":"#5959ff",
    "73":"#c459ff",
    "74":"#ff66ff",
    "75":"#ff59bc",
    "76":"#ff9c9c",
    "77":"#ffd39c",
    "78":"#ffff9c",
    "79":"#e2ff9c",
    "80":"#9cff9c",
    "81":"#9cffdb",
    "82":"#9cffff",
    "83":"#9cd3ff",
    "84":"#9c9cff",
    "85":"#dc9cff",
    "86":"#ff9cff",
    "87":"#ff94d3",
    "88":"#000000",
    "89":"#131313",
    "90":"#282828",
    "91":"#363636",
    "92":"#4d4d4d",
    "93":"#656565",
    "94":"#818181",
    "95":"#9f9f9f",
    "96":"#bcbcbc",
	"97":"#e2e2e2",
    "98":"#ffffff"
}

function isNum(charCode:number) {
    return charCode>=48 && charCode<=57;
}

function extractColorCode(str:string, index:number):string {
    let s = '';

    let i=index;
    for(; i<index+2;i++) {
        const charCode = str.charCodeAt(i);
        if(isNum(charCode)) {
            s += str.charAt(i);
        } else if(charCode === 44) { // colon ,
            break;
        }
    }
    if(str.charCodeAt(i) !== 44) return s;

    s+=',';
    i++;
    const until = i+2;
    for(; i<until;i++) {
        const charCode = str.charCodeAt(i);
        if(isNum(charCode)) {
            s += str.charAt(i);
        }
    }

    return s;
}

export function IRCStylizeText(text:string) {

    let textElements = [];
    let currentElement = null;
    for(let i=0; i<text.length; i++) {
        const char = text.charAt(i);

        if(char === '') { // Italic
            if(currentElement) textElements.push(currentElement);
            currentElement = document.createElement('span');
            currentElement.style.fontStyle = 'italic';
        } else if(char === '') { // start of bold
            if(currentElement) textElements.push(currentElement);
            currentElement = document.createElement('span');
            currentElement.style.fontWeight = 'bold';
        } else if(char === '') { // start of color
            if(currentElement) textElements.push(currentElement);

            currentElement = document.createElement('span');
            const colorCodeParts = extractColorCode(text,i+1);
            const parts = colorCodeParts.split(',');
            if(parts?.[0]?.length) {
                const colorHex = IRCColorCodeToColor?.[parts?.[0]];
                if(colorHex!=null) {
                    currentElement.style.color = colorHex
                }
            }
            if(parts?.[1]?.length) {
                const colorHex = IRCColorCodeToColor?.[parts?.[1]];
                if(colorHex!=null) {
                    currentElement.style.backgroundColor = colorHex
                }
            }
            i+=colorCodeParts.length; // skip next to chars
        } else if(char === '') {
            if(currentElement) textElements.push(currentElement);
            currentElement = document.createElement('span');
        } else {
            if(!currentElement) currentElement = document.createElement('span');
            currentElement.innerText += char;
        }
    }
    if(currentElement) textElements.push(currentElement);

    const message = textElements;
    return message;
}