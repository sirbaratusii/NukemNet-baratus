
type FunctionNNEval = (ctx:any)=>any

export function NNEval(code:string|FunctionNNEval, context?:any) {
    if(typeof code === 'function') {
        return code(context);
    } else {
        let preCode = '';
        if(context != null && typeof context == 'object') {
            for(const prop in context) {
                const val = context[prop];
                preCode += `const ${prop} = ${JSON.stringify(val)}; `; 
            }
        }
        const finalCode = `${preCode} ${code}`;
        return eval(finalCode);
    }
}

export function NNEvalWrapArray(code:string|FunctionNNEval, context?: any) {
    const result = NNEval(code, context);
    if(typeof result === 'number') {
        return [`${result}`] // convert nums to strings
    } else if(typeof result === 'string') {
        return [result];
    } else if(Array.isArray(result)) {
        return result.map(r=>typeof r === 'number'? `${r}`:r); // convert nums to strings
    } else {
        return null;
    }
}