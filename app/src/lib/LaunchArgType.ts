import { GameDefsType } from "../GameDefsTypes"

export type LaunchArgType = {
    id:string,
    label?:string,
    valueLabel?:string,
    meta?:any,
    args:string[],
    order?:number
    for?:GameDefsType.ParamType.ParamFor,
    syncOnly?:boolean,
    html?: {prop:string, value:any}
}

export interface LaunchArgType_File extends LaunchArgType {
    meta: {
        md5:string,
        name:string,
        size:number,
    }
}