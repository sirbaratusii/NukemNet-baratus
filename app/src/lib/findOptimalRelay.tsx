import { Client } from "matrix-org-irc";
import { Button, Spinner } from "solid-bootstrap";
import { createSignal, For, Show } from "solid-js";
import { createMutable } from "solid-js/store";
import { IrcClientExtra } from "../../../server/src/common/ircExtend";
import { CmdGameRoom_PingRelays, CmdGameRoom_PongRelays } from "../commands-gameroom";
import { ping } from "../NodeLibs";
import { DialogMgr } from "../Popups";
import { mainStore, nncmdReceivedEmitter } from "../store/store";
import { RelayInfo } from "./RelayInfo";
import { isWindows } from "./utils";

export async function pingAllRelays(relayOptions:RelayInfo[]): Promise<[string,number|null][]> {
    
    const relayResponses = relayOptions.map(async relay=>{
        const results = [
            relay.id,
            (await ping.promise.probe(relay.host, {timeout: 3, extra: isWindows()?[]:['-i', '1']})).times?.[0] as number || null
        ]
        if(results[1] !== null) {
            results[1] = parseInt(results[1] as any);
        }
        return results;;
    });
    const relaysAndPingsPairs = await Promise.all(relayResponses);
    return relaysAndPingsPairs as any;
}

export async function findOptimalRelay(ircClient: Client, chan:string, relays:RelayInfo[], onUseRelayId:(id:string)=>void) {
    const g = mainStore.gameForChannel(chan);
    if(!g) return;

    IrcClientExtra.sendNNCmdTo(ircClient, chan, {
        op: 'pingRelays'
    } as CmdGameRoom_PingRelays)

    const mylnick = ircClient.nick.toLowerCase();
    let lnicksMissingPing = createMutable([...Object.keys(g.participantNicks)].map(nick => nick.toLowerCase()));

    let closeCB:any;

    const popup = DialogMgr.addPopup({
        fullscreen: true,
        title: 'Optimal Relay Results',
        onClose: ()=>{closeCB();},
        closeButton:true,
        body: ()=>{

            const [finished, setFinished] = createSignal(false);
            const [someUsersTimedOut, SET_someUsersTimedOut] = createSignal(false);
            const [optimalAvg, setOptimalAvg] = createSignal(null as null|RelayInfo);
            const [optimalLowestMax, setOptimalLowestMax] = createSignal(null as null|RelayInfo);

            const lnicksToRelayPings = createMutable({} as {[nick:string]:[string,number|null][]});

            pingAllRelays(relays).then(myPings=>{
                onPong('','', '', mylnick, {
                    op: 'pongRelays',
                    data: myPings
                } as CmdGameRoom_PongRelays, ircClient);
            })

            let averagePings = createMutable([] as number[]);
            let lowestMaxPings = createMutable([] as number[]);
            let [minAvgPing, setMinAvgPing] = createSignal(Infinity as number);
            let [lowestMaxPing, setLowestMaxPing] = createSignal(Infinity as number);
            function calculatePingResults() {
                averagePings.splice(0, averagePings.length);
                lowestMaxPings.splice(0, lowestMaxPings.length);
                const usersRespondedPerRelay = [];
                for(let i=0;i<relays.length;i++) {usersRespondedPerRelay.push(0), averagePings.push(0), lowestMaxPings.push(-Infinity)};
                for(const nick in lnicksToRelayPings) {
                    const relayResult = lnicksToRelayPings[nick];
                    for(let i=0;i<relayResult.length;i++) {
                        const [,ping] = relayResult[i];
                        if(ping===null) continue;
                        averagePings[i] += ping;
                        usersRespondedPerRelay[i]++;
                        lowestMaxPings[i] = Math.max(lowestMaxPings[i], ping);
                    }
                }
                for(let i=0;i<averagePings.length;i++) {
                    averagePings[i] = parseInt(averagePings[i]/usersRespondedPerRelay[i] as any);
                }
        
                setMinAvgPing(Math.min(...averagePings));
                const relayIndexLowestAverage = averagePings.findIndex(ping=>ping===minAvgPing());
                setOptimalAvg(relays[relayIndexLowestAverage]);
                
                setLowestMaxPing(Math.min(...lowestMaxPings));
                const relayIndexLowestMaxPing = lowestMaxPings.findIndex(ping=>ping===lowestMaxPing());
                setOptimalLowestMax(relays[relayIndexLowestMaxPing]);
            }

            function onPong(_:any, __:any, ___:any, nick:string, _c:CmdGameRoom_PongRelays, ____:any) {
                const lnick = nick.toLowerCase();
                const foundIdx = lnicksMissingPing.findIndex(otherLnick=>lnick==otherLnick);
                if(foundIdx==-1) return;
                lnicksMissingPing.splice(foundIdx, 1);

                const cmd = _c as CmdGameRoom_PongRelays;
                lnicksToRelayPings[lnick] = cmd.data;
        
                calculatePingResults();
        
                if(lnicksMissingPing.length===0) {
                    nncmdReceivedEmitter.off('pongRelays', onPong);
                    setFinished(true);
                    clearTimeout(timeoutTimer);
                }
            }

            closeCB = ()=>{
                nncmdReceivedEmitter.off('pongRelays', onPong);
                clearTimeout(timeoutTimer);
            };

            const timeoutTimer = setTimeout(()=>{
                try {nncmdReceivedEmitter.off('pongRelays', onPong);} catch(e) {}
                if(finished()) return;
                lnicksMissingPing.splice(0, lnicksMissingPing.length);
                SET_someUsersTimedOut(true);
            }, 12000);

            nncmdReceivedEmitter.on('pongRelays', onPong);

            return <div>
            <Show when={someUsersTimedOut()}>
                <div>Some users did not repond with ping details.</div>
            </Show>

            <table style="width:100%"><tbody><tr>
                <Show when={optimalAvg()!=null}>
                    <td style={{"background-color":"green", "font-weight":'bolder'}}>Optimal {optimalAvg()!=optimalLowestMax()?'(average ping)':''}: {optimalAvg()?.label}
                    <Spinner style={{display:finished()?'none':undefined}} animation="border" variant="primary" />
                    <Button style="margin-left:5pt" onClick={()=>{
                        popup.close();
                        onUseRelayId(optimalAvg()!.id);
                        }}>Use</Button>
                    </td>
                </Show>
                <Show when={optimalAvg()!=null && optimalAvg()!=optimalLowestMax()}>
                    <td style={{"background-color":"orange", "font-weight":'bolder'}}>Optimal (Lowest max ping): {optimalLowestMax()?.label}
                    <Spinner style={{display:finished()?'none':undefined}} animation="border" variant="primary" />
                    <Button style="margin-left:5pt" onClick={()=>{
                        popup.close();
                        onUseRelayId(optimalLowestMax()!.id);
                        }}>Use</Button>
                    </td>
                </Show>    
                
            </tr></tbody></table>

            <table><tbody>
                <tr style="background-color:grey">
                    <td>X</td>
                    <For each={relays}>{(relay)=><td title={relay.host}>{relay.label}</td>}</For>
                </tr>
                <For each={Object.keys(lnicksToRelayPings)}>{(lnick)=>
                    <tr>
                        <td>
                            {lnick}
                            <Show when={lnicksMissingPing.includes(lnick)}>
                                <br/>
                                <Spinner animation="border" variant="primary" />
                            </Show>
                        </td><For each={lnicksToRelayPings[lnick]}>{([,relayPing])=><td style={{"background-color":relayPing==null?'red':undefined}}>{relayPing==null?"Timeout":parseInt(relayPing as any)}</td>}</For>
                    </tr>
                }</For>
                <Show when={finished()}>
                <tr>
                    <td>Average</td><For each={averagePings}>{(ping)=><td style={{"background-color":
                    minAvgPing()===ping?"green":undefined
                    }}>{isNaN(ping)?'X':ping}</td>}</For>
                </tr>
                <tr>
                    <td>Max</td><For each={lowestMaxPings}>{(ping)=><td style={{"background-color":
                    lowestMaxPing()===ping&&ping!=-Infinity?"orange":undefined
                    }}>{ping===-Infinity?'X':ping}</td>}</For>
                </tr>
                </Show>
            </tbody></table>
        </div>
        },
        buttons: [{body:"Close"}]
    })
}