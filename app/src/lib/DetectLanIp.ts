import { axios } from "../NodeLibs";
import { IpDetails } from "./IpDetails";


export async function detectLanIpOfNick(ipDetails:IpDetails, httpPort:number, nick:string, includeIpV6?:boolean) {
    let targetIp:string|null;;
    try {
        targetIp = await new Promise<string>(async (resolve,reject)=>{
            const signals = [] as AbortController[];
            let resolved = false;
            function finishUp(err?:any, ip?:string) {
                for(const abortCtl of signals) {
                    try {if(!abortCtl.signal.aborted) abortCtl.abort();} catch(e) {}
                }
                if(!resolved) {
                    clearTimeout(timoutTimer);
                    resolved = true;
                    if(err) reject(err);
                    else resolve(ip!);
                }
            }

            const timoutTimer = setTimeout(()=>{
                finishUp(new Error('Timeout: Failed to detect local ip'))
            },4000);

            let requestsCompleted = 0;
            function handleRequestCompleted() {
                requestsCompleted++;
                if(requestsCompleted >= ipDetails.localIpV4.length) {
                    finishUp(new Error('Timeout: Failed to detect local ip'))
                }
            }

            for(const ip of ipDetails.localIpV4) {
                const abortCtl = new AbortController();
                signals.push(abortCtl);
                axios.get(`http://${ip}:${httpPort}/nick`, {signal:abortCtl.signal}).catch(()=>{
                    handleRequestCompleted();
                }).then(res=>{
                    if(res?.data && (''+res.data).toLowerCase() === nick.toLowerCase()) {
                        finishUp(null, ip);
                    }
                    handleRequestCompleted();
                });
            }
        })
    } catch(e) {
        targetIp = null;
    }
    return targetIp;
}