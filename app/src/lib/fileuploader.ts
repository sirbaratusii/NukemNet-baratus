
import { Electron, fsSync, path } from '../NodeLibs';
import { AzureUpload } from './AzureBlobUpload';
import { isImgExtension, uploadImage } from './ImageUpload';

export function genericUploadLogic(filepathOrBuffer:string|Buffer, filename:string, chan:string, ircClient:any) {
  if(isImgExtension(filename)) {
      uploadImage(filepathOrBuffer, filename, chan, ircClient);
  } else {
      AzureUpload.fullUpload(filepathOrBuffer, filename, chan, ircClient)
  }
}

export function PasteHandler_Init(document:Document, chosenTabResolve:() => string, ircClient:any) {
    document.addEventListener('paste', async function(e) {
        try {
            const format = await Electron.clipboard.availableFormats()?.[0];
            if(format === 'text/plain') {
              let text = Electron.clipboard.readText();

              const fpathIndicator = "file://";
              const indexOfFpath = text.lastIndexOf(fpathIndicator)
              let pastedFromExplorer = false;
              if(indexOfFpath !== -1) {
                  pastedFromExplorer = true;
                  text = text.substring(text.lastIndexOf(fpathIndicator)+fpathIndicator.length).trim();
              }
              if(pastedFromExplorer || (e.target as HTMLInputElement).tagName.toLowerCase() !== 'input') {
                  if(await fsSync.existsSync(text)) {
                      e.preventDefault();
                      genericUploadLogic(text, path.basename(text), chosenTabResolve(), ircClient);
                  }
              }
            }
            else if(format.startsWith('image/')) {
                const imgData = await Electron.clipboard.readImage();
                const fileType = format.endsWith('png')? 'png' : 'jpg';
                const buffer = fileType==='png' ? imgData.toPNG() : imgData.toJPEG(90);
                e.preventDefault();
                uploadImage(buffer, `image.${fileType}`, chosenTabResolve(), ircClient);
            } else if(format==="text/uri-list") {
                const rawFilePath = Electron.clipboard.read('FileNameW');
                const filePath = rawFilePath.replace(new RegExp(String.fromCharCode(0), 'g'), '');
                e.preventDefault();
                genericUploadLogic(filePath, path.basename(filePath), chosenTabResolve(), ircClient);
            }
        } catch(e) {
            console.error('Failed pasting from clipboard', e);
        }
    });
}
