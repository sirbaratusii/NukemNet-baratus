import sleep from 'sleep-promise';
import {netstats} from '../NodeLibs'

export async function isPortInUse(port:number, protocol:'udp'|'tcp') {
    try {
        const portLine = (await netstats(port)).join('');
        const lPortLine = portLine.toLowerCase();
        if(lPortLine.indexOf(protocol) !== -1) {
            return true;
        } else {
            return false
        }
    } catch(e) {
        return false;
    }
}

export async function waitUntilPortIsUsedInProto(port:number, protocol:'udp'|'tcp', timeout=3000) {
    const startTime = Date.now();
    const timeoutTime = startTime+timeout;
    do {
        const isInUse = await isPortInUse(port, protocol);
        if(isInUse) return true;
        await sleep(100);
    } while(Date.now()<timeoutTime)
    return false;
}
