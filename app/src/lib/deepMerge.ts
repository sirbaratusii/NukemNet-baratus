// https://stackoverflow.com/a/34749873/230637

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
 function isObject(item:any) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}
  
  /**
   * Deep merge two objects.
   * @param target
   * @param ...sources
   */
export function deepMerge<T extends Object>(target:T, ...sources:any[]):T {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
        if (isObject(source[key])) {
            if (!(target as any)[key]) Object.assign(target, { [key]: {} });
            deepMerge((target as any)[key], source[key]);
        } else {
            Object.assign(target, { [key]: source[key] });
        }
        }
    }

    return deepMerge(target, ...sources);
}