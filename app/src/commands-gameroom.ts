import { Client } from "matrix-org-irc";
import { Cmd_Base, LeasedDedicatedServerDetails } from "../../server/src/common/commands";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import { Room_Advertisement } from "../../server/src/common/room";
import { ElementType } from "../../server/src/common/type-utils";
import { IpDetails } from "./lib/IpDetails";
import { LaunchArgType } from "./lib/LaunchArgType";

export interface IpDetailsAndPorts extends IpDetails {
    gamePort?: number,
    httpPort: number,
    ipv6tp: number // ipv6 tunnel port
}

export interface IpAndPort {
    ip:string,
    port?:number
}

export interface LanDetectionDetails {
    detectedLanIp?: string
    isDetecting?:boolean
}

export interface StunClientDetails {port:number, addr:string, symmetricNat?:boolean}

export interface NickParticipantData {
    isExecRunning:boolean,
    isExecMissing:boolean,
    fileSyncProgress?:number,
    ipsAndPorts: IpDetailsAndPorts,
    stun?: StunClientDetails
}

export interface FullGameRoomDetails {
    publicDetails: Room_Advertisement,
    params: LaunchArgType[],
    participantNicks: {[nick:string]: NickParticipantData},
    dedicatedServer?:LeasedDedicatedServerDetails
}

export const CmdGameRoom_StunSync_op = 'stun_sync';
export interface CmdGameRoom_StunSync extends Cmd_Base {
    op: typeof CmdGameRoom_StunSync_op,
    data: Partial<StunClientDetails> & {start?:true,down?:true}
}

export const CmdGameRoom_FileSync_op = 'file_sync';
export interface CmdGameRoom_FileSync extends Cmd_Base {
    op: typeof CmdGameRoom_FileSync_op,
    data: {
        doneFile?:string|'all', // sent by host only, when finished uploading file or all files
        progress?:number, // percent. if omitted, assume finished
    }
}

export const CmdGameRoom_RequestGameDetails_op = 'requestGameDetails';
export interface CmdGameRoom_RequestGameDetails extends Cmd_Base {
    op: typeof CmdGameRoom_RequestGameDetails_op,
    data: {
        channel:string
    }
}

const CmdGameRoom_GameMessage_op = 'gameMessage';
export interface CmdGameRoom_GameMessage extends Cmd_Base {
    op: typeof CmdGameRoom_GameMessage_op,
    data: {text:string}
}

const CmdGameRoom_TerminateProc_op = 'terminateProc';
export interface CmdGameRoom_TerminateProc extends Cmd_Base {
    op: typeof CmdGameRoom_TerminateProc_op,
    data: any
}

export const CmdGameRoom_SyncSendAllDetails_op = 'sync_sendAllDetails';
export interface CmdGameRoom_SyncSendAllDetails extends Cmd_Base {
    op: typeof CmdGameRoom_SyncSendAllDetails_op,
    data: FullGameRoomDetails
}

export const CmdGameRoom_ResSendAllDetails_op = 'res_sendAllDetails';
export interface CmdGameRoom_ResSendAllDetails extends Cmd_Base {
    op: typeof CmdGameRoom_ResSendAllDetails_op,
    data: FullGameRoomDetails
}

const CmdGameRoom_ParticipantAdded_op = 'sync_participantAdded';
export interface CmdGameRoom_ParticipantAdded extends Cmd_Base {
    op: typeof CmdGameRoom_ParticipantAdded_op,
    data: {nick:string, ipsAndPorts:IpDetailsAndPorts}
}

const CmdGameRoom_AskToParticipate_op = 'askToParticipate';
export interface CmdGameRoom_AskToParticipate extends Cmd_Base {
    op: typeof CmdGameRoom_AskToParticipate_op,
    data: {
        channel:string,
        ipsAndPorts:IpDetailsAndPorts
    }
}

const CmdGameRoom_LaunchGame_op = 'launchGame';
export interface CmdGameRoom_LaunchGame extends Cmd_Base {
    op: typeof CmdGameRoom_LaunchGame_op,
    data: {}
}

const CmdGameRoom_IsExecRunning_op = 'isExecRunning';
export interface CmdGameRoom_IsExecRunning extends Cmd_Base {
    op: typeof CmdGameRoom_IsExecRunning_op,
    data: {running:boolean}
}

const CmdGameRoom_IsExecMissing_op = 'isExecMissing';
export interface CmdGameRoom_IsExecMissing extends Cmd_Base {
    op: typeof CmdGameRoom_IsExecMissing_op,
    data: {isMissing:boolean}
}

const CmdGameRoom_Patch_FullGameRoomDetails_op = 'patchRoomGameDetails';
export interface CmdGameRoom_Patch_FullGameRoomDetails extends Cmd_Base {
    op: typeof CmdGameRoom_Patch_FullGameRoomDetails_op,
    data: {patch:any}
}

const CmdGameRoom_ToggleDedicatedServer_op = 'toggleDedicatedServer';
export interface CmdGameRoom_ToggleDedicatedServer extends Cmd_Base {
    op: typeof CmdGameRoom_ToggleDedicatedServer_op,
    data: {
        details?:LeasedDedicatedServerDetails // leave undefined to turn off
    }
}

const CmdGameRoom_PingRelays_op = 'pingRelays';
export interface CmdGameRoom_PingRelays extends Cmd_Base {
    op: typeof CmdGameRoom_PingRelays_op
}

const CmdGameRoom_PongRelays_op = 'pongRelays';
export interface CmdGameRoom_PongRelays extends Cmd_Base {
    op: typeof CmdGameRoom_PongRelays_op,
    data: Array<[string,number|null]>
}

export const CmdGameRoom_AllOps = [CmdGameRoom_ResSendAllDetails_op, CmdGameRoom_RequestGameDetails_op, CmdGameRoom_FileSync_op, CmdGameRoom_StunSync_op, CmdGameRoom_GameMessage_op, CmdGameRoom_PongRelays_op, CmdGameRoom_PingRelays_op, CmdGameRoom_IsExecMissing_op, CmdGameRoom_ParticipantAdded_op, CmdGameRoom_ToggleDedicatedServer_op, CmdGameRoom_Patch_FullGameRoomDetails_op, CmdGameRoom_LaunchGame_op,CmdGameRoom_SyncSendAllDetails_op, CmdGameRoom_AskToParticipate_op, CmdGameRoom_IsExecRunning_op, CmdGameRoom_TerminateProc_op] as const;
export type CmdGameRoom_AllTypes = CmdGameRoom_ResSendAllDetails|CmdGameRoom_RequestGameDetails|CmdGameRoom_FileSync|CmdGameRoom_StunSync|CmdGameRoom_GameMessage|CmdGameRoom_PingRelays|CmdGameRoom_PongRelays|CmdGameRoom_IsExecMissing|CmdGameRoom_ParticipantAdded|CmdGameRoom_ToggleDedicatedServer|CmdGameRoom_Patch_FullGameRoomDetails|CmdGameRoom_AskToParticipate|CmdGameRoom_SyncSendAllDetails|CmdGameRoom_LaunchGame|CmdGameRoom_IsExecRunning|CmdGameRoom_TerminateProc;
export type CmdGameRoom_AllOpTypes = ElementType<typeof CmdGameRoom_AllOps>

export function SEND_CmdGameRoom_IsExecRunning(channel:string, running:boolean, ircClient:Client) {
    IrcClientExtra.sendNNCmdTo(ircClient, channel, {
        op: CmdGameRoom_IsExecRunning_op,
        data: {running}
    } as CmdGameRoom_IsExecRunning)
}

export function SEND_CmdGameRoom_IsExecMissing(channel:string, missing:boolean, ircClient:Client) {
    IrcClientExtra.sendNNCmdTo(ircClient, channel, {
        op: CmdGameRoom_IsExecMissing_op,
        data: {isMissing:missing}
    } as CmdGameRoom_IsExecMissing)
}