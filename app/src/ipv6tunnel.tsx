import { RemoteInfo, Socket as UDPSocket } from "dgram";
import { Client } from "matrix-org-irc";
import { Server as TCPServer, Socket as TCPSocket } from "net";
import { Table } from "solid-bootstrap";
import { For, Show } from "solid-js";
import { IrcClientExtra, nncmdSentEmitter } from "../../server/src/common/ircExtend";
import { CmdGameRoom_SyncSendAllDetails, CmdGameRoom_SyncSendAllDetails_op } from "./commands-gameroom";
import { GameDefsType } from "./GameDefsTypes";
import { findFreeUdpTcpPortInRange, IPv6_Equal } from "./lib/utils";
import { NNCMD_afterOpIndicator } from "./nncmdHandler";
import { dgram, kcp, net } from "./NodeLibs";
import { DialogMgr } from "./Popups";
import { Settings } from "./Settings";
import { mainStore, NickIpv6TunnelData, nncmdReceivedEmitter, offline } from "./store/store";
import storeActions from "./store/storeActions";

const ipv6tunIndicator = 'nn.ipv6.tun'
const ipv6tunIndicatorBuffer = Buffer.from(ipv6tunIndicator);

const TunnelTimeoutMs = 6000;
const kcpReadInterval = 20
const tcpkillCountDownStart = 7; // seconds

export async function setupIpv6Tunnel(ircClient:Client) {

    let destroyFunc:null|(()=>void) = null;

    function doDestroy() {
        if(destroyFunc) {destroyFunc(); destroyFunc = null;};
    }

    function init(chan:string) {
        doDestroy();
        destroyFunc = function doDestroy() {
            mainStore.ipv6udpTunnel?.removeListener('message', onIPv6Message);
            IrcClientExtra.customEvents.off('otherUserNotInChannelAnymore', userNotInChannelAnymore);
            nncmdReceivedEmitter.off(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
            nncmdSentEmitter.off(CmdGameRoom_SyncSendAllDetails_op, onSentFullDetails);
            IrcClientExtra.customEvents.off('gameRoom_participantAdded', onParticipantAdded);
            destroyAllUserTunnels();
        }

        let gameRoom = mainStore.gameForChannel(chan);
        const execDef = mainStore.execDefForChannel(chan);
        const chanData = mainStore.joinedChannels[chan];
        if(!gameRoom || !execDef) {return console.error('ipv6tunnel setup failed, no game or execdef');};

        const MyTunnelPort = mainStore.ipv6TunnelPort;
        const MyGamePort = Settings.getGamePort(); // TODO can be different depending on game
        const imHost = ircClient.nick.toLowerCase() == storeActions.getGameHostNickForChannelImIn(chan)!.toLowerCase();

        async function onIPv6Message(msg:Buffer, rinfo:RemoteInfo) {
            // find related tunnel
            let tunnel: NickIpv6TunnelData|null = null;
            for(const nick in gameRoom.participantNicks) {
                const nd = gameRoom.participantNicks[nick]
                if(nd.ipsAndPorts.ipv6tp == rinfo.port && IPv6_Equal(nd.ipsAndPorts.externalIpV6, rinfo.address)) {
                    const tun = chanData.nicks[nick].localIpv6Tunnel;
                    if(!tun) {return};
                    tunnel = tun;
                    break;
                }
            }
            if(!tunnel) return;

            try {
                const starter = msg.subarray(0,ipv6tunIndicatorBuffer.length).toString();
                if(starter===ipv6tunIndicator) {
                    const actualMsg = msg.subarray(ipv6tunIndicatorBuffer.length)
                    if(lastExecDef?.isPeerToPeer || imHost) {
                        tunnel.udpSocket?.send(actualMsg, redirectionPortForHost(false), mainStore.localhostIp4);
                    } else if(tunnel.udpConnector) {
                        tunnel.udpSocket?.send(actualMsg, tunnel.udpConnector.port, tunnel.udpConnector.address);
                    }
                    return;
                }
            } catch(e) {}
            // if got here, treat as KCP or TCP message
            tunnel.kcp?.input(msg);
        }
        mainStore.ipv6udpTunnel!.on('message', onIPv6Message);

        function userNotInChannelAnymore(nick:string, leftChan?:string) {
            if(leftChan?.length && leftChan.toLowerCase() != chan.toLowerCase()) {return;}
            destroyEndpointForNick(nick);
        }
        IrcClientExtra.customEvents.on('otherUserNotInChannelAnymore', userNotInChannelAnymore);

        function onParticipantAdded(joinedChan:string, nick:string) {
            // if joined other channel or myself, ignore.
            if(joinedChan.toLowerCase() != chan.toLowerCase()) return;
            const isMe = nick.toLowerCase() == ircClient.nick.toLowerCase();
            if(isMe && imHost) return;

            const execDef = mainStore.execDefForChannel(chan);
            if(!execDef) {
                console.error(`ipv6tunnel setup failed for new joined user ${nick}, no execdef`);
                return;
            };
            syncEndpoints(execDef, isMe?undefined:nick);
        }

        function destroyAllUserTunnels() {
            for(const otherNick in chanData.nicks) {
                const lotherNick = otherNick.toLowerCase();
                if(lotherNick == ircClient.nick.toLowerCase()) continue;
                try {
                    destroyEndpointForNick(lotherNick);
                } catch(e) {}
            }
        }

        IrcClientExtra.customEvents.on('gameRoom_participantAdded', onParticipantAdded);

        let lastExecDef = mainStore.execDefForChannel(chan)!;
        let lastArgsUsedUDPTunnel = mainStore.gameChosenUdpTunnel(gameRoom);
        let lastExecConstantPorts = mainStore.execConstantPort(lastExecDef);
        let lastExecConstantPortsStr = JSON.stringify(lastExecConstantPorts); // easier to compare objects with str representation.
        async function roomWillChangeDetails(newExecDef: GameDefsType.Executable) {

            gameRoom = mainStore.gameForChannel(chan);
            const isUdpTunnel = mainStore.gameChosenUdpTunnel(gameRoom);
            const constantPorts = mainStore.execConstantPort(lastExecDef);
            const constantPortsStr = JSON.stringify(constantPorts);

            if(newExecDef == lastExecDef && isUdpTunnel == lastArgsUsedUDPTunnel && constantPortsStr == lastExecConstantPortsStr) return;

            lastExecDef = newExecDef;
            lastArgsUsedUDPTunnel = isUdpTunnel;

            if(lastExecConstantPortsStr != constantPortsStr) {
                lastExecConstantPorts = constantPorts;
                lastExecConstantPortsStr = constantPortsStr;
                // must destroy all tunnels first
                destroyAllUserTunnels();
            }
    
            syncEndpoints(newExecDef);
        }
        function onRecievedFullDetails (_:any, __:any, channel:string, otherNick:string, cmd: CmdGameRoom_SyncSendAllDetails) {
            const otherLNick = otherNick.toLowerCase();
            const hostNick = storeActions.getGameHostNickForChannelImIn(chan)!.toLowerCase();
            if(otherLNick != hostNick) return;
    
            const newExecDef = Settings.gameDefs?.games?.[cmd?.data?.publicDetails!.gameId]?.executables?.[cmd?.data?.publicDetails!.execId];
            if(newExecDef) roomWillChangeDetails(newExecDef);
        }
        function onSentFullDetails (__:any,_:any, nickOrChan:string, cmd:CmdGameRoom_SyncSendAllDetails) {
            const newExecDef = Settings.gameDefs?.games?.[cmd?.data?.publicDetails!.gameId]?.executables?.[cmd?.data?.publicDetails!.execId];
            if(newExecDef) roomWillChangeDetails(newExecDef);
        }
        nncmdReceivedEmitter.on(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
        nncmdSentEmitter.on(CmdGameRoom_SyncSendAllDetails_op, onSentFullDetails);

        function redirectionPortForHost(isTcp:boolean) {
            if(isTcp) {
                return lastExecConstantPorts?.tcp || lastExecConstantPorts?.udp || MyGamePort;
            } else {
                return lastExecConstantPorts?.udp || lastExecConstantPorts?.tcp || MyGamePort;
            }
        }

        async function upsertNickTunnelEndpoint(nick:string) {
            let udpPort:number|undefined, tcpPort:number|undefined;
            if(Object.keys(lastExecConstantPorts).length) {
                udpPort = lastExecConstantPorts?.udp;
                tcpPort = lastExecConstantPorts?.tcp;
                if(!udpPort && tcpPort) udpPort = tcpPort;
                if(!tcpPort && udpPort) tcpPort = udpPort;
                if(!udpPort && !tcpPort) console.error('Exec definition is wrong, constant ports are defined but no port numbers are defined.');
            } else {
                const port = await findFreeUdpTcpPortInRange(undefined,undefined,[MyTunnelPort, MyGamePort]);
                udpPort = tcpPort = port;
            }

            const lnick = nick.toLowerCase()
            const gameNickData = gameRoom.participantNicks[lnick]
            if(!gameNickData) return;
            const nickData = chanData.nicks[lnick]
            if(!nickData) return;
            if(nickData.localIpv6Tunnel) {return}
            nickData.localIpv6Tunnel = {};

            const localTunnel = dgram.createSocket('udp4')
            localTunnel.bind(udpPort);
            localTunnel.on('message', (tunMsg, tunRinfo)=>{
                if(nickData.localIpv6Tunnel) {nickData.localIpv6Tunnel.udpConnector = tunRinfo;}
                mainStore.ipv6udpTunnel?.send(Buffer.from([...ipv6tunIndicatorBuffer,...tunMsg]), gameNickData.ipsAndPorts.ipv6tp, gameNickData.ipsAndPorts.externalIpV6);
            });
            nickData.localIpv6Tunnel.udpSocket = localTunnel;

            let tcpkillCountDown = tcpkillCountDownStart;
            const interval = setInterval(()=>{
                if(!nickData?.localIpv6Tunnel) {clearInterval(interval); return;};
                // send handshake
                const r = (nickData.localIpv6Tunnel?.lrh || -Infinity) > (Date.now() - TunnelTimeoutMs);
                if(!r) {nickData.localIpv6Tunnel.r = false}
                nickData.localIpv6Tunnel.kcp?.send(Buffer.from(JSON.stringify({i:ipv6tunIndicator,type:"hs",r})));
                tcpkillCountDown--;
                if(tcpkillCountDown == 0) { // we don't do <= because it will keep destroying ever seconds, instead of once.
                    if(imHost) {
                        try {tcpClient?.destroy();} catch(e) {}
                        tcpClient = null;
                    } else {
                        try {tcpServerLatestConnector?.destroy();} catch(e) {}
                        tcpServerLatestConnector = null;
                    }
                }
            },1000);

            // KCP
            nickData.localIpv6Tunnel.kcp = new kcp.KCP(123, {address: gameNickData.ipsAndPorts.externalIpV6, port: gameNickData.ipsAndPorts.ipv6tp});
            nickData.localIpv6Tunnel.kcp.nodelay(0, kcpReadInterval, 0, 0);
            nickData.localIpv6Tunnel.kcp.output(function(data:Buffer, size:number, context:any) {
                mainStore.ipv6udpTunnel?.send(data, 0, size, context.port, context.address);
            });


            let tcpClient:TCPSocket|null = null; // host only

            let tcpServer:TCPServer|null = null; // client only
            let tcpServerLatestConnector:TCPSocket|null = null; // client only
            if(!imHost) {
                tcpServer = new net.Server();
                tcpServer.listen(tcpPort);
                tcpServer.on('connection', (socket)=>{
                    tcpServerLatestConnector = socket;
                    nickData.localIpv6Tunnel?.kcp?.send(Buffer.from(JSON.stringify({i:ipv6tunIndicator,type:"connectTcp"})));
                    socket.on('data', (data)=>{
                        nickData.localIpv6Tunnel?.kcp?.send(data);
                    });

                    let socketIsLive = true;

                    function destroySocketClient() {
                        if(!socketIsLive) return;
                        socketIsLive = false;
                        tcpServerLatestConnector = null;
                        try {socket.destroy();} catch(e) {}
                        nickData.localIpv6Tunnel?.kcp?.send(Buffer.from(JSON.stringify({i:ipv6tunIndicator,type:"tcpClosed"})));
                    }
                    
                    socket.on('error', destroySocketClient);
                    socket.on('close', destroySocketClient);
                    socket.on('end', destroySocketClient)
                    socket.on('timeout', destroySocketClient);                    
                })
            }

            function receivedKcpMessage(msg:Buffer) {
                if(!nickData?.localIpv6Tunnel) return;
                try {
                    const json = JSON.parse(msg.toString());
                    if(json.i == ipv6tunIndicator) {
                        if(json.type == 'hs') {
                            tcpkillCountDown = tcpkillCountDownStart;
                            nickData.localIpv6Tunnel.lrh = Date.now();
                            nickData.localIpv6Tunnel.r = !!json.r
                        } else if(json.type == 'connectTcp' && imHost) {
                            // destroy old tcp client
                            function destroyTcpConn(dontNotifyKcp?:boolean) {
                                if(!tcpClient) return;
                                try {tcpClient?.destroy();} catch(e) {}
                                tcpClient = null;
                                if(!dontNotifyKcp) {nickData.localIpv6Tunnel?.kcp?.send(Buffer.from(JSON.stringify({i:ipv6tunIndicator,type:"tcpClosed"})));}
                            }
                            destroyTcpConn(true)
                            tcpClient = net.createConnection(redirectionPortForHost(true), mainStore.localhostIp4);

                            tcpClient.on('error', destroyTcpConn);
                            tcpClient.on('close', destroyTcpConn);
                            tcpClient.on('end', destroyTcpConn)
                            tcpClient.on('timeout', destroyTcpConn);
                            tcpClient.on('data', (data)=>{nickData.localIpv6Tunnel?.kcp?.send(data);});
                        } else if(json.type == 'tcpClosed') {
                            if(imHost) {
                                try {tcpClient?.destroy()} catch(e) {}
                            } else {
                                try {tcpServerLatestConnector?.destroy();} catch(e) {}
                            }
                        }
                        return;
                    }
                } catch(e) {}

                // treat incoming TCP data
                if(imHost) {
                    try {tcpClient?.write(msg);} catch(e) {}
                } else {
                    try {tcpServerLatestConnector?.write(msg);} catch(e) {}
                }
            }

            const kcpRecIntervalId = setInterval(()=>{
                if(!nickData?.localIpv6Tunnel?.kcp) return
                nickData.localIpv6Tunnel.kcp.update(Date.now());
                let recv:Buffer|null = null;
                do {
                    recv = nickData.localIpv6Tunnel.kcp.recv();
                    if (recv) {receivedKcpMessage(recv)}
                } while (recv);
            }, kcpReadInterval);

            function onclosed() {
                try {localTunnel?.close();} catch(e) {}
                try {nickData.localIpv6Tunnel?.kcp?.release();} catch(e) {}

                // Maybe need to add those elsewhere too
                try {tcpClient?.destroy()} catch(e) {}
                try {tcpServer?.close()} catch(e) {}
                try {tcpServerLatestConnector?.destroy()} catch(e) {}
                
                delete nickData.localIpv6Tunnel;
                clearInterval(interval);
                clearInterval(kcpRecIntervalId);
            }
            localTunnel.on('close', onclosed);
            localTunnel.on('error', onclosed);
        }

        async function destroyEndpointForNick(nick:string) {
            const lotherNick = nick.toLowerCase();
            const nickData = chanData.nicks[lotherNick];
            if(!nickData.localIpv6Tunnel) {return}

            try {nickData.localIpv6Tunnel?.udpSocket?.close();} catch(e) {}
            try {nickData.localIpv6Tunnel?.kcp?.release();} catch(e) {}
            try {nickData.localIpv6Tunnel.tcpSocket?.destroy();} catch(e) {}

            delete nickData.localIpv6Tunnel;
        }

        async function syncEndpoints(execDef:GameDefsType.Executable, onlyNick?:string) {
            const isPeerToPeer = execDef.isPeerToPeer == true;

            const isUsingUdpTunnel = mainStore.gameChosenUdpTunnel(gameRoom);
            for(const otherNick in chanData.nicks) {
                const lotherNick = otherNick.toLowerCase();
                if(lotherNick == ircClient.nick.toLowerCase()) continue;
                if(onlyNick?.length && lotherNick != onlyNick.toLowerCase()) continue;

                const nickData = chanData.nicks[otherNick];
                const otherNickIsHost = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase() == lotherNick;
                
                const shouldTunnelExist = !isUsingUdpTunnel && (mainStore.userOnIpv6(otherNick) && mainStore.imOnIpv6) &&
                                            (imHost || isPeerToPeer || otherNickIsHost);

                if(nickData.localIpv6Tunnel && !shouldTunnelExist) {
                    destroyEndpointForNick(otherNick);
                } else if(!nickData.localIpv6Tunnel && shouldTunnelExist) {
                    upsertNickTunnelEndpoint(lotherNick)
                }
            }
        }
    }

    IrcClientExtra.customEvents.on('ingameroom', (chanOrNull:string|null)=>{
        if(!mainStore.imOnIpv6 || offline) return;
        if(chanOrNull != null) {init(chanOrNull);}
        else {doDestroy()}
    });
}



export function showIpv6Panel(chan:string, ircClient:Client) {

    DialogMgr.addPopup({
        title:"Ipv6 Tunneling",
        closeButton:true,
        body: ()=>{
            const lchan = chan.toLowerCase();
            const cd = mainStore.joinedChannels[lchan]
            const gd = mainStore.gameForChannel(lchan);
            const exec = mainStore.execDefForChannel(lchan);
            const isP2p = exec?.isPeerToPeer == true;
            const imHost = mainStore.imHostInChannel(lchan);
            const hostNick = storeActions.getGameHostNickForChannelImIn(lchan)?.toLowerCase();

            return <div>
            <Table striped bordered hover variant="dark" class="supported-games-table">
                <tbody>
                    <tr>
                        <td>Player</td><td>Has IPv6</td><td>Established</td>
                    </tr>
                    <For each={Object.keys(cd?.nicks || {})}>
                        {(nick)=>{
                            return <tr>
                                <td>{nick}</td>
                                <td><Show when={gd?.participantNicks?.[nick]?.ipsAndPorts!=null}>
                                    {mainStore.userOnIpv6(nick)?"Yes":"No"}
                                </Show></td>
                                <td title={!isP2p?"Non-Peer2Peer games only need to establish tunnels to host, not between each player":""}>
                                    {
                                        nick.toLowerCase()!=ircClient.nick.toLowerCase()?
                                        (!imHost&&nick.toLowerCase()!=hostNick&&!isP2p)?"NotNeeded":(
                                            (cd.nicks[nick].localIpv6Tunnel?.r==true?"Yes":"No")    
                                        ):"(me)"
                                    }
                                </td>
                            </tr>
                        }}
                    </For>
                </tbody>
            </Table>
        </div>
        }
    })
}