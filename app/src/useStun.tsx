import { Client } from "matrix-org-irc";
import { Button, Spinner, Table } from "solid-bootstrap";
import { createSignal, For, Show } from "solid-js";
import { createMutable } from "solid-js/store";
import { IrcClientExtra, nncmdSentEmitter } from "../../server/src/common/ircExtend";
import { CmdGameRoom_StunSync, CmdGameRoom_StunSync_op, CmdGameRoom_SyncSendAllDetails, CmdGameRoom_SyncSendAllDetails_op, StunClientDetails } from "./commands-gameroom";
import { GameDefsType } from "./GameDefsTypes";
import { StunController } from "./lib/stun";
import { NNCMD_afterOpIndicator } from "./nncmdHandler";
import { DialogMgr } from "./Popups";
import { Settings } from "./Settings";
import { mainStore, nncmdReceivedEmitter } from "./store/store";
import storeActions from "./store/storeActions";

let isInitiatingStun = false; // prevent for duplicate stun initiation
export async function useStun(chan:string, ircClient:Client) {
    if(isInitiatingStun) return;
    isInitiatingStun = true;

    let __stunCtl:StunController|null = null;
    try {
        const lchan = chan.toLowerCase();
        const gameRoom = mainStore.gameForChannel(chan);
        const chanData = mainStore.joinedChannels[lchan];
        if(!gameRoom || !chanData) throw new Error('No game room found for channel '+chan)

        if(chanData.stunCtl) {
            // STUN is already on, don't create a new one.
            throw 'skip';
        }
    
        const imHost = ircClient.nick.toLowerCase() == storeActions.getGameHostNickForChannelImIn(chan)!.toLowerCase();
    
        function destroyAll() {
            nncmdReceivedEmitter.off(CmdGameRoom_StunSync_op, onStunSync);
            ircClient.off('join', onJoin);
            ircClient.off('nick', onNick);
            IrcClientExtra.customEvents.off('connectionStatus', connectionStatus);
            IrcClientExtra.customEvents.off('otherUserNotInChannelAnymore', userNotInChannelAnymore);
            IrcClientExtra.customEvents.off('imNotInChannelAnymore', imNotInChannelAnymore);
            nncmdReceivedEmitter.off(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
            nncmdSentEmitter.off(CmdGameRoom_SyncSendAllDetails_op, onSentFullDetails);
    
            ctl.destroy();
            stopUseStun(chan, ircClient);
        }
    
        const myPort = Settings.getGamePort();
        const {ctl, symmetricNat} = await StunController.create(10000, 20000, myPort, destroyAll);
        __stunCtl = ctl;
        // If not in channel anymore, or host not have stun details anymore, destroy stun
        if(!chanData || (!imHost && !gameRoom?.participantNicks?.[(storeActions.getGameHostNickForChannelImIn(chan)||'').toLowerCase()]?.stun)) {
            ctl.destroy();
            throw 'skip'
        }
    
        console.log(`My Remote STUN Conn: ${ctl.conn.stunAddr.address}:${ctl.conn.stunAddr.port}`); // this is the port you give to your other peer, he adds you as his enpoint with your external IP and this port.
    
        chanData.stunCtl = ctl;
        isInitiatingStun = false;
    
        gameRoom.participantNicks[ircClient.nick.toLowerCase()].stun = {port:ctl.conn.stunAddr.port, addr:ctl.conn.stunAddr.address, symmetricNat};
    
        function destroyEndpointForNick(otherNick:string, removeStunDetails=false, existingNickData?: typeof chanData.nicks[string]) {
            const otherLnick = otherNick.toLowerCase();
            const nickData = existingNickData || chanData.nicks[otherLnick]
            
            if(nickData.stunEndpoint) {
                try {
                    chanData.stunCtl?.removeEndpoint(nickData.stunEndpoint.remoteAddr, nickData.stunEndpoint.remotePort);
                } catch(e) {}
                delete nickData?.stunEndpoint;
            }
            if(removeStunDetails) {
                delete gameRoom?.participantNicks?.[otherLnick]?.stun
            }
        }
    
        const ircChanNicks = new Set(Object.keys(mainStore.joinedChannels[lchan].nicks));
        
        function onJoin(joinedChan:string, nick:string) {
            const ljoinedChan = joinedChan.toLowerCase();
            if(ljoinedChan !== lchan) return;
            ircChanNicks.add(nick.toLowerCase());
        }
        function ircUserGone(nick:string) {
            const gonelNick = nick.toLowerCase();
            if(!ircChanNicks.has(gonelNick)) return;
            ircChanNicks.delete(nick.toLowerCase())
            destroyEndpointForNick(nick, true);
        }
        function userNotInChannelAnymore(nick:string, leftChan?:string) {
            if(leftChan) {
                // on quit/exit, leftChan is undefined
                const leftLchan = leftChan.toLowerCase();
                if(leftLchan != lchan) return;
            }
            ircUserGone(nick);
            if(ircClient.nick.toLowerCase() == nick.toLowerCase()) {
                destroyAll();
            }
        }
    
        function imHostOrPeerToPeer() {
            return imHost || mainStore.execDefForChannel(chan)?.isPeerToPeer;
        }
    
        async function onStunSync(_:any, __:any, channel:string, otherNick:string, cloneCmdObj: CmdGameRoom_StunSync) {        
            if(cloneCmdObj.data.down || cloneCmdObj.data.start ||ctl.isDestroyed) return; // handled in nncmdHandler
            
            const recChannelL = channel.toLowerCase();
            if(recChannelL != lchan) return;
    
            const otherLnick = otherNick.toLowerCase();
            const nickData = chanData.nicks[otherLnick]
            if(!nickData) return;
    
            const gameNickData = gameRoom.participantNicks[otherLnick]
            if(!gameNickData) return;
            const hostLnick = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase()
            if(!hostLnick) return;
    
            const execdef = mainStore.execDefForChannel(chan);
            if(execdef?.isPeerToPeer && !imHost && otherLnick!=hostLnick) return; // if not peerToPeer mode, only create STUN tunnel from host to all users, not one to another.
    
            const newStunData = {port:cloneCmdObj.data.port!, addr:cloneCmdObj.data.addr, symmetricNat:cloneCmdObj.data.symmetricNat} as typeof gameNickData.stun;
            upsertNickStunEndpoint(otherLnick, newStunData!);
        }
    
        async function upsertNickStunEndpoint(nick:string, newStunData:StunClientDetails) {
            const lnick = nick.toLowerCase()
            const gameNickData = gameRoom.participantNicks[lnick]
            if(!gameNickData) return;
            const nickData = chanData.nicks[lnick]
            if(!nickData) return;
    
            if(newStunData && (gameNickData.stun?.addr != newStunData.addr || gameNickData.stun?.port != newStunData.port || gameNickData.stun?.symmetricNat != newStunData.symmetricNat || !nickData.stunEndpoint)) {
                destroyEndpointForNick(lnick, true);
                gameNickData.stun = newStunData;
                const newEndpoint = await ctl.addEndpoint(newStunData.addr, newStunData.port!, imHostOrPeerToPeer()?{addr:mainStore.localhostIp4, port:myPort}:undefined);   
                if(ctl.isDestroyed) {
                    destroyEndpointForNick(lnick, true);
                    return;
                }
                nickData.stunEndpoint = newEndpoint;
            }
        }
        
        let lastExecDef = mainStore.execDefForChannel(chan)!;
        async function roomWillChangeDetails(newExecDef: GameDefsType.Executable) {
            if(mainStore.gameChosenTCPAndNotOptional(gameRoom, gameRoom.params)) {
                destroyAll();
                return;
            }
    
            if(newExecDef == lastExecDef) return;
            lastExecDef = newExecDef;
    
            syncStunEndpoints(newExecDef);
        }
    
        function onRecievedFullDetails (_:any, __:any, channel:string, otherNick:string, cmd: CmdGameRoom_SyncSendAllDetails) {
            const otherLNick = otherNick.toLowerCase();
            const hostNick = storeActions.getGameHostNickForChannelImIn(chan)!.toLowerCase();
            if(otherLNick != hostNick) return;
    
            const newExecDef = Settings.gameDefs?.games?.[cmd?.data?.publicDetails!.gameId]?.executables?.[cmd?.data?.publicDetails!.execId];
            if(newExecDef) roomWillChangeDetails(newExecDef);
        }
        function onSentFullDetails (__:any,_:any, nickOrChan:string, cmd:CmdGameRoom_SyncSendAllDetails) {
            const newExecDef = Settings.gameDefs?.games?.[cmd?.data?.publicDetails!.gameId]?.executables?.[cmd?.data?.publicDetails!.execId];
            if(newExecDef) roomWillChangeDetails(newExecDef);
        }
    
        function onNick(oldNick:string, newNick:string) {
            const oldlNick = oldNick.toLowerCase();
            if(ircChanNicks.has(oldlNick)) {
                ircChanNicks.delete(oldlNick);
                ircChanNicks.add(newNick.toLowerCase());
            }
        }
    
        function imNotInChannelAnymore(channel:string) {
            userNotInChannelAnymore(ircClient.nick, channel);
        }
    
        function connectionStatus(connected:boolean) {
            if(!connected) destroyAll();
        }
    
        nncmdReceivedEmitter.on(CmdGameRoom_StunSync_op, onStunSync);
        ircClient.on('join', onJoin);
        ircClient.on('nick', onNick);
        IrcClientExtra.customEvents.on('connectionStatus', connectionStatus);
        IrcClientExtra.customEvents.on('otherUserNotInChannelAnymore', userNotInChannelAnymore);
        IrcClientExtra.customEvents.on('imNotInChannelAnymore', imNotInChannelAnymore);
        nncmdReceivedEmitter.on(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
        nncmdSentEmitter.on(CmdGameRoom_SyncSendAllDetails_op, onSentFullDetails);
    
        // sync stun endpoints
        async function syncStunEndpoints(execDef:GameDefsType.Executable) {
            if(ctl.isDestroyed) return;
            const isPeerToPeer = execDef.isPeerToPeer == true;
    
            for(const otherNick in chanData.nicks) {
                const lotherNick = otherNick.toLowerCase();
                if(lotherNick == ircClient.nick.toLowerCase()) continue;
    
                const nickData = chanData.nicks[otherNick];
                const gameNickData = gameRoom.participantNicks[lotherNick];
                const otherNickIsHost = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase() == lotherNick;
                
                const shouldEpExist = imHost || isPeerToPeer || otherNickIsHost;
                if(nickData.stunEndpoint && !shouldEpExist) {
                    destroyEndpointForNick(otherNick);
                } else if(!nickData.stunEndpoint && shouldEpExist && gameNickData.stun?.port) {
                    upsertNickStunEndpoint(lotherNick, gameNickData.stun)
                }
            }
        }
        syncStunEndpoints(lastExecDef);
    
        IrcClientExtra.sendNNCmdTo(ircClient, chan, {
            op: CmdGameRoom_StunSync_op,
            data: {
              port: ctl.conn.stunAddr.port,
              addr: ctl.conn.stunAddr.address,
              symmetricNat,
              ...(imHost ? {start:true} : {})
            }
        } as CmdGameRoom_StunSync)
    } catch(e) {
        try {__stunCtl?.destroy()} catch(e) {}
        isInitiatingStun = false;
        if(e!='skip') {
            DialogMgr.addPopup({title:"STUN Error", body:(e as any)?.message, closeButton:true});
        }
    }
}

export function stopUseStun(chan:string, ircClient:Client) {
    const g = mainStore.gameForChannel(chan);
    if(!g) return;

    const imHost = storeActions.imGameHostInChannel(chan);

    if(imHost) {
        IrcClientExtra.sendNNCmdTo(ircClient, chan, {
            op: CmdGameRoom_StunSync_op,
            data: {down:true}
        } as CmdGameRoom_StunSync)
    }
    
    const chanData = mainStore.joinedChannels[chan.toLowerCase()];
    if(chanData.stunCtl) {
        chanData.stunCtl.destroy();
        delete chanData.stunCtl;
    }
    for(const n in chanData.nicks) {
        const nickData = chanData.nicks[n];
        delete nickData.stunEndpoint;
    }
    for(const n in g.participantNicks) {
        const nickData = g.participantNicks[n];
        delete nickData.stun;
    }
}

export function showStunPanel(chan:string, ircClient:Client) {

    const [stunInitiating, set_stunInitiating] = createSignal(false);

    let timer:any;
    DialogMgr.addPopup({
        onClose:()=>{clearInterval(timer);},
        title:"STUN",
        closeButton:true,
        body: ()=>{
            const lchan = chan.toLowerCase();
            const game = mainStore.gameForChannel(chan);
            const imHost = mainStore.imHostInChannel(chan);
            const nickToStunState = createMutable({} as {[nick:string]:{established:boolean, symmetricNat?:boolean, addr:string|null, remotePort:number|null, localPort:number|null, bind:number|null}});

            function refreshData() {
                const isPeer2Peer = mainStore.execDefForChannel(chan)?.isPeerToPeer == true;
                for (const key in nickToStunState) {delete nickToStunState[key];}
                for(const nick in mainStore.joinedChannels[lchan].nicks) {
                    const lnick = nick.toLowerCase();
                    
                    const isMe = lnick === ircClient.nick.toLowerCase();

                    const shouldShow = imHost || isPeer2Peer || lnick == storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase() || isMe;
                    if(!shouldShow) continue;

                    const chanData = mainStore.joinedChannels[lchan];
                    const nickData = chanData.nicks[nick];
                    nickToStunState[nick] = {
                        established: nickData?.stunEndpoint?.otherReceivedMyHandshake || false,
                        symmetricNat: game?.participantNicks?.[nick]?.stun?.symmetricNat,
                        localPort: nickData?.stunEndpoint?.tunnel?.address?.()?.port || null,
                        remotePort: game?.participantNicks?.[nick]?.stun?.port || null,
                        bind: isMe? (chanData.stunCtl?.conn.socket.address().port||null):null,
                        addr: game?.participantNicks?.[nick]?.stun?.addr || null
                    };
                }
            }

            setTimeout(refreshData, 0);
            timer = setInterval(refreshData,1000);

            const symmetricNatExplain = `SymmetricNAT may cause STUN to fail establishing a tunnel in some cases`;

            return <div>
            <h4>{mainStore.isChanUsingStun(chan)?'STUN Enabled':"STUN Disabled"} {imHost?'':"by host"}</h4>
            <Show when={mainStore.isChanUsingStun(chan)}>
                <Table striped bordered hover variant="dark" class="supported-games-table">
                    <tbody>
                        <tr>
                            <td>Player</td><td>Established</td><td title={symmetricNatExplain}>SymmetricNAT</td><td>Ports</td>
                        </tr>
                        <For each={Object.keys(nickToStunState)}>
                            {(nick)=>{
                                return <tr>
                                    <td>{nick}</td>
                                    <td>
                                        <Show when={nick!=ircClient.nick.toLowerCase() && nickToStunState[nick].established!=null}>
                                            <span style={{"color":nickToStunState[nick].established?"green":"yellow"}}>
                                                {nickToStunState[nick].established?"Yes":"No"}
                                            </span>
                                        </Show>
                                        <Show when={nick==ircClient?.nick?.toLowerCase()}>
                                            (me)
                                        </Show>
                                    </td>
                                    <td title={symmetricNatExplain}><Show when={nickToStunState[nick].symmetricNat!=null}>{nickToStunState[nick].symmetricNat?'Yes':'No'}</Show></td>
                                    <td title={nickToStunState[nick]?.addr||''}>
                                        Remote: {nickToStunState[nick].remotePort||''}
                                        <Show when={nick!=ircClient.nick.toLowerCase()}>
                                            <br/>Local: {nickToStunState[nick].localPort}
                                        </Show>
                                        <Show when={nickToStunState[nick].bind!=null}>
                                            <br/>Bind: {nickToStunState[nick].bind}
                                        </Show>
                                    </td>
                                </tr>
                            }}
                        </For>
                    </tbody>
                </Table>
                <p style={{"font-size":'14px'}}>
                Wait a few seconds for the tunnels to establish to other users,<br/>
                and then start the game.<br/><br/>
                You may enable STUN & Relay concurrently, by doing so,<br/>
                the Relay will be used as a fallback for users who could not establish<br/>
                STUN connection to the host
                </p>
                
            </Show>



            <Show when={imHost}>
                <Show when={!mainStore.isChanUsingStun(chan)}>
                    <Show when={!stunInitiating()}>
                        <Button onClick={async()=>{
                            set_stunInitiating(true);
                            try {await useStun(chan,ircClient)} catch(e) {}
                            set_stunInitiating(false);
                        }}>Start IPv4 STUN</Button>
                    </Show>
                    <Show when={stunInitiating()}>
                        <Spinner animation="border" variant="primary" />
                        Initiating Stun...
                    </Show>
                </Show> 
                <Show when={mainStore.isChanUsingStun(chan)}>
                    <Button onClick={()=>stopUseStun(chan,ircClient)}>Stop IPv4 STUN</Button>
                </Show>
            </Show>
        </div>
        }
    })
}