
import { BlobDownloadOptions, BlockBlobUploadStreamOptions } from "@azure/storage-blob";
import { AxiosError } from "axios";
import { IncomingMessage, ServerResponse } from "http";
import { Client } from "matrix-org-irc";
import sleep from "sleep-promise";
import { Table } from "solid-bootstrap";
import { For } from "solid-js";
import { createMutable } from "solid-js/store";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import { CmdGameRoom_FileSync, CmdGameRoom_FileSync_op, CmdGameRoom_SyncSendAllDetails, CmdGameRoom_SyncSendAllDetails_op, FullGameRoomDetails } from "./commands-gameroom";
import { GameDefsType } from "./GameDefsTypes";
import { LaunchArgType, LaunchArgType_File } from "./lib/LaunchArgType";
import { clearObj, humanFileSize } from "./lib/utils";
import { NNCMD_afterOpIndicator } from "./nncmdHandler";
import {axios, AzureStorageBlob, fs, path } from "./NodeLibs";
import { DialogMgr, DialogReturn } from "./Popups";
import { Settings } from "./Settings";
import { mainStore, nncmdReceivedEmitter, offline } from "./store/store";
import storeActions from "./store/storeActions";


export async function setupFileSync(ircClient:Client) {

    const accountName = 'nukemnet';
    const containerName = `gameres`;
    const sasUrl = `https://${accountName}.blob.core.windows.net/${containerName}?sv=2021-12-02&ss=b&srt=sco&sp=rwlactf&se=2060-03-12T22:21:16Z&st=2023-03-12T14:21:16Z&sip=0.0.0.0-255.255.255.255&spr=https,http&sig=Q80agARyU6NaRdsc4p1RH3%2Fpw0cNcLfYbDgz1adCawY%3D`;
    const azureClient = new AzureStorageBlob.ContainerClient(sasUrl);


    function getBlobNamePath(fileName:string, md5:string) {
        const basename = path.basename(fileName);
        return `${md5}/${basename}`;
    }

    async function resUrl(fileName:string, md5:string, chan:string) {
        const blobNamePath = getBlobNamePath(fileName, md5);
        if(offline) {
            const host = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase();
            const chanData = mainStore.joinedChannels?.[chan];

            let httpPort:number|undefined, ip:string|undefined
            let tries = 6;
            do {
                httpPort = chanData?.game?.participantNicks?.[host||'']?.ipsAndPorts?.httpPort;
                ip = chanData.nicks[host||'']?.lan?.detectedLanIp
                if(!httpPort || !ip?.length) {await sleep(500);}
                tries--;
            } while(tries>=0 && (!httpPort || !ip?.length));
            
            return `http://${ip}:${httpPort}/filesync/${blobNamePath}`;
        } else {
            return `https://nukemnet.blob.core.windows.net/${containerName}/${blobNamePath}`;
        }
    }

    async function uploadFile(blobName:string, filePath:string, options?:BlockBlobUploadStreamOptions) {
        const bc = azureClient.getBlockBlobClient(blobName);
        // Not using stream, because abort signal fails to do anything when using stream for some reason.
        // const bufferSize = 4 * 1024 * 1024;
        // const maxConcurrency = 20;
        // const myTransform = new stream.Transform({
        //     transform(chunk, encoding, callback) {
        //         callback(null, chunk);
        //     },
        //     decodeStrings: false
        //   });
        // const transformedReadableStream = fsSync.createReadStream(filePath)//.pipe(myTransform);
        // return await bc.uploadStream(transformedReadableStream, bufferSize, maxConcurrency, options);
        const buf = await fs.readFile(filePath);
        return await bc.uploadData(buf, options);
    }

    async function downloadFile(blobName:string, filePath:string, options?:BlobDownloadOptions) {
        const bc = azureClient.getBlockBlobClient(blobName);
        const result = await bc.downloadToFile(filePath,undefined,undefined, options);
        const body = result.blobBody;
        if(body) {
            const resolvedBody = await body;
            const buff = await resolvedBody.arrayBuffer();
            fs.writeFile(filePath, Buffer.from(buff));
        }
    }

    async function isFileUpAndReady(fileName:string, md5:string, size:number, chan:string) {
        try {
            const url = await resUrl(fileName, md5, chan);
            const encodedUrl = encodeURI(url);

            const res = await axios.head(encodedUrl);

            // Make sure filesize on Azure is same as local file.
            const remoteSize = parseInt(res?.headers?.["content-length"]);
            if(remoteSize != null && !Number.isNaN(remoteSize) && remoteSize != size) {
                return false;
            }

            // Make sure md5 is correct.
            const azureMd5 = res?.headers?.["content-md5"];
            if(typeof azureMd5 === 'string' && azureMd5?.length) {
                const md5HashHex = Buffer.from(azureMd5, 'base64').toString('hex').toLowerCase();
                if(md5HashHex != md5.toLowerCase()) {
                    return false;
                }
            }
            return true;
        } catch(e) {
            if((e as AxiosError)?.response?.status == 404) {
                return false;
            }
            throw e;
        }
    }

    let destroyFunc:null|(()=>void) = null;

    function doDestroy() {
        if(destroyFunc) {destroyFunc(); destroyFunc = null;};
    }

    function init(chan:string) {

        doDestroy();
        destroyFunc = function doDestroy() {
            clearObj(syncStatus)
            nncmdReceivedEmitter.off(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
            nncmdReceivedEmitter.off(CmdGameRoom_FileSync_op, onFileSyncFromUser);
            IrcClientExtra.customEvents.off('changedGameroomDetails', onSendingNewDetailsToClient);
            IrcClientExtra.customEvents.off('fileSyncClicked', onFileSyncClicked);
            mainStore.httpServer?.off('request', onHttpRequestReceived);
            syncDialog?.close();
            syncDialog = null;
            preparingSyncDataDialog?.close();
            preparingSyncDataDialog = null;
        };

        let gameRoom = mainStore.gameForChannel(chan);
        const execDef = mainStore.execDefForChannel(chan);
        const chanData = mainStore.joinedChannels[chan];
        const imHost = mainStore.imHostInChannel(chan);
        if(!gameRoom || !execDef || !chanData) {return console.error('fileshare setup failed, no game or execdef');};

        type FileSyncState = {
            fileName:string, // can be path too, if it's the room host
            res:string,
            gameId:string,
            gameExecId:string,
            md5:string
            size:number
            state:SyncState,
            progress?:number, // if exists, it means percent loaded, if undefined, means halted.
            error?:string,
        }

        const syncStatus = createMutable({}) as {[file_name_md5:string]: FileSyncState};
        
        function onFileSyncFromUser(_:any, __:any, channel:string, otherNick:string, cmd: CmdGameRoom_FileSync) {
            if(channel != chan) {return;}
            const otherLnick = otherNick.toLowerCase();
            const {progress} = cmd.data;

            if(progress != null) {
                gameRoom.participantNicks[otherLnick].fileSyncProgress = progress;
            } else {
                delete gameRoom.participantNicks[otherLnick].fileSyncProgress;
            }
        }

        function setOverallProgress(progress?:number|null, doneFile?:string|'all') {
            if(progress == null) {
                delete gameRoom.participantNicks[ircClient.nick.toLowerCase()].fileSyncProgress;
            } else {
                gameRoom.participantNicks[ircClient.nick.toLowerCase()].fileSyncProgress = progress;
            }
            IrcClientExtra.sendNNCmdTo(ircClient, chan, {
                op: CmdGameRoom_FileSync_op,
                data: {
                    ...(progress!=null?{progress}:{}),
                    ...(doneFile?{doneFile}:{})
                }
            } as CmdGameRoom_FileSync)
        }

        let syncDialog:DialogReturn|null;

        type SyncState = 'upload'|'download'|'wait_host_upload'|'error';

        let lastHaveStuffToSync = false;
        function assignFileSyncState(key:string, state?:FileSyncState|undefined) {
            if(state == null) {
                delete syncStatus[key];
            } else {
                syncStatus[key] = state;
            }

            const haveStuffToSync = Object.keys(syncStatus).length>0;
            if(haveStuffToSync != lastHaveStuffToSync) {
                lastHaveStuffToSync = haveStuffToSync;;
                
                const myGameData = gameRoom.participantNicks[ircClient.nick.toLowerCase()];
                if(haveStuffToSync) {
                    if(myGameData.fileSyncProgress == null) {
                        setOverallProgress(0)
                    }
                } else {
                    if(myGameData.fileSyncProgress != null) {
                        setOverallProgress();
                    }
                }
            }
        }

        let isDoingSync = false;
        async function doSync() {
            if(syncDialog || !Object.keys(syncStatus).length) {return;};
            const abortControllers = [] as AbortController[];
            function abortUploadOrDownload(abortController: AbortController) {
                if(!abortController?.signal?.aborted) {
                    try {abortController.abort();} catch(e) {}
                    try {abortControllers.splice(abortControllers.indexOf(abortController), 1);} catch(e) {}
                }
            }
            syncDialog = DialogMgr.addWindow({
                title:imHost?"Uploading...":"Downloading...",
                closeOnEscape:false,
                width:'80%',
                
                body:()=>{
                    async function fileLoad(onlyLoadKey?:string) {
                        if(isDoingSync) return;
                        isDoingSync = true;
                        try {
                            const keysInSyncStatus = Object.keys(syncStatus);
                            const totalSize = Object.keys(syncStatus).map(k=>syncStatus[k].size).reduce((a,b)=>a+b, 0);
                            let loadedSize = 0;
                            if(totalSize>0) {setOverallProgress(0);}
                            for(const key of keysInSyncStatus) {
                                const file = syncStatus?.[key];
                                if(file==null || file?.progress != null || (onlyLoadKey?.length && onlyLoadKey!=key)) continue; // if already in progress, skip.
                                
                                const filePath = await Settings.findResFile(file.gameId, file.gameExecId, file.res, file.fileName, file.md5);
                                const blobName = getBlobNamePath(file.fileName, file.md5);
                                const abortCtl = new AbortController();
                                abortControllers.push(abortCtl);
                                
                                delete syncStatus?.[key]?.error // if previously there was an error, delete it.

                                function calculateOverallPercent(loaded?:number) {
                                    const overallPercent = parseInt(Math.round(((loaded||0)+loadedSize)/totalSize*100) as any);
                                    return overallPercent;
                                }

                                let lastProgressReported = -Infinity;
                                function onProgress(progress:{loadedBytes:number}) {
                                    const percent = parseInt(Math.round(progress.loadedBytes / file.size * 100) as any);
                                    if(currentSyncDialogRef == syncDialog && syncStatus?.[key] && syncStatus?.[key].progress != null) {
                                        syncStatus[key].progress = percent;
                                        if(Date.now()>lastProgressReported+5000) {
                                            lastProgressReported = Date.now();
                                            setOverallProgress(calculateOverallPercent(progress.loadedBytes));
                                        }
                                    } else {
                                        abortUploadOrDownload(abortCtl);
                                    }
                                }
    
                                if(typeof filePath === 'string' && imHost && syncStatus?.[key] && file?.progress == null && file.state == 'upload') {
                                    try {
                                        syncStatus[key].progress = 0;
                                        await uploadFile(blobName, filePath, {abortSignal:abortCtl.signal, onProgress});
                                        loadedSize+=file.size;
                                        setOverallProgress(calculateOverallPercent(), key);
                                        assignFileSyncState(key, undefined);
                                    } catch(e) {
                                        if(syncStatus[key]) {
                                            syncStatus[key].error = (e as any)?.message || 'error';
                                            delete syncStatus[key].progress;
                                        }
                                    }
                                } else if (!imHost && syncStatus?.[key]) {
                                    if(typeof filePath == 'string') { // if file exists, remove it from sync (TESTING DOWNLOAD: add && false)
                                        assignFileSyncState(key, undefined);
                                    } else {
                                        const downloadPath = await Settings.preferredDownloadFilePathForGameResource(file.gameId, file.gameExecId, file.res, file.fileName, file.md5);
                                        if(syncStatus?.[key] && file?.progress == null && file.state == 'download') {
                                            try {
                                                syncStatus[key].progress = 0;
                                                if(offline) {
                                                    const url = await resUrl(file.fileName, file.md5, chan);
                                                    const res = await axios({url, method: 'GET', responseType: 'arraybuffer', signal:abortCtl.signal, onDownloadProgress: (progress)=>{
                                                        onProgress({loadedBytes:progress.loaded});
                                                    }});
                                                    const buff = await res.data as Buffer
                                                    await fs.writeFile(downloadPath, buff);
                                                } else {
                                                    await downloadFile(blobName, downloadPath, {abortSignal:abortCtl.signal, onProgress});
                                                }
                                                loadedSize+=file.size;
                                                setOverallProgress(calculateOverallPercent());
                                                assignFileSyncState(key, undefined);
                                            } catch(e) {
                                                if(syncStatus[key]) {
                                                    syncStatus[key].error = (e as any)?.message || 'error';
                                                    delete syncStatus[key].progress;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch(e) {}
                        if(!Object.keys(syncStatus).length) {
                            syncDialog?.close();
                            setOverallProgress()
                        }
                        isDoingSync = false;
                    };
                    
                    function getStatSummary() {
                        return Object.keys(syncStatus).map(stat=>`${stat}_${syncStatus[stat].state}`).join(',');
                    }

                    (async function doLogic(){
                        let newDataFromHost = false;
                        function newFileSyncFromUser(_:any, __:any, channel:string, otherNick:string, cmd: CmdGameRoom_FileSync) {
                            const otherLnick = otherNick.toLowerCase();
                            const otherIsHost = otherLnick === storeActions.getGameHostNickForChannelImIn(channel)?.toLowerCase()
                            const {progress, doneFile} = cmd.data;
                            if(otherIsHost && (progress == null || doneFile?.length)) {
                                for(const key of Object.keys(syncStatus)) {
                                    if(progress == null || doneFile === 'all' || doneFile === key) {
                                        syncStatus[key].state = 'download';
                                    }
                                }
                                newDataFromHost = true;
                            }
                        }
                        nncmdReceivedEmitter.on(CmdGameRoom_FileSync_op, newFileSyncFromUser);
                        
                        syncloop: do {
                            let prevSyncSummary:string;
                            let newSyncSummary:string;
                            do {
                                prevSyncSummary = getStatSummary();
                                try {await fileLoad();} catch(e) {}
                                newSyncSummary = getStatSummary();
                            } while(syncDialog!=null && currentSyncDialogRef == syncDialog && newSyncSummary!=prevSyncSummary);

                            while(syncDialog==currentSyncDialogRef && syncDialog!=null) {
                                await sleep(1000);
                                if(newDataFromHost) {
                                    newDataFromHost = false;
                                    continue syncloop;
                                }
                            }
                        } while(0);

                        nncmdReceivedEmitter.off(CmdGameRoom_FileSync_op, newFileSyncFromUser);
                    })();
                    
                    return <div>
                        <Table striped bordered hover variant="dark" class="supported-games-table">
                            <tbody>
                                <tr>
                                    <td>Res</td><td>File</td><td>Status</td><td>Progress</td>
                                </tr>
                                <For each={Object.keys(syncStatus)}>
                                    {(key)=>{
                                        const file = syncStatus[key];
                                        return <tr title={`MD5: ${file.md5} Size: ${humanFileSize(file.size)}`}>
                                            <td>{file?.res}</td>
                                            <td>{file.fileName}</td>
                                            <td>{file.state}{file?.error?.length?<><br/>{file.error}</>:""}</td>
                                            <td>{typeof file?.progress === 'number'? (file.progress+'%'):<>waiting</>}</td>
                                        </tr>
                                    }}
                                </For>
                            </tbody>
                        </Table>
                    </div>
                },
                close:()=>{
                    let syncFilesRemain = false;
                    for(const key in syncStatus) {
                        syncFilesRemain = true;
                        delete syncStatus[key].progress; // tell uploads to stop.
                    }
                    if(syncFilesRemain) {setOverallProgress(0);}
                    syncDialog = null;
                    for(const abortCtl of abortControllers) {abortUploadOrDownload(abortCtl);}
                }
            });
            const currentSyncDialogRef = syncDialog;
        }

        let preparingSyncDataDialog:DialogReturn|null = null
        async function prepareSyncData(forced:boolean, fullDetails:FullGameRoomDetails = mainStore.gameForChannel(chan)) {
            if(imHost && offline) return; // on offline mode, host serves files as http server, so no need to upload to azure
            if(syncDialog && !forced) return;
            if(preparingSyncDataDialog) return;
            preparingSyncDataDialog = DialogMgr.addPopup({
                title: 'Preparing FileSync Data',
                body: imHost?"Checking which files need to be uploaded...":"Checking which files need to be downloaded...",
                persistent:true
            });

            try {
                const execDef = Settings?.gameDefs?.games?.[fullDetails?.publicDetails?.gameId]?.executables?.[fullDetails?.publicDetails?.execId];
                if(!execDef) {
                    return console.error(`filesync: Exec Definition not found for channel ${chan}`)
                }
    
                const filesFound = new Set<string>();
                for(const _arg of fullDetails.params) {
                    const paramDef = execDef?.parameters?.[_arg.id]
                    if(!Settings.filterParam_For(imHost, paramDef?.for)) continue;
                    
                    if(paramDef?.type === 'file' && paramDef?.res?.length) {
    
                        const arg = _arg as LaunchArgType_File;
    
                        const key = `${arg.meta.name}_${arg.meta.md5}`;
                        const res = paramDef.res;
    
                        const resLocalFileResult = await Settings.findResFile(fullDetails.publicDetails.gameId, fullDetails.publicDetails.execId, paramDef.res,
                            lastFileParams[`${arg.meta.name}_${arg.meta.md5}`].fileName,
                            arg.meta.md5);
                        
                        const resFileFoundLocallyAndHashMatched = typeof resLocalFileResult === 'string';
    
                        function markFileForSyncing(state:SyncState, error?:string) {
                            if(syncStatus?.[key] != null) {
                                if(syncStatus?.[key]?.state == state && syncStatus?.[key]?.error == error) {
                                    // Do nothing
                                    filesFound.add(key);
                                    return;
                                }
                            }
                            assignFileSyncState(key, {fileName:lastFileParams[`${arg.meta.name}_${arg.meta.md5}`].fileName, size:arg.meta.size, md5:arg.meta.md5, res, gameId:fullDetails.publicDetails.gameId, gameExecId:fullDetails.publicDetails.execId, state, error});
                            filesFound.add(key);
                        }
    
                        async function isFileReadyShortcut() {
                            if(offline || Settings?.read()?.lanOnlyMode) return true; // On offline LAN mode, no need to send head request to check for file existence.
                            return await isFileUpAndReady(arg.meta.name, arg.meta.md5, arg.meta.size, chan);
                        }
    
                        try {
                            if(imHost) {
                                if(resFileFoundLocallyAndHashMatched) {
                                    if(!await isFileReadyShortcut()) {
                                        markFileForSyncing('upload'); 
                                    }
                                } else {
                                    markFileForSyncing('error', resLocalFileResult.message);
                                }
                            } else {
                                if(!resFileFoundLocallyAndHashMatched) { // (TESTING DOWNLOAD: add || true)
                                    if(await isFileReadyShortcut()) {
                                        markFileForSyncing('download');
                                    } else {
                                        markFileForSyncing('wait_host_upload');
                                    }
                                }
                            }
                        } catch(e) {
                            markFileForSyncing('error', "Request to azure failed: "+(e as any)?.message);
                        }
                    }
                }
    
                // remove irrelevant files
                for(const k in syncStatus) {
                    if(!filesFound.has(k)) {
                        assignFileSyncState(k, undefined);
                    }
                }
    
                if(!Object.keys(syncStatus).length) {
                    setOverallProgress()
                }
                
                doSync();
            } catch(e) {}
            preparingSyncDataDialog?.close();
            preparingSyncDataDialog = null;
        }

        function calculateRelevantFiles(execDef:GameDefsType.Executable, args:LaunchArgType[]) {
            let currentRelevantFiles = {} as {[key:string]:{fileName:string, res:string}};
            for(const p of args) {
                const paramDef = execDef.parameters?.[p.id];
                if(!paramDef || paramDef.type != 'file' || !Settings.filterParam_For(imHost, paramDef.for) || !paramDef.res?.length) continue;
                currentRelevantFiles[`${p.meta.name}_${p.meta.md5}`] = {
                    fileName: imHost && (p.html?.value as string).length?(p.html!.value as string):p.meta.name,
                    res: paramDef.res
                };
            }
            return currentRelevantFiles;
        }
        let lastFileParams = calculateRelevantFiles(mainStore.execDefForChannel(chan)!, mainStore.gameForChannel(chan).params);

        function onChanEditGameOrParams(gameDetails:FullGameRoomDetails) {
            const newExecDef = Settings.gameDefs?.games?.[gameDetails?.publicDetails!.gameId]?.executables?.[gameDetails?.publicDetails!.execId];
            if(newExecDef) {
                const newRelevantFiles = calculateRelevantFiles(newExecDef, gameDetails.params);
                // Only if sync files were changed, we re-prepare the sync data.
                if(JSON.stringify(Object.keys(newRelevantFiles).sort()) != JSON.stringify(Object.keys(lastFileParams).sort())) {
                    lastFileParams = newRelevantFiles
                    prepareSyncData(true, gameDetails);
                }
            }
        }

        function onRecievedFullDetails (_:any, __:any, channel:string, otherNick:string, cmd: CmdGameRoom_SyncSendAllDetails) {
            const otherLNick = otherNick.toLowerCase();
            const hostNick = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase();
            if(otherLNick != hostNick) return;
    
            onChanEditGameOrParams(cmd.data);
        }
        function onSendingNewDetailsToClient(channel:string, gameDetails:FullGameRoomDetails) {
            onChanEditGameOrParams(gameDetails);
        }

        function onFileSyncClicked() {
            prepareSyncData(false);
        }

        IrcClientExtra.customEvents.on('changedGameroomDetails', onSendingNewDetailsToClient);
        IrcClientExtra.customEvents.on('fileSyncClicked', onFileSyncClicked);
        nncmdReceivedEmitter.on(CmdGameRoom_SyncSendAllDetails_op+NNCMD_afterOpIndicator, onRecievedFullDetails);
        nncmdReceivedEmitter.on(CmdGameRoom_FileSync_op, onFileSyncFromUser);

        prepareSyncData(true, gameRoom);

        // on offline lan mode, sync happens by direct http communication to room host
        async function onHttpRequestReceived(req:IncomingMessage, res:ServerResponse<IncomingMessage>) {
            const parts = req.url?.split('/');
            if(parts?.[1] != 'filesync') return;
            if(!parts || parts.length < 4) return res.end();

            try {
                const md5 = parts[2];
                const name = parts[3];
                const httpMethod = req.method?.toLowerCase()
                const foundFile = lastFileParams[`${name}_${md5}`];
                const chanDetails = mainStore.gameForChannel(chan)?.publicDetails;
                const foundFilePath = await Settings.findResFile(chanDetails?.gameId, chanDetails?.execId, foundFile?.res, foundFile?.fileName, md5);
                if(typeof foundFilePath != 'string') {throw 'not found'}
                if(httpMethod==='head') {            
                    res.writeHead(200);
                    res.end();
                } else if(httpMethod==='get') {
                    const fileName = path.basename(foundFilePath);
                    res.setHeader('Content-disposition', 'attachment; filename='+fileName);
                    const fileBuffer = await fs.readFile(foundFilePath);
                    res.writeHead(200);
                    res.end(fileBuffer);
                } else {throw 'not found';}
            } catch(e) {
                res.writeHead(404);
                res.end();
            }
        }
        if(imHost && offline) {
            mainStore.httpServer?.on('request', onHttpRequestReceived);
        }
    }

    IrcClientExtra.customEvents.on('ingameroom', (chanOrNull:string|null)=>{
        if(chanOrNull != null) {init(chanOrNull);}
        else {doDestroy()}
    });
}
