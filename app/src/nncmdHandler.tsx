import { Client } from "matrix-org-irc";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import * as NNCmd from '../../server/src/common/commands'
import * as NNCmd_Meta from '../../server/src/common/commands-meta'
import { CmdMeta_AllOpTypes } from "../../server/src/common/commands-meta";
import { BotMetaChannel_lowercase, BotName_lowercase, IrcUserModSigns_High } from "../../server/src/common/constants";
import { mainStore, nncmdReceivedEmitter, serverHost, serverHttpPort } from "./store/store";
import { CmdGameRoom_AllOps, CmdGameRoom_AllOpTypes, CmdGameRoom_AllTypes, CmdGameRoom_IsExecRunning, CmdGameRoom_IsExecMissing, CmdGameRoom_ParticipantAdded, CmdGameRoom_SyncSendAllDetails, CmdGameRoom_ToggleDedicatedServer, FullGameRoomDetails, CmdGameRoom_PongRelays, CmdGameRoom_GameMessage, CmdGameRoom_StunSync, CmdGameRoom_RequestGameDetails, CmdGameRoom_ResSendAllDetails, CmdGameRoom_ResSendAllDetails_op } from "./commands-gameroom";
import storeActions from "./store/storeActions";
import { fullLanDetectionLogic } from "./lib/LanDetection";
import { clientLaunchGame } from "./launchGameAsClient";
import { NNPB } from "../../server/src/common/protobuf/export";
import { packAndSendIrcCmd } from "../../server/src/common/protobuf/ProtoBufMgr";

import { IRCEventCustom_GameDetails, IRCEvent_ProcessTerminatedByHost } from "./irc-events/ircEvents";
import { Settings } from "./Settings";
import { DialogMgr } from "./Popups";
import { axios } from "./NodeLibs";
import { RelayInfo } from "./lib/RelayInfo";
import { pingAllRelays } from "./lib/findOptimalRelay";
import { KillProc } from "./lib/KillProc";
import { stopUseStun, useStun } from "./useStun";
import sleep from "sleep-promise";
import {compare, applyPatch} from 'fast-json-patch'
import { delProc } from "./terminateProc";

// NNPB=NukemNetProtocolBuffers. NukemNet commands using Google Protocol Buffers (most commands don't use it yet).
const nnpbCommandsGameRoom = new Map<string, (channel:string|null, nick:string, cmd:any, ircClient:Client, message:any)=>any>();


type NNCMD_FuncHandlerType = (channel:string|null, nick:string, cmd:CmdGameRoom_AllTypes, ircClient:Client, message:any)=>any;
const commandsGameRoom = new Map<CmdGameRoom_AllOpTypes,NNCMD_FuncHandlerType>();

commandsGameRoom.set('sync_participantAdded', async (chan, nick, _c, ircClient)=>{
    const syncCmd = _c as CmdGameRoom_ParticipantAdded;
    if(!chan?.length) return;

    const lchan = chan.toLowerCase();
    const lnick = nick.toLowerCase();

    const hostNick = storeActions.getGameHostNickForChannelImIn(chan);
    const lhostNick = hostNick?.toLowerCase();
    if(lhostNick != lnick) return;

    const g = mainStore.gameForChannel(chan);

    // Do stun if needed
    const hostStunDetails = g.participantNicks[lhostNick].stun;
    if(hostStunDetails) {useStun(chan, ircClient);}

    const newParticipantLnick = syncCmd.data.nick.toLowerCase();

    const myLnick = ircClient.nick.toLowerCase();
    const newParticipantIsMe = newParticipantLnick === myLnick;
    if(newParticipantIsMe) {
        mainStore.refreshIsGameExecMissing_ReturnWhetherUpdateFound(g.publicDetails.gameId, g.publicDetails.execId, ircClient, lchan);

        // run in parallel
        (async ()=>{
            try {await fullLanDetectionLogic(lchan)} catch(e) {}

            // If game exec allows mid-game join, and game is started by host, and I'm not already in-game, ask me if I want to join.
            // If he does, launch game as client.
            const execDef = mainStore.execDefForChannel(lchan);
            if(execDef?.midGameJoin && g?.participantNicks?.[lhostNick]?.isExecRunning && !g?.participantNicks?.[myLnick]?.isExecRunning) {
                
                // Wait 2 seconds for tunnels to establish before offering to join game
                await sleep(2000);
                DialogMgr.addPopup({
                    title: 'Join game?',
                    body: ()=><>Game is already started. Do you want to join?</>,
                    buttons: [{body:'Yes', onClick:()=>{
                        clientLaunchGame(lchan, lhostNick, ircClient);
                    }}, {
                        body:'No'
                    }]
                });
            }

        })();
        IrcClientExtra.customEvents.emit('ingameroom', lchan);
    } else {
        g.participantNicks[syncCmd.data.nick] = {isExecRunning:false, isExecMissing:false, ipsAndPorts: syncCmd.data.ipsAndPorts};
        fullLanDetectionLogic(lchan, syncCmd.data.nick);
    }
    IrcClientExtra.customEvents.emit('gameRoom_participantAdded', lchan, newParticipantLnick);
    mainStore.resolveCountryCodeForIpForNick(syncCmd.data.nick, chan);
})

commandsGameRoom.set('terminateProc', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;
    const lchan = chan.toLowerCase();
    const lhostNick = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase();
    if(lhostNick != nick.toLowerCase()) return;

    const chanData = mainStore.joinedChannels[lchan];
    delProc(chanData.proc!);

    mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {
        type: 'custom_processTerminatedByHost',
        ts: Date.now(),
        channel:lchan
    } as IRCEvent_ProcessTerminatedByHost);

    Settings.playSoundEffect('terminated');
})

commandsGameRoom.set('requestGameDetails', async (chan, nick, _c, ircClient)=>{
    const cmd = _c as CmdGameRoom_RequestGameDetails;
    if(chan?.length) return;
    if(!cmd?.data?.channel?.length) return;

    const imHost = storeActions.imGameHostInChannel(cmd.data.channel);
    if(!imHost) return;

    const g = mainStore.gameForChannel(cmd.data.channel);
    if(!g) return;

    return {
        op: CmdGameRoom_ResSendAllDetails_op,
        data: g
    } as CmdGameRoom_ResSendAllDetails;
});

commandsGameRoom.set('sync_sendAllDetails', async (chan, nick, _c, ircClient)=>{
    // Receiving room details from host, probably immediately after this user joined a channel that is a game room

    const syncCmd = _c as CmdGameRoom_SyncSendAllDetails;

    const gameLchannel = syncCmd?.data?.publicDetails?.channel?.toLowerCase();
    if(!gameLchannel.length) {
        return // channel is missing, should not happen
    }

    const isMessageFromChannel = !!chan?.length // whether message came in channel (not private message from host)

    if(isMessageFromChannel && gameLchannel != chan.toLowerCase()) {
        return // channels are not related, ignore message. should not happen unless someone hacks around
    }

    if(!mainStore?.joinedChannels?.[gameLchannel]) {
        return; // We are not in that channel, ignore message
    }

    const lnick = nick.toLowerCase();
    if(!IrcUserModSigns_High.has(mainStore.joinedChannels[gameLchannel].nicks[lnick].sign)) {
        return // Ignore message, came from a user without high permission on that channel, so he can't be the host of the room.
    }

    if(mainStore.imAlreadyInAGameRoom(gameLchannel)) {
        DialogMgr.addPopup({
            icon:"error",
            title:"Already in game",
            body:`You are already in another game room`,
            buttons:[{body:"ok"}]
        });
        ircClient.send('PART', gameLchannel, 'Already in another game room');
        return;
    }
    
    // If game/exec not defined in client, leave room
    const gameDef = Settings?.gameDefs?.games?.[syncCmd?.data?.publicDetails?.gameId]?.executables?.[syncCmd?.data?.publicDetails?.execId];
    if(!gameDef) {
        DialogMgr.addPopup({
            icon:"error",
            title:"Missing game/exec definition",
            body:()=><>Game/Exec definition missing:<br/>{syncCmd?.data?.publicDetails?.gameName}/{syncCmd?.data?.publicDetails?.execName}<br/>({syncCmd?.data?.publicDetails?.gameId}/{syncCmd?.data?.publicDetails?.execId})<br/>
            Either you and game room owner have different NukemNet versions, or some game defs were manually editted.
            </>,
            buttons:[{body:"ok"}]
        });
        ircClient.part(gameLchannel,()=>{});
        return;
    }

    let playersNumWithoutMe=0;
    if(syncCmd?.data?.participantNicks?.length) {
        for(const n in syncCmd.data.participantNicks) {
            const otherlNick = n.toLowerCase();
            const isMe = otherlNick === ircClient.nick.toLowerCase();
            if(!isMe) playersNumWithoutMe++;
        }
    }

    if(playersNumWithoutMe >= syncCmd.data.publicDetails.maxPlayers) {
        // No room left, leave channel.
        await ircClient.part(gameLchannel, ()=>{
            DialogMgr.addPopup({
                title: 'Error',
                body: 'Game room is full',
                icon: 'error',
                buttons:[{body:"ok"}]
            })
        });
        return
    }


    if(mainStore?.joinedChannels?.[gameLchannel]?.game == null) {
        mainStore.joinedChannels[gameLchannel].game = syncCmd.data;
    } else {
        const diff = compare(mainStore.joinedChannels[gameLchannel].game!, syncCmd.data);
        applyPatch(mainStore.joinedChannels[gameLchannel].game!, diff);
    }

    // The below logic is problematic, had problems with InPersuitOfGreed
    // It might delete host sent params
    // (async function loadSavedParams() {
    //     try {
    //         const d = syncCmd.data.publicDetails;
    //         const launchDefaults = await Settings.launchDefaults();
    //         const savedParams = launchDefaults.games[d.gameId].executables![d.execId].params!;
    //         const params: LaunchArgType[] = [];
    //         for(const paramId in savedParams) {
    //             const def = savedParams[paramId];
    //             if(def.for && GameDefsType.ParamType.ParamFor_Unshared.includes(def.for)) {
    //                 const origParamDef = Settings.gameDefs.games[d.gameId].executables[d.execId].parameters?.[paramId];
    //                 const newDef = deepClone(def) as LaunchDefaults_NormalArg&LaunchDefaults_ManualArg&typeof origParamDef;
    //                 if(origParamDef && !paramId.startsWith("_m_")) {
    //                     (newDef as unknown as typeof origParamDef).label = origParamDef.label
    //                 }
                    
    //                 params.push({
    //                     id: paramId,
    //                     args: newDef.args,
    //                     for: newDef.for,
    //                     valueLabel: newDef.valueLabel,
    //                     order: newDef.order,
    //                     label: newDef.label
    //                 })
    //             }
    //         }
    //         Object.assign(mainStore.joinedChannels[gameLchannel].game!.params, params);
    //     } catch(e) {}
    // })();

    // If private message cmd (happens while user joined a room, the hosts sends him a private msg cmd)
    if(!isMessageFromChannel) {
        // Add myself to participants
        mainStore.joinedChannels[gameLchannel].game!.participantNicks[mainStore.myNick.toLowerCase()] = {isExecRunning:false, isExecMissing:false, ipsAndPorts: mainStore.myIpsAndPorts()}
        // If it's private message from host, ask to participate
        await packAndSendIrcCmd(ircClient, lnick, {
            data: {
                $case: 'cmdAskToParticipate',
                cmdAskToParticipate: {channel:gameLchannel, ipsAndPorts: mainStore.myIpsAndPorts()}
            }
          } as NNPB.RootCommand);
    } else {
        mainStore.addEventTo('channel', gameLchannel, mainStore.joinedChannels[gameLchannel], {type:'custom_gameDetails', ts:Date.now()} as IRCEventCustom_GameDetails);
        mainStore.refreshIsGameExecMissing_ReturnWhetherUpdateFound(syncCmd.data.publicDetails.gameId, syncCmd.data.publicDetails.execId, ircClient, gameLchannel);
    }

    mainStore.resolveCountryCodeForIpForNick(undefined, gameLchannel);
})

nnpbCommandsGameRoom.set('cmdSyncSendAllDetails', async (chan, nick, cmd: NNPB.CmdGameRoomSyncSendAllDetails, ircClient, message)=>{
    const oldOpFunc = commandsGameRoom.get('sync_sendAllDetails');
    return await oldOpFunc!.apply(undefined, [chan, nick, {op:'sync_sendAllDetails', data: cmd as FullGameRoomDetails}, ircClient, message]);
})

nnpbCommandsGameRoom.set('cmdPingToHost', async (chan, nick, cmd: NNPB.CmdGameRoomPingToHost, ircClient, message)=>{

    if(cmd?.ping == null) return;

    const lchan = chan?.toLowerCase();
    if(!lchan?.length) return

    const lnick = nick?.toLowerCase()
    if(!lnick?.length) return

    const nickChanData = mainStore?.joinedChannels?.[lchan]?.nicks?.[lnick];
    if(!nickChanData) return;

    nickChanData.pingToHost = cmd.ping;
})

//

nnpbCommandsGameRoom.set('cmdAskToParticipate', async (chan, nick, cmd: NNPB.CmdGameRoomAskToParticipate, ircClient, message)=>{
    if(!cmd.channel) return // missing channel

    const lchan = cmd.channel.toLowerCase();
    const lnick = nick.toLowerCase();

    if(storeActions.imGameHostInChannel(lchan)) {
        // Make sure room is left
        const gameData = mainStore.joinedChannels[lchan].game!;
        const participantsWithoutUser = Object.keys(gameData.participantNicks).filter(p=>p.toLowerCase() != lnick);
        const participantsCountWithoutUser = participantsWithoutUser.length;
        if(participantsCountWithoutUser >= gameData.publicDetails.maxPlayers) {
            // Kick user, no room left.
            try {
                await IrcClientExtra.kickNick(ircClient, lchan, lnick, 'No room left');
            } catch(e){}
            return
        }

        // Add user to participants, if he's not already there
        if(!(lnick in gameData.participantNicks)) {
            gameData.participantNicks[lnick] = {isExecRunning:false, isExecMissing:false, ipsAndPorts: cmd.ipsAndPorts!};

            gameData.publicDetails.playersIn = Object.keys(gameData.participantNicks).length;

            IrcClientExtra.sendNNCmdToBot(ircClient, {
                op:'requestUpdateRoom',
                data: {
                    channel: lchan,
                    patch: {
                        playersIn: gameData.publicDetails.playersIn
                    }
                }
            } as NNCmd.Cmd_RequestUpdateRoom)

            // add participant
            IrcClientExtra.sendNNCmdTo(ircClient, lchan, {
                op: 'sync_participantAdded',
                data: { nick: lnick, ipsAndPorts: cmd.ipsAndPorts}
            } as CmdGameRoom_ParticipantAdded)

            IrcClientExtra.customEvents.emit('gameRoom_participantAdded', lchan, lnick);

            fullLanDetectionLogic(cmd.channel, lnick);
            mainStore.resolveCountryCodeForIpForNick(lnick, cmd.channel)
        }
    }
})
commandsGameRoom.set('isExecRunning', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;

    const cmd = _c as CmdGameRoom_IsExecRunning;

    const lnick = nick.toLowerCase()

    if(mainStore.isChannelGameRoom(chan)) {
        const game = mainStore.gameForChannel(chan);
        const nickDetails = game?.participantNicks?.[lnick]
        if(nickDetails) {
            nickDetails.isExecRunning = cmd.data.running;
        }
    }
});

commandsGameRoom.set('isExecMissing', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;

    const cmd = _c as CmdGameRoom_IsExecMissing;

    const lnick = nick.toLowerCase()

    if(mainStore.isChannelGameRoom(chan)) {
        const game = mainStore.gameForChannel(chan);
        const nickDetails = game?.participantNicks?.[lnick]
        if(nickDetails) {
            nickDetails.isExecMissing = cmd.data.isMissing;
        }
    }
});

commandsGameRoom.set('toggleDedicatedServer', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return

    const lchannel = chan.toLowerCase();
    const lnick = nick.toLowerCase()
    
    // Has to be game room
    if(!mainStore.isChannelGameRoom(lchannel)) return;

    // Ignore non-host message
    if(storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase() != lnick) return

    const cmd = _c as CmdGameRoom_ToggleDedicatedServer;

    const g = mainStore.gameForChannel(lchannel);
    if(cmd?.data?.details == null) {
        delete g.dedicatedServer;
    } else {
        g.dedicatedServer = cmd.data.details
    }
})

commandsGameRoom.set('gameMessage', async (chan, nick, _c, ircClient)=>{
    const cmd = _c as CmdGameRoom_GameMessage;
    try {
        const shouldDisableSound = mainStore.myExecIsRunningInChan(chan!);
        storeActions.message(`🎮${nick}`, chan!, cmd.data.text, "", false, false, shouldDisableSound);
    } catch(e) {}
});

commandsGameRoom.set('launchGame', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;
    clientLaunchGame(chan, nick, ircClient);
});

commandsGameRoom.set('stun_sync', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;
    const isMe = ircClient.nick.toLowerCase() == nick.toLowerCase();
    if(isMe) return;

    const lhostNick = storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase();
    const isFromHost = lhostNick == nick.toLowerCase()
    if(!isFromHost) return;

    const cmd = _c as CmdGameRoom_StunSync;
    if(cmd.data.start) {
        try {stopUseStun(chan, ircClient);} catch(e) {}
        try {mainStore.joinedChannels[chan.toLowerCase()].game!.participantNicks[lhostNick].stun = {port:cmd.data.port!, addr:cmd.data.addr!, symmetricNat: cmd.data.symmetricNat}} catch(e) {};
        useStun(chan, ircClient);
    } else if(cmd.data.down) {
        stopUseStun(chan, ircClient);
    }
});

commandsGameRoom.set('pingRelays', async (chan, nick, _c, ircClient)=>{
    if(!chan?.length) return;
    const lhostnick = nick.toLowerCase();
    if(storeActions.getGameHostNickForChannelImIn(chan)?.toLowerCase() != lhostnick) return

    const relaysListResponse = await axios.get(`http://${serverHost}:${serverHttpPort}/relay`);
    const relayOptions = relaysListResponse.data as RelayInfo[];
    
    const relaysAndPingsPairs = await pingAllRelays(relayOptions)

    IrcClientExtra.sendNNCmdTo(ircClient, lhostnick, {
        op:'pongRelays',
        data: relaysAndPingsPairs
    } as CmdGameRoom_PongRelays)
});

commandsGameRoom.set('pongRelays', async (chan, nick, _c, ircClient)=>{}); // STUB just to receive it

const commandsFromBotPrvMsg = new Map<string,(channel:string|null, nick:string, metaCmd:NNCmd.Cmd_Base, ircClient:Client,message:any)=>any>();

commandsFromBotPrvMsg.set('relayClosed', (chan, nick, c, ircClient)=>{
    if(mainStore.myLeasedDedicatedServer) {
        delete mainStore.myLeasedDedicatedServer;
        for(const chan in mainStore.joinedChannels) {
            if(mainStore.isChannelGameRoom(chan) && mainStore.imHostInChannel(chan)) {
                const g = mainStore.gameForChannel(chan);
                delete g.dedicatedServer;
                IrcClientExtra.sendNNCmdTo(ircClient, chan, {
                    op:'toggleDedicatedServer',
                    data: {}
                } as CmdGameRoom_ToggleDedicatedServer)
            } 
        }
        DialogMgr.addPopup({
            title: 'Relay Server',
            body: 'Relay Server lease has ended.',
            icon: 'info',
            buttons:[{body:"ok"}]
        })
    }
})

const commandsFromMetaChannel = new Map<CmdMeta_AllOpTypes,(channel:string|null, nick:string, metaCmd:NNCmd_Meta.CmdMeta_AllTypes, ircClient:Client,message:any)=>any>();
commandsFromMetaChannel.set('newRoom', (chan, nick, c, ircClient)=>{
    const newRoomCmd = c as NNCmd_Meta.CmdMeta_NewRoom;
    mainStore.advertisedGameRooms[newRoomCmd.data.channel.toLowerCase()] = {
        publicDetails: newRoomCmd.data
    }

    // only play sound effect of a different user created a room
    if(newRoomCmd?.data?.owner?.toLowerCase() != ircClient?.nick?.toLowerCase()) {
        Settings.playSoundEffect('gameroom-new');
    }
})

commandsFromMetaChannel.set('roomsDeleted', (chan, nick, c, ircClient)=>{
    const roomsDeletedCmd = c as NNCmd_Meta.CmdMeta_RoomDeleted
    roomsDeletedCmd.data.channels.forEach(chan=>{
        delete mainStore.advertisedGameRooms[chan.toLowerCase()];
    })  
})

commandsFromMetaChannel.set('roomDetailsUpdate', (chan, nick, c, ircClient)=>{
    const upd = c as NNCmd_Meta.CmdMeta_RoomDetailsUpdate;
    const roomPubDetails = mainStore?.advertisedGameRooms?.[upd?.data?.channel?.toLowerCase()]?.publicDetails;
    if(!roomPubDetails) return;

    for(const prop in upd.data.patch) {
        const val = (upd.data.patch as any)[prop];
        if(val == null) {
            delete (roomPubDetails as any)[prop]
        } else {
            (roomPubDetails as any)[prop] = val;
        }
    }
})


export const NNCMD_afterOpIndicator = ':afterOp';

const msgBuffersProtoBuf: {[from:string]: {buffer:Buffer, length:number, timerId:any}} = {}
const msgBuffers: {[from:string]: {strBuffer:string, timerId:any}} = {}
export default {
    async handleCommandMessage(channel:string|null, nick:string, text:string, message:any, ircClient:Client):Promise<boolean> {
        try {
            const cmdObjOrPb = await IrcClientExtra.handleMultipartCommandBuffer(channel, nick, text, msgBuffers, message.buffer, msgBuffersProtoBuf);
            if(cmdObjOrPb === 'multipart')  {
                return true
            }

            const lchannel = channel?.toLowerCase();
            const lnick = nick?.toLowerCase();

            // Handling protobuf commands
            if((cmdObjOrPb as NNPB.RootCommand).data?.$case != null) {
                // if got here, this is a new protobuf command
                const nnpbCmd = cmdObjOrPb as NNPB.RootCommand;
                
                const nnpbOpName = ((nnpbCmd as any).data.$case) as string;
                let opFunc = nnpbCommandsGameRoom.get(nnpbOpName);

                if(opFunc!=null) {
                    // TODO support req/res
                    const cmdData = (nnpbCmd as any).data;
                    let subCmd:any = null;
                    for(const prop in cmdData) {
                        if(prop==='$case') continue;
                        else {
                            subCmd = (cmdData as any)[prop];
                            break;
                        }
                    }
                    if(subCmd == null) return true;

                    function emitEvent(afterOp?:boolean) {
                        const cmdEventName = nnpbOpName+(afterOp?NNCMD_afterOpIndicator:'');
                        nncmdReceivedEmitter.emit(cmdEventName, cmdEventName, 'protobuf', channel, nick, subCmd, ircClient, message);
                        nncmdReceivedEmitter.emit('*'+(afterOp?NNCMD_afterOpIndicator:''), cmdEventName, 'protobuf', channel, nick, subCmd, ircClient, message);
                    }

                    emitEvent();
                    try {
                        await opFunc(channel, nick, subCmd, ircClient, message);
                    } catch(e) {
                        console.error('Error in handling protobuf NNCmd', e);
                    }
                    emitEvent(true);
                    return true;
                } else {
                    return true
                }
            }

            // If we got here, this is not a protobuf command

            const cmdObj = cmdObjOrPb as NNCmd.Cmd_Base;

            let opFunc = null

            // If MetaChannel command from Bot
            if(NNCmd_Meta.CmdMeta_AllOps.includes(cmdObj.op as any) && lchannel === BotMetaChannel_lowercase && lnick == BotName_lowercase) {
                opFunc = commandsFromMetaChannel.get(cmdObj.op as CmdMeta_AllOpTypes);
            } else if (CmdGameRoom_AllOps.includes(cmdObj.op as any)) {
                opFunc = commandsGameRoom.get(cmdObj.op as CmdGameRoom_AllOpTypes);
            } else if (commandsFromBotPrvMsg.has(cmdObj.op)) {
                opFunc = commandsFromBotPrvMsg.get(cmdObj.op);
            }
            // TODO ensure all message fields are correctly typed

            // IMPORTANT we do this because otherwise the PROXY of the mainStore mutable is lost, and view is not updated, (TODO maybe) find out why that happens
            const cloneCmdObj = cmdObj?JSON.parse(JSON.stringify(cmdObj as any)):{} as typeof cmdObj;
            
            function emitEvent(afterOp?:boolean) {
                if(cmdObj?.op?.length) {
                    const cmdOp = cmdObj.op+(afterOp?NNCMD_afterOpIndicator:'');
                    nncmdReceivedEmitter.emit(cmdOp, cmdOp, 'normal', channel, nick, cloneCmdObj, ircClient, message);
                    nncmdReceivedEmitter.emit('*'+(afterOp?NNCMD_afterOpIndicator:''), cmdOp, 'normal', channel, nick, cloneCmdObj, ircClient, message);
                }
            }
            emitEvent();
            if(!opFunc) return true

            let retVal:NNCmd.Cmd_Base|null = null;
            try {
                retVal = await opFunc(channel, nick, cloneCmdObj, ircClient, message);
            } catch(e) {
                console.error('Error in handling NNCmd', e);
            }

            emitEvent(true);
            if(retVal?.op) {
                if(cmdObj.id) {
                    retVal.resToReqId = cmdObj.id;
                }
                await IrcClientExtra.sendNNCmdTo(ircClient, nick, retVal);
            }
            return true
        } catch(e) {
            return false;
        }
    }
}