import { GameDefsType } from "./GameDefsTypes";
import { path, fs, fsSync, SudoPrompt, glob } from './NodeLibs'
import { escapeRegExp } from "./lib/RegexEscape";
import { NNEval } from "./lib/nneval";
import { removeNonAlphaNumChars } from "../../server/src/common/StringUtils";
import { deepClone } from "./lib/deepClone";
import { Settings } from "./Settings";
import { compare } from "fast-json-patch";
import { DialogMgr } from "./Popups";
import INI from 'ini'
import { escapeGlob, findPathCaseInsensitiveSync } from "./lib/utils";

const resolveEditEntry = (editEntry:GameDefsType.Runnable_CmdBeforeRun_EditBase_Entry, env:any)=>{
    try {
        let val = NNEval(editEntry.value, env);
        if(typeof val === 'string' && !editEntry.unquote) {
            return '"'+val+'"';
        } else {
            return val;
        }
    } catch(e) {}
    return null
}

async function filterEdits_AddIf(editsLeft:{addIf?: (context:any)=>Promise<boolean>}[], env:any) {
    for(let i=editsLeft.length-1;i>=0;i--) {
        const edit = editsLeft[i];
        if(edit.addIf) {
            const shouldAdd = await edit.addIf(env);
            if(!shouldAdd) {
                editsLeft.splice(i,1);
            }
        }
    }
}

async function prepareFileDir(fileDef: GameDefsType.FileDef, gameExecDir:string) {
    const iniFilePath = fileDef.path!;
    const joinedFilePath = fileDef.baseDirWin? path.join(await Settings.resolveWindowsPath(fileDef.baseDirWin), iniFilePath) : path.resolve(gameExecDir, iniFilePath);
    try {await fs.mkdir(path.dirname(joinedFilePath), {recursive:true});} catch(e) {}

    let resolvedFilePath:string|undefined = undefined;
    try {
        resolvedFilePath = (await glob.glob(escapeGlob(path.basename(joinedFilePath)), {cwd: path.dirname(joinedFilePath), nocase:true, absolute:true}))?.[0];
    } catch(e) {}
    return resolvedFilePath || joinedFilePath;
}

export async function LaunchGameCmd_editIni(execDef: GameDefsType.Executable, cmdBeforeRun: GameDefsType.Runnable_CmdBeforeRun_EditIni, gameExecDir: string, env:any) {

    // file to edit
    const fileDef = execDef?.files?.[cmdBeforeRun?.file];
    const finalFilePath = await prepareFileDir(fileDef, gameExecDir);

    // file to copy from before editting
    if(cmdBeforeRun?.originFile) {
        const originIniFilePath = execDef?.files?.[cmdBeforeRun?.originFile]?.path!;
        const finalOriginFilePath = path.resolve(gameExecDir, originIniFilePath);
        if(!fsSync.existsSync(finalFilePath)) {
            fsSync.copyFileSync(finalOriginFilePath, finalFilePath);   
        }
    }

    let iniCfgText;
    try {
        iniCfgText = (await fs.readFile(finalFilePath, 'utf-8'));
    } catch(e) {
        iniCfgText = '';
    }
    const iniCfgLines = iniCfgText.match(/[^\r\n]+/g)?.map((t:string)=>t.trim()) || [];

    function getCategoryOfLine(line:string) {
        if(line.startsWith('[') && line.endsWith(']')) {
            return line.split('[')[1].split(']')[0];
        } else return null;
    }

    let output = '';
    let currentCategory = null;

    const editsLeftUncalculated = deepClone(cmdBeforeRun.edits) as typeof cmdBeforeRun.edits;
    const editsLeftPromise = editsLeftUncalculated.map(async (edit)=>{
        const cat = typeof edit.category==='function'?(await edit.category(env)):edit.category as string
        return {...edit, category: cat};
    });

    const editsLeft = await Promise.all(editsLeftPromise);
    await filterEdits_AddIf(editsLeft, env);

    for(let i=0;i<iniCfgLines.length;i++) {
        const line = iniCfgLines[i];

        const newCatergory = getCategoryOfLine(line);
        if(newCatergory) {
            if(currentCategory != newCatergory && currentCategory != null) {
                // fill missing entries before moving on to next category
                for(let e=editsLeft.length-1;e>=0;e--) {
                    const edit = editsLeft[e];
                    if(edit.category === currentCategory) {
                        editsLeft.splice(e,1)
                        const resVal = resolveEditEntry(edit, env);
                        if(resVal != null) {
                            output += edit.entry + '=' + resVal+ '\r\n';
                        } else if(edit.skipNull) {
                            output += line +'\r\n';
                        }
                    }
                }
            }

            currentCategory = newCatergory;
            output += line+'\r\n';
            continue;
        }

        let foundAndReplacedEntry = false
        for(let e=editsLeft.length-1;e>=0;e--) {
            const edit = editsLeft[e];
            if(edit.category === currentCategory && new RegExp(`^${escapeRegExp(edit.entry)}[ \t]*=.*`).test(line)) {
                // Check if line contains entry (with INI format syntax, e.g blabla = "test")
                editsLeft.splice(e,1)
                const resVal = resolveEditEntry(edit, env);
                if(resVal != null) {
                    output += edit.entry + '=' + resVal+ '\r\n';
                } else if(edit.skipNull) {
                    output += line +'\r\n';
                }
                foundAndReplacedEntry = true
                break;
            }
        }
        if(!foundAndReplacedEntry) {output += line +'\r\n'}
    }

    (function addMissingEntries() { 
        // If edits remain, add them
        type CategoryEditEntry = GameDefsType.Runnable_CmdBeforeRun_EditIni_Entry;

        const lastCategoryEntries:CategoryEditEntry[] = [];
        const categoryToEntries:{[cat:string]: CategoryEditEntry[]} = {};
        for(const edit of editsLeft) {
            if(edit.category === currentCategory && currentCategory?.length) {
                lastCategoryEntries.push(edit);
            } else {
                if(!categoryToEntries[edit.category]) categoryToEntries[edit.category] = [];
                categoryToEntries[edit.category].push(edit);
            }
        }

        const categoryNames = Object.keys(categoryToEntries);
        for(let i=0;i<=categoryNames.length;i++) {
            let entries:CategoryEditEntry[] = [];
            let cat:string;
            if(i===categoryNames.length) {
                if(lastCategoryEntries.length) {
                    cat = currentCategory!;
                    entries = lastCategoryEntries;
                } else {
                    break;
                }
            } else {
                cat = categoryNames[i];
                entries =  categoryToEntries[cat];
                output += `[${cat}]\r\n`
            }
            
            for(const entry of entries) {
                const resVal = resolveEditEntry(entry, env);
                if(resVal != null) {
                    output += `${entry.entry}=${resVal}\r\n`
                }
            }
        }
    })();
    
    output = output.trim();
    try {
        await fs.writeFile(finalFilePath, output)
    } catch(e) {
        // If writing fails, try to write as admin, if allowed.
        // Worms Armageddon needs admin permission to write "%SystemRoot%\win.ini" (host port and player name)
        if(!fileDef?.allowAdmin) return;
        try {
            iniCfgText = iniCfgLines.join('\r\n'); // npm lib 'ini' doesn't work properly without \r\n endings together.
            const oldIniObj = INI.decode(iniCfgText);
            const newIniObj = INI.decode(output);
            const diff = compare(oldIniObj, newIniObj);
    
            if(diff.length) {
                // Only if there's a difference, try to write as admin
                await new Promise(resolve=>{
                    try {
                        const lines = output.match(/[^\r\n]+/g);
                        let isFirst = true;
                        const commands = lines?.map(line=>{
                            if(isFirst) {
                                isFirst=false;
                                return `echo ${line} > "${finalFilePath}"`;
                            } else {
                                return `echo ${line} >> "${finalFilePath}"`;
                            }
                        }) || [];
                        if(commands.length) {

                            let diffLinesHtml = '';
                            try {
                                diffLinesHtml = diff.map(d=>{
                                    const pathParts = d.path.split('/');

                                    const sectionPath = pathParts.length>=3 ? `[${pathParts?.[1]}] ` : '';
                                    const entry = pathParts[pathParts.length-1];
                                    const entryPart = (d as any)?.value != null? `${entry}=${(d as any).value}` : entry;

                                    return `${d.op} ${sectionPath}${entryPart}`
                                }).join('<br/>');
                            } catch(e) {}

                            DialogMgr.addPopup({
                                title: "Admin permission required", buttons: [{body:"Ok"}], closeButton: true,
                                body:$(`<p>${env.GameName} ${env.GameExecName} needs admin permission to write important settings in ${finalFilePath}.<br/>
                                    Without writing, the game may not work properly.<br/>
                                    <br/>
                                    ${diffLinesHtml}
                                    </p>`)
                            }).then(()=>{
                                SudoPrompt.exec(`${commands.join('\r\n')}`, {
                                    name:"NukemNet INI changes"
                                }, resolve);
                            })
                        } else {
                            resolve(null);
                        }
                    } catch(e) {
                        resolve(null);
                    }
                })
            }
        } catch(e) {}
    }
}



export async function LaunchGameCmd_editSpaceDelimitedFile(execDef: GameDefsType.Executable, cmdBeforeRun: GameDefsType.Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile, gameExecDir: string, env:any) {

    const finalFilePath = await prepareFileDir(execDef?.files?.[cmdBeforeRun?.file], gameExecDir);

    let textInFile;
    try {
        textInFile = await fs.readFile(finalFilePath, 'utf-8');
    } catch(e) {
        textInFile = '';
    }
    const cfgLines = textInFile.match(/[^\r\n]+/g)?.map((t:string)=>t.trim()) || [];

    let output = '';
    const editsLeft = deepClone(cmdBeforeRun.edits);
    await filterEdits_AddIf(editsLeft, env);

    for(const line of cfgLines) {

        let foundEntry:GameDefsType.Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile_Entry|null=null;
        for(let i=0;i<editsLeft.length;i++) {
            const edit = editsLeft[i];
            if(new RegExp(`^${escapeRegExp(edit.entry)}[ \t]*.*`).test(line)) {
                foundEntry = edit;
                editsLeft.splice(i,1);
                break;
            }
        }
    
        if(foundEntry) {
            const foundEntryName = foundEntry.entry;
            const value = resolveEditEntry(foundEntry, env)
            if(value != null) {
                output += foundEntryName + ' ' + value + '\n';
            } else if(foundEntry.skipNull) {
                output += line + '\n';    
            }
        } else {
            output += line + '\n';
        }
    }

    for(const edit of editsLeft) {
        const foundEntryName = edit.entry;
        const value = resolveEditEntry(edit, env)
        if(value != null) {
            output += foundEntryName + ' ' + value + '\n';
        }
    }

    output = output.trim();
    await fs.writeFile(finalFilePath, output)
}


export async function LaunchGameCmd_EditBinary(execDef: GameDefsType.Executable, cmdBeforeRun: GameDefsType.Runnable_CmdBeforeRun_EditBinary, gameExecDir: string, baseEvalObj:any) {
    const finalFilePath = await prepareFileDir(execDef?.files?.[cmdBeforeRun?.file], gameExecDir);
    
    const resolveEditEntry = (editEntry:GameDefsType.Runnable_CmdBeforeRun_EditBinary_Entry)=>{
        try {
            let val = NNEval(editEntry.value, baseEvalObj);
            return val;
        } catch(e) {}
        return null
    }

    
    if(!findPathCaseInsensitiveSync(finalFilePath)) {
        try {
            if(!cmdBeforeRun.createMissing) return;
            const buffer = await cmdBeforeRun.createMissing();
            await fs.writeFile(finalFilePath, buffer);
        } catch(error) {
            const e = error as any;
            console.error(`Cannot create file ${finalFilePath}`, e?.stack || e?.message || e)
            return;
        }
    } // If no CFG file, cannot edit.

    const bytes = await fs.readFile(finalFilePath);

    const editsLeft = deepClone(cmdBeforeRun.edits) as typeof cmdBeforeRun.edits;
    await filterEdits_AddIf(editsLeft, baseEvalObj);

    for(const edit of cmdBeforeRun.edits) {
        const val = resolveEditEntry(edit);
        const zeroFillVal = edit.zeroFillByte!=null?edit.zeroFillByte:0;

        if(typeof val === 'string') {
            const strBuffer = Buffer.from(removeNonAlphaNumChars(val)).subarray(0,edit.length);
            
            const endByteOffset = edit.offset+edit.length;
            for(let i=edit.offset,s=0;i<endByteOffset;i++,s++) {
                if(s<strBuffer.length) {
                    bytes[i] = strBuffer[s];
                } else {
                    bytes[i] = zeroFillVal;
                }
            }
        } else if(typeof val === 'number') {
            const endByteOffset = edit.offset+edit.length;
            const numBytesArrayBuffer = new ArrayBuffer(edit.length);
            const view = new DataView(numBytesArrayBuffer);
            (view as any)[edit.numType!=null?edit.numType!:'setUint32'](0, val, edit.litteEndian==true);
            
            const numBytes = Buffer.from(numBytesArrayBuffer);

            for(let i=edit.offset,s=0;i<endByteOffset;i++,s++) {
                if(s<numBytes.length) {
                    bytes[i] = numBytes[s];
                } else {
                    bytes[i] = zeroFillVal;
                }
            }
        }
    }

    await fs.writeFile(finalFilePath, bytes);
}