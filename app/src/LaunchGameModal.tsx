import { Accordion, Button, Form, Modal } from "solid-bootstrap"
import { Component, createSignal, For, Show } from "solid-js"
import { GameDefsType } from "./GameDefsTypes"
import { LaunchDefaults_ManualArg, LaunchDefaults_NormalArg, NNLinks_DirName, Settings, SettingsDef } from "./Settings";
import { LaunchArgType, LaunchArgType_File } from "./lib/LaunchArgType";
import { NNEvalWrapArray } from "./lib/nneval";
import sleep from 'sleep-promise';
import { mainStore } from "./store/store";
import { createMutable } from "solid-js/store";
import { deepClone } from "./lib/deepClone";
import { maxChannelNameLength } from "../../server/src/common/constants"
import { DialogMgr } from "./Popups";
import { crypto, electronRemote, fsSync, glob, path } from "./NodeLibs";
import { createRandomHash } from "../../server/src/common/hash";
import { SetCaretAtEnd } from "./lib/utils";
import storeActions from "./store/storeActions";

const elementIdPrefix = 'game-param-';
const checkboxElementIdPrefix = 'checkbox.';
const fullCheckboxIdPrefix = checkboxElementIdPrefix+elementIdPrefix;

const defaultMaxPlayers = 8;

const OptionalGameParamCmp:Component<{children?:any, label:string, id:string, description?:string}> = (props)=>{
    const checkboxId = checkboxElementIdPrefix+props.id;
    const [optionEnabled, setOptionEnabled] = createSignal(false);
    return <>
        <Form.Check title={props.description} id={checkboxId} type="checkbox" checked={optionEnabled()} onchange={(el:any)=>{
          setOptionEnabled(el.target.checked)
          }}/>
        <Form.Label title={props.description} for={checkboxId}>{props.label}</Form.Label>
        <Show when={optionEnabled()}>
            <span title={props.description}>{...props.children}</span>
        </Show>
    </>
}

const RequiredGameParamCmp:Component<{children?:any, label:string, id:string, description?:string}> = (props)=>{
    return <>
        <Form.Label title={props.description} for={props.id}>{props.label}</Form.Label>
        <span title={props.description}>{...props.children}</span>
    </>
}

async function preConfigFromFile(fromGameExec?:{gameId:string, execId:string}) {
  const launchDefaults = await Settings.launchDefaults()!;
  const gameAndExec = fromGameExec != null? fromGameExec: {gameId:launchDefaults.defaultGameAndExec?.gameId, execId:launchDefaults?.defaultGameAndExec?.execId}
  if(gameAndExec?.gameId && gameAndExec?.execId) {
    try {
      const execData = launchDefaults?.games?.[gameAndExec.gameId]?.executables?.[gameAndExec.execId];
      const args:LaunchArgType[] = [];
      for(const paramId in execData?.params||{}) {
        const unknownData = execData!.params![paramId];
        if(paramId.startsWith('_m_')) {
          // manual arg
          const data = unknownData as LaunchDefaults_ManualArg
          args.push({
            id: paramId,
            args: data.args,
            for: data.for,
            order: data.order
          })  
        } else {
          const data = unknownData as LaunchDefaults_NormalArg;
          args.push({
            id: paramId,
            args:[],
            for: data.for,
            html: {prop:data.htmlProp, value: data.value}
          })  
        }
      }
      const preconfigFromFile:LaunchGameModalResult = {
        game: gameAndExec.gameId,
        exec: gameAndExec.execId,
        maxPlayers: execData?.maxPlayers,
        mode: execData!.mode!,
        roomName: launchDefaults.roomName,
        hideRoom: !!launchDefaults?.hideRoom,
        password: launchDefaults.roomPass,
        args:args
      };
      return preconfigFromFile;
    } catch(e) {}
  }
  return null;
}


export type LaunchGameModalResult = {game:string, exec:string, mode:GameDefsType.GameModes, args:LaunchArgType[], hideRoom?:boolean, maxPlayers?:number, roomName?:string, password?:string, settings?:SettingsDef, editChannel?:string}
const LaunchGameModal:Component<{show:boolean, onCancel?:()=>any, onSubmit?:(result:LaunchGameModalResult)=>void, gameDefs: GameDefsType.Root, preConfig?:LaunchGameModalResult|null}> = (props)=>{
    const manualParams = createMutable([] as LaunchArgType[]);
    const elementId = {
      newParamText:"new-param-text",
      newParamOrder:"new-param-order",
      newParamFor:"new-param-for",
      manualParamsContainer: "manual-params-container"
    }

    const [selectedGameId, setSelectedGameId] = createSignal(Object.keys(props.gameDefs.games)[0]) 
    function onGameSelect(element:any) {
        const selectedValue = element.target.selectedOptions[0].value;
        setSelectedGameId(selectedValue);
        manualParams.splice(0,manualParams.length);
    }

    const [selectedExec, setSelectedExec] = createSignal('') 
    function onExecSelect(element:any) {
        const selectedValue = element.target.selectedOptions[0].value;
        setSelectedExec(''); // erase all previous choices
        manualParams.splice(0,manualParams.length);
        setTimeout(()=>{
          setSelectedExec(selectedValue);
          loadPreConfigData({gameId: selectedGameId(), execId:selectedExec()});
        })
    }
    const executableChoices = ()=>{
        const _selectedGameId = selectedGameId();
        if(!_selectedGameId) return {};
        const execs = props?.gameDefs?.games?.[_selectedGameId]?.executables || {};
        return execs
    }

    const paramsForExec = ()=>{
        const exec = selectedExec();
        if(!exec?.length) return {};
        try {
            const p = props.gameDefs.games[selectedGameId()].executables[exec].parameters! || {};
            return p;
        } catch(e) {
            return {}
        }
    }
    const sectionsForExec = ()=>{
      const exec = selectedExec();
      if(!exec?.length) return {};
      try {
          const p = props.gameDefs.games[selectedGameId()].executables[exec].paramSections! || {};
          return p;
      } catch(e) {
          return {}
      }
  }

    const modesForExec = ()=>{
      const exec = selectedExec();
      if(!exec?.length) return [];
      try {
          const p = Object.keys(props.gameDefs.games[selectedGameId()].executables[exec].runnables);
          return p;
      } catch(e) {
          return []
      }
    }

    const execDef = ()=>{
      return props?.gameDefs?.games?.[selectedGameId()]?.executables?.[selectedExec()] || null;
    }

    const [selectedMode, setSelectedMode] = createSignal('' as GameDefsType.GameModes);
    function onModeSelect(element:any) {
      const selectedValue = element.target.selectedOptions[0].value;
      manualParams.splice(0,manualParams.length);
      setSelectedMode(selectedValue)
      setTimeout(()=>{
        loadPreConfigData({gameId: selectedGameId(), execId:selectedExec(), ignoreMode:true});
      });
    }
    const resolvedMode = ()=>{
      try {
        if(!selectedMode()?.length) return modesForExec()[0];
        if(modesForExec().indexOf(selectedMode()) < 0) return modesForExec()[0];
        return selectedMode();
      } catch(e) {
        return null
      }
    }

    const paramsForExecAndModeBySection:()=>{sectionName?:string, params:{[pId:string]: GameDefsType.ParamType.AllTypes}}[] = ()=>{
      try {
        const params = paramsForExec();
        const sections = sectionsForExec();
        const mode = resolvedMode();
        if(!mode || !params) return [];
  
        const retVal = [{params:{}}] as any; // sectionless params are first.

        const sectionIdToIndex = {} as {[sectionId:string]:number};
        for(const sectionId in sections) {
          const section = sections[sectionId];
          retVal.push({sectionName:section.name, params:{}})
          sectionIdToIndex[sectionId] = retVal.length-1;
        }

        for(const pname in params) {
          const pdata = params[pname];
          if(!pdata.modeSupport || pdata.modeSupport.includes(mode as GameDefsType.GameModes)) {
            const sectionIndex = pdata.section?sectionIdToIndex[pdata.section]:0;
            retVal[sectionIndex].params[pname] = pdata;
          }
        }
  
        return retVal
      } catch(e) {
        return {}
      }
    }

    const hasModeAndExec = ()=>{
      return resolvedMode() && resolvedMode()?.length && selectedExec()?.length
    }

    const [roomName,setRoomName] = createSignal('');
    const [password,setPassword] = createSignal('');
    const [maxPlayers,_setMaxPlayers] = createSignal(defaultMaxPlayers);
    const [hideRoom,setHideRoom] = createSignal(false);

    function setMaxPlayers(p:number) {
      const maxPlayersForExec = execDef()?.maxPlayers||defaultMaxPlayers;
      if(p<=maxPlayersForExec) {
        _setMaxPlayers(p);
      } else {
        _setMaxPlayers(maxPlayersForExec);
      }
    }

    const isInClientEditMode = mainStore.gameForChannel(props?.preConfig?.editChannel || '') && !mainStore.imHostInChannel(props?.preConfig?.editChannel || '');

    async function loadPreConfigData(forceFromFileDefaults?:{gameId:string, execId:string, ignoreMode?:boolean}) {

      let preConfig = props.preConfig;
      if(preConfig == null || forceFromFileDefaults != null) {
        // try from file
        preConfig = await preConfigFromFile(forceFromFileDefaults);
      }

      if(isInClientEditMode && preConfig?.exec && preConfig.game) {
        Object.assign(preConfig, await preConfigFromFile({execId: preConfig.exec, gameId: preConfig.game}))
      }

      if(preConfig) {
        const p = preConfig;
        if(p.game) {
          setSelectedGameId(p.game);
        }
        if(p.exec) {
          setSelectedExec(p.exec);
        }
        if(p.maxPlayers!=null) {
          setMaxPlayers(p.maxPlayers)
        }

        if(!forceFromFileDefaults?.ignoreMode) {
          if(props?.preConfig?.mode?.length) {
            setSelectedMode(props.preConfig.mode)
          } else if(p?.mode?.length) {
            setSelectedMode(p.mode)
          }
        }

        if(p.password) {
          setPassword(p.password)
        }
        if(p.roomName) {
          setRoomName(p.roomName);
        }
        if(p.hideRoom != null) {
          setHideRoom(!!p.hideRoom);
        }
        
        await sleep(1);
  
        for(const a of p.args || []) {
          manualParams.splice(0,manualParams.length)
          // Manual arg
          if(a.id.startsWith('_m_')) {
            if(!isInClientEditMode || a.for==='private') {
              for(const subA of a.args) {
                const arg = deepClone(a);
                arg.args = [subA];
                manualParams.push(arg);
              }
            }
          } else {
            // Normal arg
            const checkboxId = `${fullCheckboxIdPrefix}${a.id}`;
            const elId = `${elementIdPrefix}${a.id}`;
    
            const checkboxElement = document.getElementById(checkboxId) as HTMLInputElement;
            if(checkboxElement && checkboxElement instanceof HTMLInputElement && checkboxElement.checked != null) {
              checkboxElement.checked = true;
              checkboxElement.dispatchEvent(new Event('change'));
            }
  
            await sleep(1);
  
            // Set saved value to element
            try {
              const el = document.getElementById(elId);
              if(el && a?.html?.prop != null) {
                const propName = a.html!.prop!;
                if(propName in el) {
                  (el as any)[propName] = a.html?.value;
                  el.dispatchEvent(new Event('change'));
                }
              }
            } catch(e) {}
            
          }
          
        }
      }
    }
    loadPreConfigData();

    return <Modal
    show={props.show}
    onHide={props.onCancel}
    backdrop="static"
    keyboard={false}
  >
    <Modal.Header closeButton>
      <Modal.Title>{props?.preConfig?.editChannel != null? 'Edit Game':'Launch Game'}</Modal.Title>
    </Modal.Header>
    <Modal.Body>

    <Show when={!isInClientEditMode}>
      <div class="game-choice">
        <label for="game">Choose a game:</label>
        <select id="game" onChange={onGameSelect}>
          <For each={Object.keys(props.gameDefs.games).sort((a,b)=>(props.gameDefs.games[a].name>props.gameDefs.games[b].name?1:-1))}>
            {(gameId:string)=>{
              return <option selected={gameId===selectedGameId()} value={gameId}>{props.gameDefs.games[gameId].name}</option>
            }}
          </For>
        </select>
      </div>
      
      <div class="exec-choice">
        <label for="exec">Choose executable:</label>
        <select id="exec" onChange={onExecSelect}>
          <option value="">Please select</option>
          <For each={Object.keys(executableChoices())}>
            {(execId:string)=>{
              return <option selected={selectedExec()===execId} value={execId}>{executableChoices()[execId].name}</option>
            }}
          </For>
        </select>
      </div>

      <Show when={modesForExec().length}>
        <div>
            <label for="mode">Choose mode:</label>
            <Form.Select id="mode" onChange={onModeSelect} disabled={props?.preConfig?.editChannel != null}>
              <For each={modesForExec()}>
                {(mode)=><option value={mode} selected={selectedMode()===mode}>{mode}</option>}
              </For>
            </Form.Select>
        </div>
      </Show>

    </Show>
    

    <div>
        <For each={paramsForExecAndModeBySection()}>
            {(section)=>{
                const sectionParamElements = <For each={Object.keys(section.params)}>
                    {(pname)=>{
                      const param = section.params[pname];

                      if(isInClientEditMode && !GameDefsType.ParamType.ParamFor_LocalOnly.includes(param?.for||'' as any)) {
                        return
                      }
                      
                      const elementId = elementIdPrefix+pname;
                      let element = null;
      
                      if(param.type==='boolean') {
                          element = <Form.Check data-type={param.type} id={elementId} type="checkbox" />                    
                      } else if(param.type==='numrange') {
                        const [num, setNum] = createSignal(param.min);
                        let numRef:HTMLInputElement;
                        element = <div style={{"display":"flex","flex-direction":'row',"align-items": "center"}}>
                            <input type="number" value={num()} onchange={(el)=>{
                              const inputEl = el.target as HTMLInputElement;
                              const value = parseInt(inputEl?.value);
                              
                              if(value<+inputEl.min) {
                                setNum(+inputEl.min)
                              } else if(value>+inputEl.max) {
                                setNum(+inputEl.max)
                              } else {
                                setNum(value)
                              }
                              inputEl!.value = ''+num();
                              
                            }} ref={numRef!} step={param.delta} max={param.max} min={param.min} />
                            <Form.Range data-type={param.type} onchange={e=>numRef.value=(e.target as any).value} id={elementId} value={num()} oninput={(el:any)=>{setNum(parseInt(el.target.value))}} step={param.delta} max={param.max} min={param.min} />
                        </div>
                      } else if(param.type==='choice') {
                          if(!param.allowMultiple) {
                            element = <>
                              <Form.Select data-type={param.type} id={elementId}>
                                <For each={param.choices}>
                                  {(choice)=>{
                                    let value = undefined;
                                    try {value = JSON.stringify(choice.value)} catch(e) {}
                                    return <option value={value}>{choice.label}</option> // TODO once I support multiple choice, remove cast "choice.value as string"
                                  }}
                                </For>
                              </Form.Select>
                            </>
                          }
                      } else if(param.type === 'text') {
                        element = <>
                            <Form.Control {...(param.props||{})} type="text" data-type={param.type} id={elementId} />
                        </>
                      } else if(param.type === 'file') {
                        let fileTextInput:HTMLInputElement;
                        element = <span style={{display:'flex', "flex-direction":'row'}}>
                          <Button onclick={async ()=>{
                            const gameExecSettings = Settings.read().games?.[selectedGameId()]?.[selectedExec()];
                            let defaultPath:string|undefined;
                            
                            // First try path written in text input
                            if(fileTextInput?.value?.trim()?.length) {
                              const relativeConvertedToAbsolutePath = path.join(gameExecSettings?.path||'', fileTextInput.value)
                              if(fsSync.existsSync(fileTextInput.value)) {
                                defaultPath = fileTextInput.value;
                              } else if(fsSync.existsSync(relativeConvertedToAbsolutePath)) {
                                defaultPath = relativeConvertedToAbsolutePath;
                              }
                            }
                            // If not found path from text input, try user configured exec resPath, or exec path
                            if(!defaultPath?.length) {
                              if(param?.res && gameExecSettings?.resPath?.[param?.res]?.length) {
                                defaultPath = gameExecSettings?.resPath?.[param?.res]
                              } else if(gameExecSettings?.path?.length) {
                                defaultPath = gameExecSettings!.path
                              }
                            }
                            
                            const result = await electronRemote.dialog.showOpenDialog({
                              properties: ['openFile'],
                              defaultPath: defaultPath,
                              ...(param.props||{})
                            });
                            try {
                              const newPath = path.resolve(result.filePaths?.[0]);
                              if(newPath?.length) {
                                fileTextInput.value = newPath;
                                SetCaretAtEnd(fileTextInput);
                              }
                            } catch(e) {}
                          }}>Locate</Button>
                          <Form.Control ref={fileTextInput!} {...(param.props||{})} type="text" data-type={param.type} id={elementId} />
                        </span>
                      } else if(param.type === 'dirfiles') {
                        const gamePath = Settings?.read()?.games?.[selectedGameId()]?.[selectedExec()]?.path;
                        let files:string[];
                        try {
                          if(!gamePath) throw '';
                          const ignoredGlobDirs = [`**/${NNLinks_DirName}/**`];
                          if(param?.ignore?.length) {
                            if(Array.isArray(param.ignore)) {
                              ignoredGlobDirs.push(...param.ignore);
                            } else if(typeof param.ignore === 'string') {
                              ignoredGlobDirs.push(param.ignore);
                            }
                          }

                          files = glob.globSync(param.path, {nocase:true,
                            cwd:gamePath,
                            ignore:ignoredGlobDirs,
                            ...(param.globOpts||{})
                          });
                          if(Array.isArray(param.extenions)) {
                            files = files.filter(f=>param.extenions!.includes(path.parse(f).ext.toLowerCase()));
                          }
                          if(param.excludePath) {
                            const exclude = (Array.isArray(param.excludePath)?
                              param.excludePath:
                              [param.excludePath]).
                              map(p=>p.toLowerCase());
                            files = files.filter(f=>!exclude.includes(f.toLowerCase()));
                          }
                          if(param.stripExt) {
                            files = files.map(f=>f.match(/(.*)\.[^.]+$/)?.[1] || f);
                          }
                          if(param.stripPath) {
                            files = files.map(f=>{
                              const parsed = path.parse(f);
                              return `${parsed.name}${parsed.ext}`
                            });
                          }
                        } catch(e) {
                          files = [];
                        }
      
                        element = <>
                          <Form.Select data-type={param.type} id={elementId}>
                            <For each={files}>
                              {(file)=>{
                                return <option value={file}>{file}</option>
                              }}
                            </For>
                          </Form.Select>
                        </>
                      }
      
                      if(element != null) {
                          return (
                              <div style={{display:'flex', "flex-direction":"row", "align-items": "center"}}>
                                  {param.optional?
                                      <OptionalGameParamCmp description={param.description} label={param.label} id={elementId} children={[element]} />:
                                      <RequiredGameParamCmp description={param.description} label={param.label} id={elementId} children={[element]} />}
                              </div>)
                      } else return null;
                    }}
                </For>;
                
                if(section.sectionName) {
                  if(!Object.keys(section.params).length) return <></>;

                  return <Accordion defaultActiveKey="-1">
                    <Accordion.Item eventKey="0">
                      <Accordion.Header>{section.sectionName}</Accordion.Header>
                      <Accordion.Body>
                        {sectionParamElements}
                      </Accordion.Body>
                    </Accordion.Item>
                  </Accordion>
                }
    
                return sectionParamElements;
            }}
        </For>
    </div>

    <Show when={!isInClientEditMode && hasModeAndExec() && resolvedMode()==='multiplayer'}>
      <div>
        <Form.Control
          placeholder="Room Name"
          type="plain"
          value={roomName()}
          onKeyUp={(element)=>{setRoomName((element.target as HTMLInputElement).value)}}
        />
      </div>

      <div>
        <Form.Control
          placeholder="Password"
          type="password"
          id="password"
          value={password()}
          onFocus={()=>{setPassword('')}}
          onKeyUp={(element)=>{setPassword((element.target as HTMLInputElement).value)}}
        />
      </div>
      
      <Show when={!props?.preConfig?.editChannel?.length}>
        <div>
          <Form.Check 
            type="switch"
            label="Hide Room"
            checked={hideRoom()}
            onChange={(element)=>{setHideRoom(!!(element.target as HTMLInputElement)?.checked)}}
          />
        </div>
      </Show>

      <div style={{"display":"flex","flex-direction":'row',"align-items": "center"}}>
        <label for="max-players">Max Players</label>
        <input type="number" value={maxPlayers()} onchange={(el)=>{
          const inputEl = el.target as HTMLInputElement;
          const value = parseInt(inputEl?.value);
          
          if(value<+inputEl.min) {
            setMaxPlayers(+inputEl.min)
          } else if(value>+inputEl.max) {
            setMaxPlayers(+inputEl.max)
          } else {
            setMaxPlayers(value)
          }
          
          }} step={1} max={execDef()?.maxPlayers||defaultMaxPlayers} min={2} />
        <Form.Range id="max-players" value={maxPlayers()} oninput={(el:any)=>{setMaxPlayers(parseInt(el.target.value))}} step={1} max={execDef()?.maxPlayers||defaultMaxPlayers} min={2} />
      </div>

    </Show>

    <Show when={selectedExec()?.length}><div>
      <hr/>
      <h5>Extra Manual Arguments</h5>
      <form id="manual-param-controls" style={{display:"flex", "flex-direction":"row"}}>
        <input id={elementId.newParamText} type="text" placeholder="Insert SINGLE Param Here" style={{"flex-grow":1}} />
        <input id={elementId.newParamOrder} type="number" placeholder="Order" style={{width:'60pt'}} />
        <select id={elementId.newParamFor} style={{display: resolvedMode()==='multiplayer'?'':'none'}}>
          <Show when={!isInClientEditMode}>
            <option value="shared">Everyone</option>
            <option value="client-only-from-host">Clients</option>
            <option value="host-only-private">Server</option>
          </Show>
          <option value="private">Me</option>
        </select>
        <Button type="submit" onClick={async (event)=>{
          event.preventDefault();

          const el_text = document.getElementById(elementId.newParamText) as HTMLInputElement;
          const el_order = document.getElementById(elementId.newParamOrder) as HTMLInputElement;
          const el_for = document.getElementById(elementId.newParamFor) as HTMLSelectElement;
          if(!el_text?.value?.length) return;

          const forValue = (el_for?.selectedOptions?.[0]?.value || 'private') as GameDefsType.ParamType.ParamFor;

          const chosenOrder = parseInt(el_order?.value || '0');

          const argSpaceParts = el_text.value.split(/\s+/);
          const argsToAdd = [] as string[];
          if(argSpaceParts.length>1) {
            const popres = await DialogMgr.addPopup({
              title: 'Argument Contains Space',
              body: ()=><div>
                This argument contains space.<br/>
                Would you like to split it to multiple args?<br/>
                Or Keep it as one arg?<br/>
                <b>(Usually you want to split it to multiple args)</b>
              </div>,
              buttons:[{body:"Split Multiple"}, {body:"One Arg"}]
            });

            if(popres.buttonIndex) {
              argsToAdd.push(el_text.value);
            } else {
              argsToAdd.push(...argSpaceParts);
            }
          } else {
            argsToAdd.push(el_text.value);
          }

          manualParams.push({
            args: argsToAdd,
            id:'_m_'+createRandomHash(),
            for: forValue,
            order:chosenOrder
          })

          manualParams.sort((a,b)=>{
            const orderNum = a.order!-b.order!;
            if(orderNum===0) {
              return manualParams.indexOf(a)>manualParams.indexOf(b)? 1:-1
            } else {
              return orderNum
            }
          })

          el_text.value='';
        }}>Add</Button>
      </form>
      
      <div id={elementId.manualParamsContainer}></div>

      <ol>
        <For each={manualParams}>
          {(p,idx)=>{
            return <li style={{"margin-top":"2pt"}}>{p.args.join(' ')} {p.args.length>1?'(multiple)':''} {resolvedMode()==='multiplayer'?`(${p.for})`:''} {p.order!=0?`[${p.order}]`:''} <Button onClick={()=>{manualParams.splice(idx(),1)}}>❌</Button></li>
          }}
        </For>
      </ol>
      
    </div></Show>
  
    </Modal.Body>
    <Modal.Footer>
      
      {Settings?.gameDefs?.games?.[selectedGameId()]?.executables?.[selectedExec()]?.tips?
        <Button variant="secondary" onClick={()=>storeActions.showTipsForDef(selectedGameId(), selectedExec())}>💡Tips</Button>:undefined
      }
      
      <Button variant="secondary" onClick={props.onCancel}>❌Cancel</Button>
      <Show when={hasModeAndExec()}>
        <Button variant="primary" onClick={async ()=>{

          const gameId = selectedGameId(), execId = selectedExec();
          if(!mainStore.hasGameExec(gameId, execId)) {
            DialogMgr.addPopup({
              title: "Error",
              icon: "error",
              body: `You did not setup ${Settings?.gameDefs?.games?.[gameId]?.name} ${Settings?.gameDefs?.games?.[gameId]?.executables?.[execId]?.name}\r\nPlease locate your game executable using the menu`,
              buttons:[{body:"ok"}]
            })
            return
          }

          if(roomName()?.length>maxChannelNameLength) {
            DialogMgr.addPopup({
              title: "Error",
              icon: "error",
              body: `Channel name max length is ${maxChannelNameLength}`,
              buttons:[{body:"ok"}]
            })
            return
          }
          
          const argsInfo:LaunchArgType[] = []

          const settings = Settings.read()
          const gameExecSettings = settings.games?.[gameId][execId]!;
          
          const baseEvalObject = {Settings: settings}

          const elements = document.querySelectorAll(`[id^='${CSS.escape(elementIdPrefix)}']`);
          const pnamesShown:string[] = [];
          try {
            elements.forEach(el=>{
              const pname = el.id.split(elementIdPrefix)[1];
              const paramDef = paramsForExec()[pname];
              pnamesShown.push(pname);
            
              const type = (el as any)['type'];
              if((paramDef.type==='choice' || paramDef.type==='dirfiles') && !paramDef.allowMultiple && type==='select-one') {
                {/* TODO Add multi-select support, and repeatable params */}
                const selectEl = el as HTMLSelectElement;
                let selectedValue = selectEl?.selectedOptions?.[0]?.value || undefined;
                if(paramDef.type==='choice') {
                  try {
                    selectedValue = JSON.parse(selectedValue!);
                  } catch(e) {
                    selectedValue = undefined
                  }
                }
  
                const argInfo: LaunchArgType = {
                  id:pname,
                  label:paramDef.label,
                  valueLabel:selectEl.selectedOptions?.[0]?.innerText || '',
                  args: [],
                  html: {prop:'selectedIndex', value:selectEl.selectedIndex},
                  ...(paramDef.for && {for:paramDef.for}),
                  ...(paramDef.order!=null && {order:paramDef.order}),
                  ...(paramDef.syncOnly!=null && {syncOnly:paramDef.syncOnly})
                }
  
                if(Array.isArray(paramDef.value)) {
                  const selectedValueArray = Array.isArray(selectedValue)?selectedValue:[selectedValue];
                  for(let i=0;i<paramDef.value.length;i++) {
                    const p = paramDef.value[i];
                    const evaluated = NNEvalWrapArray(p, {...baseEvalObject, value:selectedValueArray})
                    if(evaluated != null) argInfo.args.push(...evaluated)
                  }
                } else {
                  const evaluated = NNEvalWrapArray(paramDef.value as string, {...baseEvalObject, value:selectedValue})
                  if(evaluated != null) argInfo.args.push(...evaluated)
                }
                argsInfo.push(argInfo);
              } else if(paramDef.type==='boolean' && type==='checkbox') {
  
                const argsArr:string[] = []
  
                const checkbox = el as HTMLInputElement;
                const checked = checkbox.checked
                if(checked || paramDef.addFalse) {
                  const paramValue = paramDef.value;
                  if(Array.isArray(paramDef.value)) {
                    for(const p of paramValue) {
                      const evaluated = NNEvalWrapArray(p, {...baseEvalObject, value:checked})
                      if(evaluated != null) argsArr.push(...evaluated)
                    }
                  } else {
                    const evaluated = NNEvalWrapArray(paramValue as string, {...baseEvalObject, value:checked})
                    if(evaluated != null) argsArr.push(...evaluated)
                  }
                }
                if(argsArr.length) {
                  argsInfo.push({
                    id:pname,
                    label:paramDef.label,
                    valueLabel:checked?'yes':'no',
                    args: argsArr,
                    html: {prop:'checked', value:checked},
                    ...(paramDef.for && {for:paramDef.for}),
                    ...(paramDef.order!=null && {order:paramDef.order}),
                    ...(paramDef.syncOnly!=null && {syncOnly:paramDef.syncOnly})
                  })
                }
              } else if(paramDef.type==='numrange' && type==='range') {
                const rangeEl = el as HTMLInputElement;
                const value = parseInt(rangeEl.value);
  
                const argsArr:string[] = []
  
                if(Array.isArray(paramDef.value)) {
                  for(const p of paramDef.value) {
                    const evaluated = NNEvalWrapArray(p, {...baseEvalObject, value})
                    if(evaluated != null) argsArr.push(...evaluated)
                  }
                } else {
                  const evaluated = NNEvalWrapArray(paramDef.value as string, {...baseEvalObject, value})
                  if(evaluated != null) argsArr.push(...evaluated)
                }
  
                const argInfo: LaunchArgType = {
                  id:pname,
                  label:paramDef.label,
                  valueLabel:''+value,
                  args: argsArr,
                  html: {prop:'value', value:rangeEl.value},
                  ...(paramDef.for && {for:paramDef.for}),
                  ...(paramDef.order!=null && {order:paramDef.order}),
                  ...(paramDef.syncOnly!=null && {syncOnly:paramDef.syncOnly})
                }
                argsInfo.push(argInfo);
              } else if((paramDef.type==='text' || paramDef.type==='file') && type==='text') {

                const textEl = el as HTMLInputElement;
                let value:string|undefined = textEl.value
  
                const argsArr:string[] = []
  
                if(Array.isArray(paramDef.value)) {
                  for(const p of paramDef.value) {
                    const evaluated = NNEvalWrapArray(p, {...baseEvalObject, value})
                    if(evaluated != null) argsArr.push(...evaluated)
                  }
                } else {
                  const evaluated = NNEvalWrapArray(paramDef.value as string, {...baseEvalObject, value})
                  if(evaluated != null) argsArr.push(...evaluated)
                }
                
                if(paramDef.type==='file') {
                  // If file path chosen by host is not absolute, assume it's relative to game dir
                  if(value?.length && !path.isAbsolute(value) && gameExecSettings?.path?.length) {
                      value = path.join(gameExecSettings.path, value);
                  }

                  if(!value?.length || !fsSync.existsSync(value)) {
                    DialogMgr.addPopup({
                      closeButton:true, buttons:[{body:'OK'}],
                      title:"Chosen file does not exist",
                      body: `The file "${value}" does not exist.`
                    })
                    throw '';
                  }

                  // this is slow, but we need to do it to get the file md5 hash
                  const fileBuffer = fsSync.readFileSync(value);
                  const md5 = crypto.createHash('md5').update(fileBuffer).digest("hex");
                  const filename = path.basename(value);

                  const argInfo: LaunchArgType_File = {
                    id:pname,
                    label:paramDef.label,
                    valueLabel:filename,
                    args: argsArr,
                    meta: {
                      md5,
                      name: filename,
                      size: fileBuffer.buffer.byteLength,
                    },
                    html: {prop:'value', value:textEl.value},
                    ...(paramDef.for && {for:paramDef.for}),
                    ...(paramDef.order!=null && {order:paramDef.order}),
                    ...(paramDef.syncOnly!=null && {syncOnly:paramDef.syncOnly})
                  }
                  argsInfo.push(argInfo);
                } else if(paramDef.type === 'text') {
                  const argInfo: LaunchArgType = {
                    id:pname,
                    label:paramDef.label,
                    valueLabel:''+value,
                    args: argsArr,
                    html: {prop:'value', value:textEl.value},
                    ...(paramDef.for && {for:paramDef.for}),
                    ...(paramDef.order!=null && {order:paramDef.order}),
                    ...(paramDef.syncOnly!=null && {syncOnly:paramDef.syncOnly})
                  }
                  argsInfo.push(argInfo);
                }
              }
            })
          } catch(e) {
            return;
          }
          
          // Manual args
          const orderAndFor2Param:{[orderAndFor:string]: LaunchArgType[]} = {};
          for(const p of manualParams) {
            const key = p.order!+':'+p.for!;
            if(!orderAndFor2Param[key]) orderAndFor2Param[key] = [];
            orderAndFor2Param[key].push(p);
          }

          // put manual args under the same object array, if they have the same "row" and "order"
          const manualArgsResult:LaunchArgType[] = [];
          for(const orderAndFor in orderAndFor2Param) {
            const param = orderAndFor2Param[orderAndFor];

            const arg = param[0];
            for(let i=1;i<param.length;i++) {
              const p = param[i];
              arg.args.push(...p.args);
            }
            manualArgsResult.push(arg);
          }
          argsInfo.push(...manualArgsResult)

          // save
          const savedParams: {[pid: string]: LaunchDefaults_NormalArg|LaunchDefaults_ManualArg} = {}
          for(const arg of argsInfo) {
            const prop = arg?.html?.prop;
            if(prop == null) {
              if(arg.id.startsWith('_m_')) { // manual arg
                savedParams[arg.id] = {
                  args: arg.args,
                  for: arg.for,
                  order: arg.order,
                  valueLabel: arg.valueLabel
                }
              }
            } else {
              savedParams[arg.id] = {
                value: arg.html!.value,
                htmlProp: prop,
                for: arg.for,
                args: arg.args,
                valueLabel: arg.valueLabel
              }
            }
          }
          try {
            const launchDefaults = (await Settings.launchDefaults());
            
            if(!launchDefaults.games) launchDefaults.games = {};
            if(!launchDefaults.games[selectedGameId()]) launchDefaults.games[selectedGameId()] = {}
            if(!launchDefaults.games[selectedGameId()].executables) launchDefaults.games[selectedGameId()].executables = {}
            if(!launchDefaults.games[selectedGameId()].executables?.[selectedExec()]) launchDefaults.games[selectedGameId()].executables![selectedExec()] = {}
            if(!launchDefaults.games[selectedGameId()].executables?.[selectedExec()]?.params) launchDefaults.games[selectedGameId()].executables![selectedExec()].params = {}

            const existingParamsFromPreviousSave = launchDefaults.games[selectedGameId()].executables![selectedExec()].params! || {};
            if(isInClientEditMode) {
              const prevParamIds = pnamesShown.concat(Object.keys(existingParamsFromPreviousSave).filter(p=>p.startsWith("_m_"))) // append manual params

              // If is in client edit mode, only merge settings instead of replace, because only some of the params are shown in client edit mode
              Object.assign(existingParamsFromPreviousSave, savedParams);
              // Delete missing props that are presented to user in client edit mode
              
              for(const shownParamId of prevParamIds) {
                if(!(shownParamId in savedParams)) {
                  delete existingParamsFromPreviousSave[shownParamId];
                }
              }
            } else {
              launchDefaults.games[selectedGameId()].executables![selectedExec()] = {
                params: savedParams,
                maxPlayers: maxPlayers(),
                mode: resolvedMode() as GameDefsType.GameModes,
              };
              launchDefaults.roomName = roomName();
              launchDefaults.roomPass = password();
              launchDefaults.hideRoom = hideRoom();
              launchDefaults.defaultGameAndExec = {gameId: selectedGameId(), execId: selectedExec()};
            }
            
            (await Settings.launchDefaults({save:true}));
          } catch(e) {}
          // end of save
          
          
          props?.onSubmit?.({
            game: selectedGameId(),
            exec: selectedExec(),
            mode: resolvedMode() as GameDefsType.GameModes,
            args: argsInfo,
            maxPlayers: maxPlayers(),
            roomName: roomName(),
            password: password(),
            hideRoom: hideRoom(),
            settings,
            editChannel: props?.preConfig?.editChannel
          });
        }}>{props?.preConfig?.editChannel != null? 'Update':(selectedMode()==='multiplayer'?"🎮Create Room":"🎮Launch")} </Button>

      </Show>
    </Modal.Footer>
  </Modal>
}

export default LaunchGameModal;