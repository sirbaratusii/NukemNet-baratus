import { type ChildProcess } from "child_process";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import { CmdGameRoom_TerminateProc } from "./commands-gameroom";
import { IRCEvent_ProcessTerminatedByHost } from "./irc-events/ircEvents";
import { KillProc } from "./lib/KillProc";
import { Settings } from "./Settings";
import { ircClient, mainStore } from "./store/store";

const gameProcessesRunning = new Set<ChildProcess>();

export function addProc(proc:ChildProcess) {
  gameProcessesRunning.add(proc);
}

export function delProc(proc:ChildProcess) {
  KillProc(proc).catch(()=>null);
  gameProcessesRunning.delete(proc);
}

export function destroyAllRunningGameProcesses() {
  for(const proc of Array.from(gameProcessesRunning)) {
    delProc(proc);
  }
}

export async function terminateGameProcess(includingSinglePlayerProcesses:boolean, channel?:string, imHost?:boolean) {

  if(includingSinglePlayerProcesses) {
    destroyAllRunningGameProcesses();
  }
  Settings.playSoundEffect('terminated');

  if(!channel?.length) {
    const gameRoom = mainStore.getGameRoom();
    if(gameRoom?.chan?.length) {
        channel = gameRoom.chan;
    } else {
      return;
    }
  }

  if(imHost == null)
    imHost = mainStore.imHostInChannel(channel);

  const lchan = channel.toLowerCase();
  const chanDetails = mainStore.joinedChannels[lchan];

  await KillProc(chanDetails.proc!);

  if(imHost) {
    await IrcClientExtra.sendNNCmdTo(ircClient, lchan, {
      // send all kill
      op:'terminateProc'
    } as CmdGameRoom_TerminateProc);

    mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {
      type: 'custom_processTerminatedByHost',
      ts: Date.now(),
      channel
    } as IRCEvent_ProcessTerminatedByHost);
  }
}