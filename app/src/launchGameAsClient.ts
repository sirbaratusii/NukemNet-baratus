import { Client } from "matrix-org-irc";
import { IRCEvent_ProcessStartedByHost, IRCEvent_ProcessTerminatedByHost } from "./irc-events/ircEvents";
import { launchGame } from "./launchGame";
import { isIpLocal } from "./lib/IpDetails";
import { mainStore } from "./store/store";
import storeActions from "./store/storeActions";

export async function clientLaunchGame(chan:string, hostNick:string, ircClient:Client) {

    const lchan = chan.toLowerCase();
    const lHostNick = hostNick.toLowerCase();

    const chanData = mainStore.joinedChannels?.[lchan];
    if(!chanData) return
    const game = chanData?.game!;
    if(!game) return

    const channelIsAGameRoom_AndSenderIsHost_AndImParticipating =
    storeActions.isChannelGameRoom(chan) &&
    storeActions.getGameHostNickForChannelImIn(lchan)?.toLowerCase() === lHostNick &&
    game.participantNicks[mainStore.myNick.toLowerCase()] != null

    if(!channelIsAGameRoom_AndSenderIsHost_AndImParticipating) return;

    mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {
        type: 'custom_processStartedByHost',
        ts: Date.now(),
        channel:lchan
      } as IRCEvent_ProcessStartedByHost);
    storeActions.showStartGameAnimation();

    const hostNickGameData = game.participantNicks[lHostNick];
    const hostNickChanData = chanData.nicks[lHostNick];

    let foundWorkingTunnel = false;

    const isHostLanIp = !!(chanData?.nicks?.[lHostNick]?.lan?.detectedLanIp?.length);

    let ownerResolvedIp = isHostLanIp?chanData.nicks[lHostNick].lan.detectedLanIp:(mainStore.userExternalIpV4(lHostNick))
    let ownerPort = hostNickGameData.ipsAndPorts.gamePort!;
    
    // If resolved LAN ip, use that.
    if(isHostLanIp) {
        foundWorkingTunnel = true;
    } else {
        // If established STUN Tunnel, use that instead.
        const hostStunEndpont = chanData.nicks[lHostNick].stunEndpoint;
        if(hostStunEndpont && hostStunEndpont.otherReceivedMyHandshake && hostNickGameData.stun?.port) {
            ownerResolvedIp = mainStore.localhostIp4
            ownerPort = hostStunEndpont.tunnel.address().port
            foundWorkingTunnel = true;
        }
        // If established IPv6 Tunnel, use that instead.
        if(mainStore.userOnIpv6(hostNick) && hostNickChanData?.localIpv6Tunnel?.r) { // r means established
            ownerResolvedIp = mainStore.localhostIp4;
            ownerPort = hostNickChanData.localIpv6Tunnel?.udpSocket?.address()?.port!;
            foundWorkingTunnel = true;
        }
    }

    const execDef = mainStore.execDefForChannel(lchan);
    const otherResolvedIpsAndPorts = execDef?.isPeerToPeer? Object.values(await mainStore.getResolvedIpsInGameRoom(lchan, mainStore.myNick.toLowerCase())):[];

    const shouldUseDedicatedServer = game.dedicatedServer && !(isHostLanIp || isIpLocal(ownerResolvedIp!)) && (game.dedicatedServer?.forced || !foundWorkingTunnel);

    const hostIpDetails = {
        // connect to dedicated server only if host is not on same lan, if host is on the same lan, just connect to his local lan ip
        ip: shouldUseDedicatedServer? game.dedicatedServer!.ipOrHostname : ownerResolvedIp!,
        port: shouldUseDedicatedServer? parseInt(game.dedicatedServer!.port as any) : ownerPort
    };

    const res = await launchGame({
        args: game.params, 
        exec: game.publicDetails.execId, 
        game: game.publicDetails.gameId,
        mode: 'multiplayer'
    }, false, Object.keys(game.participantNicks).length, hostIpDetails,
    game.dedicatedServer!=null,
    otherResolvedIpsAndPorts,
    chan)
    if(!res) return;

    const proc = res.proc;

    proc?.once('close', ()=>{
        delete chanData?.proc;
        const isExecRunning = false;
        game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
        mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
    })
    proc?.once('spawn', ()=>{
        chanData.proc = proc;
        const isExecRunning = true;
        game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
        mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
    })
}