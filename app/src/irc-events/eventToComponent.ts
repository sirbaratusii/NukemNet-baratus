import { IrcEventCmp_ChangeNick } from "./components/change-nick";
import { IrcEventCmpCustom_Disconnected } from "./components/custom_disconnected";
import { IrcEventCmpCustom_GameDetails } from "./components/custom_gameDetails";
import { IrcEventCmpCustom_Raw } from "./components/custom_raw";
import { IrcEventCmp_Invite } from "./components/invite";
import { IrcEventCmp_Join } from "./components/join";
import { IrcEventCmp_Kick } from "./components/kick";
import { IrcEventCmp_Kill } from "./components/kill";
import { IrcEventCmp_Message } from "./components/message";
import { IrcEventCmp_Mode } from "./components/mode";
import { IrcEventCmp_Motd } from "./components/motd";
import { IrcEventCmp_Notice } from "./components/notice";
import { IrcEventCmp_Part } from "./components/part";
import { IrcEventCmpCustom_ProcessStartedByHost } from "./components/ProcessStartedBtHost";
import { IrcEventCmpCustom_ProcessTerminatedByHost } from "./components/ProcessTerminatedByHost";
import { IrcEventCmp_Quit } from "./components/quit";
import { IrcEventCmp_Registered } from "./components/registered";
import { IrcEventCmp_Topic } from "./components/topic";
import { IrcEventCmp_Whois } from "./components/whois";

export const eventTypeToComponentMapping = {
    'message': IrcEventCmp_Message,
    'nick': IrcEventCmp_ChangeNick,
    'topic':IrcEventCmp_Topic,
    'quit':IrcEventCmp_Quit,
    'kill':IrcEventCmp_Kill,
    'join':IrcEventCmp_Join,
    'part':IrcEventCmp_Part,
    'kick':IrcEventCmp_Kick,
    '+mode':null,
    '-mode':null,
    'invite':IrcEventCmp_Invite,
    'notice':IrcEventCmp_Notice,
    'whois':IrcEventCmp_Whois,
    'motd':IrcEventCmp_Motd,
    'registered':IrcEventCmp_Registered,
    'custom_disconnected':IrcEventCmpCustom_Disconnected,
    'custom_raw': IrcEventCmpCustom_Raw,
    'custom_gameDetails':IrcEventCmpCustom_GameDetails,
    "custom_processTerminatedByHost": IrcEventCmpCustom_ProcessTerminatedByHost,
    "custom_processStartedByHost": IrcEventCmpCustom_ProcessStartedByHost,
    'mode': IrcEventCmp_Mode
}

export type IRCEvent_TypeNames = keyof typeof eventTypeToComponentMapping;
export const IRCEvent_AllTypeNames = Object.keys(eventTypeToComponentMapping) as IRCEvent_TypeNames[]
