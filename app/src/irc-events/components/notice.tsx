import {Component} from 'solid-js'
import { IRCStylizeText } from '../../lib/IrcStyling';
import { IRCEvent_Notice } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Notice:Component<IRCEvent_Notice> = (props)=>{
    const isFromServer = props.nick == null;
    const preText = isFromServer?'notice:':`<${props.nick}>`

    const text = IRCStylizeText(props.text);
    return <IrcEventCmp_Base {...props}>{preText} {text}</IrcEventCmp_Base>
}
