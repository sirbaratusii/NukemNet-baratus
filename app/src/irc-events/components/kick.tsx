import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Kick } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Kick:Component<IRCEvent_Kick> = (props)=>{
    const kickerStyle = {"background-color":colorFromStr(props.kicked)}
    const kickedStyle = {"background-color":colorFromStr(props.kicker)}

    if(props.kickedIsMe) {
        if(props.kickerIsMe) {
            return <IrcEventCmp_Base {...props}> You have kicked yourself out of {props.channel}</IrcEventCmp_Base>
        } else {
            return <IrcEventCmp_Base {...props}> You were kicked from {props.channel} by <a style={kickerStyle}>{props.kicker}</a></IrcEventCmp_Base>
        }
    } else {
        if(props.kickerIsMe) {
            return <IrcEventCmp_Base {...props}> You have kicked <a style={kickedStyle}>{props.kicked}</a> out of {props.channel}</IrcEventCmp_Base>
        } else {
            return <IrcEventCmp_Base {...props}> <a style={kickedStyle}>{props.kicked}</a> was kicked from {props.channel} by <a style={kickerStyle}>{props.kicker}</a></IrcEventCmp_Base>
        }
    }

    // TODO click nick to private msg
}