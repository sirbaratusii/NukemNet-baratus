import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Invite } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Invite:Component<IRCEvent_Invite> = (props)=>{
    const style = {"background-color":colorFromStr(props.from)}
    return <IrcEventCmp_Base {...props}> You were invited to <a onClick={props.clickChan} href="javascript:;">{props.channel}</a> by <a style={style}>{props.from}</a></IrcEventCmp_Base>
}