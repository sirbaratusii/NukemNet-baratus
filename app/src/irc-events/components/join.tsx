import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Join } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Join:Component<IRCEvent_Join> = (props)=>{
    if(props.isMe) {
        return <IrcEventCmp_Base {...props}>You have joined channel {props.channel}
        {props.opSign?.length?'with parm: '+props.opSign:''}
        {props.reason?.length?'('+props.reason+')':''}
        </IrcEventCmp_Base>
    }

    const style = {"background-color":colorFromStr(props.nick)}
    // TODO click nick to private msg
    return <IrcEventCmp_Base {...props}>{props.opSign}<a style={style}>{props.nick}</a> joined channel {props.channel} {props?.reason?.length>0? ('('+props.reason+')'):''}</IrcEventCmp_Base>
}