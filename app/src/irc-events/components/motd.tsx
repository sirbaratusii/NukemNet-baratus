import {Component, For} from 'solid-js'
import { IRCStylizeText } from '../../lib/IrcStyling';
import { IRCEvent_Motd } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Motd:Component<IRCEvent_Motd> = (props)=>{
    const messages:any[] = props.message.split('-').map(t=>IRCStylizeText(t))
    return <IrcEventCmp_Base {...props}>
        <For each={messages}>
            {(m:any)=>{return <div>{m}</div>}}
        </For>
    </IrcEventCmp_Base>
}