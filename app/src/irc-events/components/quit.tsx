import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Quit } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Quit:Component<IRCEvent_Quit> = (props)=>{
    const style = {"background-color":colorFromStr(props.nick)}
    return <IrcEventCmp_Base {...props}> <a style={style}>{props.nick}</a> ({props.userId}) quit the server {props.reason?.length? props.reason:''}</IrcEventCmp_Base>
}