import {Component} from 'solid-js'
import { IrcUserMod_CmdToSign } from '../../../../server/src/common/constants';
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Mode } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Mode:Component<IRCEvent_Mode> = (props)=>{
    if(!props?.channel?.length) return
    if(props?.on?.startsWith('#')) return // TODO support channel modes

    const byStyle = {"background-color":colorFromStr(props.byUser)}
    const onStyle = {"background-color":colorFromStr(props.on)}
    const sign = props.add? '+':'-'
    const signAndMod = `${sign}${props.mode}`;

    const isModOnUser = IrcUserMod_CmdToSign[props.mode] != null;
    // byUser can be missing
    if(isModOnUser) {
        return <IrcEventCmp_Base {...props}> <a style={byStyle}>{props.byUser||''}</a> sets mode {signAndMod} on <a style={onStyle}>{props.on}</a> in {props.channel}</IrcEventCmp_Base>
    } else {
        // On channel
        return <IrcEventCmp_Base {...props}> <a style={byStyle}>{props.byUser||''}</a> sets mode {signAndMod} ({props.on}) on {props.channel}</IrcEventCmp_Base>
    }

    // TODO click nick to private msg
}