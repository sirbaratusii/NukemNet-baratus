import {Component} from 'solid-js'
import { IRCEvent_Whois } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Whois:Component<IRCEvent_Whois> = (props)=>{
    return <IrcEventCmp_Base {...props}> whois info: {JSON.stringify(props.info,null,2)}</IrcEventCmp_Base>

}
