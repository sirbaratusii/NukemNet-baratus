import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_Part } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Part:Component<IRCEvent_Part> = (props)=>{
    if(props.isMe) {
        return <IrcEventCmp_Base {...props}>You have left channel {props.channel}</IrcEventCmp_Base>
    }
    
    const style = {"background-color":colorFromStr(props.nick)}
    // TODO click nick to private msg
    return <IrcEventCmp_Base {...props}>{props.opSign}<a style={style}>{props.nick}</a> left channel {props.channel} {props?.reason?.length>0? ('('+props.reason+')'):''}</IrcEventCmp_Base>
}