import { LaunchGameModalResult } from "./LaunchGameModal";
import { child_process, path, fs, fsSync, Electron, crypto } from "./NodeLibs";
import { NNLinks_DirName, pathOfUserDir, pathOfUserTempDir, Settings } from "./Settings";
import { IpAndPort } from "./commands-gameroom";
import { GameDefsType } from "./GameDefsTypes";
import { LaunchArgType, LaunchArgType_File } from "./lib/LaunchArgType";
import { appLogger } from "./lib/Logger";
import { LaunchGameCmd_EditBinary, LaunchGameCmd_editIni, LaunchGameCmd_editSpaceDelimitedFile as LaunchGameCmd_editSpaceDelimitedFile } from "./LaunchGameCmds";
import { NNEval, NNEvalWrapArray } from "./lib/nneval";
import { ircClient, mainStore } from "./store/store";
import { DialogMgr } from "./Popups";
import { ChildProcess } from "child_process";
import { namedPipeChatSync } from "./lib/NamedPipeChatSync";
import { checkProcessRunSuccess, escapeShellArg, findFreeUdpTcpPortInRange, isWindows, resolveHostIpv4, testIfTcpPortIsBindable, testIfUdpPortIsBindable } from "./lib/utils";
import storeActions from "./store/storeActions";
import { createUDPTunnel_client } from './lib/kcp/kcp_client';
import { createUDPTunnel_server } from "./lib/kcp/kcp_server";
import { deepClone } from "./lib/deepClone";
import { addProc, delProc } from "./terminateProc";

type ChildProcess_SpawnArgs = Parameters<typeof child_process.spawn>;

export async function launchGame(result:LaunchGameModalResult, imHost:boolean, players:number, hostIpDetails?:IpAndPort, useDedicated?:boolean, othersIpDetails:IpAndPort[]=[], channelName?:string, hostAsClientToSelf:boolean=false, hostTunnelChosenPort?:number): Promise<{proc:ChildProcess, evalArgs:any, hostTunnelPort?:number}|undefined> {
    const allArgs = arguments;
    const cloneOfAllArgs = deepClone(Array.from(allArgs));
    
    // If game launches 2 instances, one for server and one for client connecting to self, and using UDPTunnel (tunneler), skip the the tunnel part to connect to self as client.
    if(hostIpDetails?.port != null && hostTunnelChosenPort != null) {
        hostIpDetails.port = hostTunnelChosenPort;
    }

    const settings = result.settings||Settings.read();
    const gameDef = Settings.gameDefs.games[result.game]
    const execDef = gameDef.executables[result.exec];
    const gameExecSettings = settings.games?.[result?.game]?.[result?.exec]!;
    if(!gameExecSettings) {
        DialogMgr.addPopup({
            title: 'Launch Failed',
            body: 'You have not configured this game yet',
            icon: 'error',
            buttons:[{body:"ok"}]
        })
        return
    }
    const gameExecDir = path.resolve(gameExecSettings?.path!);

    if(!gameExecDir?.length || !fsSync.existsSync(gameExecDir)) {
        DialogMgr.addPopup({
            title: 'Launch Failed',
            body: `game/exec dir missing for ${result?.game}:${result?.exec}`,
            icon: 'error',
            buttons:[{body:"ok"}]
        })
        return
    }

    const runnable = execDef.runnables[result.mode]!;
    const isDosbox = runnable?.runTool?.type === 'dosbox';
    const hasImgMount = (gameExecSettings?.dosbox?.imgmount?.length||0)>0;
    const imgMountDrive = typeof execDef?.mountImg === 'string'? execDef.mountImg : 'D';

    const argsWithOrder: LaunchArgType[] = [];
    
    const MyPort = Settings.getGamePort();

    const evalArgs = await Settings.getBaseEvalObject(settings) as any;
    // Removing spaces from in-game nick name if using dosbox, some games don't handle "param value" correctly (e.g DOS Blood)
    evalArgs.MyName = (isDosbox?settings?.InGamePlayerName?.replace?.(/\s/g, ''):settings?.InGamePlayerName) || 'NNGamer',
    evalArgs.GameRoom = {
        NumberOfPlayers: players,
        MyPort: MyPort,
        DestIp: hostIpDetails?.ip,
        DestPort: hostIpDetails?.port,
        ImHost: imHost,
        Mode: result.mode,
        Args: argsWithOrder
    }
    if(hasImgMount) {
        evalArgs.imgMountDrive = imgMountDrive;
    }
    evalArgs.GameName = gameDef.name;
    evalArgs.GameExecName = execDef.name;
    evalArgs.GameRoom.ParamDefs = {};
    for(const p in execDef.parameters) {    
        const paramDef = execDef.parameters[p];    
        evalArgs.GameRoom.ParamDefs[p] = paramDef;
    }
    evalArgs.GameDir = gameExecDir;
    if(result.maxPlayers && result.maxPlayers>=2) {
        evalArgs.GameRoom.MaxPlayers = result.maxPlayers;
    }
    if(channelName?.length) {
        evalArgs.GameRoom.ChanName = channelName.substring(1);
    }
    if(execDef?.networking?.allocate_RandomPort) {
        evalArgs.GameRoom.RandomPort = await findFreeUdpTcpPortInRange(undefined,undefined, [MyPort]);
    }
    const needsTempDir = execDef?.needsTempDir ||
        (isDosbox && result.args.find(arg=>{
            const v = execDef?.parameters?.[arg?.id] as GameDefsType.ParamType.File;
            const overlayFileUsed = v?.type=='file' && (v as GameDefsType.ParamType.File)?.symlink === 'overlay';
            return overlayFileUsed;
        }))

    if(needsTempDir) {
        const randomName = crypto.randomBytes(8).toString('hex')
        const randomDirPath = path.resolve(pathOfUserTempDir, result.game, result.exec, randomName);
        try {await fs.mkdir(randomDirPath, {recursive: true});} catch(e) {}
        evalArgs.TempDir = randomDirPath;
    }

    const onHostMachine = imHost || hostAsClientToSelf;

    let hasOverlayFilesInTempDir = false

    const GameRoomParamIdToValue:{[paramId:string]: string[]} = {}
    // Sort, Filter, and Add arguments.
    for(const arg of result.args) {
        if(!Settings.filterParam_For(imHost, arg.for)) continue;

        const clonedParam = deepClone(arg) as LaunchArgType;

        // Remove empty/null/undefined params
        clonedParam.args = clonedParam.args.filter(a=>{
            if(a==null) return false;
            if(`${a}`.length<=0) return false;
            return true;
        })

        const paramDef = execDef?.parameters?.[arg.id];
        // special case, handle 'file' type arg
        if(paramDef?.type === 'file') {
            const argdef = arg as LaunchArgType_File;
            let filePath = await Settings.findResFile(result.game, result.exec, paramDef.res, onHostMachine?argdef.html?.value:argdef.meta.name, argdef.meta.md5);
            if(filePath instanceof Object) {
                DialogMgr.addPopup({
                    title: `File ${argdef.meta.name} Error`,
                    body: filePath.message,
                });
                return;
            }
            if(paramDef?.symlink || isDosbox) { // On dosbox, we must always use relative path to gamedir (the dir we mount on dosbox)

                const useOverlayFile = paramDef?.symlink==='overlay' && isDosbox;

                const fileName = argdef.meta.name;
                const nukemnetLinksPath = path.join(gameExecDir, NNLinks_DirName);
                const linkPath = useOverlayFile?
                    path.join(evalArgs.TempDir, fileName.toUpperCase()): // On linux, overlay only shows upper case files.
                    path.join(nukemnetLinksPath, fileName);

                if(useOverlayFile) {
                    hasOverlayFilesInTempDir = true
                }
                
                const resolvedFilePath = path.resolve(filePath);
                const resolvedGameDir = path.resolve(gameExecDir);
                // check if resoure is inside the game dir (we replace slashes to make it work on any platform)
                if(!isDosbox && paramDef.symlink!='forced' && resolvedFilePath.toLowerCase().replaceAll('\\','/').startsWith(resolvedGameDir.toLowerCase().replaceAll('\\','/')+'/')) { // dosbox works better with symlinks, even if file path is in the mounted dir (tested in duke3d)
                    // if file is in game dir, use relative path, no need for symlink
                    filePath = path.relative(resolvedGameDir, resolvedFilePath);
                } else {

                    // need to make symlink, or copy file, unless it's already there.
                    // test if file exists in nnlinks dir.
                    // if it exists and same size/dates, skip, do nothing.

                    function setFilePathToNNlinksEntry() {
                        if(useOverlayFile) {
                            filePath = fileName;
                        } else {
                            filePath = path.join(NNLinks_DirName, fileName);
                        }
                    }

                    const [linkStat, fileStat] = (await Promise.allSettled([
                        fs.stat(linkPath),
                        fs.stat(filePath)
                    ])).map(r=>r.status==='fulfilled'?r?.value:null);

                    const linkExistsAndIsSameFile = linkStat &&
                        fileStat?.mtimeMs == linkStat?.mtimeMs &&
                        fileStat?.ctimeMs == linkStat?.ctimeMs &&
                        fileStat?.size    == linkStat?.size

                    if(linkExistsAndIsSameFile) {
                        setFilePathToNNlinksEntry();
                    } else {

                        // Delete previous leftovers
                        try {await fs.rm(linkPath, {force:true})} catch(e) {}
                        try {await fs.rmdir(linkPath)} catch(e) {}
                        try {await fs.unlink(linkPath)} catch(e) {}
                        try {await fs.mkdir(nukemnetLinksPath, {recursive:true})} catch(e) {}
                    
                        //
                        // First try to create symlink
                        //

                        try {
                            let err:any = new Error("SymlinkError");
                            try {fsSync.symlinkSync(filePath, linkPath, 'file');} catch(e) {err=e}
                            if(fsSync.existsSync(linkPath)) {
                                setFilePathToNNlinksEntry()
                            } else throw err;
                        } catch(e) {
                            const logStr = `Could not create symlink into ${linkPath}, usually as a result of using old file systems (exFAT, FAT32)\r\nFalling back to raw file copy.`; 
                            appLogger.log(logStr)
                            console.log(logStr);

                            //
                            // Couldn't create symlink, try raw file copy
                            //

                            try {
                                try {await fs.copyFile(filePath, linkPath);} catch(e) {}
                                if(fsSync.existsSync(linkPath)) {
                                    setFilePathToNNlinksEntry()
                                } else throw new Error('CopyFileError');
                            } catch(e) {
                                filePath = 'SymlinkError';
                                DialogMgr.addPopup({
                                    title:"Symlink Error", noCenter:true,
                                    body: ()=><div>
                                    Could not create symlink or raw copy to {fileName} {paramDef?.res?`(${paramDef.res})`:""}<br/>
                                    In your game folder {gameExecDir}<br/>
                                    This can happen if you have no permissions on that folder,<br/>
                                    Or if no storage left on that drive,<br/>
                                    Or if that folder is in a drive on a filesystem that doesn't support symlinks,<br/>
                                    Such as exFAT, FAT32..<br/>
                                    If that's the case, you should move your {gameDef.name}: {execDef.name} folder to another drive.
                                    </div>,
                                    closeButton: true, buttons:[{body:"Ok"}]
                                });
                                return;
                            }
                        }
                    }

                }
            }

            if(isDosbox) {
                filePath = filePath.replaceAll('/', '\\');
                if(filePath.startsWith('.\\')) {
                    filePath = filePath.substring(2);
                }
            }

            clonedParam.args = [];
            if(Array.isArray(paramDef.value)) {
                for(const p of paramDef.value) {
                    const evaluated = NNEvalWrapArray(p, {...evalArgs, value:filePath })
                    if(evaluated != null) clonedParam.args.push(...evaluated);
                }
            } else {
                const evaluated = NNEvalWrapArray(paramDef.value as string, {...evalArgs, value:filePath })
                if(evaluated != null) clonedParam.args.push(...evaluated)
            }
        }

        GameRoomParamIdToValue[arg.id] = clonedParam.args;
        if(!clonedParam.syncOnly) {
            argsWithOrder.push(clonedParam)
        }
    };

    evalArgs.GameRoom.Params = GameRoomParamIdToValue

    // UDPTunnel logic
    let udpTunnel = null as ReturnType<typeof createUDPTunnel_client|typeof createUDPTunnel_server>|null
    const execTcpPort = execDef?.networking?.tcpPort?.port;
    const useUdpTunnel = GameRoomParamIdToValue?.[GameDefsType.UDPTUNNEL_CHOICE_PARAM_ID]?.[0] === 'udp4tunnel'
    if(execTcpPort != null && useUdpTunnel && !hostTunnelChosenPort) {
        const resolvedLocalhostIp = mainStore.localhostIp4;
        const forbiddenPortNumbers =
            [execTcpPort].
            concat((execDef.networking?.secondaryPorts||[]).map(p=>p.port)).
            concat([MyPort]);
        if(execDef?.networking?.udpPort?.port != null) forbiddenPortNumbers.push(execDef.networking.udpPort.port);        

        let useRandomPort = false;
        let tcpPortToUse = execDef?.networking?.tcpPort?.constant?execDef.networking.tcpPort.port:(useRandomPort=true, undefined)
        let udpPortToUse = execDef?.networking?.udpPort? (
            execDef?.networking?.udpPort?.constant? execDef.networking.udpPort.port:(useRandomPort=true, undefined)
        ): undefined;

        if(useRandomPort) {
            const randomPort = await findFreeUdpTcpPortInRange(undefined, undefined, forbiddenPortNumbers);
            udpPortToUse = randomPort;
            tcpPortToUse = randomPort;
        } else {
            // check that ports are bindable
            let errorPorts = '';
            if(tcpPortToUse && !await testIfTcpPortIsBindable(tcpPortToUse!)) {
                errorPorts += ` TCP port ${tcpPortToUse}`
            }
            if(udpPortToUse && !await testIfUdpPortIsBindable(udpPortToUse!)) {
                errorPorts += ` UDP port ${udpPortToUse}`
            }
            if(errorPorts.length) {
                DialogMgr.addPopup({
                    title: 'Could not bind to port',
                    body: $(`<div>Could not bind to${errorPorts}.<br/>Please make sure no other application is using it.</div>`),
                    buttons: [{body: 'OK'}]
                });
                return;
            }
        }

        if(imHost) {
            udpTunnel = createUDPTunnel_server(MyPort, resolvedLocalhostIp, tcpPortToUse!, resolvedLocalhostIp, udpPortToUse, forbiddenPortNumbers);
            evalArgs.GameRoom.MyPort = tcpPortToUse;
        } else {
            udpTunnel = createUDPTunnel_client(hostIpDetails!.ip, hostIpDetails!.port!, tcpPortToUse!, udpPortToUse);
            evalArgs.GameRoom.DestIp = resolvedLocalhostIp;
            evalArgs.GameRoom.DestPort = tcpPortToUse;
        }

    }
    // End of UDPTunnel logic

    // Some games do not support command line args, so we copy connection details to clipboard for quick entry in game gui
    // Such as Age of Empires
    if(execDef.clientCopyHostAddressToClipboard && !imHost) {
        if(execDef?.networking?.tcpPort?.constant) {
            Electron.clipboard.writeText(evalArgs.GameRoom.DestIp);
        } else {
            Electron.clipboard.writeText(evalArgs.GameRoom.DestIp+':'+evalArgs.GameRoom.DestPort);
        }
    }

    const runnableFile = typeof runnable.file === 'function'? (await runnable.file(evalArgs)):runnable.file;
    evalArgs.GameRoom.FileRef = runnableFile;

    const fileDef = execDef.files[runnableFile];

    const detachedShell = await (async()=>{
        if(typeof fileDef.runDetachedShell === 'boolean') {
            return fileDef.runDetachedShell;
        } else if(typeof fileDef.runDetachedShell === 'function') {
            return await fileDef.runDetachedShell(evalArgs);
        }
        return false;
    })();

    const runShell = await (async()=>{
        if(typeof fileDef.runShell === 'boolean') {
            return fileDef.runShell;
        } else if(typeof fileDef.runShell === 'function') {
            return await fileDef.runShell(evalArgs);
        }
        return false;
    })();

    // Resolve static params
    if(execDef.parameters) {
        for(const pname in execDef.parameters) {
            const paramDef = execDef.parameters[pname];
            if(paramDef.modeSupport == null || !paramDef.modeSupport.includes(result.mode)) {
                continue;
            }

            if(!Settings.filterParam_For(imHost, paramDef.for)) continue;

            if(paramDef.type==='static') {
                const argsArr:string[] = []

                if(typeof paramDef.addIf === 'function') {
                    if(!(await paramDef.addIf(evalArgs))) continue;;
                } else if(typeof paramDef.addIf === 'object') {
                    if(paramDef.addIf.hasImgMount != null) {
                        if(paramDef.addIf.hasImgMount != hasImgMount) {
                            continue;
                        }
                    }
                }

                if(Array.isArray(paramDef.value)) {
                    for(const p of paramDef.value) {
                        const evaluated = NNEvalWrapArray(p, {...evalArgs, })
                        if(evaluated != null) argsArr.push(...evaluated)
                    }
                } else {
                    const evaluated = NNEvalWrapArray(paramDef.value as string, {...evalArgs, })
                    if(evaluated != null) argsArr.push(...evaluated)
                }

                // Add to launch arg array
                if(argsArr.length) {
                    const a = {args:argsArr} as LaunchArgType;
                    if(paramDef.order != null) {
                        a.order = paramDef.order
                    }
                    GameRoomParamIdToValue[pname] = a.args;
                    if(!paramDef.syncOnly) {
                        argsWithOrder.push(a)
                    }
                }
            }
        }
    }

    argsWithOrder.sort((a,b)=>{
        if(a.order == null) {
            if(b.order == null) return 0;
            if(b.order > 0) return -1;
            if(b.order < 0) return 1;
        } else if(b.order == null) {
            if(a.order == null) return 0;
            if(a.order > 0) return 1;
            if(a.order < 0) return -1;
        } else {
            return a.order - b.order;
        }
        return 0;
    });

    const args:string[] = [];
    if(isDosbox) {
        argsWithOrder.forEach(info=>{
            info.args.forEach(a=>{
                let p = `${a}`;
                p = p.replaceAll("'", `\\'`).replaceAll('"',''); // Cleanup parameters for safety, making sure there are no malicious params.
                if(p.includes(" ") || p.includes('\t')) {
                    p = `'${p}'`;
                }
                args.push(p)
            })
        })
    } else {
        argsWithOrder.forEach(info=>{
            info.args.forEach(a=>{args.push(`${a}`)})
        })
    }

    async function doActualExecution() {
        if(runnable?.beforeRun?.length) {
            const promises:Promise<any>[] = [];
            for(const cmdBeforeRun of runnable.beforeRun) {
                promises.push((async ()=>{
                    if(!Settings.filterParam_For(imHost, cmdBeforeRun.for)) return;
        
                    try {
                        if(cmdBeforeRun.addIf) {
                            const shouldAdd = await cmdBeforeRun.addIf(evalArgs);
                            if(!shouldAdd) return;
                        }
        
                        if(cmdBeforeRun.cmd === 'edit-ini') {
                            await LaunchGameCmd_editIni(execDef, cmdBeforeRun, gameExecDir!, evalArgs);
                        } else if(cmdBeforeRun.cmd === 'edit-space-delimited') {
                            await LaunchGameCmd_editSpaceDelimitedFile(execDef, cmdBeforeRun as GameDefsType.Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile, gameExecDir!, evalArgs);
                        } else if(cmdBeforeRun.cmd === 'edit-binary') {
                            await LaunchGameCmd_EditBinary(execDef, cmdBeforeRun as GameDefsType.Runnable_CmdBeforeRun_EditBinary, gameExecDir!, evalArgs);
                        } else if(cmdBeforeRun.cmd === 'function') {
                            await cmdBeforeRun.run(evalArgs, execDef, ...cloneOfAllArgs);
                        }
                    } catch(e) {
                        if(cmdBeforeRun.try) {return}
                        throw e;
                    }
                })());
            }

            try {
                await Promise.all(promises);
            } catch(e) {
                DialogMgr.addPopup({
                    title: 'Launch Failed',
                    body: (e as any).message,
                    icon: 'error',
                    buttons:[{body:"ok"}]
                })
                return;
            }

            
        }
    
        // BEGIN stuff related to extensions
        async function notifyExtensionsBeforeLaunch(spawnArgs:ChildProcess_SpawnArgs) {
            try {
                for(const extName in Settings.loadedJsExtensions) {
                    const ext = Settings.loadedJsExtensions[extName];
                    try {
                        await ext?.processWillStart?.(spawnArgs, execDef, ...cloneOfAllArgs);
                    } catch(e) {
                        console.error(`Extension ${extName} processWillStart error`, e);
                    }
                }
            } catch(e) {}
        }
        function newProcStarted(proc:ChildProcess) {
    
            // delete temp dir on exit
            function procEnded() {try {
                delProc(proc);
                fsSync.rmSync(evalArgs.TempDir, {recursive:true,force:true});
                fsSync.rmSync(pathOfUserTempDir, {recursive:true,force:true});
            } catch(e) {}}
            proc.on('exit',procEnded);
            proc.on('error',procEnded);
            proc.on('close',procEnded);
            proc.on('disconnect',procEnded);
            proc.on('spawn', ()=>{
                addProc(proc);
            })

            // Write important info to channel
            if(execDef.hostInfoOnLaunch && imHost && channelName) {
                (async function() {
                    const msg = typeof execDef.hostInfoOnLaunch === 'function'? (await execDef.hostInfoOnLaunch(evalArgs)): execDef.hostInfoOnLaunch;
                    if(!msg?.length) return;
                    storeActions.message(gameDef.name,channelName,msg,'', true, false);
                })();
            }
            if(execDef.clientInfoOnLaunch && !imHost && channelName) {
                (async function() {
                    const msg = typeof execDef.clientInfoOnLaunch === 'function'? (await execDef.clientInfoOnLaunch(evalArgs)): execDef.clientInfoOnLaunch;
                    if(!msg?.length) return;
                    storeActions.message(gameDef.name,channelName,msg,'', true, false);
                })();
            }
            
            // Notify extensions
            try {
                for(const extName in Settings.loadedJsExtensions) {
                    const ext = Settings.loadedJsExtensions[extName];
                    try {
                        ext?.processStarted?.(proc, execDef, ...cloneOfAllArgs);
                    } catch(e) {
                        console.error(`Extension ${extName} processStarted error`, e);
                    }
                }
            } catch(e) {}
        }
        // END stuff related to extensions
    
        // NamedPipe for NetDuke32
        function runNamedPipe(proc:ChildProcess) {
            if(execDef.namedPipeChatSync) {
                const name = isWindows()?execDef.namedPipeChatSync.windows:execDef.namedPipeChatSync.unix;
                if(name) {namedPipeChatSync(proc, ircClient, name!, channelName!)}
            }
        };
    
        if(isDosbox) {
            if(!settings?.dosbox?.path_exe || !fsSync.existsSync(settings.dosbox.path_exe)) {
                DialogMgr.addPopup({
                    icon:"error",
                    title:"DOSBox failed to launch",
                    body:`DOSBox executable not found, please assign it's path in "Setup".`,
                    buttons:[{body:"ok"}]
                })
                return
            }
    
            const ep = path.resolve(settings.dosbox.path_exe);
    
            const dosboxCommands = [] as string[];
            const extraConfArgs = [];
            (function addExtraConfArgs() {
                extraConfArgs.push('-userconf');
                const dosboxExecFileNameWithoutExtension = path.parse(ep).name;
                const defaultConfFile = 'dosbox';
                extraConfArgs.push('-conf', `${defaultConfFile}.conf`);
    
                if(dosboxExecFileNameWithoutExtension.toLowerCase() != defaultConfFile.toLowerCase()) {
                    extraConfArgs.push('-conf', `${dosboxExecFileNameWithoutExtension}.conf`);
                }
            })();
    
    
            const isMultiplayer = result.mode === 'multiplayer';
            let extraConfLines = ''
            if(isMultiplayer) {
                // Checking if ipx or serial chosen.
                let chosenNet = 'ipx' as GameDefsType.Dosbox_NetTypes;
                {
                    const chosenNetInParamList = GameRoomParamIdToValue?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0];
                    if(chosenNetInParamList) {chosenNet = chosenNetInParamList as any}
                    else if(execDef.runnables.multiplayer?.runTool?.type === 'dosbox' &&
                        typeof execDef.runnables.multiplayer?.runTool.data.net==='string') {
                        chosenNet = execDef.runnables.multiplayer.runTool.data.net;
                    }
                }
    
                // The line below works for normal dosbox. however, it does not do anything in DosBox-X fork, you have to save config and restart for 'ipxnet' to be available
                // So we add another .conf file to add in param
                if(chosenNet == 'ipx') {
                    dosboxCommands.push('CONFIG -set "ipx=true"');
                    extraConfLines += `
[ipx]
ipx=true
`;
                }
    
                if(imHost) {
                    if(players!=2 && chosenNet==='serial') {
                        DialogMgr.addPopup({
                            title: 'Launch Failed',
                            body: `Serial net mode in dosbox supports 2 players only`,
                            icon: 'error',
                            buttons:[{body:"ok"}]
                        })
                        return;
                    }
    
                    const portPart = Settings.getGamePort();
                    if(chosenNet==='ipx') dosboxCommands.push(`ipxnet startserver ${portPart}`)
                    else if(chosenNet==='serial') dosboxCommands.push(`serial1=nullmodem port:${portPart} sock:1`)
                } else {
                    if(chosenNet==='ipx') {
                        const gamePortPart = hostIpDetails?.port? ` ${hostIpDetails.port}`:'';
                        // Sometimes first "ipxnet connect" command happens too early, so we run this command multiple times,
                        // in order to retry, in case the host dosbox process started a bit later than the participants.
                        // once the command is successful, the executive commands finish immediately so that's not an issue
                        for(let i=0;i<7;i++) {
                            dosboxCommands.push(`ipxnet connect ${hostIpDetails!.ip}${gamePortPart}`)
                        }
                    } else if(chosenNet==='serial') {
                        const gamePortPart = hostIpDetails?.port? ` port:${hostIpDetails.port}`:'';
                        dosboxCommands.push(`serial1=nullmodem server:${hostIpDetails!.ip}${gamePortPart} sock:1`)
                    }
                }
            }

            if(hasImgMount && gameExecSettings?.dosbox?.imgmount) {
                dosboxCommands.push(`IMGMOUNT ${imgMountDrive} "${gameExecSettings.dosbox!.imgmount}" -t iso`);
            }
    
            // Extra DOSBOX Conf
            const nukemnetDosboxConf = path.resolve(pathOfUserDir, 'nukemnet.db.conf');
            extraConfLines += `
[sdl]
autolock = true
capture_mouse = onstart middlegame
raw_mouse_input = true
[dosbox]
startup_verbosity = low
memsize = 63
[ethernet]
ne2000=false
`;
            const extraIni = runnable?.runTool?.data?.ini;
            if(typeof extraIni === 'string') {
                extraConfLines += extraIni;
            } else if(typeof extraIni === 'function') {
                extraConfLines += `${NNEval(extraIni, evalArgs)}`
            }
            await fs.writeFile(nukemnetDosboxConf, extraConfLines);
            extraConfArgs.push('-conf', nukemnetDosboxConf)
    
            const dbArgs = deepClone(runnable.runTool?.data?.args || []);
            const dbCommandsFromExecDef = deepClone(runnable.runTool?.data?.commands || []);

            const fullExecFilePathToRun = Settings.resolveFilePathForFileRef(runnableFile, {gameId:execDef._gameId, execId:execDef._execId});
            if(!fullExecFilePathToRun?.length) {
                DialogMgr.addPopup({
                    icon:"error",
                    title:"DOSBox failed to launch",
                    body:`Executable file not found, please assign it's path in "Setup"`,
                    buttons:[{body:"ok"}]
                })
                return;
            }
            
            const execFilePathRelativeToGameDir = path.relative(gameExecDir, fullExecFilePathToRun)
            
            const execFileNameConvertedToDos = (()=>{
                let result = execFilePathRelativeToGameDir;
                if(runnable?.runTool?.data?.cdInto) {
                    result = path.basename(execFilePathRelativeToGameDir);
                    const dosDirNamePath = path.dirname(execFilePathRelativeToGameDir).replaceAll('/',"\\")
                    const isSameDir = dosDirNamePath === '.' || !dosDirNamePath?.length;
                    if(!isSameDir) {
                        dbCommandsFromExecDef.push(`cd ${dosDirNamePath}`)
                    }
                }
                return result.replaceAll('/',"\\");
            })();

            appLogger.log('dosbox game launch:' + args);
            
            Settings.playSoundEffect('gameroom-launch');
    
            const dosboxCWD = path.join(pathOfUserDir, 'dosbox');
            if(!fsSync.existsSync(dosboxCWD)) {fsSync.mkdirSync(dosboxCWD, {recursive:true});}

            const appArgsStr = args.join(' ').replaceAll('\r','').replaceAll('\n','')
            dosboxCommands.push(`mount c "${gameExecDir}"`);
            if(hasOverlayFilesInTempDir) {
                dosboxCommands.push(`mount c "${evalArgs.TempDir.replaceAll('\\','/')}" -t overlay`);
            }

            dosboxCommands.push(
                "C:",
                ...dbCommandsFromExecDef,
                `${execFileNameConvertedToDos} ${appArgsStr}`,
                'exit'
            );
            const allDosboxCommandsString = (()=>{
                if(!settings?.debugDosbox) return dosboxCommands.join('\n');

                let foundIpxConnectCommand = false
                const finalCommands = dosboxCommands.map(line=>{
                    // see comment about "ipxnet connect".
                    // not putting too many "pause" commands after each "ipxnet connect" try.
                    if(line.toLowerCase().startsWith('ipxnet connect')) {
                        if(foundIpxConnectCommand) {
                            return `${line}\n`;
                        } else {
                            foundIpxConnectCommand = true;
                            return `cd\necho ${line}\npause\n${line}\n`;
                        }
                    }
                    return `cd\necho ${line}\npause\n${line}\n`;
                });

                finalCommands.unshift("@echo off\n");
                return finalCommands.join('');
            })();

            const spawnArgs:ChildProcess_SpawnArgs = [ep, ['-noconsole', ...dbArgs, ...extraConfArgs,`-c`, allDosboxCommandsString], {detached: detachedShell, cwd:dosboxCWD}]; 
            await notifyExtensionsBeforeLaunch(spawnArgs);
            const proc = child_process.spawn(...spawnArgs);
            checkProcessRunSuccess(proc).catch(e=>{
                DialogMgr.addPopup({
                    title: 'Could not run dosbox executable',
                    body: $(`<pre>${e?.message}</pre>`)
                })
            });

            runNamedPipe(proc);
            newProcStarted(proc);
            return {proc, evalArgs};
        } else {
            
            const finalExecFile = await Settings.resolveFilePathForFileRef(runnableFile, {dir:gameExecDir, gameId:execDef._gameId, execId:execDef._execId});
            if(finalExecFile == null) {
                DialogMgr.addPopup({icon:'error', "title":'Error', body:'Missing executable file ' + (fileDef?.glob || fileDef?.path || '').toString(), buttons:[{body:"ok"}]});
                return;
            }
    
            const fullGameExecSpawnPath = finalExecFile; // path.resolve(gameExecDir, finalExecFile); using full path is not working with Unreal Gold for some reason..
    
            let spawnPath:string;
    
            // If it's not windows, check if need to run with wine
            let isUsingWine = false;
            if(!isWindows()) {
                const isWinExe = finalExecFile.toLowerCase().endsWith('.exe');
                if(isWinExe) {
                    // Run with wine
                    isUsingWine = true;
                    const wineExe = (detachedShell || runShell)? settings?.wine?.console_path_exe : settings?.wine?.path_exe;
                    if(!wineExe?.length) {
                        DialogMgr.addPopup({
                            icon:"error",
                            title: 'Wine path missing',
                            body: "Wine executable not defined, please go to 'Setup' first",
                            buttons:[{body:"ok"}]
                        });
                        return;
                    }
                    spawnPath = wineExe!;
                    args.unshift(fullGameExecSpawnPath);
                } else {
                    spawnPath = fullGameExecSpawnPath;
                }
            } else {
                spawnPath = fullGameExecSpawnPath;
            }
            const needToUseShell = (detachedShell || runShell) && !isUsingWine;
    
            Settings.playSoundEffect('gameroom-launch');
    
            const prePath = (!isWindows() && !spawnPath.startsWith('/')) ?'./':'';
            let finalPath = `${prePath}${spawnPath}`;
            if(needToUseShell) {
                // escape args for shell
                finalPath = `"${finalPath}"`;
                const useWindowsCmdEscapeSyntax = isWindows() || isUsingWine;
                for(let i=0;i<args.length;i++) {
                    args[i] = escapeShellArg(args[i], useWindowsCmdEscapeSyntax);
                }
            };
    
            const spawnArgs:ChildProcess_SpawnArgs = [finalPath, args, {
                stdio: 'inherit',
                cwd:gameExecDir,
                shell: needToUseShell,
                detached: detachedShell
            }];
            
            appLogger.log(`game launch: ${spawnPath}\n${args}`);

            notifyExtensionsBeforeLaunch(spawnArgs);
            const proc = child_process.spawn(...spawnArgs);
            checkProcessRunSuccess(proc).catch(e=>{
                DialogMgr.addPopup({
                    title: 'Could not run executable',
                    body: $(`<pre>${e?.message}</pre>`)
                })
            });

            runNamedPipe(proc);
            newProcStarted(proc);
            return {proc, evalArgs};
        }    
    }

    const executeResult = await doActualExecution();
    
    if(executeResult?.proc) {
        executeResult.proc.on('close', ()=>udpTunnel?.close());
    } else {
        udpTunnel?.close()
    }

    if(udpTunnel?.gamePort) {
        return {...executeResult!, hostTunnelPort:udpTunnel.gamePort};
    } else {
        return executeResult;
    }
}