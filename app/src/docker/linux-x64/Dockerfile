from amd64/ubuntu:20.04

# Setting timezone to skip interactive prompts
ENV TZ=US/Central
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Installing NodeJS and required global yarn CLI
RUN apt update
RUN apt install -y curl git wget zip make gcc g++
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt install -y nodejs
RUN npm i -g yarn

# Getting Ergo Executable (used in offline LAN mode, NukemNet client starts it's own local IRC Daemon server)
RUN cd /
RUN wget https://github.com/ergochat/ergo/releases/download/v2.11.1/ergo-2.11.1-linux-x86_64.tar.gz
RUN tar -xvzf ergo-2.11.1-linux-x86_64.tar.gz ergo-2.11.1-linux-x86_64/ergo
RUN mkdir -p nn/app/res/libs/ergo && cp ergo-2.11.1-linux-x86_64/ergo nn/app/res/libs/ergo/

# Common: install dependencies
COPY server/src/common/npm-shrinkwrap.json nn/server/src/common/npm-shrinkwrap.json
COPY server/src/common/package.json nn/server/src/common/package.json
RUN cd /nn/server/src/common && npm ci --loglevel verbose

# Copying common NukemNet files required to build App
COPY server nn/server
COPY relay-service nn/relay-service

# Install app dependencies:
# Some npm libs will be compiled on the spot as a result with node-gyp, such as the KCP npm lib.
# Some npm libs may copy native exe files depending on current OS, such as ps-list npm lib.
COPY app/npm-shrinkwrap.json nn/app/npm-shrinkwrap.json
COPY app/package.json nn/app/package.json
RUN cd /nn/app && npm ci --loglevel verbose

COPY app nn/app

RUN cd /nn/app && npm run make-linux-x64
