import { Button, Modal } from "solid-bootstrap"
import { Component, For, Index, JSX } from "solid-js"
import { createMutable } from "solid-js/store";
import { createRandomHash } from "../../server/src/common/hash";
import { isElement, isJqueryElement } from "./lib/utils";
type PlainText_HTMLElement_JQueryElement_JSX = string|(()=>JSX.Element)|HTMLElement|JQuery<HTMLElement>;

type PopupProps = {
    icon?:'success' | 'error' | 'warning' | 'info' | 'question', // TODO implement
    noCenter?:boolean,
    size?:"sm" | "lg" | "xl",
    fullscreen?:boolean
    onClose?:(btnIndex?:number)=>void,
    title?:PlainText_HTMLElement_JQueryElement_JSX,
    closeButton?:boolean
    body?: PlainText_HTMLElement_JQueryElement_JSX,
    persistent?:boolean,
    buttons?: {
        body:PlainText_HTMLElement_JQueryElement_JSX,
        variant?:'primary'|'secondary'|'success'|'danger'|'warning'|'info'|'light'|'dark'|'muted'|'white',
        onClick?:()=>void,
        noClose?:boolean
    }[];
}

function valueOf(val?:PlainText_HTMLElement_JQueryElement_JSX|undefined) {
    if(val==null) return undefined;

    if(typeof val === 'function') return val(); // SolidJS component

    if(isElement(val)) {
        return <>{val as any}</>;
    } else if(isJqueryElement(val)) {
        return <>{...(Array.from(val as any))}</>;
    }

    return val;
}

export type DialogReturn = Promise<{buttonIndex?:number}>&{close:()=>void}
export type AppWindowReturn = Promise<{}>&{close:()=>void}

export const DialogMgr = {
    addPopup:(props:PopupProps)=>({} as DialogReturn),
    addWindow:(props:JQueryUI.DialogOptions&{body?:PlainText_HTMLElement_JQueryElement_JSX})=>({} as DialogReturn),
};

const Popups:Component<{}> = ()=>{
    const dialogs = createMutable({} as {[id: string]:()=>JSX.Element});
    const windows = createMutable({} as {[id: string]:()=>JSX.Element});

    DialogMgr.addPopup = function addPopup(props:PopupProps):DialogReturn {

        const dialogId = createRandomHash();

        const centerStyle = props.noCenter?undefined:{"margin":"auto", "text-align":"center"} as JSX.CSSProperties;
        const modalFunc = ()=><Modal
            size={props.size?props.size:undefined}
            show={true}
            scrollable={true}
            fullscreen={props.fullscreen?true:undefined}
            onHide={()=>{
                if(!props?.persistent) {
                    closeDialog();
                }
            }}
          >
            <Modal.Header closeButton={props?.closeButton}>
              <Modal.Title>
                {valueOf(props?.title) as any}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body style={centerStyle}>
                {valueOf(props?.body)  as any}
            </Modal.Body>

            <Modal.Footer style={centerStyle}>
                <Index each={props?.buttons}>
                    {(btn,i)=>{
                        const button = btn();
                        let buttonRef:any;
                        if(i===0) {
                            setTimeout(()=>{
                                if(document?.activeElement instanceof HTMLInputElement) {
                                    return;
                                }
                                if(buttonRef) try {buttonRef?.focus();} catch(e) {}
                            });
                        } 
                        return <Button ref={buttonRef} variant={button?.variant}
                            onClick={()=>{
                                if(button?.onClick) button.onClick();
                                if(button?.noClose) {} else {closeDialog(i);}
                            }}>
                            {valueOf(button.body) as any}
                        </Button>;
                    }}
                </Index>
            </Modal.Footer>

          </Modal>

        function closeDialog(btnIndex?:number) {
            delete dialogs[dialogId];
            if(props?.onClose) props.onClose(btnIndex);
            promiseProps.res({buttonIndex:btnIndex});
        }

        dialogs[dialogId] = modalFunc;

        const promiseProps = {res:()=>{},rej:()=>{}} as any;
        const promise = new Promise((ok,err)=>{promiseProps.res=ok;promiseProps.rej=err});
        (promise as any).close = closeDialog;

        return promise as any;
    }

    DialogMgr.addWindow = function addWindow(props:JQueryUI.DialogOptions&{body?:PlainText_HTMLElement_JQueryElement_JSX}):AppWindowReturn {

        const windowId = createRandomHash();

        const modalFunc = ()=><div id={windowId}>{valueOf(props?.body) as any}</div>;
        function closeWindow(a:any,b:any) {
            d.parent().remove();
            delete windows[windowId];
            promiseProps.res({});

            if(props.close) {
                props.close(a,b);
            }
        }

        windows[windowId] = modalFunc;
        const d = $(`#${windowId}`).dialog({...props,close:(a,b)=>{
            closeWindow(a,b);
        }}).resizable();
        
        const promiseProps = {res:()=>{},rej:()=>{}} as any;
        const promise = new Promise((ok,err)=>{promiseProps.res=ok;promiseProps.rej=err});
        (promise as any).close = closeWindow;

        return promise as any;
    }

    return <div>
        <For each={Object.values(dialogs)}>
            {entry=>entry()}
        </For>
        <For each={Object.values(windows)}>
            {entry=><div>{entry()}</div>}
        </For>
    </div>
}

export default Popups;
