/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/jqueryui/index.d.ts" />

import 'bootstrap/dist/css/bootstrap.min.css'

// kill possible old tunneler process
require('electron').ipcRenderer.send('subprocess-pids', {action:"killall"});

import jQuery from 'jquery'
(window as any).jQuery = (window as any).$ = jQuery;

import './vendor/jquery-ui/jquery-ui.min.js'
import './vendor/jquery-ui/jquery-ui.min.css'
import './vendor/jquery-ui/jquery-ui.theme.min.css'

// globals
(window as any).nnglobal = {
    stun: {
      log: {
        warnings: false,
        all: false,
        sent: {
          handshake: false,
          raw: false
        },
        received: {
          handshake: false,
          raw: false
        }
      }
    }
  }

function onEverythingLoaded(cb:()=>void) {
    window.addEventListener('DOMContentLoaded', () => {
        var imgs = document.images,
        len = imgs.length,
        counter = 0;
    
        [].forEach.call( imgs, function( img:any ) {
            if(img.complete)
            incrementCounter();
            else
            img.addEventListener('load', incrementCounter, false );
        } );
    
        function incrementCounter() {
            counter++;
            if ( counter === len ) {
                setTimeout(()=>{
                    cb();
                },1)
            }
        }
    });
}

/* @refresh reload */
import { render } from 'solid-js/web';
import App from './App';
import { Settings } from './Settings';

onEverythingLoaded(async function() {
  await Settings.loadAllExtensions();
  Settings.playSoundEffect('welcome');
  render(() => App({}), document.getElementById('root') as HTMLElement);
})
