
import { GameDefsType } from './GameDefsTypes';
import { child_process, electronRemote, fs, fsSync, glob, path, ps, crypto, ip2loc } from './NodeLibs'
import * as AllNodeLibs from './NodeLibs';
import sleep from 'sleep-promise';
import { deepMerge } from './lib/deepMerge';
import { createPathObj } from './lib/createPathObj';
import { deepSetValue } from './lib/deepSetValue';
import { deepClone } from './lib/deepClone';
import { ircClient, mainStore, minimumNukemNetExtensionVersionSupport, nncmdReceivedEmitter } from './store/store';
import { compareDottedStringNumbers } from './lib/CompareDottedNumberString';
import { Client } from 'matrix-org-irc';
import { IpDetails } from './lib/IpDetails';
import { IrcClientExtra, nncmdSentEmitter } from '../../server/src/common/ircExtend';
import { ChildProcess } from 'child_process';
import { DialogMgr } from './Popups';
import storeActions from './store/storeActions';
import { LaunchGameModalResult } from './LaunchGameModal';
import { DateNow_YYYYMMDD, isWindows } from './lib/utils';
import * as utils from './lib/utils';
import { terminateGameProcess } from './terminateProc';
import { GlobOptions } from 'glob';

type ChildProcess_SpawnArgs = Parameters<typeof child_process.spawn>;

export interface SettingsDef {
    IrcNick?:string
    IrcNickPass?:string
    jwt?:string,
    InGamePlayerName?:string,
    muteAll?:boolean,
    allowExtensionJs?:boolean,
    soundVolume?:number,
    lanOnlyMode?:boolean,
    ipv4Mode?:boolean,
    disableLogging?:boolean,
    disableAutoPortChecker?:boolean,
    debugDosbox?:boolean,
    shortcutKeys?: {
        terminateProc?:string,
    },
    dosbox?: {
        path_exe?:string
    },
    wine?: {
        path_exe?:string,
        console_path_exe?:string
    },
    gamePort:number,

    // Directory path of games and their execs.
    games?: {[gameId:string]: {
        [execId:string]: {
            path?: string,
            resPath?: {
                [resId:string]: string
            }
            dosbox?: {
                imgmount?:string
            }
        }
    }}
}

export interface LaunchDefaults_NormalArg { // if manual arg, paramId has to start with _m_
    // Predefined arg
    value:any,
    htmlProp:string,
    for?: GameDefsType.ParamType.ParamFor
    args: string[],
    valueLabel?:string
}

export interface LaunchDefaults_ManualArg {
    // Manual arg
    args: string[]
    for?: GameDefsType.ParamType.ParamFor
    order?: number,
    valueLabel?:string
}


export interface LaunchDefaults {

    defaultGameAndExec?: {"gameId":string, "execId": string},
    roomName?:string,
    roomPass?:string,
    hideRoom?:boolean,
    "games": {
        [gameId:string]: {
            executables?: {
                [execId:string]: {
                    params?: {
                        // if manual arg, paramId has to start with _m_
                        [paramId:string]: LaunchDefaults_NormalArg|LaunchDefaults_ManualArg
                    },
                    mode?:GameDefsType.GameModes,
                    maxPlayers?: number
                }
            }
        }
    }
  }


export const programDirPath = electronRemote.getGlobal('paths').programDir;
export const electronDir = (window as any).electron.electronDir
export const pathOfRes = path.resolve(electronDir, 'res');
export const pathOfUserDir = path.resolve(ps.cwd(), 'user');

export const pathOfSettings = path.resolve(pathOfUserDir, 'Settings.json');
export const pathOfLaunchDefaults = path.resolve(pathOfUserDir, 'LaunchDefaults.json');

export const pathOfDefs = path.resolve(pathOfRes, 'defs');
export const pathOfSounds = path.resolve(pathOfRes, 'sounds');
export const pathOfUserSounds = path.resolve(pathOfUserDir, 'sounds');
export const pathOfExtensions = path.resolve(pathOfUserDir, 'extensions');
export const pathOfUserTempDir = path.resolve(pathOfUserDir, 'temp')


export const pathOfIrcLogs = path.resolve(pathOfUserDir, 'irclogs');
export const pathOfIrcLogs_backlogFile = path.resolve(pathOfIrcLogs, 'backlog.log');
export const pathOfIrcLogs_channels = path.resolve(pathOfIrcLogs, 'chan');
export const pathOfIrcLogs_private = path.resolve(pathOfIrcLogs, 'prv');

export const defaultGamePort = 23513;
export const ApiUrl = `http://localhost:9999`;

export const NNLinks_DirName = 'nnlinks';

const SoundName = {
    "gameroom-new": 'gameroom-new',
    "gameroom-join": 'gameroom-join',
    "gameroom-launch": 'gameroom-launch',
    "gameroom-message": 'gameroom-message',
    "gameroom-part": 'gameroom-part',
    "room-join": 'room-join',
    "room-message": 'room-message',
    "room-part": 'room-part',
    "private-message": 'private-message',
    "terminated": 'terminated',
    "switch": "switch",
    "welcome": 'welcome'
}
export type SoundTypes_All_Types = keyof typeof SoundName;
export const SoundTypes_All = Object.values(SoundName) as (keyof typeof SoundName)[];

let gameDefs: GameDefsType.Root|null = null;
let launchDefaultsObj = null as LaunchDefaults|null;

interface NNExtension {
    nnversion:string,
    init:()=>Promise<void>,
    destroy:()=>Promise<void>,
    gameDefs?:(gameDefs:GameDefsType.Root, context:any)=>void,
    ircConnection?:(connected:boolean)=>Promise<void>,
    onPureIrcMessage?: (nick:string|null, to:string, text:string, raw:any)=>Promise<void>,
    refreshedIpDetails?:(ipDetails:IpDetails)=>Promise<void>,
    willPlaySoundFile?:(fullPath:string, soundType:string)=>Promise<string|null>;
    topbarCmd?:(cmd:string)=>Promise<void>;
    processWillStart?:(spawnArgs:ChildProcess_SpawnArgs, execDef:GameDefsType.Executable, ...args:any)=>void;
    processStarted?:(proc: ChildProcess, execDef:GameDefsType.Executable, ...args:any)=>void;
    updateSettings?:(settings:SettingsDef)=>Promise<void>;
    gameRoomDetailsSet?: (result:LaunchGameModalResult)=>Promise<void>;
}

type NNExtentionRootFunc = (props:{
    libs:typeof AllNodeLibs,
    paths:{[name:string]:string},
    settings:SettingsDef,
    Settings:typeof Settings,
    ircClient:Client,
    mainStore: typeof mainStore,
    storeActions: typeof storeActions,
    nncmdSentEmitter: typeof nncmdSentEmitter,
    nncmdReceivedEmitter: typeof nncmdReceivedEmitter,
    DialogMgr: typeof DialogMgr,
    IrcClientExtra: typeof IrcClientExtra,
    sendNNCmdTo: (target:string, cmd:{op:string, data:any})=>Promise<void>
})=>NNExtension;

let lastSettingsData:SettingsDef;
export const Settings = {

    loadedJsExtensions: {} as {[ext:string]:NNExtension},

    async loadAllExtensions() {
        try {
            const extensions = await fs.readdir(pathOfExtensions);
            for(const ext of extensions) {
                await this.toggleExtension(ext, true);
            }
        } catch(e) {
            // extensions dir does not exist.
        }
    },

    async toggleLanMode() {
        const s = Settings.read();
        s.lanOnlyMode = !s.lanOnlyMode;
        Settings.write(s);
        location.reload();
    },

    async toggleIpv4Mode() {
        function doToggle() {
            s.ipv4Mode = !s.ipv4Mode;
            Settings.write(s);
            location.reload();
        }
        const s = Settings.read();
        if(!s.ipv4Mode) {
            await DialogMgr.addPopup({
                title: 'Strict IPv4 Mode', body: $(`
                <div>It's recommended to use IPv4/6 Mixed mode.<br/>
                However, if you know you're not on an IPv6 network,<br/>
                and not gonna get an IPv6 address anytime soon,<br/>
                it might be better to go for it.<br/>Change mode?
                </div>`), closeButton:true, buttons:[
                {body:'Just do it', onClick: doToggle}, {body:'Cancel'}
            ]});
        } else {
            doToggle();
        }
    },

    async removeAllExtensions() {
        document.querySelectorAll(`[nnext]`).forEach(e => e.remove());
        const allPromises = [];
        for(const ext in this.loadAllExtensions) {
            allPromises.push(this.toggleExtension(ext, false));
        }
        try {
            await Promise.allSettled(allPromises);
        } catch(e) {}
    },
    
    async toggleExtension(extension:string, add:boolean) {

        if(!add) {
            document.querySelectorAll(`[nnext=${extension}]`).forEach(e => e.remove()); // Remove previous theme if there was one loaded
            const extensionPlugin = this.loadedJsExtensions[extension];
            delete this.loadedJsExtensions[extension];
            try {
                await extensionPlugin.destroy();
            } catch(e) {
                console.error('Error while destroying extension', extension, e);    
            }
            return;
        }
        
        try {
            const extDirPath = path.resolve(pathOfExtensions, extension).replaceAll('\\','/');

            const files = fsSync.readdirSync(extDirPath);
            const stylesDirName = files.find(f=>f.toLowerCase()==='styles');
            const stylesDirPath = stylesDirName?.length? path.join(extDirPath, stylesDirName):undefined;

            // Load extension styles (optional)
            if(stylesDirPath?.length) {
                try {
                    const styleFilePaths = await fs.readdir(stylesDirPath);
                    const fileReads = [] as Promise<string>[];
                    for(const file of styleFilePaths) { 
                        const lfile = file.toLowerCase();
                        if(lfile.endsWith('.css')) {
                            fileReads.push(fs.readFile(path.join(stylesDirPath, file), 'utf-8'));
                        }
                    }
                    const cssFiles = await Promise.all(fileReads);
                    for(let css of cssFiles) {
                        css = css.replaceAll('nnext://', 'file://'+extDirPath+'/');
                        const styleElement = document.createElement('style');
                        styleElement.setAttribute('nnext', extension);
                        styleElement.innerHTML = css;
                        document.head.prepend(styleElement);
                    }
                } catch(e) {}
            }

            // Try load extension js (optional)
            (async() => {
                const extJsPath = path.resolve(extDirPath, 'js', 'index.js');
                if(!fsSync.existsSync(extJsPath)) return;

                const s = Settings.read();
                if(!s.allowExtensionJs) {
                    const allow = confirm(`Extension "${extension}" has a javascript file.\r\nDo you want to enable javascript in extensions?\r\n(Only add extensions you trust!)`);
                    if(!allow) return;
                    s.allowExtensionJs = true;
                    Settings.write(s);
                }

                const extObjFunc = require(extJsPath) as NNExtentionRootFunc;
                
                try {
                    const extObj = extObjFunc({
                        libs:AllNodeLibs,
                        paths:{pathOfDefs, pathOfExtensions, pathOfIrcLogs, pathOfIrcLogs_backlogFile, pathOfIrcLogs_channels, pathOfIrcLogs_private, pathOfLaunchDefaults, pathOfRes, pathOfSettings, pathOfSounds, pathOfUserDir, pathOfUserSounds, electronDir},
                        settings:Settings.read(),
                        Settings,
                        ircClient,
                        IrcClientExtra,
                        mainStore,
                        storeActions,
                        nncmdReceivedEmitter: nncmdReceivedEmitter,
                        nncmdSentEmitter: nncmdSentEmitter,
                        DialogMgr,
                        sendNNCmdTo: async (target:string, cmd:{op:string, data:any})=>{
                            try {
                                cmd.op = `E.${extension}.${cmd.op}`
                                return await IrcClientExtra.sendNNCmdTo(ircClient, target, cmd);
                            } catch(e) {
                                console.error(`Extension ${extension}: error while sending nncmd`, e);
                            }
                        }
                    });
                    // verify extension support for current NukemNet version before loading
                    if(!extObj?.nnversion?.length) {
                        throw new Error('nnversion not defined in extension object');
                    }
                    const extensionDeprecated = compareDottedStringNumbers(minimumNukemNetExtensionVersionSupport, extObj.nnversion)>0;
                    if(extensionDeprecated) {
                        throw new Error(`Extension ${extension} is deprecated. Minimum supported version is ${minimumNukemNetExtensionVersionSupport} and this extension is ${extObj.nnversion}\r\nPlease contact the extension author to update the JS code to support the latest NukemNet version.`);
                    }
                    const extensionForNewerNukemNetRelease = compareDottedStringNumbers(extObj.nnversion, Settings.pjson.version)>0;
                    if(extensionForNewerNukemNetRelease) {
                        throw new Error(`Extension ${extension} is is for newer NukemNet version. Your NukemNet version is ${Settings.pjson.version} and this extension only supports version ${extObj.nnversion} and above.\r\nPlease update your NukemNet version, or contact the extension author to fix that.`);
                    }

                    await extObj.init()
                    this.loadedJsExtensions[extension] = extObj;
                } catch(e) {
                    console.error(`Extension ${extension} failed to init: ${e}`);
                }
                
            })();

        } catch(e) {
            console.error('Could not load theme: '+extension);
        }
    },

    logout() {
        const s = this.read(true);
        delete s.jwt;
        delete s.IrcNickPass;
        this.write(s);
    },

    read(overrideCache:boolean = false): SettingsDef {
        if(!overrideCache && lastSettingsData != null) {
            return lastSettingsData;
        } 

        let retVal: SettingsDef;
        let failedRead = false;
        try {
            this.createDefaultSettingsFileDirIfNotExists();
            retVal = JSON.parse(fsSync.readFileSync(pathOfSettings, 'utf-8'))
        } catch(e) {
            failedRead = true;
            retVal = {gamePort:defaultGamePort}
        }

        if(!retVal.gamePort) retVal.gamePort = defaultGamePort;
        if(!retVal.IrcNick?.length) retVal.IrcNick = "NNGamer";
        if(!retVal.InGamePlayerName?.length) retVal.InGamePlayerName = "NNGamer";

        if(failedRead) {
            this.write(retVal);
        }

        lastSettingsData = retVal;
        return retVal
    },

    async write(newSettings:SettingsDef) {
        await this.createDefaultSettingsFileDirIfNotExists();

        try {
            for(const extName in Settings.loadedJsExtensions) {
                const ext = Settings.loadedJsExtensions[extName];
                try {
                    ext?.updateSettings?.(newSettings);
                } catch(e) {
                    console.error(`Extension ${extName} updateSettings error`, e);
                }
            }
        } catch(e) {}

        const str = JSON.stringify(newSettings, null, 2);
        await fs.writeFile(pathOfSettings, str);
        lastSettingsData = newSettings;
    },

    createDefaultSettingsFileDirIfNotExists() {
        const dir = path.parse(pathOfSettings).dir;
        if(!fsSync.existsSync(dir)) {
            fsSync.mkdirSync(dir, { recursive: true });
        }
    },

    getGamePort() {
        const settings = this.read();
        return settings.gamePort || defaultGamePort
    },

    getIrcNick(s?:SettingsDef) {
        const settings:SettingsDef = s || this.read();
        return settings.IrcNick || "NNGamer"
    },

    updateSettings(mergeObj: Partial<SettingsDef> , settings?:SettingsDef) {
        let set = settings||this.read();
        const updated = deepMerge(set, mergeObj);
        fsSync.writeFileSync(pathOfSettings, JSON.stringify(updated, null, 2));
    },

    isFileNativeRunnable(e:GameDefsType.Executable, fileRef:string) {
        for(const id in e.runnables) {
            const runnable = (e.runnables as any)[id as any] as GameDefsType.Runnable;

            // If this file is a native executable, and not an executable that runs by emulator such as dosbox
            const thisIsTheFileRef = (typeof runnable.file === 'string' && runnable.file === fileRef) ||
            (Array.isArray(runnable.files) && runnable.files.includes(fileRef)); 

            if(thisIsTheFileRef && runnable?.runTool == null) {
                return true;
            }
        }
        return false;
    },

    resolveFilePathForFileRef(fileRef:string, props:{gameId:string, execId:string, dir?:string}):string|null {
        const execInfo = Settings.gameDefs?.games?.[props?.gameId]?.executables?.[props?.execId];   
        const fileInfo = deepClone(execInfo?.files?.[fileRef]);
        if(!fileInfo) throw new Error(`Could not find file ${fileRef} in game ${props.gameId} executable ${props.execId}`);

        const dir = props?.dir || Settings.read().games?.[props?.gameId]?.[props?.execId]?.path;
        if(!dir) throw new Error(`Could not find path for game ${props.gameId} executable ${props.execId}`);

        if(fileInfo?.glob?.length) {
            const res = glob.globSync(fileInfo.glob, {
                nocase:true, // overridable options
                ...(fileInfo?.globOpts || {}),

                // non-overridable options
                cwd:dir, absolute:true
            });
            return res?.[0];
        }
        
        if(fileInfo?.path?.length) {
            const resolvedPath = path.resolve(dir, fileInfo.path!);
            const foundPath = utils.findPathCaseInsensitiveSync(resolvedPath);
            if(foundPath) {
                return foundPath;
            } else if(Settings.isFileNativeRunnable(execInfo, fileRef)) {
                const ext = path.parse(resolvedPath)?.ext;
                if(ext?.length) {
                    if(!isWindows()) {
                        // try again without ext
                        return utils.findPathCaseInsensitiveSync(utils.removeExtFromPath(resolvedPath));
                    }
                } else {
                    // try again with ext
                    return utils.findPathCaseInsensitiveSync(resolvedPath+'.exe');
                }
            }
        }
        return null;
    },

    recursiveGetGameDefsObj(objPath:string[]):any {
        const currentIterationFullPath = path.resolve(pathOfDefs, ...objPath);
        const dir = fsSync.readdirSync(currentIterationFullPath);

        let objMerged = {};
        for(const entry of dir) {
            const entryPathFull = path.resolve(currentIterationFullPath, entry)
            const stat = fsSync.statSync(entryPathFull);
            
            if(stat.isFile()) {
                const lEntry = entry.toLowerCase();
                let fileExt:string = path.extname(lEntry);    
                
                let fileName:string|null
                let fileObj:any = null;
                const isJson = fileExt === '.json';
                const isJs = fileExt === '.js';
                const isRelevantExt = isJson || isJs

                if(isRelevantExt) {
                    fileName = path.parse(entry).name;

                    // Ignore files
                    if(lEntry?.startsWith('_i_') || lEntry?.endsWith('.i.js')) continue;

                    try {
                        if(isJson) {
                            const fileContents = fsSync.readFileSync(entryPathFull, 'utf8')
                            fileObj = JSON.parse(fileContents);
                        } else if(isJs) {
                            const func = require(entryPathFull);
                            fileObj = func({deepClone, GameDefsType, Settings, mainStore, utils, NodeLibs:AllNodeLibs, sleep}); // pass common utils
                        }
                    } catch(e) {
                        (e as any).file = entryPathFull;
                        throw e;
                    }
                }
                if(fileObj == null) continue;

                let baseObjToMerge = {};
                let mergePath;
                if(fileName!.startsWith('_p_')) {
                    mergePath = objPath;   
                } else {
                    mergePath = objPath.concat(fileName!);
                }
                baseObjToMerge = createPathObj(objPath);
                deepSetValue(baseObjToMerge, fileObj, mergePath);

                objMerged = deepMerge(objMerged, baseObjToMerge);
            } else if(stat.isDirectory()) {
                const baseObjToMerge = this.recursiveGetGameDefsObj([...objPath, entry]);
                objMerged = deepMerge(objMerged, baseObjToMerge);
            }
        }
        return objMerged
    },

    get gameDefs():GameDefsType.Root {
        if(gameDefs == null) {
            try {
                // We clone to disconnect references in the gamedefs tree..
                // If we don't do that, a NukemNet extension could modify one internal reference,
                // which might have the side effect of affecting other places in the gamedefs tree if they refer to the same modified objects
                gameDefs = deepClone(this.recursiveGetGameDefsObj([])) as GameDefsType.Root;

                for(const ext in this.loadedJsExtensions) {
                    const obj = this.loadedJsExtensions[ext];
                    try {
                        obj?.gameDefs?.(gameDefs, {deepClone, GameDefsType});
                    } catch(e) {
                        console.error('Error in extension gameDefs', ext, e);
                    }
                }

                
                //
                // Adding internal NukemNet params:
                //
                for(const gameId in gameDefs.games) {
                    const game = gameDefs?.games[gameId];
                    if(!game.executables) continue;;
                    for(const execId in game.executables) {
                        const exec = game?.executables?.[execId];
                        if(!exec.parameters) exec.parameters = {};

                        exec._execId = execId;
                        exec._gameId = gameId;

                        // add ipx/serial param for dosbox games
                        if(exec?.runnables?.multiplayer?.runTool?.type === 'dosbox') {
                            const net = exec?.runnables?.multiplayer?.runTool?.data?.net;
                            if(Array.isArray(net)) {
                                // Add param
                                exec.parameters[GameDefsType.DOSBOX_NETTYPE_PARAM_ID] = {
                                    "syncOnly": true,
                                    "modeSupport": ["multiplayer"],
                                    "type": "choice",
                                    "label": "Net",
                                    description: "IPX/Serial(COM1)",
                                    value: ["value[0]", "value[1]"],
                                    choices: net.map(n=>({label:n, value:n.startsWith('serial')?['serial', n.split(':')?.[1]]:[n]}))
                                }
                            }
                        }

                        // if game uses a tcp port, add an option to use NukemNet's udp tunneler, or plain sockets with TCP.
                        if(exec?.networking?.tcpPort?.port != null) {
                            const label =  exec.networking?.udpPort?.port? 'TCP&UDP':'TCP';
                            exec.parameters[GameDefsType.UDPTUNNEL_CHOICE_PARAM_ID] = {
                                "syncOnly": true,
                                "modeSupport": ["multiplayer"],
                                "type": "choice",
                                "label": "Protocol",
                                description: "Use IPv4 UDP Tunneler (with TCP over KCP), or standard TCP",
                                value: ["value[0]"],
                                choices: [
                                    {label, value:['standard']}
                                ].concat(exec?.networking?.disableUdpTunnel?[]:[{label:'UDP4Tunnel', value:['udp4tunnel']}])
                            }
                        }

                        // Add option to select dedicated server mode with no participation
                        if(exec?.runnables?.multiplayer?.hostAlsoRunsAsClient) {
                            exec.parameters[GameDefsType.DEDICATEDMODE_PARAM_ID] = {
                                "modeSupport": ["multiplayer"],
                                "type": "boolean",
                                "label": "Dedicated",
                                "description": "Starts in dedicated-server mode, without participating in the game.",
                                "value": "'Yes'",
                                "for":"host-only-shared",
                                syncOnly: true
                            }
                        }
                    }
                }
                // Freezing the entire gameDef tree to prevent unwanted modifications.
                // gameDefs = utils.deepFreeze(gameDefs) as GameDefsType.Root
            } catch(e) {
                console.error(`Could not parse GameDefs\r\n${(e as any)?.file} at ${(e as any)?.position}\r\n${(e as any)?.message}\r\n${(e as any)?.stack}`)
            }
        }

        return gameDefs as GameDefsType.Root;
    },

    soundEffectDebounceMap: {} as {[snd:string]:number},
    async playSoundEffect(snd: SoundTypes_All_Types, volume?:number, disableDebounce?:boolean) {
        if(mainStore.muteAll) return;

        const str = snd as unknown as string;

        if(!disableDebounce) {
            const now = Date.now();
            if((this?.soundEffectDebounceMap?.[str]||0)+500 > now) {
                return;
            } else {
                this.soundEffectDebounceMap[str] = now;
            }
        }
        
        try {
            const fullFilesystemPathForSoundFolder = path.resolve(pathOfSounds, str);
            const fullFilesystemPathForUserSoundFolder = path.resolve(pathOfUserSounds, str);
            
            const sound_path_files = await (async function():Promise<[string,string[]]> {
                try {
                    const userSoundFiles = await fs.readdir(fullFilesystemPathForUserSoundFolder);
                    return [fullFilesystemPathForUserSoundFolder, userSoundFiles];
                } catch(e) {}
                const resSoundFiles = await fs.readdir(fullFilesystemPathForSoundFolder);
                return [fullFilesystemPathForSoundFolder, resSoundFiles];
            })();
            
            const playableSoundFiles = sound_path_files[1].filter(f=>{
                try {
                    const info = path.parse(f);
                    const ext = info.ext.toLowerCase();
                    return ['.mp3', '.wav', '.ogg'].includes(ext);
                } catch(e) {
                    return false;
                }
            })

            const fileIndexToUse = Math.floor(Math.random() * playableSoundFiles.length);
            const fileToUse = playableSoundFiles[fileIndexToUse];

            const webFullPath = 'file://'+path.resolve(sound_path_files[0],fileToUse).replaceAll('\\', '/');

            let finalPathToPlay:string|null = webFullPath;
            for(const ext in this.loadedJsExtensions) {
                const obj = this.loadedJsExtensions?.[ext];
                if(obj?.willPlaySoundFile != null) {
                    const finalSound = await obj.willPlaySoundFile(webFullPath, fileToUse);
                    if(finalSound?.length) {
                        finalPathToPlay = finalSound;
                    } else if(finalSound === null) {
                        finalPathToPlay = null;
                    }
                    break;
                }
            }
            if(finalPathToPlay == null) return;
            const audio = new Audio(finalPathToPlay);
            
            if(volume!=null) {
                audio.volume = volume;
            } else if(Number.isInteger(Settings.read()?.soundVolume)) {
                audio.volume = Settings.read().soundVolume!/100;
            } else {
                audio.volume = 1;
            }
            audio.play();
        } catch(e) {}
    },

    forbiddenGamePortNumbers: new Set<number>(),
    checkIfPortIsAllowed(port:number) {
        if(this.forbiddenGamePortNumbers.size<=0) {
            for(const gameId in this.gameDefs.games) {
                const game = this.gameDefs.games[gameId];
                for(const execId in game.executables) {
                    const exec = game.executables[execId];
                    if(exec.networking) {
                        if(exec.networking.udpPort?.constant) {this.forbiddenGamePortNumbers.add(exec.networking.udpPort.port);}
                        if(exec.networking.tcpPort?.constant) {this.forbiddenGamePortNumbers.add(exec.networking.tcpPort.port);}
                        if(exec.networking.secondaryPorts) {
                            for(const portEntry of exec.networking.secondaryPorts) {
                                if(portEntry.constant) {this.forbiddenGamePortNumbers.add(portEntry.port)}
                            }
                        }
                    }
                }
            }
        }
        return !this.forbiddenGamePortNumbers.has(port)
    },

    async getBaseEvalObject(existingSettings?:any) {
        const settings = existingSettings? existingSettings:this.read();
        const baseEvalObject = {Settings: settings}
        return baseEvalObject;
    },


    async launchDefaults(props?:{refresh?:boolean, save?:boolean}): Promise<LaunchDefaults> {
        if(props?.save) {
            try {
                await fs.writeFile(pathOfLaunchDefaults, JSON.stringify(launchDefaultsObj, null, 2));
            } catch(e) {}
            return launchDefaultsObj!;
        }

        if(launchDefaultsObj == null || props?.refresh) {
            try {
                launchDefaultsObj = JSON.parse(await fs.readFile(pathOfLaunchDefaults, 'utf8'));
            } catch(e) {
                launchDefaultsObj = {games: {}};
            }
        }
        return launchDefaultsObj!;
    },

    ip2country(ip:string):string|null {
        try {
            const country = ip2location.getCountryShort(ip);
            if(country?.length === 2) return country; 
        } catch(e) {}
        return null;
    },

    get pjson() {
        return (window as any).electron.PackageJson as any;
    },

    get hasStoredCredentials() {
        const json = this.read(true);
        return !!(json?.IrcNick?.length && json?.IrcNickPass?.length/* && json?.jwt?.length*/);
    },

    
    async resolveWindowsPath(winPath:string) {
        if(isWindows()) {
            const replaced = winPath.replace(/%([^%]+)%/g, (_,n) => ps.env[n]);
            return replaced;
        } else if(Settings.read()?.wine?.path_exe) {
            // Use WINE to resolve path
            const proc = child_process.spawn(Settings.read()!.wine!.path_exe!, ['cmd', '/c', `echo ${winPath}`]);

            const cmdLineResult:string = await new Promise((res, rej)=>{
                let ended = false;

                function end(result?:string, err?:any) {
                    if(ended) return;
                    ended = true;
                    try {proc.removeAllListeners();} catch(e) {}
                    try {proc.stdout.removeAllListeners();} catch(e) {}
                    try {proc.kill()} catch(e) {}
                    if(result) {
                        res(result);
                    } else {
                        rej(err);
                    }
                }

                proc.stdout.on('data', (msg:Buffer)=>{
                    end(msg.toString().trim());
                });
                proc.on('close', ()=>{end(undefined,new Error('Wine CMD closed without output'));});
                proc.on('error', (e)=>{end(undefined,e);});
            });

            const parts = cmdLineResult.split(":\\");

            const driveLowerCase = parts[0].toLowerCase();

            const restOfPath = parts[1].replaceAll("\\", "/");

            const actualPath = path.join(ps.env.HOME as string,`/.wine/dosdevices/${driveLowerCase}:/${restOfPath}`);
            return actualPath
        } else {
            throw new Error('Missing WINE path');
        }
    },

    async findResFile(gameId:string, execId:string, res:string|undefined, _filePath:string, hash:string, secondRun=false):
    Promise<string|{error:"not_found"|"diff_hash",message:string}> {
        // first iteration, we find file.
        // if found file, but different hash, we try again, but this time we look for a file with specific hash.

        let filePath = _filePath;
        const fileName = path.basename(filePath);

        const gameExecSettings = Settings.read().games?.[gameId]?.[execId]!;
        const gameExecDir = gameExecSettings.path;

        if(fileName != filePath && !path.isAbsolute(filePath) && gameExecDir?.length) {
            filePath = path.join(gameExecDir, filePath);
        }
        
        if(!fsSync.existsSync(filePath)) {
            filePath = '' // not found
            const fileNameGlobEscaped = utils.escapeGlob(fileName);
            async function lookupFile(basePath:string) {
                let result = (await glob.glob(`**/`+`${fileNameGlobEscaped}`, {cwd:basePath, nocase:true, absolute:true, ignore:`**/${NNLinks_DirName}/**`}));

                // remove non-files
                for(let i=result.length-1; i>=0; i--) {
                    const entry = result[i];
                    const s = await fs.stat(entry);
                    if(!s.isFile() /*|| s.isSymbolicLink()*/) {
                        result.splice(i,1);
                    }
                }

                // if secondRun iterate all files in result, and find one matching hash.
                if(result?.length) {
                    if(secondRun) {
                        for(const file of result) {
                            if(hash == crypto.createHash('md5').update(await fs.readFile(file)).digest("hex")) {
                                return file;
                            }
                        }
                    } else {
                        return result[0]
                    }
                }
                return '' // not found
            }

            const resPath = gameExecSettings?.resPath?.[res||''];
            if(resPath?.length) {
                filePath = await lookupFile(resPath);
            }
            if(!filePath?.length && gameExecDir?.length) {
                filePath = await lookupFile(gameExecDir);    
            }
            if(!filePath?.length && res?.length) {
                const possiblePath = this.getPathOfResDirInUserDir(gameId, execId, res);
                filePath = await lookupFile(possiblePath)
            }
        }

        if(!filePath?.length || !fsSync.existsSync(filePath)) {
            return {error: 'not_found', message:"File not found: "+fileName};
        }

        const fileBuf = await fs.readFile(filePath);
        const fileMd5 = crypto.createHash('md5').update(fileBuf).digest("hex");
        if(fileMd5 != hash) {
            if(!secondRun) {
                const secondResult = await this.findResFile(gameId, execId, res, _filePath, hash, true);
                if(typeof secondResult === 'string' && secondResult?.length) {
                    return secondResult;
                }
            }
            return {error: 'diff_hash', message:"File hash mismatch: "+fileName};
        }
        return filePath;
    },

    getPathOfResDirInUserDir(gameId:string, execId:string, res:string) {
        return path.join(pathOfUserDir, 'games', gameId, execId, 'res', res);
    },

    async preferredDownloadFilePathForGameResource(gameId:string, execId:string, res:string, fileName:string, hash:string) {
        fileName = path.basename(fileName);
        const gameExecSettings = Settings.read().games?.[gameId]?.[execId]!;
        const gameExecDir = gameExecSettings.path;

        const resPath = gameExecSettings?.resPath?.[res];
        const resultFolder = await (async ()=>{
            if(resPath?.length && fsSync.existsSync(resPath)) {
                return resPath;
            }
            if(gameExecDir?.length && fsSync.existsSync(gameExecDir)) {
                const resPath = path.join(gameExecDir, `nnres_${res}`);
                try {
                    if(fsSync.existsSync(resPath)) return resPath;
                    await fs.mkdir(resPath, {recursive:true});
                    return resPath;
                } catch(e) {}
            }
            const userResDir = this.getPathOfResDirInUserDir(gameId, execId, res);
            if(!fsSync.existsSync(userResDir)) {
                try {await fs.mkdir(userResDir, {recursive:true});} catch(e) {}
            }
            return userResDir;
        })();
        
        const fullFilePath = path.join(resultFolder, fileName);

        if(fsSync.existsSync(fullFilePath)) {
            try {
                if(crypto.createHash('md5').update(await fs.readFile(fullFilePath)).digest("hex") != hash) {
                    const subfolderName = `${fileName}_${DateNow_YYYYMMDD()}_${hash}`
                    const newPath = path.join(resultFolder,subfolderName,fileName);
                    try {await fs.mkdir(newPath, {recursive:true});} catch(e) {}
                    return newPath;
                }
            } catch(e) {}
        }
        return fullFilePath;
    },

    registerShortcuts() {
        electronRemote.globalShortcut.unregisterAll();
        const s = this.read();
        if(s.shortcutKeys?.terminateProc?.length) {
            try {
                electronRemote.globalShortcut.register(s.shortcutKeys.terminateProc, ()=>{
                    terminateGameProcess(true);
                });
            } catch(e) {
                DialogMgr.addPopup({
                    title: "Malformed shortcut key",
                    body: $(`
                     <div>Shortcut key format for terminating game process is malformed:<br/><b>${s.shortcutKeys.terminateProc}</b></div>
                    `),
                    closeButton: true, buttons: [{body:"OK"}]
                });
                delete s.shortcutKeys?.terminateProc;
                Settings.write(s);
            }
        }
    },

    filterParam_For(imHost:boolean, pFor?:any) {
        if(pFor != null) {
            if(imHost && GameDefsType.ParamType.ParamFor_ClientOnly.includes(pFor)) return false;
            if(!imHost && GameDefsType.ParamType.ParamFor_HostOnly.includes(pFor)) return false;
        }
        return true
    }
}

const ip2location = new ip2loc.IP2Location();
ip2location.open(path.join(pathOfRes, 'libs', 'IP2LOCATION-LITE-DB1.BIN'));
