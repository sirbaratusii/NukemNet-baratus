import { Navbar, Container, Nav, Button, Spinner, Switch } from "solid-bootstrap"
import { Component, Show } from "solid-js"
import { electronRemote } from "./NodeLibs";
import { Settings } from "./Settings";
import { mainStore } from "./store/store";

const TopBar:Component<{onCommandClick:(cmd:string)=>void}> = (props)=>{
    return <Navbar collapseOnSelect bg="dark" variant="dark">
        <Container>
            <Navbar.Brand>

                {/* enable once I support login system
                <Show when={mainStore.isRegistered}>
                    <Show when={!mainStore.isLoggedIn}>
                        <Button style={{"font-size":'9pt', 'margin':'2pt'}} onClick={()=>props.onCommandClick('login')}>Login</Button>
                        <Button style={{"font-size":'9pt', 'margin':'2pt'}} onClick={()=>props.onCommandClick('register')}>Register</Button>
                    </Show>
                    <Show when={mainStore.isLoggedIn}>
                        <Button onClick={()=>props.onCommandClick('logout')}>Logout</Button>
                    </Show>
                </Show> */}
                <span style={{height:'auto',width:'100px'}} class="image_logo_png"></span>
            </Navbar.Brand>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav class="me-auto" variant="pills" activeKey="1">
                    <Nav.Link accessKey="1" style={{"background-color":'rgb(38 95 123)'}} eventKey="1" onClick={()=>props.onCommandClick('launch_game')}><b>🎮Play</b></Nav.Link>
                    <Nav.Link style={{"background-color":'rgb(64 51 10)', "margin-left":'3pt'}} onClick={()=>props.onCommandClick('nukemnet_settings')}><b>⚙️Setup</b></Nav.Link>
                    <Nav.Link onClick={()=>props.onCommandClick('port_forward')}>PortConfig</Nav.Link>
                    <Nav.Link style="display:flex; flex-justify:center" onClick={()=>{props.onCommandClick('toggle_roomlist');Settings.playSoundEffect('switch', undefined, true)}}>Rooms&nbsp;<span style={{height:'23px',width:'23px'}} class={mainStore.showRoomsList?"image_swon_png":"image_swoff_png"}></span></Nav.Link>
                                
                    <Show when={mainStore.botIsAvailable()}>
                        <Nav.Link disabled={mainStore.isRefreshingGameRoomsList} class={mainStore.isRefreshingGameRoomsList?"refreshing-rooms":''} title="Refresh game rooms list.\r\nNormally you don't need to use it\r\nRooms are live updated.\r\nIt's here just in case." onClick={()=>props.onCommandClick('refresh_rooms')}>RefreshRooms</Nav.Link>
                    </Show>
                    <Nav.Link title="Join room by manually specifiying channel" onClick={()=>props.onCommandClick('join_room')}>Join</Nav.Link>

                    <Nav.Link color="primary" title="About" onClick={()=>props.onCommandClick('about')}>About</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Container>
        <Button title={mainStore.muteAll?"Click to unmute":"Click to mute"} style={{"margin-right":'10pt'}} onclick={()=>{
            mainStore.muteAll = !mainStore.muteAll;
            const settings = Settings.read();
            settings.muteAll = mainStore.muteAll;
            Settings.write(settings);
        }}>{mainStore.muteAll?'🔇':'🔊'}</Button>
        <Button title="Hide to tray" style={{"margin-right":'10pt'}} onclick={()=>{electronRemote.getCurrentWindow().hide();}}>🔽</Button>
    </Navbar>
}

export default TopBar;