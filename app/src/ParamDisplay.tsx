import { Component, For, Show } from "solid-js";
import { LaunchArgType } from "./lib/LaunchArgType";
import { FullGameRoomDetails } from "./commands-gameroom";
import { mainStore } from "./store/store";

type Prop = {channel:string, game:FullGameRoomDetails}

export const ParamDisplay: Component<Prop> = (props:Prop) => {

    const {channel, game} = props;

    const manualParams = ()=>{
        try {
            const params = game.params.filter(p=>p?.id?.startsWith("_m_"));
            const args = [] as string[];
            params.forEach(p=>args.push(...p.args))
            return args;
        } catch(e) {
            return []
        }
    }

    return <Show when={game != null}>
        <table style={{"margin-top": "10pt"}}>
        <tbody style={{"text-align":'left'}}>
            <tr>
            <td class="side-panel-param-label">Host</td><td class="side-panel-param-value">{game.publicDetails.owner}</td>
            </tr>
            <tr>
            <td class="side-panel-param-label">Room</td><td class="side-panel-param-value">{game.publicDetails.name||game.publicDetails.channel}</td>
            </tr>
            <tr>
            <td class="side-panel-param-label">Game</td><td class="side-panel-param-value">{game.publicDetails.gameName}</td>
            </tr>
            <tr>
            <td class="side-panel-param-label">Exec</td><td class="side-panel-param-value">{game.publicDetails.execName}</td>
            </tr>
            <tr>
            <td class="side-panel-param-label">Players</td><td class="side-panel-param-value">{Object.keys(game.participantNicks).length}/{game.publicDetails.maxPlayers}</td>
            </tr>
            <For each={game.params}>
            {p=>{
                const description = mainStore.execDefForChannel(channel)?.parameters?.[p?.id]?.description || undefined;
                if(p.label == null || p.valueLabel == null) return null;
                return <tr title={description}><td class="side-panel-param-label">{p.label}</td><td class="side-panel-param-value">{p.valueLabel}</td></tr>
            }}
            </For>
            <Show when={manualParams()?.length}>
            <tr><td class="side-panel-param-label">Manual</td><td class="side-panel-param-value">{manualParams().join(' ')}</td></tr>
            </Show>
        </tbody>
    </table>
    </Show>
};