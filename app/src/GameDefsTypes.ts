import { AffiliateType } from "./affiliate";

export namespace GameDefsType {

    export type GameModes = "multiplayer"|"singleplayer"|"settings"|"mapeditor"



    export namespace ParamType {

        // private means host or any client can have they own separate values, and they don't need to share their choice, for example, -name PlayerName, each player has it's own.
        // shared is the default, param is added for both host and clients
        // host-only: param set by host, and runs only in host machine
        // client-only: param set by host, but not executed by host, the param is added only for the clients
        export type ParamFor = 'shared'|'private'|'host-only-private'|'host-only-shared'|'client-only-from-host'|'client-only-private'
        export const ParamFor_Unshared = ['private','host-only-private','client-only-private'] as ParamFor[]
        export const ParamFor_HostOnly = ['host-only-private', 'host-only-shared'] as ParamFor[];
        export const ParamFor_ClientOnly = ['client-only-from-host', 'client-only-private'] as ParamFor[];
        export const ParamFor_LocalOnly = ['private', 'client-only-private'] as ParamFor[];

        export type AllParamTypeStrings = "file"|"static"|"choice"|"boolean"|"numrange"|"dirfiles"|"text"; // Todo "file" not supported yet
        export const ParamFor_default: ParamFor = 'shared';

        export type NNEval_Func = (ctx:any)=>any

        export interface BaseParameter {
            modeSupport?: GameModes[]
            type: AllParamTypeStrings
            value: string|Array<string>|NNEval_Func|Array<NNEval_Func>,
            label: string
            allowMultiple?: boolean // TODO: not supported yet, ignored
            optional?: boolean,
            for?: ParamFor
            order?: number,
            section?: string, // section name to group params in UI
            description?: string, // text when hovering over the param

            addIf?: {
                hasImgMount?:boolean // including this param only if cd img is mounted on dosbox
            } |
            ((context:any)=>boolean), // only supported on static params

            syncOnly?: boolean // Whether to omit this param from cmd-line argument, and only share it with participants (usecase example, for command 'edit-ini')
        }
        export interface Static extends BaseParameter {
            type: "static"
        }
        export interface Boolean extends BaseParameter {
            type: "boolean",
            addFalse?: boolean // whether to calculate the param value even if the checkbox is unchecked.
        }
        export interface NumRange extends BaseParameter {
            type: "numrange"
            min:number
            max:number
            delta:number
        }
        export interface Choice extends BaseParameter {
            type: "choice"
            choices: {
                label: string,
                value: string|Array<string|number>
            }[]
        }
        export interface DirFiles extends BaseParameter {
            type: "dirfiles"
            path: string, // glob pattern path
            extenions?: string[], // e.g: [".grp"], although "path" property is glob pattern and can specify extension there.
            stripExt?: boolean,
            stripPath?: boolean,
            globOpts?: any // lookup "glob" npm package for it's options argument
            ignore?: string[]|string, // ignore listing some files (glob syntax)
            excludePath?: string[]|string // ignore listing some file (exact path syntax, case insensitive)
        }

        export interface Text extends BaseParameter {
            type: "text",
            props?: {} // properties to put on HTML input element
        }

        // has auto-file sharing on multiplayer
        export interface File extends BaseParameter {
            type: "file",
            res?: string, // resource folder (e.g: "maps", "mods"). if undefined, file sharing is disabled for it.

            // whether to symlink the file into the exec folder (instead of full path in parameter), some games/execs require it, such as hDuke and NetDuke32.
            // true: if resource file is not relative to game exec directory, create symlink.
            // 'forced' - always create symlink, even if file is relative to game exec directory, this is helpful for gaes wher er the argument for file path must be the same for all participants, such as netduke32.
            // 'overlay' creates an dosbox overlay mount into the mounted game exec directory. only applicable for dosbox games.
            symlink?: boolean|'forced'|'overlay', 
            
            props?: {} // properties of electronRemote.dialog.showOpenDialog
        }

        export type AllTypes = ParamType.File | ParamType.Text | ParamType.Static | ParamType.Boolean | ParamType.Choice | ParamType.NumRange | ParamType.DirFiles
    }

    export type Runnable_CmdBeforeRun_Base = {
        for?: ParamType.ParamFor,
        cmd:string,
        try?:boolean // try means: don't panic if failed, mark it as an optional command
        addIf?: (context:any)=>Promise<boolean>,
    }

    export type Runnable_CmdBeforeRun_EditBase_Entry = {"entry":string, "value":string, unquote?:boolean, skipNull?:boolean, addIf?: (context:any)=>Promise<boolean>} // skipNull does not remove the entry if value is null.

    export type Runnable_CmdBeforeRun_EditIni_Entry = Runnable_CmdBeforeRun_EditBase_Entry&{"category":string|((ctx:any)=>Promise<string>)}
    export interface Runnable_CmdBeforeRun_EditIni extends Runnable_CmdBeforeRun_Base {
        "cmd":"edit-ini"|"edit-space-delimited",
        "file":string,
        originFile?:string, // fileRef to another file, to copy it's content to the file before editing it. (e.g UnrealTournament.ini serves as the base for the new generated NukemNet.ini)
        "edits":Runnable_CmdBeforeRun_EditIni_Entry[]
    }


    export interface Runnable_CmdBeforeRun_Function extends Runnable_CmdBeforeRun_Base {
        "cmd":"function",
        run:(ctx:any, execDef:GameDefsType.Executable, ...rest:any[])=>Promise<void>
    }

    export type Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile_Entry = Runnable_CmdBeforeRun_EditBase_Entry
    export interface Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile extends Runnable_CmdBeforeRun_Base {
        "cmd":"edit-space-delimited",
        "file":string,
        "edits":Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile_Entry[]
    }

    export type Runnable_CmdBeforeRun_EditBinary_Entry = {"offset":number,"length":number, "value":any, numType?:string, litteEndian?:boolean, zeroFillByte?:number, addIf?: (context:any)=>Promise<boolean>}
    export interface Runnable_CmdBeforeRun_EditBinary extends Runnable_CmdBeforeRun_Base {
        "cmd":"edit-binary",
        "file":string,
        "edits":Runnable_CmdBeforeRun_EditBinary_Entry[]
        createMissing?:()=>Promise<Buffer>
    }

    export type Runnable_CmdBeforeRun_ALLTYPES = Runnable_CmdBeforeRun_EditIni|Runnable_CmdBeforeRun_EditBinary|Runnable_CmdBeforeRun_ReplaceSpaceDelimitedFile|Runnable_CmdBeforeRun_Function

    export interface RunTool_Base {
        type: "dosbox", // "java" (not supported yet)
        data?: any
    }

    export const Dosbox_NetType = {
        ipx: 'ipx',
        serial:'serial',
        serial_9600: 'serial:9600',
        serial_14400:'serial:14400',
        serial_19200:'serial:19200',
        serial_38400:'serial:38400',
        serial_57600:'serial:57600'
    }
    export const Dosbox_NetTypesAll = Object.values(Dosbox_NetType) as Array<keyof typeof Dosbox_NetType>;
    export const Dosbox_NetTypesAllSpecific = Object.values(Dosbox_NetType).filter(n=>n!='serial') as Array<keyof Omit<typeof Dosbox_NetType,'serial'>>;

    export type Dosbox_NetTypes = keyof typeof Dosbox_NetType

    export interface RunTool_Dosbox extends RunTool_Base {
        type: "dosbox",
        data: {
            net:Dosbox_NetTypes|Array<Dosbox_NetTypes>,
            args?:string[],
            commands?:string[],
            ini?:string|ParamType.NNEval_Func, // Extra INI entries for dosbox conf
            cdInto?:boolean // inside the dosbox instance, cd into the directory of the game (see GTA game definition for example)
        }
    }

    // param id for dosbox nettype, automatically added to the game's params by NukemNet, if using dosbox nettypes in Dosbox_NetType.
    // the param value will be the user's nettype choice.
    // 
    // this allows you to refer to selected net type in your args like so:
    // GameRoom?.Params?._nn_db_net?.[0] will be 'ipx' or 'serial'
    // GameRoom?.Params?._nn_db_net?.[1] will contain the baudrate if [0] is serial:9600 (for example) or will by null if "serial" is chosen (no baudrate specified)
    export const DOSBOX_NETTYPE_PARAM_ID = '_nn_db_net';

    // When using tunneler (UDPTunnel)
    export const UDPTUNNEL_CHOICE_PARAM_ID = '_nn_tunneler';

    // When using dedicated server mode (only server instance, no particiption) choice.
    export const DEDICATEDMODE_PARAM_ID = '_nn_dedicated';

    // export interface RunTool_Java extends RunTool_Base { // Not Supported yet
    //     type: "java"
    // }
    export type RunTool_AllTypes = RunTool_Dosbox // |RunTool_Java (Java not supported yet)

    export interface Runnable {
        file:string|((obj:any)=>string),
        files?:string[] // array of filerefs must be given if file is a function
        runTool?:RunTool_AllTypes, // which runTool to use, such as dosbox.
        beforeRun?:Runnable_CmdBeforeRun_ALLTYPES[], // some commands to run before launching the game, such as editing ini files.
        hostAlsoRunsAsClient?:boolean|((obj:any)=>boolean), // needed for games like Rise of the triad (standalone server), and Unreal/UT99 (launches both the game and dedicated server)
        waitForListenPortOnProto?:'udp' // Wait until the launched game binds to the udp port, before ordering other clients to launch the game. // |'tcp' TODO TCP not supported yet
        waitForMS?: number, // Once host game is launched, wait this amount of time before the clients launch as well.
        waitForFunc?: (obj:any)=>Promise<boolean>, // Wait until this function returns true before launching the game.
        useNetlist?:boolean // Custom netlist.txt file logic for xDuke/hDuke/nDuke etc...
    }

    export interface PortDef {type:"udp"|'tcp',port:number, constant?:boolean}

    export interface FileDef {
        path?:string,
        glob?:string|string[], // if given, uses glob pattern instead of entry in "path". only supported for runnable files.
        globOpts?:any // lookup "glob" npm package for it's options argument
        optional?:boolean,
        baseDirWin?:string,
        allowAdmin?:boolean, // allowAdmin only supported in edit-ini launch cmd for now
        runDetachedShell?:boolean|((obj:any)=>Promise<boolean>),
        runShell?:boolean|((obj:any)=>Promise<boolean>)
    }


    export type ExecutablePatchOpts = {dir:string, files:{[fileref:string]:string}}

    export interface Executable {
        _gameId:string, // automatically added by NukemNet, do not override it
        _execId:string, // automatically added by NukemNet, do not override it

        base?: string,
        maxPlayers?:number,
        minPlayers?:number,
        mountImg?:boolean|string, // true defaults to D, in string you can specify mount char
        midGameJoin?:boolean, // whether game support mid-game join.
        name: string,
        tips?: {
            popupBeforeLaunch?:boolean,
            html?:string,
            patches?: {
                [key:string]: {
                    title?:string,
                    files?:string[]
                    html?:string
                    apply?:(opts:ExecutablePatchOpts)=>Promise<{title:string, body:string}>
                    undo?:(opts:ExecutablePatchOpts)=>Promise<{title:string, body:string}>
                }
            },
        }
        clientCopyHostAddressToClipboard?:boolean, // copy host's address to clipboard when starting game.
        isPeerToPeer?: boolean,
        networking?: { // used only for non-dosbox executables
            disableUdpTunnel?:boolean, // don't allow udp tunneling for this game. the UDPTunnel util only runs as long as the game process is running, so if the game process stops the tunnel stops. this is a problem with some games such as Worms Armageddon on Steam, when starting the game, the process stops immediately and spawns a different process, so UDPTunnel is disabled for tha game.
            tcpPort?: {port:number, constant?:boolean, optional?:boolean}, // some games, like Unreal/UT99, have a tcp port for file downloads, but can work without it. using optional:true notifies NukemNet to show Port-Forward workarounds even in TCP mode (Relay/STUN)
            udpPort?: {port:number, constant?:boolean},
            secondaryPorts?:PortDef[],
            allocate_RandomPort?:boolean // whether to allocate a randomized ports
        },
        needsTempDir?:boolean, // whether the game needs a temp dir to be created for it (exposed to params as TempDir variable)
        hostInfoOnLaunch?:string|((ctx:any)=>Promise<string|null>), // shows the host a message in game room upon launch game.
        clientInfoOnLaunch?:string|((ctx:any)=>Promise<string|null>), // shows the client a message in game room upon launch game.
        namedPipeChatSync?: {unix?:string, windows?:string}, // used to sync messages between the irc game room, and the game internally (works with NetDuke32 and NotBlood only at the moment).
        files: {
            [id:string]: FileDef
        },
        runnables: {
            multiplayer?: Runnable,
            singleplayer?: Runnable,
            mapeditor?: Runnable,
            settings?: Runnable
        },
        parameters?: {
            [paramId:string]: ParamType.AllTypes
        },
        paramSections?: {
            [sectionId:string]: {
                name: string
            }
        }
    }

    export interface GetGameAfilliate {
        type:AffiliateType,
        nameId:string,
        name?:string // If no name given, defaults to name in game def
    }

    export interface GetGameAfilliate_GOG extends GetGameAfilliate {
        type: 'gog',
        prodId:string,
    }

    export interface GetGameAfilliate_ZoomPlatform extends GetGameAfilliate {
        type: 'zoom-platform'
    }

    export interface Game { // Root type returned for game definition
        name: string,
        executables: {
            [execId:string]: Executable
        },
        get: (GetGameAfilliate_GOG|GetGameAfilliate_ZoomPlatform)[]
    }

    export interface Root {
        // baseExecutables: { // ignore for now
        //         [id:string]: Executable
        // },
        games: {
            [gameId:string]: Game
        }
    }
}