
import fs from 'fs/promises'
import { GameDefsType } from "./src/GameDefsTypes";
import { deepClone } from "./src/lib/deepClone";

(async function main() {
    console.log("Generating /site/SupportedGames.json")

    const defFiles = (await fs.readdir('./res/defs/games')).filter((f:string)=>
        (f.endsWith('.js') || f.endsWith('.json')) &&
        !f.startsWith('_i_') && !f.endsWith('.i.js') && !f.endsWith('.i.json'));
    
    const resObj:any = {};
    for(const f of defFiles) {
        let def:GameDefsType.Game;
        if(f.endsWith('.js')) {
            const func = require('./res/defs/games/'+f);
            def = func({deepClone, GameDefsType});
        } else if(f.endsWith('.json')) {
            def = require('./res/defs/games/'+f);
        } else {
            throw new Error('Unknown file type');
        }
        if(!resObj?.[def.name]) resObj[def.name] = {e:[]}
        if(def.get) resObj[def.name].get = def.get;

        Object.keys(def.executables).forEach(execId=>{
            resObj[def.name].e.push(def.executables[execId].name);
        });
    }

    await fs.writeFile('../site/SupportedGames.json', JSON.stringify(resObj, null, 2));
    
    console.log('Done.');
})();