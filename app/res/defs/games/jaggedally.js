module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };
// TODO: doesn't start, fix
    return {
        "name": "Jagged Alliance",
        "get": [{
            type:"zoom-platform",
            nameId:"jagged-alliance",
            name:"Jagged Alliance"
        }],
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "snd": {"path":"SETSOUND.EXE"},
                    "main": {"path":"JA.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}