module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i')

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const commonExecBase = {
        "files": {
            "main": {"path":"STRIFE.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": null
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "I'm too young to die",
                    "value": "1"
                }, {
                    "label": "Hey, not too rough",
                    "value": "2"
                }, {
                    "label": "Hurt me plenty",
                    "value": "3"
                }, {
                    "label": "Ultra-Violence",
                    "value": "4"
                }, {
                    "label": "Nightmare!",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "fastMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Fast Monsters (3x)",
                "value": "'-fast'"
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    
    const Shareware_EpisodeAndLevels = [{
        "label": "32: Sanctuary",
        "value": ["-warp", "1"]
    }, {
        "label": "33: Town",
        "value": ["-warp", "2"]
    }, {
        "label": "34: Movement Base",
        "value": ["-warp", "3"]
    }]

    const Full_EpisodeAndLevels = [{
        "label": "1: Sanctuary",
        "value": ["-warp", "1"]
    }, {
        "label": "2: Town",
        "value": ["-warp", "2"]
    }, {
        "label": "3: Front Base",
        "value": ["-warp", "3"]
    }, {
        "label": "4: Power Station",
        "value": ["-warp", "4"]
    }, {
        "label": "5: Prison",
        "value": ["-warp", "5"]
    }, {
        "label": "6: Sewers",
        "value": ["-warp", "6"]
    }, {
        "label": "7: Castle",
        "value": ["-warp", "7"]
    }, {
        "label": "8: Audience Chamber",
        "value": ["-warp", "8"]
    }, {
        "label": "9: Castle: Programmer's Keep",
        "value": ["-warp", "9"]
    }, {
        "label": "10: New Front Base",
        "value": ["-warp", "10"]
    }, {
        "label": "11: Borderlands",
        "value": ["-warp", "11"]
    }, {
        "label": "12: The Temple of the Oracle",
        "value": ["-warp", "12"]
    }, {
        "label": "13: Catacombs",
        "value": ["-warp", "13"]
    }, {
        "label": "14: Mines",
        "value": ["-warp", "14"]
    }, {
        "label": "15: Fortress: Administration",
        "value": ["-warp", "15"]
    }, {
        "label": "16: Fortress: Bishop's Tower",
        "value": ["-warp", "16"]
    }, {
        "label": "17: Fortress: The Bailey",
        "value": ["-warp", "17"]
    }, {
        "label": "18: Fortress: Stores",
        "value": ["-warp", "18"]
    }, {
        "label": "19: Fortress: Security Complex",
        "value": ["-warp", "19"]
    }, {
        "label": "20: Factory: Receiving",
        "value": ["-warp", "20"]
    }, {
        "label": "21: Factory: Manufacturing",
        "value": ["-warp", "21"]
    }, {
        "label": "22: Factory: Forge",
        "value": ["-warp", "22"]
    }, {
        "label": "23: Order Commons",
        "value": ["-warp", "23"]
    }, {
        "label": "24: Factory: Conversion Chapel",
        "value": ["-warp", "24"]
    }, {
        "label": "25: Catacombs: Ruined Temple",
        "value": ["-warp", "25"]
    }, {
        "label": "26: Proving Grounds",
        "value": ["-warp", "26"]
    }, {
        "label": "27: The Lab",
        "value": ["-warp", "27"]
    }, {
        "label": "28: Alien Ship",
        "value": ["-warp", "28"]
    }, {
        "label": "29: Entity's Lair",
        "value": ["-warp", "29"]
    }, {
        "label": "30: Abandoned Front Base",
        "value": ["-warp", "30"]
    }, {
        "label": "31: Training Facility",
        "value": ["-warp", "31"]
    }, ...Shareware_EpisodeAndLevels]

    /*const VE_EpisodeAndLevels = [...Full_EpisodeAndLevels, {
        "label": "35: Factory: Production",
        "value": ["-warp", "35"]
    }, {
        "label": "36: Castle Clash",
        "value": ["-warp", "36"]
    }, {
        "label": "37: Killing Grounds",
        "value": ["-warp", "37"]
    }, {
        "label": "38: Ordered Chaos",
        "value": ["-warp", "38"]
    }]*/

    
    const sw = deepClone(commonExecBase);
    sw.parameters.episodeAndLevel.choices = Shareware_EpisodeAndLevels;

    const full = deepClone(commonExecBase);
    full.parameters.episodeAndLevel.choices = Full_EpisodeAndLevels;
    full.files.main.path = 'STRIFE1.EXE';
    
    /*const ve = deepClone(commonExecBase);
    ve.parameters.episodeAndLevel.choices = VE_EpisodeAndLevels;*/


    return {
        "name": "Strife",
        "get": [{
            type:"gog",
            prodId:"1432899949",
            nameId:"strife_veteran_edition",
            name:"The Original Strife: Veteran Edition"
        }],
        "executables": {
            "shareware": {
                "name":"Shareware (DOS)",
                ...sw
            },
            "full": {
                "name":"Full (DOS)",
                ...full
            }
            /*,
            "ve": {
                "name":"Veteran Edition (DOS)",
                ...ve
            }*/
        }
    }
}
