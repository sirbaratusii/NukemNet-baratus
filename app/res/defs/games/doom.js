module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i');

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const DoomShareware_EpisodeAndLevels = [{
        "label": "E1: Knee-Deep in the Dead",
        "value": ["-episode", "1"]
    }, {
        "label": "E1M1: Hangar",
        "value": ["-warp", "1", "1"]
    }, {
        "label": "E1M2: Nuclear Plant",
        "value": ["-warp", "1", "2"]
    }, {
        "label": "E1M3: Toxin Refinery",
        "value": ["-warp", "1", "3"]
    }, {
        "label": "E1M4: Command Control",
        "value": ["-warp", "1", "4"]
    }, {
        "label": "E1M5: Phobos Lab",
        "value": ["-warp", "1", "5"]
    }, {
        "label": "E1M6: Central Processing",
        "value": ["-warp", "1", "6"]
    }, {
        "label": "E1M7: Computer Station",
        "value": ["-warp", "1", "7"]
    }, {
        "label": "E1M8: Phobos Anomaly",
        "value": ["-warp", "1", "8"]
    }, {
        "label": "E1M9: Military Base",
        "value": ["-warp", "1", "9"]
    }];

    const DoomFull_EpisodeAndLevels = [...DoomShareware_EpisodeAndLevels, {
        "label": "E2: The Shores of Hell",
        "value": ["-episode", "2"]
    }, {
        "label": "E2M1: Deimos Anomaly",
        "value": ["-warp", "2", "1"]
    }, {
        "label": "E2M2: Containment Area",
        "value": ["-warp", "2", "2"]
    }, {
        "label": "E2M3: Refinery",
        "value": ["-warp", "2", "3"]
    }, {
        "label": "E2M4: Deimos Lab",
        "value": ["-warp", "2", "4"]
    }, {
        "label": "E2M5: Command Center",
        "value": ["-warp", "2", "5"]
    }, {
        "label": "E2M6: Halls of the Damned",
        "value": ["-warp", "2", "6"]
    }, {
        "label": "E2M7: Spawning Vats",
        "value": ["-warp", "2", "7"]
    }, {
        "label": "E2M8: Tower of Babel",
        "value": ["-warp", "2", "8"]
    }, {
        "label": "E2M9: Fortress of Mystery",
        "value": ["-warp", "2", "8"]
    }, {
        "label": "E3: Inferno",
        "value": ["-episode", "3"]
    }, {
        "label": "E3M1: Hell Keep",
        "value": ["-warp", "3", "1"]
    }, {
        "label": "E3M2: Slough of Despair",
        "value": ["-warp", "3", "2"]
    }, {
        "label": "E3M3: Pandemonium",
        "value": ["-warp", "3", "3"]
    }, {
        "label": "E3M4: House of Pain",
        "value": ["-warp", "3", "4"]
    }, {
        "label": "E3M5: Unholy Cathedral",
        "value": ["-warp", "3", "5"]
    }, {
        "label": "E3M6: Mt. Erebus",
        "value": ["-warp", "3", "6"]
    }, {
        "label": "E3M7: Limbo",
        "value": ["-warp", "3", "7"]
    }, {
        "label": "E3M8: Dis",
        "value": ["-warp", "3", "8"]
    }, {
        "label": "E3M9: Warrens",
        "value": ["-warp", "3", "9"]
    }]

    const DoomUltimate_EpisodeAndLevels = [...DoomFull_EpisodeAndLevels, {
        "label": "E4: Thy Flesh Consumed",
        "value": ["-episode", "4"]
    }, {
        "label": "E4M1: Hell Beneath",
        "value": ["-warp", "4", "1"]
    }, {
        "label": "E4M2: Perfect Hatred",
        "value": ["-warp", "4", "2"]
    }, {
        "label": "E4M3: Sever the Wicked",
        "value": ["-warp", "4", "3"]
    }, {
        "label": "E4M4: Unruly Evil",
        "value": ["-warp", "4", "4"]
    }, {
        "label": "E4M5: They Will Repent",
        "value": ["-warp", "4", "5"]
    }, {
        "label": "E4M6: Against Thee Wickedly",
        "value": ["-warp", "4", "6"]
    }, {
        "label": "E4M7: And Hell Followed",
        "value": ["-warp", "4", "7"]
    }, {
        "label": "E4M8: Unto the Cruel",
        "value": ["-warp", "4", "8"]
    }, {
        "label": "E4M9: Fear",
        "value": ["-warp", "4", "8"]
    }]


    const commonExecBase = {
        "maxPlayers":4,
        "files": {
            "main": {"path":"DOOM.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "wad": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "file",
                "res":"wads",
                "value": ["'-file'", "value"],
                "label": "WAD File",
                symlink:'forced',
                props: {filters: [{name: 'WAD Files', extensions: ['wad']}]},
                "optional":true
            },
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]", "value[2]"],
                "optional":true,
                "choices": DoomShareware_EpisodeAndLevels
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "I'm too young to die",
                    "value": "1"
                }, {
                    "label": "Hey, not too rough",
                    "value": "2"
                }, {
                    "label": "Hurt me plenty",
                    "value": "3"
                }, {
                    "label": "Ultra-Violence",
                    "value": "4"
                }, {
                    "label": "Nightmare!",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "fastMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Fast Monsters (3x)",
                "value": "'-fast'"
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    const shareware = deepClone(commonExecBase);
    shareware.parameters.episodeAndLevel.choices = DoomShareware_EpisodeAndLevels;

    const full = deepClone(commonExecBase);
    full.parameters.episodeAndLevel.choices = DoomFull_EpisodeAndLevels;

    const ultimate = deepClone(commonExecBase);
    ultimate.parameters.episodeAndLevel.choices = DoomUltimate_EpisodeAndLevels

    return {
        "name": "Doom",
        "get": [{
            type:"gog",
            prodId:"1440164514",
            nameId:"doom_1993",
            name:"DOOM (1993)"
        }],
        "executables": {
            "shareware": {
                "name":"Shareware (DOS)",
                ...shareware
            },
            "full": {
                "name":"Full (DOS)",
                ...full
            },
            "ultimate": {
                "name":"Ultimate (DOS)",
                ...ultimate  
            }
        }
    }
}
