module.exports = function({GameDefsType}) {

    const path = require('path');
    const fs = require('fs/promises')

    const prepareCfg = [{"cmd":"edit-binary","file":"serial_dat", "edits":[
        {"offset":0x0C, length:1, numType:'setUint8', "value":`1`}, // COM1
        {"offset":0x08, length:1, numType:'setUint8', "value":`(()=>{
            const speed = GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1];
            if(speed == 1200) return 0;
            else if(speed == 2400) return 1;
            else if(speed == 4800) return 2;
            else if(speed == 9600) return 3;
            else if(speed == 19200) return 4;
            else if(speed == 38400) return 5;
            else if(speed == 57600) return 6;
            return 0;
        })();`},
        {"offset":0x04, length:2, numType:'setUint16', litteEndian:true, "value":`(()=>{
            const speed = GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1];
            return speed?parseInt(speed):0;
        })();`}
    ], createMissing: async ()=>{
        return fs.readFile(path.join(__dirname, 'WINGS_SERIAL.DAT'));
    }}];

    const dosboxPreCommands = ['CONFIG -set "core=auto"', 'CONFIG -set "cycles=max"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":[
        "serial:1200",
        "serial:2400",
        "serial:4800",
        "serial:9600",
        "serial:19200",
        "serial:38400",
        "serial:57600",
    ]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer,
            beforeRun: prepareCfg
        },
        "settings": {"file":"main", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const tips = {html: `
    <h3>Both players do the following steps:</h3>
    <ol>
        <li>In game menu, click "Remote"</li>
        <li>click "Serial"</li>
        <li>click "Connect"</li>
    </ol>
    <p>The correct COM port number and speed are automatically set.</p>
    `};

    return {
        "name": "Wings",
        "executables": {
            "full": {
                tips,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"WINGS.EXE"},
                    serial_dat: {path: "SERIAL.DAT", optional:true}
                },
                "runnables": runnables
            }
        }
    };
}