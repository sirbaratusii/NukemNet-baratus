module.exports = function ({}) {
    const fs = require('fs/promises');
    const path = require('path');
    
    const prepareSetupCfg = [{"cmd":"edit-binary","file":"setup_cfg", "edits":[

        // These 3 are only saved in SETUP.CFG for reference, they are passed as command line args instead
        // {"offset":0x40, length:1, numType:'setUint8', "value":"GameRoom?.Params?.character?.[0]||0"},
        // {"offset":0x44, length:2, numType:'setUint16', "value":"2323"}, // Socket
        // {"offset":0x48, length:1, numType:'setUint8', "value":"GameRoom.NumberOfPlayers"},

        // These cannot be passed as command line params, must be set in SETUP.CFG
        {"offset":0x61, length:12, zeroFillByte:0x20, "value":"MyName"},
        {"offset":0x86, length:1, numType:'setUint8', "value":"parseInt(GameRoom?.Params?.map?.[0]||0x17)"},
    ], createMissing: async ()=>{
        return fs.readFile(path.join(__dirname, 'INPURSUIT.CFG'));
    }}];

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":"ipx"}},
            "beforeRun": prepareSetupCfg,
        }
    }

    const EpisodeAndLevels = [{
        "label": "1 Demo Level",
        "value": ["0x16"]
    }, {
        "label": "2 Reactor",
        "value": ["0x17"]
    }, {
        "label": "3 Lava Caves",
        "value": ["0x18"]
    }, {
        "label": "4 Courtyard",
        "value": ["0x19"]
    }, {
        "label": "5 Melee",
        "value": ["0x1A"]
    }, {
        "label": "6 Pillars",
        "value": ["0x1B"]
    }, {
        "label": "7 Castle",
        "value": ["0x1C"]
    }, {
        "label": "8 Sewers",
        "value": ["0x1D"]
    }, {
        "label": "9 Techno",
        "value": ["0x1E"]
    }, {
        "label": "10 Huge",
        "value": ["0x1F"]
    }];

    const execBase = {
        "files": {
            "main": {"path":"GREED.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "netgreed": {"path":"NETGREED.EXE"},
            "setup_cfg": {"path":"SETUP.CFG", optional:true},
        },
        "runnables": {
            "multiplayer": {
                "file":"netgreed",
                "runTool":vars.game.runToolOptsMultiplayer,
                "beforeRun":vars.game.beforeRun
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "numplayers": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "order":-10,
                "value": ["'/players'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "socket": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "order":-9,
                "value": ["'/socket'", "'2351'"],
                "for":"private"
            },
            "character": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Character",
                "value":["'/char'", "value"],
                "order":-8,
                "for":"private",
                "choices": [{
                    "label": "Cyborg",
                    "value": "0"
                },{
                    "label": "Lizard Man",
                    "value": "1"
                }, {
                    "label": "MooMan",
                    "value": "2"
                }, {
                    "label": "Specimen 7",
                    "value": "3"
                }, {
                    "label": "Dominatrix",
                    "value": "4"
                }]
            },
            "map": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Map",
                "value":["''+parseInt(value[0])"],
                "syncOnly": true,
                "choices": EpisodeAndLevels
            }
            /*
"modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Map",
            "optional":true,
            "value":["'/v' + value[0]", "'/l' + value[1]"],
            "choices": atomicAllLevels
            */
        }
    }

    return {
        "name": "In Pursuit of Greed",
        "executables": {
            "full": {
                "name":"Full (DOS)",
                ...execBase
            }
        }
    }
}
