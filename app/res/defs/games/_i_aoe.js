module.exports = function({deepClone}) {

    const runnables = {
        "multiplayer": {
            "file":"main",
        },
        "singleplayer": {"file":"main"}
    };

    return {
        "name": "Age of Empires",
        "executables": {
            "full": {
                "name":"Full",
                "clientInfoOnLaunch": (ctx)=>`Connect to ${ctx.GameRoom.DestIp} (copied to clipboard)`,
                tips: {
                    html: `<div>
                    <b>On Launch, connection details are copied to clipboard</b><br/>
                    <u>In game, click on:</u><br/>
                    1. Multiplayer<br/>
                    2. Internet TCP/IP Connection For DirectPlay<br/>
                    3.a <b>Host</b> clicks "Create".<br/>
                    3.b <b>Client</b> clicks "Show Games", and paste the IP address from the clipboard.<br/>
                    </div>`
                },
                networking: {
                    tcpPort: {port:2300, constant:true},
                    udpPort: {port:2350, constant:true},
                },
                clientCopyHostAddressToClipboard:true,
                "files": {
                    "main": {"path":"Empires.exe"}
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}