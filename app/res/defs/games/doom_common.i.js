const fs = require('fs/promises')
const path = require('path')
module.exports = {
    ensureSerIpxSetupFiles: (utils)=>[{"cmd":"function", async run(ctx) {
        for(const fileName of ['IPXSETUP.EXE','SERSETUP.EXE']) {
            try {
                const destination = path.resolve(ctx.GameDir, fileName);
                try {
                    if(!utils.findPathCaseInsensitiveSync(destination)) {
                        await fs.copyFile(path.resolve(__dirname, fileName), destination, fs.constants.COPYFILE_EXCL);
                    }
                } catch(e) {}
            } catch(e) {}
        }
    }}]
}