module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Abuse",
        "executables": {
            "v2_0": {
                "name":"V2.00 (DOS)",
                midGameJoin: true,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"ABUSE.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}