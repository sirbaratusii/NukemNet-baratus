module.exports = function() {
    const dosboxPreCommands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']; /* Descent runs well on these cycles */
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "args":["-machine", "vesa_nolfb"]}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands,"net":["ipx", "serial"],"args":["-machine", "vesa_nolfb"]}};

    const files = {
        "setup": {"path":"SETUP.EXE"},
        "main": {"path":"RALLY.EXE"}
    };

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool":runToolOpts},
        "singleplayer": {"file":"main", "runTool":runToolOpts}
    };

    return {
        "name": "Death Rally",
        // "get": [{
        //     type:"zoom-platform", // doesn't support multiplayer
        //     nameId:"death-rally",
        //     name:"Death Rally"
        // }],
        "executables": {
            "sw": {
                midGameJoin: true,
                "name":"Shareware (DOS)",
                "files": files,
                "runnables": runnables,
                "parameters": {}
            },
            "full1_1": {
                midGameJoin: true,
                "name":"Full 1.1 (DOS)",
                "files": files,
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}