module.exports = function({deepClone}) {


    function chosenGameMode(ctx) {
        return ctx.GameRoom.Params.mode[0];
    }

    const iniEdits = [{"cmd":"edit-ini",addIf:(ctx)=>ctx?.GameRoom?.ImHost==true, "originFile":"gameini", "file":"nnini", "edits":[

        {"category":"Engine.GameInfo","entry":"AdminPassword", unquote:true, "value":"''"},
        {"category":"Engine.GameInfo","entry":"GamePassword", unquote:true, "value":"''"},
        
        {"category":"Engine.GameInfo","entry":"MaxPlayers", unquote:true, "value":"16"},

        {"category":chosenGameMode,"entry":"bCoopWeaponMode", unquote:true, "value":"GameRoom?.Params?.wpnstay?.[0]?'True':'False'"},
        {"category":chosenGameMode,"entry":"bChangeLevels", unquote:true, "value":"GameRoom?.Params?.chnglvls?.[0]?'True':'False'"},

        {"category":chosenGameMode,"entry":"FragLimit", unquote:true, "value":"GameRoom?.Params?.frglmt||0"},
        {"category":chosenGameMode,"entry":"TimeLimit", unquote:true, "value":"GameRoom?.Params?.timelmt||0"},
        
        {"category":chosenGameMode,"entry":"InitialBots", unquote:true, "value":"GameRoom?.Params?.botsnum||0"},
        {"category":chosenGameMode,"entry":"bMultiPlayerBots", unquote:true, "value":"(GameRoom?.Params?.botsnum||0)>0"},

        {"category":'UnrealShare.BotInfo',"entry":"Difficulty", unquote:true, skipNull:true, "value":"GameRoom?.Params?.botdfclty?.[0]||null"},
        {"category":'UnrealShare.BotInfo',"entry":"bAdjustSkill", unquote:true, "value":"GameRoom?.Params?.botadjdfclty?.[0]?'True':'False'"}

    ]}]

    const unrealGoldIniEdits = [...iniEdits, {"cmd":"edit-ini",addIf:(ctx)=>ctx?.GameRoom?.ImHost==true, "file":"webserverini", "edits":[
        {"category":'UWebAdmin.WebAdminManager',"entry":"bEnabled", unquote:true, skipNull:true, "value":"'True'"},
        {"category":'UWebAdmin.SubWebManager',"entry":"Accounts", unquote:true, skipNull:true, "value":`'(Username="admin",Password="admin")'`}, // delete entry if exists, so no username or password needed for web ui
        {"category":'UWebAdmin.WebServer',"entry":"ListenPort", unquote:true, skipNull:true, "value":`GameRoom.MyPort`},
    ]}]

    const runnables = {
        "multiplayer": {
            "file":(obj)=>{return obj?.GameRoom?.ImHost?'ucc':'main'},
            "files":['ucc','main'],
            "beforeRun":iniEdits,
            hostAlsoRunsAsClient:true
        },
        "settings": {"file":"main"},
        "singleplayer": {"file":"main"}
    };

    const params = {
        "vidsetup": {
            "modeSupport": ["settings"],
            "type": "static",
            "value": [`'-firstrun'`],
            "for":"private"
        },
        "server": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'server'", "GameRoom.Params.map+'?alladmin?game='+GameRoom.Params.mode[0]", "'port='+GameRoom.MyPort", "'multihome=0.0.0.0'", "'ini=NukemNet.ini'"
            ],
            "for":"host-only-private",
        },
        "mode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Mode",
            "value":"value",
            "choices": [{
                "label": "Deathmatch",
                "value": "UnrealShare.DeathMatchGame"
            },{
                "label": "Team Deathmatch",
                "value": "UnrealShare.TeamGame"
            },{
                "label": "DarkMatch",
                "value": "UnrealI.DarkMatch"
            },{
                "label": "Cooperative",
                "value": "UnrealShare.CoopGame"
            },{
                "label": "King of the hill",
                "value": "UnrealI.KingOfTheHill"
            }],
            "for":"host-only-shared",
            syncOnly: true
        },
        "map": {
            "modeSupport": ["multiplayer"],
            "type": "dirfiles",
            "value": ["value[0]"],
            "label": "Map",
            "path": "Maps/*.unr",
            "stripExt": true,
            "stripPath":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "client": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'unreal://'+GameRoom.DestIp + ':'+GameRoom.DestPort"],
            "for":"client-only-private"
        },
        "wpnstay": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Weapon Stay",
            "description": "bMultiWeaponStay",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "chnglvls": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Change Levels",
            "description": "bChangeLevels",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "botdfclty": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0, // TODO write skill names
            "max":3,
            "delta":1,
            "label": "Bot Difficulty",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "botadjdfclty": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Adjust Difficulty",
            "description": "Adjust Bot Difficulty",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "botsnum": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":16,
            "delta":1,
            "label": "Bots",
            "description": "Bots Number",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "frglmt": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "label": "Frag Limit",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "timelmt": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "label": "Time Limit",
            "description": "Time Limit (minutes)",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        }
    }

    const execDef = {
        "maxPlayers":16,
        midGameJoin: true,
        networking: {
            tcpPort: {port:7777, optional:true},
            udpPort: {port:7777},
            secondaryPorts:[{type:"udp",port:7775, constant:true}]
        },
        "files": {
            "main": {"path":"System/Unreal.exe"},
            "ucc": {"path":"System/ucc.exe", runDetachedShell:true},
            "gameini": {"path":"System/Unreal.ini"},
            "nnini": {"path":"System/NukemNet.ini", optional:true}
        },
        "runnables": runnables,
        "parameters": params,
    }


    const goldExecDef = deepClone(execDef);
    goldExecDef.hostInfoOnLaunch = (ctx)=>`Server Management in: http://${ctx.GameRoom.DestIp}:${ctx.GameRoom.MyPort} (user&pass=admin)`,
    goldExecDef.files.webserverini = {"path":"System/WebServer.ini", optional:true}
    goldExecDef.runnables.multiplayer.beforeRun = unrealGoldIniEdits;

    return {
        "name": "Unreal",
        "executables": {
            "gold": {
                "name":"Gold",
                ...goldExecDef
            },
            "classic226": {
                "name":"ClassicV226",
                ...execDef
            }
        }
    };
}