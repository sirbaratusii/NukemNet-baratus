module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO edit ERAD.CFG config file to COM1, and baud rate on behalf of the user
    const tips = {html: `
    <h3>The following steps are only required if chosen Serial connectivity (not IPX)</h3>
    <ol>
        <li>In game menu, choose COM1</li>
        <li>Both players set the same Baud Rate</li>
    </ol>
    `};

    return {
        "name": "Eradicator",
        "get": [{
            type:"gog",
            prodId:"1454313816",
            nameId:"eradicator",
            name:"Eradicator"
        }],
        "executables": {
            "full": {
                "maxPlayers":8,
                "mountImg": true,
                tips,
                "name":"Full (DOS)",
                "files": {
                    "snd": {"path":"SETSOUND.EXE"},
                    "main": {"path":"ERAD.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}