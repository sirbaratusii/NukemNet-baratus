module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i')

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const TNT_EpisodeAndLevels = [{
        "label": "1: System Control",
        "value": ["-warp", "1"]
    }, {
        "label": "2: Human Bbq",
        "value": ["-warp", "2"]
    }, {
        "label": "3: Power Control",
        "value": ["-warp", "3"]
    }, {
        "label": "4: Wormhole",
        "value": ["-warp", "4"]
    }, {
        "label": "5: Hanger",
        "value": ["-warp", "5"]
    }, {
        "label": "6: Open Season",
        "value": ["-warp", "6"]
    }, {
        "label": "7: Prison",
        "value": ["-warp", "7"]
    }, {
        "label": "8: Metal",
        "value": ["-warp", "8"]
    }, {
        "label": "9: Stronghold",
        "value": ["-warp", "9"]
    }, {
        "label": "10: Redemption",
        "value": ["-warp", "10"]
    }, {
        "label": "11: Storage Facility",
        "value": ["-warp", "11"]
    }, {
        "label": "12: Crater",
        "value": ["-warp", "12"]
    }, {
        "label": "13: Nukage Processing",
        "value": ["-warp", "13"]
    }, {
        "label": "14: Steel Works",
        "value": ["-warp", "14"]
    }, {
        "label": "15: Dead Zone",
        "value": ["-warp", "15"]
    }, {
        "label": "16: Deepest Reaches",
        "value": ["-warp", "16"]
    }, {
        "label": "17: Processing Area",
        "value": ["-warp", "17"]
    }, {
        "label": "18: Mill",
        "value": ["-warp", "18"]
    }, {
        "label": "19: Shipping/Respawning",
        "value": ["-warp", "19"]
    }, {
        "label": "20: Central Processing",
        "value": ["-warp", "20"]
    }, {
        "label": "21: Administration Center",
        "value": ["-warp", "21"]
    }, {
        "label": "22: Habitat",
        "value": ["-warp", "22"]
    }, {
        "label": "23: Lunar Mining Project",
        "value": ["-warp", "23"]
    }, {
        "label": "24: Quarry",
        "value": ["-warp", "24"]
    }, {
        "label": "25: Baron's Den",
        "value": ["-warp", "25"]
    }, {
        "label": "26: Ballistyx",
        "value": ["-warp", "26"]
    }, {
        "label": "27: Mount Pain",
        "value": ["-warp", "27"]
    }, {
        "label": "28: Heck",
        "value": ["-warp", "28"]
    }, {
        "label": "29: River Styx",
        "value": ["-warp", "29"]
    }, {
        "label": "30: Last Call",
        "value": ["-warp", "30"]
    }, {
        "label": "31: Pharaoh",
        "value": ["-warp", "31"]
    }, {
        "label": "32: Caribbean",
        "value": ["-warp", "32"]
    }]

    const Plutonia_EpisodeAndLevels = [{
        "label": "1: Congo",
        "value": ["-warp", "1"]
    }, {
        "label": "2: Well of Souls",
        "value": ["-warp", "2"]
    }, {
        "label": "3: Aztec",
        "value": ["-warp", "3"]
    }, {
        "label": "4: Caged",
        "value": ["-warp", "4"]
    }, {
        "label": "5: Ghost Town",
        "value": ["-warp", "5"]
    }, {
        "label": "6: Baron's Lair",
        "value": ["-warp", "6"]
    }, {
        "label": "7: Caughtyard",
        "value": ["-warp", "7"]
    }, {
        "label": "8: Realm",
        "value": ["-warp", "8"]
    }, {
        "label": "9: Abattoire",
        "value": ["-warp", "9"]
    }, {
        "label": "10: Onslaught",
        "value": ["-warp", "10"]
    }, {
        "label": "11: Hunted",
        "value": ["-warp", "11"]
    }, {
        "label": "12: Speed",
        "value": ["-warp", "12"]
    }, {
        "label": "13: The Crypt",
        "value": ["-warp", "13"]
    }, {
        "label": "14: Genesis",
        "value": ["-warp", "14"]
    }, {
        "label": "15: The Twilight",
        "value": ["-warp", "15"]
    }, {
        "label": "16: The Omen",
        "value": ["-warp", "16"]
    }, {
        "label": "17: Compound",
        "value": ["-warp", "17"]
    }, {
        "label": "18: Neurosphere",
        "value": ["-warp", "18"]
    }, {
        "label": "19: NME",
        "value": ["-warp", "19"]
    }, {
        "label": "20: The Death Domain",
        "value": ["-warp", "20"]
    }, {
        "label": "21: Slayer",
        "value": ["-warp", "21"]
    }, {
        "label": "22: Impossible Mission",
        "value": ["-warp", "22"]
    }, {
        "label": "23: Tombstone",
        "value": ["-warp", "23"]
    }, {
        "label": "24: The Final Frontier",
        "value": ["-warp", "24"]
    }, {
        "label": "25: The Temple of Darkness",
        "value": ["-warp", "25"]
    }, {
        "label": "26: Bunker",
        "value": ["-warp", "26"]
    }, {
        "label": "27: Anti-Christ",
        "value": ["-warp", "27"]
    }, {
        "label": "28: The Sewers",
        "value": ["-warp", "28"]
    }, {
        "label": "29: Odyssey of Noises",
        "value": ["-warp", "29"]
    }, {
        "label": "30: The Gateway of Hell",
        "value": ["-warp", "30"]
    }, {
        "label": "31: Cyberden",
        "value": ["-warp", "31"]
    }, {
        "label": "32: Go 2 it",
        "value": ["-warp", "32"]
    }]

    const commonExecBase = {
        "files": {
            "main": {"path":"DOOM2.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "wad": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "file",
                "res":"wads",
                "value": ["'-file'", "value"],
                "label": "WAD File",
                symlink:'forced',
                props: {filters: [{name: 'WAD Files', extensions: ['wad']}]},
                "optional":true
            },
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]"],
                "optional": true,
                "choices": null
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "I'm too young to die",
                    "value": "1"
                }, {
                    "label": "Hey, not too rough",
                    "value": "2"
                }, {
                    "label": "Hurt me plenty",
                    "value": "3"
                }, {
                    "label": "Ultra-Violence",
                    "value": "4"
                }, {
                    "label": "Nightmare!",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "fastMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Fast Monsters (3x)",
                "value": "'-fast'"
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    const tnt = deepClone(commonExecBase);
    tnt.parameters.episodeAndLevel.choices = TNT_EpisodeAndLevels;

    const plutonia = deepClone(commonExecBase);
    plutonia.parameters.episodeAndLevel.choices = Plutonia_EpisodeAndLevels;


    return {
        "name": "Final Doom",
        "get": [{
            type:"gog",
            prodId:"1440161275",
            nameId:"doom_ii",
            name:"Doom II"
        }],
        "executables": {
            "tnt": {
                "name":"TNT/Evilution (DOS)",
                ...tnt
            },
            "plutonia": {
                "name":"The Plutonia Experiment (DOS)",
                ...plutonia  
            }
        }
    }
}