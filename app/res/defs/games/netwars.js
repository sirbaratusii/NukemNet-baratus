module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "NetWars",
        "executables": {
            "adv": {
                "maxPlayers":6,
                midGameJoin: true,
                "name":"Advanced (DOS)",
                "files": {
                    "main": {"path":"netwars.exe"}
                },
                "runnables": runnables,
                "parameters": {},
            },
            "v2": {
                "maxPlayers":4,
                midGameJoin: true,
                "name":"v2 (DOS)",
                "files": {
                    "main": {"path":"netwars.exe"}
                },
                "runnables": runnables,
                "parameters": {},
            },
        }
    };
}