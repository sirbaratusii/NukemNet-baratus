module.exports = function() {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const runToolOpts = {"type":"dosbox", data:{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"install", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Transport Tycoon Deluxe",
        "executables": {
            "full": {
                "mountImg": true,
                "name":"Full (DOS)",
                "maxPlayers":2,
                "files": {
                    "main": {"path":"TTDX.EXE"},
                    "install": {"path":"INSTALL.EXE"}
                },
                "runnables": runnables,
                parameters: {
                    "drive": {
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "static",
                        "value": [`imgMountDrive+":\\\\"`],
                        "addIf": {"hasImgMount":true},
                        "for":"private"
                    }
                }
            },
            "openttd": {
                "name":"OpenTTD",
                "maxPlayers":255,
                midGameJoin: true,
                networking: {
                    tcpPort: {port:3979},
                    udpPort: {port:3979}
                },
                "files": {
                    "main": {"glob":["openttd.exe","openttd"], "runDetachedShell":(obj)=>{return obj?.GameRoom?.ImHost==true}}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        hostAlsoRunsAsClient:true
                    },
                    "singleplayer": {"file":"main"}
                },
                parameters: {
                    "editor": {
                        "modeSupport": ["singleplayer"],
                        "type": "boolean",
                        "value": `'-e'`,
                        label:"Start Editor",
                        "for":"private"
                    },
                    "seed": {
                        "modeSupport": ["singleplayer", "multiplayer"],
                        "type": "text",
                        "value": [`'-G'`, `value`],
                        label:"Random seed",
                        "for":"shared",
                        "optional":true
                    },
                    "year": {
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":10000,
                        "delta":1,
                        "label": "Starting Year",
                        "value": ["'-t'", "value"],
                        "optional":true
                    },
                    "dedi": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": [`'-D'`, `'0.0.0.0:'+GameRoom.MyPort`],
                        "for":"host-only-private"
                    },
                    "join": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": [`'-n'`, "GameRoom.DestIp + ':' + GameRoom.DestPort"],
                        "for":"client-only-private"
                    }
                }
            }
        }
    };
}