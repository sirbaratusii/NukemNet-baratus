
module.exports = function ({}) {

    const common = require('./hl_common.i')();

    const game = 'svencoop';

    return {
        "name": "Sven Co-op",
        executables: {
            "compat": {
                name:"Compat",
                "maxPlayers":32,
                midGameJoin: true,
                tips: {html:`<div>
                The game directory is usually under C:\\Program Files (x86)\\Steam\\steamapps\\common\\Sven Co-op.<br/>
                You must launch Steam manually before launching the game.
                </div>`},
                "files": {
                    "dedicated": {glob:["svends.exe", "svends", "svends_run"], runDetachedShell:true, optional:true},
                    "main": {glob:["svencoop.exe", "svencoop", "svencoop_run"]},
                    "maindir": {"path":game}
                },
                "runnables": {
                    "multiplayer": {
                        "file":(ctx)=>ctx?.GameRoom?.Params?._dedicated?.[0]?'dedicated':'main',
                        beforeRun: common.writeCommandsCfg
                    }
                },
                parameters: common.getParams(game)
            }
        }
    }
}