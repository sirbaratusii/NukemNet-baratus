module.exports = function({deepClone}) {

    const iniEditWinDir = {"cmd":"edit-ini", try:true, "file":"windows_ini", "edits":[
        {addIf:(ctx)=>ctx?.GameRoom?.ImHost==true, "category":"NetSettings","entry":"HostingPort", unquote:true, "value":"GameRoom.MyPort"}
        //{"category":"NetSettings","entry":"PlayerName", unquote:true, "value":"MyName"}
    ]};

    const iniEditLocalAppDataDir = deepClone(iniEditWinDir);
    iniEditLocalAppDataDir.file = "localappdata_ini";

    const iniEdits = [iniEditWinDir, iniEditLocalAppDataDir]

    const runnables = {
        "multiplayer": {
            "file":"main",
            "beforeRun":iniEdits,
        },
        "singleplayer": {"file":"main"}
    };

    const ropeKnockingAndBloodAmount_static = `[(GameRoom.Params?.bloodam?.[0]||''),(GameRoom.Params?.ropekn?.[0]||'')].filter(p=>p?.length).join(',')`;

    const params = {
        "bloodam": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":4,
            "delta":1,
            "label": "Blood Amount",
            "value": "'B'+String.fromCharCode(97+value)",
            "optional":true,
            syncOnly: true
        },
        "ropekn": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":11,
            "delta":1,
            "label": "Rope Knocking",
            "description": "Set rope Pushing power level",
            "value": "'P'+String.fromCharCode(97+value)",
            "optional":true,
            syncOnly: true
        },
        "noi": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'/nointro'"],
            "for":"private",
        },
        "host": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": [`'wa:host?gameid=NN&scheme='+
            ${ropeKnockingAndBloodAmount_static}`],
            "for":"host-only-private",
            "order":100
        },
        "client": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": [`
            'wa://'+GameRoom.DestIp+':'+GameRoom.DestPort+'?gameid=NN&scheme='+
            ${ropeKnockingAndBloodAmount_static}`],
            "for":"client-only-private",
        }
    }

    return {
        "name": "Worms 2",
        "get": [{
            type:"gog",
            prodId:"1462173886",
            nameId:"worms_armageddon",
            name:"Worms: Armageddon"
        }],
        "executables": {
            "wa": {
                "name":"Armageddon",
                networking: {
                    disableUdpTunnel:true, // because steam version closes the game immediately, causing the UDPTunnel to close, and then steam spawns the actual game process
                    tcpPort: {port:17011}
                },
                tips: {
                    html: `<div>
                    <h5>Windows Users</h5>
                    <p>
                        For worms to launch properly on Steam version, make sure Steam is already running.<br/><br/>
                        <b>NOTE</b><br/>
                        To set the game socket listen port number for Worms Armageddon,<br/>
                        the file %SystemRoot%\\win.ini is edited. This requires admin privileges.<br/>
                        This is where this specific game stores its settings.
                    </p>
                    <h5>WINE Users</h5/>
                    <p>Worms Armageddon is known to work with WINE. However, you may need change a few things:</p>
                    <ul>
                        <li>Run wine regedit YourArmageddonDir/Tweaks/Renderer_Direct3D_9_Shader_Palette.reg</li>
                        <li>Optional: manually set these registry entries:</li>
<pre>
HKCU/Software/Wine/Direct3D:
VideoPciVendorID=DWORD 10de (just an example hex value)
VideoPciDeviceID=DWORD 402 (just an example hex value)
</pre>
                    </ul>
                    </div>`
                },
                "files": {
                    "main": {"path":"WA.exe", runShell:true}, // without shell, Steam version does not spawn the Steam app, and you have to open Steam manually first.
                    "windows_ini": {"path":"win.ini", baseDirWin:"%SystemRoot%", allowAdmin:true, optional:true},
                    "localappdata_ini": {"path":"win.ini", baseDirWin:"%LOCALAPPDATA%\\VirtualStore\\Windows", optional:true}
                },
                "runnables": runnables,
                "parameters": params,
            }
        }
    };
}