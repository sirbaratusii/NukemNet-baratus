
module.exports = function({deepClone, GameDefsType, mainStore, utils}) {

    const fs = require('fs/promises');
    const path = require('path');

    const sharewareEpisodeOneLevels = [
        {"label": "Episode 1: L.A Meltdown","value": ["1", "1"]},
        {"label": "E1L1 Hollywood Holocaust","value": ["1", "1"]},
        {"label": "E1L2 Red Light District","value": ["1","2"]},
        {"label": "E1L3 Death Row","value": ["1","3"]},
        {"label": "E1L4 Toxic Dump","value": ["1","4"]},
        {"label": "E1L5 The Abyss","value": ["1","5"]},
        {"label": "E1L6 Launch Facility","value": ["1","6"]}
    ];

    const atomicAllLevels = sharewareEpisodeOneLevels.concat([
        {"label": "E1L7 Faces of Death","value": ["1", "7"]},
        {"label": "E1L8 User Map","value": ["1", "8"]},


        {"label": "Episode 2: Lunar Apocalypse","value": ["2", "1"]},

        {"label": "E2L1 Spaceport","value": ["2", "1"]},
        {"label": "E2L2 Incubator","value": ["2", "2"]},
        {"label": "E2L3 Warp Factor","value": ["2", "3"]},
        {"label": "E2L4 Fusion Station","value": ["2", "4"]},
        {"label": "E2L5 Occupied Territory","value": ["2", "5"]},
        {"label": "E2L6 Tiberius Station","value": ["2", "6"]},
        {"label": "E2L7 Lunar Reactor","value": ["2", "7"]},
        {"label": "E2L8 Dark Side","value": ["2", "8"]},
        {"label": "E2L9 Overlord","value": ["2", "9"]},
        {"label": "E2L10 Spin Cycle","value": ["2", "10"]},
        {"label": "E2L11 Lunatic Fringe","value": ["2", "11"]},

        {"label": "Episode 3: Shrapnel City","value": ["3", "1"]},

        {"label": "E3L1 Raw Meat","value": ["3", "1"]},
        {"label": "E3L2 Bank Roll","value": ["3", "2"]},
        {"label": "E3L3 Flood Zone","value": ["3", "3"]},
        {"label": "E3L4 L.A. Rumble","value": ["3", "4"]},
        {"label": "E3L5 Movie Set","value": ["3", "5"]},
        {"label": "E3L6 Rabid Transit","value": ["3", "6"]},
        {"label": "E3L7 Fahrenheit","value": ["3", "7"]},
        {"label": "E3L8 Hotel Hell","value": ["3", "8"]},
        {"label": "E3L9 Stadium","value": ["3", "9"]},
        {"label": "E3L10 Tier Drops","value": ["3", "10"]},
        {"label": "E3L11 Freeway","value": ["3", "11"]},

        {"label": "Episode 4: The Birth","value": ["4", "1"]},
        
        {"label": "E4L1 It's Impossible","value": ["4", "1"]},
        {"label": "E4L2 Duke-Burger","value": ["4", "2"]},
        {"label": "E4L3 Shop-n-Bag","value": ["4", "3"]},
        {"label": "E4L4 Babe Land","value": ["4", "4"]},
        {"label": "E4L5 Pigsty","value": ["4", "5"]},
        {"label": "E4L6 Going Postal","value": ["4", "6"]},
        {"label": "E4L7 XXX-Stacy","value": ["4", "7"]},
        {"label": "E4L8 Critical Mass","value": ["4", "8"]},
        {"label": "E4L9 Derelict","value": ["4", "9"]},
        {"label": "E4L10 The Queen","value": ["4", "10"]},
        {"label": "E4L11 Area 51","value": ["4", "11"]}
    ]);

    const PlayDemoParam = {
        "modeSupport": ["singleplayer"],
        "type": "dirfiles",
        "value": ["'/d'+value[0]"],
        "label": "Play Recorded Demo",
        optional:true,
        "path": "**/*.dmo",
    };

    const dosExecParamsCommon = {
        "pname": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "static",
            "value": ["'-name'", "MyName"],
            "for":"private"
        },
        "playdmo": PlayDemoParam,
        "recordDmo": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "Record DMO",
            "value": "'/r'",
            "for": "private"
        },
        "monstersSkill": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "numrange",
            "min":1,
            "max":4,
            "delta":1,
            "label": "Monsters Skill",
            "value": "'/s' + value",
            "optional":true
        },
        "noMonsters": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "No Monsters",
            "value": "'/m'"
        },
        "respawn": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Respawn",
            "value":"'/t' + value",
            "optional":true,
            "choices": [{
                "label": "Monsters",
                "value": "1"
            }, {
                "label": "Items",
                "value": "2"
            }, {
                "label": "Inventory",
                "value": "3"
            }, {
                "label": "All",
                "value": "x"
            }]
        },
        "botsNum": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "numrange",
            "min":2,
            "max":8,
            "delta":1,
            "label": "Bots",
            "value": "'/q' + value",
            "optional":true
        },
        "netmode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "NetMode",
            "optional":true,
            "value":["'/i' + value"],
            "description": "Peer2Peer is usually faster with 2-4 players",
            "choices": [
                {"label": "Peer2Peer","value": 1},
                {"label": "Master/Slave","value": 0}
            ]
        },
        "map": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"maps",
            "value": ["'-map'", "value"],
            "label": "Map",
            symlink:'forced',
            props: {filters: [{name: 'Map Files', extensions: ['map']}]},
            "optional":true
        },
        "grp": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'/g'+value"],
            "label": "GRP",
            order:99,
            symlink:'forced',
            props: {filters: [{name: 'Grp Files', extensions: ['grp']}]},
            "optional":true
        },
        "con": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'/x'+value"],
            "label": "CON",
            symlink:'forced',
            props: {filters: [{name: 'Con Files', extensions: ['con']}]},
            "optional":true
        },
        "f": {
            "modeSupport": ["multiplayer"],
            "description": "Send Fewer Packets during Multi-Player Mode",
            "type": "choice",
            "label": "PacketsPerSec",
            "value":"'/f' + value",
            "optional":true,
            "choices": [{
                "label": "30 (default)",
                "value": "1"
            }, {
                "label": "15",
                "value": "2"
            }, {
                "label": "7.5",
                "value": "4"
            }]
        }
    };

    const dosExecParamsAtomic = Object.assign({}, dosExecParamsCommon, {
        "botsAi": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "Bots AI",
            "value": "'/a'"
        },
        "playDmo": {
            "modeSupport": ["singleplayer"],
            "type": "file",
            "format": "*.dmo", 
            "label": "Play DMO",
            "value": "'/d' + value",
            "dosboxMount": true,
            "optional": true
        },
        "episodeAndLevel": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Level",
            "optional":true,
            "value":["'/v' + value[0]", "'/l' + value[1]"],
            "choices": atomicAllLevels
        },
        "multiplayerMode": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Multiplayer Mode",
            "value":"'/c' + value",
            "optional":true,
            "choices": [{
                "label": "Dukematch (Spawn)",
                "value": "1"
            }, {
                "label": "Coop",
                "value": "2"
            }, {
                "label": "Dukematch (No Spawn)",
                "value": "3"
            }]
        }
    });

    const dosExecParamsShareware = Object.assign({},dosExecParamsCommon, {
        "episodeAndLevel": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Episode",
            "optional":true,
            "value":["'/v' + value[0]", "'/l' + value[1]"],
            "choices": sharewareEpisodeOneLevels
        },
        "multiplayerMode": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Multiplayer Mode",
            "value":"'/c' + value",
            "optional":true,
            /* Shareware params are different than atomic 0=deathmatch (spawn), 1=coop, 2=deathmatch (nospawn) */
            /* "DUKE3D.EXE /?" presents wrong options for this param on shareware version, bug on 3drealms end. */
            "choices": [{
                "label": "Dukematch (Spawn)",
                "value": "0"
            },{
                "label": "Coop",
                "value": "1"
            }, {
                "label": "Dukematch (No Spawn)",
                "value": "2"
            }]
        }
    });


    const xDukeParams = {...deepClone(dosExecParamsAtomic), ...{
        netlist: {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-net'", "'netlist.txt'"],
            "order": 99,
            "for":"private"
        }   
    }}
    delete xDukeParams.netmode;
    delete xDukeParams.f;

    const nDukeParams = {...deepClone(xDukeParams), ...{
        "timelimit": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":960,
            "delta":1,
            "label": "Time Limit",
            "value": "'/k' + value",
            "optional":true
        },
        "disableautoaim": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "No AutoAim",
            "value": "'/disableautoaim'",
            "for": "host-only-shared"
        },
        "coords": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "Coords On",
            "value": "'/w'",
            "for": "private"
        },
        "startweapon": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":1,
            "max":11,
            "delta":1,
            "label": "Start Weapon",
            "value": "'/startweapon' + value",
            "optional":true,
            "for": "host-only-shared"
        },
        team: {
            "modeSupport": ["multiplayer", "singleplayer"],
            "for": "private",
            "type": "choice",
            "label": "Team",
            "value":["'-team'", "value[0]"],
            "optional":true,
            "choices": [{
                "label": "Blue",
                "value": ["1"]
            }, {
                "label": "Red",
                "value": ["2"]
            }, {
                "label": "Green",
                "value": ["3"]
            }, {
                "label": "Brown",
                "value": ["4"]
            }]
        },
        color: {
            "modeSupport": ["multiplayer", "singleplayer"],
            "for": "private",
            "type": "choice",
            "label": "Color",
            "value":["'-color'", "value[0]"],
            "optional":true,
            "choices": [{
                "label": "Auto",
                "value": ["0"]
            }, {
                "label": "Blue",
                "value": ["1"]
            }, {
                "label": "Red",
                "value": ["2"]
            }, {
                "label": "Green",
                "value": ["3"]
            }, {
                "label": "Gray",
                "value": ["4"]
            }, {
                "label": "Dark-Gray",
                "value": ["5"]
            }, {
                "label": "Dark-Green",
                "value": ["6"]
            }, {
                "label": "Brown",
                "value": ["7"]
            }, {
                "label": "Dark-Blue",
                "value": ["8"]
            }, {
                "label": "Bright-Red",
                "value": ["9"]
            }, {
                "label": "Yellow",
                "value": ["10"]
            }]
        },
        "multiplayerMode": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Multiplayer Mode",
            "value":"'/c' + value",
            "optional":true,
            "choices": [{
                "label": "Dukematch (Spawn)",
                "value": "0"
            }, {
                "label": "Coop",
                "value": "1"
            }, {
                "label": "Dukematch (No Spawn)",
                "value": "2"
            }, {
                "label": "TeamDM (Spawn)",
                "value": "3"
            }, {
                "label": "TeamDM (No Spawn)",
                "value": "4"
            }, {
                "label": "CTF (Spawn)",
                "value": "5"
            }, {
                "label": "CTF (No Spawn)",
                "value": "6"
            }, {
                "label": "1Flag CTF (Spawn)",
                "value": "7"
            }, {
                "label": "1Flag CTF (No Spawn)",
                "value": "8"
            }, {
                "label": "Atk/Def CTF (Spawn)",
                "value": "9"
            }, {
                "label": "Atk/Def CTF (No Spawn)",
                "value": "10"
            }]
        }
    }}
    
    const hDukeParams = {...deepClone(xDukeParams), ...{
            sm: {
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "static",
                "value": ["'-specmenu'"],
                "for":"private"
            },
            kh: {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-kalihost'"],
                "for":"host-only-private"
            },
            kj: {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-kalijoin'"],
                "for":"client-only-private"
            },
            netlist: {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-net'", "'netlist.txt'"],
                "order": 99,
                "for":"private"
            },
            teams: {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Team Game",
                "value":["value[0]"],
                "optional":true,
                "choices": [{
                    "label": "TeamDukematch",
                    "value": ["-team"]
                }, {
                    "label": "TeamDukematch (2 forts)",
                    "value": ["-teamforts"]
                }, {
                    "label": "CTF",
                    "value": ["-ctf"]
                }]
            },
            spectator: {
                "for":"private",
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "Spectator",
                "value": "'-spectator'"
            },
            forcerespawn: {
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "Force Respawn",
                "value": "'-forcerespawn'"
            },
            "showitems": {
                "for":"private",
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "Show items always",
                "value": "'-showitems'"
            },
            "fraglimit": {
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "numrange",
                "min":0,
                "max":99,
                "delta":1,
                "label": "Frag Limit",
                "value": "'/y' + value",
                "optional":true
            },
            "chronometer": {
                "for":"private",
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "ChronoMeter",
                "value": "'-gametime'",
                section:"misc"
            },
            "nopistol": {
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "No Pistol",
                "value": "'-nopistol'",
                section:"misc"
            },
            "scoretxt": {
                "for":"private",
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "Score File",
                "value": "'-scoretxt'",
                section:"misc"
            },
            "newfragbar": {
                "for":"private",
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "boolean",
                "label": "New FragBar",
                "value": "'-newfragbar'",
                section:"misc"
            }
        }
    };
    delete hDukeParams.recordDmo;
    delete hDukeParams.grp;
    delete hDukeParams.con;
    ['monstersSkill', 'botsNum', 'botsAi'].forEach(p=>hDukeParams[p].section='misc');

    const eduke32baseParamsWithoutSettingsDef = deepClone(dosExecParamsAtomic);
    for(const p in eduke32baseParamsWithoutSettingsDef) {
        const pDef = eduke32baseParamsWithoutSettingsDef[p];
        if(Array.isArray(pDef.modeSupport)) {
            pDef.modeSupport = pDef.modeSupport.filter(item=>item != 'settings');
        }
    }

    delete eduke32baseParamsWithoutSettingsDef.con;
    delete eduke32baseParamsWithoutSettingsDef.f;

    /* TODO add duke64 levels, for rednukem */
    const rednukemParams = Object.assign({}, eduke32baseParamsWithoutSettingsDef, {
        "episodeAndLevel": {},
        "setup": {
            "modeSupport": ["settings"],
            "type": "static",
            "value": ["'-setup'"],
            "for":"private"
        },
        "nosetup": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-nosetup'"],
            "for":"private"
        },
        "host_portAndNumPlayers": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-server'", "'-port'", "GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "client_DestIpPort": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-connect'", "GameRoom.DestIp + ':' + GameRoom.DestPort"],
            "for":"client-only-private"
        }
    });
    delete rednukemParams.netmode;

    const netduke32ExecParams = Object.assign({
        "mod": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "file",
            "res":"mods",
            "value": ["'/g'+value"],
            "label": "Mod",
            symlink:'forced',
            props: {filters: [{name: 'Mod Files', extensions: ['grp',"zip","pk3"]}]},
            "optional":true
        },
        "con": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'/x'","value"], // in eDuke32 based source ports, a space is required between /x and the con filename.
            "label": "CON",
            symlink:'forced',
            props: {filters: [{name: 'Con Files', extensions: ['con']}]},
            "optional":true
        }
    }, eduke32baseParamsWithoutSettingsDef, {
        "setup": {
            "modeSupport": ["settings"],
            "type": "static",
            "value": ["'-setup'"],
            "for":"private"
        },
        "nosetup": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-nosetup'"],
            "for":"private"
        },
        "noFriendlyFire": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "No Friendly Fire",
            "value": "'-noffire'"
        },
        "weaponStay": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "Weapon Stay",
            "value": "'-wstay'"
        },
        "fragLimit": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":2,
            "max":100,
            "delta":1,
            "label": "Frag Limit",
            "value": "'-y' + value",
            "optional":true
        },
        /* net related parameters have to be last (see order value) */
        "host_portAndNumPlayers": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-net'", "'/n0:' + GameRoom.NumberOfPlayers",  "'-p'+GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "client_DestIpPort": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-net'", "'/n0'", "GameRoom.DestIp + ':' + GameRoom.DestPort"],
            "for":"client-only-private"
        },
        "multiplayerMode": {
            "for": "host-only-shared",
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Multiplayer Mode",
            "value":"'/c' + value",
            "optional":true,
            "choices": [{
                "label": "Dukematch",
                "value": "1"
            }, {
                "label": "Coop",
                "value": "2"
            }, {
                "label": "Team Dukematch",
                "value": "3"
            }]
        },
        "respawn": {
            "for": "host-only-shared",
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Respawn",
            "value":"'-t' + value",
            "optional":true,
            "choices": [{
                "label": "All",
                "value": "123"
            },{
                "label": "Monsters",
                "value": "1"
            }, {
                "label": "Items",
                "value": "2"
            }, {
                "label": "Inventory",
                "value": "3"
            },{
                "label": "Items&Inventory",
                "value": "23"
            },{
                "label": "Items&Monsters",
                "value": "12"
            },{
                "label": "Inventory&Monsters",
                "value": "13"
            }]
        }
    });
    delete netduke32ExecParams.grp;
    delete netduke32ExecParams.netmode;

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "duke3d": {
            "runToolOpts": {"type":"dosbox", "data":{commands, "args":["-machine", "vesa_nolfb"]}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net": GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}},
            "beforeRun":[{"cmd":"edit-ini","file":"commit_dat", "edits":[
                {"category":"Commit","entry":"SOCKETNUMBER", "value":"34889"},{"category":"Commit","entry":"LAUNCHNAME", "value":"'DUKE3D.EXE'"},{"category":"Commit","entry":"NUMPLAYERS", "value":"GameRoom.NumberOfPlayers"},

                {"category":"Commit","entry":"COMMTYPE", "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]==='serial'?1:3`}, // ipx or serial

                // serial entries:
                
                {"category":"Commit","entry":"COMPORT", "value":"1"},
                {"category":"Commit","entry":"IRQNUMBER", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"UARTADDRESS", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"PORTSPEED", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
            ]}]
        }
    };

    const rancidMeatBeforeRun = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {
        const MyPort = ctx.GameRoom.MyPort;
        let netlistText = "interface 0.0.0.0:"+MyPort+"\r\n";
        netlistText += 'mode peer\r\n';

        if(useDedicated) {
            // Dedicated server logic for peer2peer (only supported for 2 players)
            if(imHost) {
                const localPort = mainStore.myLeasedDedicatedServer?.rc?.boundPort;
                netlistText += `allow ${mainStore.localhostIp4}:${localPort}\r\n`    
            } else {
                netlistText += `allow ${ctx.GameRoom.DestIp}:${ctx.GameRoom.DestPort}\r\n`
            }
        } else {
            // Normal peer2peer logic (the usual case)
            for(const ipd of othersIpDetails) {
                netlistText += `allow ${ipd.ip}:${ipd.port}\r\n`
            }
        }

        await fs.writeFile(path.resolve(ctx.GameDir, 'netlist.txt'), netlistText);
    }}];

    const patches = require(path.join(__dirname, 'buildmfx.i.js'))(utils, "duke");

    return {
        "name": "Duke Nukem 3D",
        "get": [{
            type:"zoom-platform",
            nameId:"duke-nukem-3d-atomic-edition",
            name:"Duke Nukem 3D: Atomic Edition"
        }],
        "executables": {
            "shareware1_3d": {
                "name":"Shareware 1.3D (DOS)",
                tips: {
                    patches
                },
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "duke": {"path":"DUKE3D.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat": {"path":"COMMIT.DAT", "optional":true},
                    "duke3dgrp": {"path":"DUKE3D.GRP"}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"commit",
                        "runTool":vars.duke3d.runToolOptsMultiplayer,
                        "beforeRun":vars.duke3d.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.duke3d.runToolOpts },
                    "singleplayer": {"file":"duke", "runTool":vars.duke3d.runToolOpts}
                },
                "parameters": dosExecParamsShareware
            },
            "atomic1_5": {
                "name":"Atomic 1.5 (DOS)",
                tips: {
                    patches
                },
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "duke": {"path":"DUKE3D.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat":{"path":"COMMIT.DAT", "optional":true},
                    "mapeditor": {"path":"BUILD.EXE", "optional":true},
                    "duke3dgrp": {"path":"DUKE3D.GRP"}
                },
                "runnables": {
                    "multiplayer": {"file":"commit", "runTool":vars.duke3d.runToolOptsMultiplayer, "beforeRun":vars.duke3d.beforeRun},
                    "settings": {"file":"setup", "runTool":vars.duke3d.runToolOpts},
                    "singleplayer": {"file":"duke", "runTool":vars.duke3d.runToolOpts},
                    "mapeditor": {"file":"mapeditor", "runTool":vars.duke3d.runToolOpts}
                },
                "parameters": dosExecParamsAtomic
            },
            "netduke32": {
                "name": "NetDuke32",
                "maxPlayers":16,
                "namedPipeChatSync": {windows:"\\\\.\\pipe\\NetDuke32", unix:"/tmp/netduke32pipe"},
                "files": {
                    "main": {"path":"netduke32.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "singleplayer": {"file":"main"},
                    "settings": {"file":"main"},
                },
                "parameters": netduke32ExecParams
            },
            "hduke": {
                "name":"hDuke",
                "isPeerToPeer":true,
                "minPlayers":2,
                "files": {
                    "main": {"path":"hduke.exe", runDetachedShell:true}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "beforeRun":rancidMeatBeforeRun},
                    "singleplayer": {"file":"main"}
                },
                "parameters": hDukeParams,
                paramSections: {
                    misc: {
                        name: "Miscellaneous"
                    }
                }
            },
            "xduke": {
                "name":"xDuke",
                "isPeerToPeer":true,
                "minPlayers":2,
                "files": {
                    "main": {"path":"duke3d_w32.exe", runDetachedShell:true}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "beforeRun":rancidMeatBeforeRun},
                    "singleplayer": {"file":"main"}
                },
                "parameters": xDukeParams
            },
            "rduke": {
                "name":"rDuke",
                "isPeerToPeer":true,
                "minPlayers":2,
                "files": {
                    "main": {"path":"rduke_r10.exe", runDetachedShell:true}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "beforeRun":rancidMeatBeforeRun},
                    "singleplayer": {"file":"main"}
                },
                "parameters": xDukeParams
            },
            "nduke": {
                "name":"nDuke",
                "isPeerToPeer":true,
                "minPlayers":2,
                "files": {
                    "main": {"path":"nDuke.exe", runDetachedShell:true}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "beforeRun":rancidMeatBeforeRun},
                    "singleplayer": {"file":"main"}
                },
                "parameters": nDukeParams
            },
            "rednukem": {
                "name": "RedNukem",
                "files": {
                    "main": {"path":"rednukem.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "waitForListenPortOnProto": "udp"},
                    "singleplayer": {"file":"main"},
                    "settings": {"file":"main"},
                },
                "parameters": rednukemParams
            }
        }
    };
}