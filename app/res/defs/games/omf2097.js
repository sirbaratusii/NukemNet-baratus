module.exports = function ({GameDefsType}) {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx", 'serial:9600', 'serial:19200', 'serial:38400']}};

    const runnables = {
        "multiplayer": {
            "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'netmodem':'netipx'},
            "runTool":runToolOptsMultiplayer,
            "files":["netmodem", "netipx"]
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const params = {
        "ipx": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'netarena'", "'-l'", "'0'", "'file0001'"],
            "for":"shared",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]!='serial'}
        },
        "serial": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-c'", "1", "'-b'", `GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]`, "'-s'", "'NETARENA'", "'-l'", "'1'", "'FILE0001'"],
            "for":"shared",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]=='serial'}
        }
    };

    return {
        "name": "One Must Fall 2097",
        "executables": {
            "full": {
                "name":"Full (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"OMF.EXE"},
                    "netipx": {"path":"NETIPX.EXE"},
                    "netmodem": {"path":"NETMODEM.EXE"},
                },
                "runnables": runnables,
                "parameters": params
            },
            "shareware": {
                "name":"Shareware (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"OMF.EXE"},
                    "netipx": {"path":"NETIPX.EXE"},
                    "netmodem": {"path":"NETMODEM.EXE"},
                },
                "runnables": runnables,
                "parameters": params
            }
        }
    };
}