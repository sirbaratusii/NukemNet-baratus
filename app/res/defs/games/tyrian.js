module.exports = function({GameDefsType}) {
    const commands = ['CONFIG -set "cycles=auto 5000 limit 40000"']
    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx", 'serial:9600', 'serial:19200', 'serial:38400']}};

    const runnables = {
        "multiplayer": {
            "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'netmodem':'netipx'},
            "runTool":runToolOptsMultiplayer,
            "files":["netmodem", "netipx"]
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const tyrian2kTips = {html: `
    <p>
    By default Tyrian 2k does not work in multiplayer, due to missing files.<br/>
    To play, you need to copy over additional files into the tyrian 2k directory<br/>
    </p>

    
    <ol>
    <li>Download Tyrian 2k, you may find it in GOG.</li>
    <li><b>Extract the files into your Tyrian 2k directory from this zip: https://nukemnet.blob.core.windows.net/misc/T2kMulti.zip</b><br/></li>
    </ol>
    
    Origin: https://www.gog.com/forum/tyrian_2000/tyrian_2000_network_multiplayer
    `};

    const params = {
        "ipx": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'netarena'", "'-l'", "'1'", "'file0001'"],
            "for":"shared",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]!='serial'}
        },
        "serial": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-c'", "1", "'-b'", `GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]`, "'-s'", "'NETARENA'", "'-l'", "'1'", "'FILE0001'"],
            "for":"shared",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]=='serial'}
        }
    };

    return {
        "name": "Tyrian",
        "get": [{
            type:"gog",
            prodId:"1207658901",
            nameId:"tyrian_2000",
            name:"Tyrian 2000"
        }],
        "executables": {
            "v2k": {
                "name":"2000 (DOS)",
                tips:tyrian2kTips,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"glob":["TYRIAN2K.EXE", "TYRIAN.EXE"]},
                    "netipx": {"path":"NETIPX.EXE"},
                    "netmodem": {"path":"NETMODEM.EXE"},
                },
                "runnables": runnables,
                "parameters": params
            },
            "v2_0": {
                "name":"v2.0 (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"TYRIAN.EXE"},
                    "netipx": {"path":"NETIPX.EXE"},
                    "netmodem": {"path":"NETMODEM.EXE"},
                },
                "runnables": runnables,
                "parameters": params
            },
            "shareware": {
                "name":"Shareware (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"TYRIAN.EXE"},
                    "netipx": {"path":"NETIPX.EXE"},
                    "netmodem": {"path":"NETMODEM.EXE"},
                },
                "runnables": runnables,
                "parameters": params
            }
        }
    };
}