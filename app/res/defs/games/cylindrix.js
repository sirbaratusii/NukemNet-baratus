module.exports = function({deepClone}) {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // Full also supports IPX
    const runnablesFull = deepClone(runnables);
    runnablesFull.multiplayer.runTool.data.net = ['ipx','serial'];

    // TODO edit config baud and com1
    const tips = {html: `
    <h3>If playing on Serial, use COM1</h3>
    `};

    return {
        "name": "Cylindrix",
        "executables": {
            "full": {
                "maxPlayers":6,
                tips,
                "name":"Full (DOS)",
                "files": {
                    "main": {"glob":["CYLINDRX.EXE", "cylindrix.exe", "CYLINDRX.COM"]}
                },
                "runnables": runnablesFull,
                "parameters": {}
            },
            "demo": {
                "maxPlayers":2,
                tips,
                "name":"Demo (DOS)",
                "files": {
                    "main": {"glob":["CYLINDRX.EXE", "cylindrix.exe", "CYLINDRX.COM"]}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}