module.exports = function ({deepClone}) {

    const commands = [
        'CONFIG -set "cycles=max"',
        'CONFIG -set "core=auto"',
    ];

    const multiCommands = [...commands,
        // `@bin\\black.exe`,
        `@call batch\\locale.bat`,
        `SET NETINFO=1`,
        `@cls`,
    ];
    
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands:multiCommands, "net":"ipx"}}
        }
    };

    return {
        "name": "Worms",
        "get": [{
            type:"gog",
            prodId:"1207658991",
            nameId:"worms_united",
            name:"Worms United"
        }],
        "executables": {
            "united": {
                "name":"United (DOS)",
                "mountImg": true,
                "files": {
                    "main": {"path":"BIN/WRMS.EXE"}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main", "runTool":vars.game.runToolOptsMultiplayer
                    },
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": {}
            }
        }
    }
}
