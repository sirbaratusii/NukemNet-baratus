module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i')
    
    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const Full_EpisodeAndLevels = [{
        "label": "Episode One",
        "value": ["-warp", "1"]
    }, {
        "label": "E1L1: GenEmp Corp",
        "value": ["-warp", "1"]
    }, {
        "label": "E1L2: Tunnel Town",
        "value": ["-warp", "2"]
    }, {
        "label": "E1L3: Lava Annex",
        "value": ["-warp", "3"]
    }, {
        "label": "E1L4: Alcatraz",
        "value": ["-warp", "4"]
    }, {
        "label": "E1L5: Cyber Circus",
        "value": ["-warp", "5"]
    }, {
        "label": "E1L6: Digi-Ota",
        "value": ["-warp", "6"]
    },
    
    
    {
        "label": "Episode Two",
        "value": ["-warp", "7"]
    }, {
        "label": "E2L1: The Great Wall",
        "value": ["-warp", "7"]
    }, {
        "label": "E2L2: Garden of Delights",
        "value": ["-warp", "8"]
    }, {
        "label": "E2L3: Hidden Fort",
        "value": ["-warp", "9"]
    }, {
        "label": "E2L4: Anarchist Dream",
        "value": ["-warp", "10"]
    }, {
        "label": "E2L5: Notus Us!",
        "value": ["-warp", "11"]
    },

    {
        "label": "Episode Three",
        "value": ["-warp", "12"]
    }, {
        "label": "E3L1: Gothik Gauntlet",
        "value": ["-warp", "12"]
    }, {
        "label": "E3L2: The Sewers",
        "value": ["-warp", "13"]
    }, {
        "label": "E3L3: Trode Wars",
        "value": ["-warp", "14"]
    }, {
        "label": "E3L4: Twilight",
        "value": ["-warp", "15"]
    },

    {
        "label": "Episode Four",
        "value": ["-warp", "31"]
    }, {
        "label": "E4L1: Desiccant Room",
        "value": ["-warp", "31"]
    }, {
        "label": "E4L2: Protean Cybex",
        "value": ["-warp", "16"]
    }, {
        "label": "E4L3: River of Blood",
        "value": ["-warp", "17"]
    }, {
        "label": "E4L4: Bizarro by Anthony",
        "value": ["-warp", "18"]
    }, {
        "label": "E4L4: The War Rooms",
        "value": ["-warp", "19"]
    }, {
        "label": "E4L4: Intruder Alert!",
        "value": ["-warp", "20"]
    }];

    const commonExecBase = {
        "files": {
            "main": {"path":"HACX.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": null
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "Please don't shoot!",
                    "value": "1"
                }, {
                    "label": "Arrgh, I Need Health!",
                    "value": "2"
                }, {
                    "label": "Let's Rip Them Apart!",
                    "value": "3"
                }, {
                    "label": "I Am Immortal",
                    "value": "4"
                }, {
                    "label": "Insanity!",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    
    
    const full = deepClone(commonExecBase);
    full.parameters.episodeAndLevel.choices = Full_EpisodeAndLevels;

    const dhacx = deepClone(commonExecBase);
    dhacx.parameters.episodeAndLevel.choices = Full_EpisodeAndLevels;
    dhacx.files.ipxsetup.path = "DHIPXSET.EXE";
    dhacx.files.main.path = 'DHACX.EXE'

    return {
        "name": "HACX",
        "executables": {
            "full": {
                "name":"Full 1.X (DOS)",
                ...full
            },
            "dhacx": {
                "name":"DHACX Standalone (DOS)",
                ...dhacx
            }
        }
    }
}
