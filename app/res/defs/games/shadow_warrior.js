module.exports = function({GameDefsType, utils}) {
    const path = require('path');

    const sharewareEpisodeOneLevels = [
        {"label": "1 Suppuku Station","value": "1"},
        {"label": "2 Zilla Construction","value": "2"},
        {"label": "3 Master Leep's Temple","value": "3"},
        {"label": "4 Dark Woods of the Serpent","value": "4"}
    ];
    const fullAllLevels = sharewareEpisodeOneLevels.concat([
        {"label": "5 Rising Son","value": "5"},
        {"label": "6 Killing Fields","value": "6"},
        {"label": "7 Hara-Kiri Harbor","value": "7"},
        {"label": "8 Zilla's Villa","value": "8"},
        {"label": "9 Monastery","value": "9"},
        {"label": "10 Raider of the Lost Wang","value": "10"},
        {"label": "11 Sumo Sky Palace","value": "11"},
        {"label": "12 Bath House","value": "12"},
        {"label": "13 Unfriendly Skies","value": "13"},
        {"label": "14 Crude Oil","value": "14"},
        {"label": "15 Coolie Mines","value": "15"},
        {"label": "16 Subpen 7","value": "16"},
        {"label": "17 The Great Escape","value": "17"},
        {"label": "18 Floating Fortress","value": "18"},
        {"label": "19 Water Torture","value": "19"},

        {"label": "20 Stone Rain","value": "20"},
        {"label": "21 Shaghai Shipwreck","value": "21"},
        {"label": "22 Auto Maul","value": "22"},

        {"label": "23 Heavy Metal (DM Only)","value": "23"},
        {"label": "24 Reaper Valley (DM Only)","value": "24"},
        {"label": "25 House of Wang (DM Only)","value": "25"},
        {"label": "26 Lo Wang Rally (DM Only)","value": "26"},

        {"label": "27 Ruins of the Ronin (CTF)","value": "27"},
        {"label": "28 Killing Fields (CTF)","value": "28"}
    ]);

    const dosExecParamsCommon = {
        "map": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"maps",
            "value": ["'-map'", "value"],
            "label": "Map",
            symlink:'forced',
            props: {filters: [{name: 'Map Files', extensions: ['map']}]},
            "optional":true
        },
        "grp": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'-g'+value"],
            "label": "GRP",
            description:"Load an extra GRP or ZIP file",
            symlink:'forced',
            props: {filters: [{name: 'Grp Files', extensions: ['grp','zip']}]},
            "optional":true
        },
        "def": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'-h'+value"],
            description:"Use a different .def file, instead of SW.DEF",
            "label": "DEF",
            symlink:'forced',
            props: {filters: [{name: 'Def Files', extensions: ['def']}]},
            "optional":true
        },
        "pname": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "static",
            "value": ["'-name'", "MyName"],
            "for":"private"
        },
        "monstersSkill": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "numrange",
            "min":1,
            "max":4,
            "delta":1,
            "label": "Monsters Skill",
            "value": "'/s' + value",
            "optional":true
        },
        "nopred": {
            "modeSupport": ["multiplayer", "settings"],
            "type": "boolean",
            "label": "Disable Net Prediction Method",
            "value": "'-nopred'"
        },
        "noMonsters": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "No Monsters",
            "value": "'-monst'"
        },
        "playback": {
            "modeSupport": ["singleplayer"],
            "type": "file",
            "value": ["'-dp'+value"],
            "label": "Playback DMO",
            symlink:'forced',
            props: {filters: [{name: 'DMO Files', extensions: ['dmo']},{name: 'All Files', extensions: ['*']}]},
            "optional":true
        },
        "recordDmo": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "text",
            "label": "Record DMO",
            "value": ["'-dr'+value"],
            optional:true,
            props: {
                placeholder: "Demo File Name"
            },
            "for": "private"
        },
        "f": {
            "modeSupport": ["multiplayer"],
            "description": "Send Fewer Packets during Multi-Player Mode",
            "type": "choice",
            "label": "PacketsPerSec",
            "value":"value?('/f' + value):undefined",
            "optional":true,
            "choices": [{
                "label": "40 (default)",
                "value": undefined
            }, {
                "label": "20",
                "value": "2"
            }, {
                "label": "10",
                "value": "4"
            }, {
                "label": "5",
                "value": "8"
            }]
        }
    };

    
    const dosExecParamsShareware = Object.assign({},dosExecParamsCommon, {
        "level": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Episode",
            "optional":true,
            "value":["'-level' + value[0]"],
            "choices": sharewareEpisodeOneLevels
        }
    });
    
    const dosExecParamsFull = Object.assign({}, dosExecParamsCommon, {
        "level": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Episode",
            "optional":true,
            "value":["'-level' + value[0]"],
            "choices": fullAllLevels
        }
    });

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "sw": {
            "runToolOpts": {"type":"dosbox", "data":{commands, "args":["-machine", "vesa_nolfb"]}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}},
            "beforeRun":[{"cmd":"edit-ini","file":"commit_dat", "edits":[
                {"category":"Commit","entry":"SOCKETNUMBER", "value":"34889"},{"category":"Commit","entry":"LAUNCHNAME", "value":"'SW.EXE'"},{"category":"Commit","entry":"NUMPLAYERS", "value":"GameRoom.NumberOfPlayers"},

                {"category":"Commit","entry":"COMMTYPE", "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]==='serial'?1:3`}, // ipx or serial

                // serial entries:
                {"category":"Commit","entry":"COMPORT", "value":"1"},
                {"category":"Commit","entry":"IRQNUMBER", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"UARTADDRESS", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"PORTSPEED", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
            ]}],
            "baseGameEditions": ["shareware1_2","full"]
        }
    };


    const voidswExecParams = Object.assign({}, dosExecParamsFull, {
        "nosetup": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-nosetup'"],
            "for":"private"
        },

        /* net related parameters have to be last (see order value) */
        "host_portAndNumPlayers": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-net'", "'-server'", "'-p'+GameRoom.MyPort", "'/n0:' + GameRoom.NumberOfPlayers"],
            "for":"host-only-private"
        },
        "client_DestIpPort": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-net'", "'-connect'", "GameRoom.DestIp + ':' + GameRoom.DestPort"],
            "for":"client-only-private"
        }
    });
    // voidsw does not yet support DMO files.s
    delete voidswExecParams.playback;
    delete voidswExecParams.recordDmo;
    delete voidswExecParams.f;

    const patches = require(path.join(__dirname, 'buildmfx.i.js'))(utils, "sw");

    return {
        "resourceFolders": ["maps", "mods"],
        "name": "Shadow Warrior",
        "get": [{
            type:"gog",
            prodId:"1618073558",
            nameId:"shadow_warrior_classic_redux",
            name:"Shadow Warrior Classic Redux"
        }, {
            type:"zoom-platform",
            nameId:"shadow-warrior-classic-and-complete",
            name:"Shadow Warrior: Classic and Complete"
        }],
        "executables": {
            "shareware1_2": {
                "name":"Shareware 1.2 (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "sw": {"path":"SW.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat": {"path":"COMMIT.DAT", "optional":true},
                    "swgrp": {"path":"SW.GRP"}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {
                        "file":"commit",
                        "runTool":vars.sw.runToolOptsMultiplayer,
                        "beforeRun":vars.sw.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.sw.runToolOpts},
                    "singleplayer": {"file":"sw", "runTool":vars.sw.runToolOpts}
                },
                "parameters": dosExecParamsShareware,
                "resources": {}
            },
            "full": {
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "sw": {"path":"SW.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat": {"path":"COMMIT.DAT", "optional":true},
                    "swgrp": {"path":"SW.GRP"}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {
                        "file":"commit",
                        "runTool":vars.sw.runToolOptsMultiplayer,
                        "beforeRun":vars.sw.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.sw.runToolOpts},
                    "singleplayer": {"file":"sw", "runTool":vars.sw.runToolOpts}
                },
                "parameters": dosExecParamsFull,
                "resources": {}
            },
            "voidsw": {
                "name": "VoidSW",
                "files": {
                    "main": {"path":"voidsw.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "waitForListenPortOnProto": "udp"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": voidswExecParams
            }
        }
    };
}