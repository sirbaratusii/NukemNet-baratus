module.exports = function() {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const runToolOpts = {"type":"dosbox", data:{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Theme Hospital",
        "get": [{
            type:"gog",
            prodId:"1207659026",
            nameId:"theme_hospital",
            name:"Theme Hospital"
        }],
        "executables": {
            "full": {
                "mountImg": true,
                "name":"Full (DOS)",
                "maxPlayers":4,
                "files": {
                    "main": {"path":"HOSPITAL.EXE"},
                    "setup": {"path":"sound/setsound.exe"}
                },
                "runnables": runnables
            }
        }
    };
}