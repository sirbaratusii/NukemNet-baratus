module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i')

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const Shareware_EpisodeAndLevels = [{
        "label": "E1: City of the Damned",
        "value": ["-episode", "1"]
    }, {
        "label": "E1M1: The Docks",
        "value": ["-warp", "1", "1"]
    }, {
        "label": "E1M2: The Dungeons",
        "value": ["-warp", "1", "2"]
    }, {
        "label": "E1M3: The Gatehouse",
        "value": ["-warp", "1", "3"]
    }, {
        "label": "E1M4: The Guard Tower",
        "value": ["-warp", "1", "4"]
    }, {
        "label": "E1M5: The Citadel",
        "value": ["-warp", "1", "5"]
    }, {
        "label": "E1M6: The Cathedral",
        "value": ["-warp", "1", "6"]
    }, {
        "label": "E1M7: The Crypts",
        "value": ["-warp", "1", "7"]
    }, {
        "label": "E1M8: Hell's Maw",
        "value": ["-warp", "1", "8"]
    }, {
        "label": "E1M9: The Graveyard",
        "value": ["-warp", "1", "9"]
    }];

    const Full_EpisodeAndLevels = [...Shareware_EpisodeAndLevels, {
        "label": "E2: Hell's Maw",
        "value": ["-episode", "2"]
    }, {
        "label": "E2M1: The Crater",
        "value": ["-warp", "2", "1"]
    }, {
        "label": "E2M2: The Lava Pits",
        "value": ["-warp", "2", "2"]
    }, {
        "label": "E2M3: The River of Fire",
        "value": ["-warp", "2", "3"]
    }, {
        "label": "E2M4: The Ice Grotto",
        "value": ["-warp", "2", "4"]
    }, {
        "label": "E2M5: The Catacombs",
        "value": ["-warp", "2", "5"]
    }, {
        "label": "E2M6: The Labyrinth",
        "value": ["-warp", "2", "6"]
    }, {
        "label": "E2M7: The Great Hall",
        "value": ["-warp", "2", "7"]
    }, {
        "label": "E2M8: The Portals of Chaos",
        "value": ["-warp", "2", "8"]
    }, {
        "label": "E2M9: The Glacier",
        "value": ["-warp", "2", "8"]
    }, {
        "label": "E3: The Dome of D'Sparil",
        "value": ["-episode", "3"]
    }, {
        "label": "E3M1: The Storehouse",
        "value": ["-warp", "3", "1"]
    }, {
        "label": "E3M2: The Cesspool",
        "value": ["-warp", "3", "2"]
    }, {
        "label": "E3M3: The Confluence",
        "value": ["-warp", "3", "3"]
    }, {
        "label": "E3M4: The Azure Fortress",
        "value": ["-warp", "3", "4"]
    }, {
        "label": "E3M5: The Ophidian Lair",
        "value": ["-warp", "3", "5"]
    }, {
        "label": "E3M6: The Halls of Fear",
        "value": ["-warp", "3", "6"]
    }, {
        "label": "E3M7: The Chasm",
        "value": ["-warp", "3", "7"]
    }, {
        "label": "E3M8: D'Sparil's Keep",
        "value": ["-warp", "3", "8"]
    }, {
        "label": "E3M9: The Aquifer",
        "value": ["-warp", "3", "9"]
    }]

    const SOTSR_EpisodeAndLevels = [...Full_EpisodeAndLevels, {
        "label": "E4: The Ossuary",
        "value": ["-episode", "4"],
    }, {
        "label": "E4M1: Catafalque",
        "value": ["-warp", "4", "1"]
    }, {
        "label": "E4M2: Blockhouse",
        "value": ["-warp", "4", "2"]
    }, {
        "label": "E4M3: Ambulatory",
        "value": ["-warp", "4", "3"]
    }, {
        "label": "E4M4: Sepulcher",
        "value": ["-warp", "4", "4"]
    }, {
        "label": "E4M5: Great Stair",
        "value": ["-warp", "4", "5"]
    }, {
        "label": "E4M6: Halls of the Apostate",
        "value": ["-warp", "4", "6"]
    }, {
        "label": "E4M7: Ramparts of Perdition",
        "value": ["-warp", "4", "7"]
    }, {
        "label": "E4M8: Shattered Bridge",
        "value": ["-warp", "4", "8"]
    }, {
        "label": "E4M9: Mausoleum",
        "value": ["-warp", "4", "9"]
    }, 

    {
        "label": "E5: The Stagnant Demesne",
        "value": ["-episode", "5"],
    }, {
        "label": "E5M1: Ochre Cliffs",
        "value": ["-warp", "5", "1"]
    }, {
        "label": "E5M2: Rapids",
        "value": ["-warp", "5", "2"]
    }, {
        "label": "E5M3: Quay",
        "value": ["-warp", "5", "3"]
    }, {
        "label": "E5M4: Courtyard",
        "value": ["-warp", "5", "4"]
    }, {
        "label": "E5M5: Hydratyr",
        "value": ["-warp", "5", "5"]
    }, {
        "label": "E5M6: Colonnade",
        "value": ["-warp", "5", "6"]
    }, {
        "label": "E5M7: Foetid Manse",
        "value": ["-warp", "5", "7"]
    }, {
        "label": "E5M8: Field of Judgement",
        "value": ["-warp", "5", "8"]
    }, {
        "label": "E5M9: Skein of D'Sparil",
        "value": ["-warp", "5", "9"]
    },

    {
        "label": "E6: Fate's Path",
        "value": ["-episode", "6"],
    }, {
        "label": "E6M1: Raven's Lair",
        "value": ["-warp", "6", "1"]
    }, {
        "label": "E6M2: The Water Shrine",
        "value": ["-warp", "6", "2"]
    }, {
        "label": "E6M3: American's Legacy",
        "value": ["-warp", "6", "3"]
    }];

    const commonExecBase = {
        "maxPlayers":4,
        "files": {
            "main": {"path":"HERETIC.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]", "value[2]"],
                "optional":true,
                "choices": null
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "Thou Needeth a Wet-Nurse",
                    "value": "1"
                }, {
                    "label": "Yellowbellies-R-US",
                    "value": "2"
                }, {
                    "label": "Bringest them Oneth",
                    "value": "3"
                }, {
                    "label": "Thou Art a Smite-Meister",
                    "value": "4"
                }, {
                    "label": "Black Plague Possesses Thee",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    const shareware = deepClone(commonExecBase);
    shareware.parameters.episodeAndLevel.choices = Shareware_EpisodeAndLevels;

    const full = deepClone(commonExecBase);
    full.parameters.episodeAndLevel.choices = Full_EpisodeAndLevels;

    const sotsr = deepClone(commonExecBase);
    sotsr.parameters.episodeAndLevel.choices = SOTSR_EpisodeAndLevels;

    return {
        "name": "Heretic",
        "get": [{
            type:"gog",
            prodId:"1345971634",
            nameId:"heretic_hexen_collection",
            name:"Heretic + Hexen Collection"
        },{
            type:"gog",
            prodId:"1290366318",
            nameId:"heretic_shadow_of_the_serpent_riders",
            name:"Heretic: Shadow of the Serpent Riders"
        }],
        "executables": {
            "shareware": {
                "name":"Shareware (DOS)",
                ...shareware
            },
            "full": {
                "name":"Full (DOS)",
                ...full
            },
            "sotsr": {
                "name":"Shadow of the Serpent Riders (DOS)",
                ...sotsr
            }
        }
    }
}
