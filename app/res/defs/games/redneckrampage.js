module.exports = function({GameDefsType, utils}) {

    const path = require('path');

    const fullLevels = [
        {"label": "Episode 1: Outskirts","value": ["1", "1"]},
        {"label": "E1L1 Taylor Town","value": ["1", "1"]},
        {"label": "E1L2 Lumberland","value": ["1","2"]},
        {"label": "E1L3 Junkyard","value": ["1","3"]},
        {"label": "E1L4 Drive-In","value": ["1","4"]},
        {"label": "E1L5 Dairyair Farms","value": ["1","5"]},
        {"label": "E1L6 Sewers","value": ["1","6"]},
        {"label": "E1L7 Smeltin' Plant","value": ["1", "7"]},

        {"label": "Episode 2: Downtown","value": ["2", "1"]},
        {"label": "E2L1 Downtown Hickston","value": ["2", "1"]},
        {"label": "E2L2 Nut House","value": ["2", "2"]},
        {"label": "E2L3 J.Cluck's!","value": ["2", "3"]},
        {"label": "E2L4 The Ruins","value": ["2", "4"]},
        {"label": "E2L5 Grimley's Mortuary","value": ["2", "5"]},
        {"label": "E2L6 Uranium Mines","value": ["2", "6"]},
        {"label": "E2L7 Beaudry Mansion","value": ["2", "7"]},

        {"label": "Episode 3: Pissin' Contest","value": ["3", "1"]},
        {"label": "E3L1 Country Livin'","value": ["3", "1"]},
        {"label": "E3L2 Barnyard Hijinks","value": ["3", "2"]},
        {"label": "E3L3 Condemned","value": ["3", "3"]},
        {"label": "E3L4 Used Cars","value": ["3", "4"]},
        {"label": "E3L5 Trainspotting","value": ["3", "5"]},
        {"label": "E3L6 Mud Alley!","value": ["3", "6"]},
        {"label": "E3L7 The Factory","value": ["3", "7"]}
    ];


    const ridesAgainLevels = [
        {"label": "Episode 1: Land of The Lost","value": ["1", "1"]},
        {"label": "E1L1 Area 69","value": ["1", "1"]},
        {"label": "E1L2 Camino Del Diablo","value": ["1","2"]},
        {"label": "E1L3 El Peso","value": ["1","3"]},
        {"label": "E1L4 Jack O' Lope Farm","value": ["1","4"]},
        {"label": "E1L5 Wako","value": ["1","5"]},
        {"label": "E1L6 El Peso' Again","value": ["1","6"]},
        {"label": "E1L7 Refinery","value": ["1", "7"]},

        {"label": "Episode 2: Homeward Bound","value": ["2", "1"]},
        {"label": "E2L1 Sunny Shores","value": ["2", "1"]},
        {"label": "E2L2 Gamblin' Boat","value": ["2", "2"]},
        {"label": "E2L3 Lummockston","value": ["2", "3"]},
        {"label": "E2L4 Disgrace Land","value": ["2", "4"]},
        {"label": "E2L5 Moto Madness","value": ["2", "5"]},
        {"label": "E2L6 Brothel","value": ["2", "6"]},
        {"label": "E2L7 Back to Hickston","value": ["2", "7"]},

        {"label": "Episode 3: Pissin' Contest","value": ["3", "1"]},
        {"label": "E3L1 Pipe Dreams","value": ["3", "1"]},
        {"label": "E3L2 Swamp Buggies","value": ["3", "2"]},
        {"label": "E3L3 Square Dancin'","value": ["3", "3"]},
        {"label": "E3L4 Hog Wild","value": ["3", "4"]},
        {"label": "E3L5 Road Rage","value": ["3", "5"]},
        {"label": "E3L6 Snake Canyon","value": ["3", "6"]},
        {"label": "E3L7 Luck Sore","value": ["3", "7"]}
    ];

    const dosExecParamsCommon = {
        "pname": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "static",
            "value": ["'-name'", "MyName"],
            "for":"private"
        },
        "recordDmo": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "Record DMO",
            "value": "'/r'",
            "for": "private"
        },
        "monstersSkill": {
            "modeSupport": ["multiplayer","singleplayer","settings"],
            "type": "choice",
            "label": "Monsters Skill",
            "value": "'/s' + value",
            "optional":true,
            "choices": [{
                "label": "Wuss",
                "value": "1"
            }, {
                "label": "Meejum",
                "value": "2"
            }, {
                "label": "Hard Ass",
                "value": "3"
            }, {
                "label": "Kill Billy",
                "value": "4"
            }, {
                "label": "Psychobilly",
                "value": "5"
            }]
        },
        "noMonsters": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "No Monsters",
            "value": "'/m'"
        },
        "respawn": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Respawn",
            "value":"'/t' + value",
            "optional":true,
            "choices": [{
                "label": "Monsters",
                "value": "1"
            }, {
                "label": "Items",
                "value": "2"
            }, {
                "label": "Inventory",
                "value": "3"
            }, {
                "label": "All",
                "value": "x"
            }]
        },
        "botsNum": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "numrange",
            "min":2,
            "max":8,
            "delta":1,
            "label": "Bots",
            "value": "'/q' + value",
            "optional":true
        },
        "netmode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "NetMode",
            "optional":true,
            "value":["'/i' + value"],
            "description": "Peer2Peer is usually faster with 2-4 players",
            "choices": [
                {"label": "Peer2Peer","value": 1},
                {"label": "Master/Slave","value": 0}
            ]
        },
        "map": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"maps",
            "value": ["'-map'", "value"],
            "label": "Map",
            symlink:'forced',
            props: {filters: [{name: 'Map Files', extensions: ['map']}]},
            "optional":true
        },
        "grp": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'/g'+value"],
            "label": "GRP",
            order:99,
            symlink:'forced',
            props: {filters: [{name: 'Grp Files', extensions: ['grp']}]},
            "optional":true
        },
        "con": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'/x'","value"],
            "label": "CON",
            symlink:'forced',
            props: {filters: [{name: 'Con Files', extensions: ['con']}]},
            "optional":true
        },
        "f": {
            "modeSupport": ["multiplayer"],
            "description": "Send Fewer Packets during Multi-Player Mode",
            "type": "choice",
            "label": "PacketsPerSec",
            "value":"'/f' + value",
            "optional":true,
            "choices": [{
                "label": "30 (default)",
                "value": "1"
            }, {
                "label": "15",
                "value": "2"
            }, {
                "label": "7.5",
                "value": "4"
            }]
        }
    };

    const dosExecParamsFull = Object.assign({}, dosExecParamsCommon, {
        "botsAi": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "Bots AI",
            "value": "'/a'"
        },
        "playDmo": {
            "modeSupport": ["singleplayer"],
            "type": "file",
            "format": "*.dmo", 
            "label": "Play DMO",
            "value": "'/d' + value",
            "dosboxMount": true,
            "optional": true
        },
        "episodeAndLevel": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Level",
            "optional":true,
            "value":["'/v' + value[0]", "'/l' + value[1]"],
            "choices": fullLevels
        },
        "multiplayerMode": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Multiplayer Mode",
            "value":"'/c' + value",
            "optional":true,
            "choices": [{
                "label": "Redneckmatch (Spawn)",
                "value": "1"
            }, {
                "label": "Coop",
                "value": "2"
            }, {
                "label": "Redneckmatch (No Spawn)",
                "value": "3"
            }]
        }
    });

    const dosExecParamsDemo = Object.assign({}, dosExecParamsFull);
    delete dosExecParamsDemo.episodeAndLevel; // demo cannot switch levels;

    const dosExecParamsRidesAgain = Object.assign({},dosExecParamsFull, {
        "episodeAndLevel": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "choice",
            "label": "Level",
            "optional":true,
            "value":["'/v' + value[0]", "'/l' + value[1]"],
            "choices": ridesAgainLevels
        }
    });

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        base: {
            "runToolOpts": {"type":"dosbox", "data":{commands, "args":["-machine", "vesa_nolfb"]}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}},
            "beforeRun":[{"cmd":"edit-ini","file":"commit_dat", "edits":[
                {"category":"Commit","entry":"SOCKETNUMBER", "value":"34889"}, {"category":"Commit","entry":"NUMPLAYERS", "value":"GameRoom.NumberOfPlayers"},

                {"category":"Commit","entry":"COMMTYPE", "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]==='serial'?1:3`}, // ipx or serial

                // serial entries:
                {"category":"Commit","entry":"COMPORT", "value":"1"},
                {"category":"Commit","entry":"IRQNUMBER", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"UARTADDRESS", "unquote":true, "value":"'~'"},
                {"category":"Commit","entry":"PORTSPEED", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
            ]}]
        }
    };

    vars.rr = structuredClone(vars.base);
    vars.rr.beforeRun[0].edits.push({"category":"Commit","entry":"LAUNCHNAME", "value":"'RR.EXE'"});
    vars.ra = structuredClone(vars.base);
    vars.ra.beforeRun[0].edits.push({"category":"Commit","entry":"LAUNCHNAME", "value":"'RA.EXE'"});
    vars.demo = structuredClone(vars.base);
    vars.demo.beforeRun[0].edits.push({"category":"Commit","entry":"LAUNCHNAME", "value":"'REDNECK.EXE'"});

    const patches = require(path.join(__dirname, 'buildmfx.i.js'))(utils, "main");

    return {
        "name": "Redneck Rampage",
        "get": [{
            type:"gog",
            prodId:"1207658674",
            nameId:"redneck_rampage_collection",
            name:"Redneck Rampage Collection"
        }],
        "executables": {
            "full": {
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"RR.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat":{"path":"COMMIT.DAT", "optional":true},
                    "grp": {"path":"REDNECK.GRP"}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {"file":"commit", "runTool":vars.rr.runToolOptsMultiplayer, "beforeRun":vars.rr.beforeRun},
                    "settings": {"file":"setup", "runTool":vars.rr.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.rr.runToolOpts}
                },
                "parameters": dosExecParamsFull
            },
            "ridesagain": {
                "name":"Rides Again (DOS)",
                "mountImg": true,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"RA.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat":{"path":"COMMIT.DAT", "optional":true},
                    "grp": {"path":"REDNECK.GRP"}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {"file":"commit", "runTool":vars.rr.runToolOptsMultiplayer, "beforeRun":vars.ra.beforeRun},
                    "settings": {"file":"setup", "runTool":vars.rr.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.rr.runToolOpts}
                },
                "parameters": dosExecParamsRidesAgain
            },
            "demo": {
                "name":"Demo (DOS)",
                "mountImg": true,
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"REDNECK.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat":{"path":"COMMIT.DAT", "optional":true},
                    "grp": {"path":"REDNECK.GRP"}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {"file":"commit", "runTool":vars.rr.runToolOptsMultiplayer, "beforeRun":vars.demo.beforeRun},
                    "settings": {"file":"setup", "runTool":vars.rr.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.rr.runToolOpts}
                },
                "parameters": dosExecParamsDemo
            },
        }
    };
}