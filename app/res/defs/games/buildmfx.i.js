const fs = require('fs');

function getByteDefs() {
    const originalStartBuffer = Buffer.from('8B069931D029D089C18B039931D029D039C17E128B0389C2B903000000C1FA1FF7F98903EB108B0689C2B903000000C1FA1FF7F98906C126058B138D04950000000029D0C1E005B90F00000089038B1D', 'hex');
    const originalLast3Buffer = Buffer.from('008B06', 'hex');
    const patchedStartBuffer =  Buffer.from('EB4C8B160FAFD0C1E205C1FA0F89160FAF036BC060C1F80F8903EB4100C1FA1FF7F98903EB108B0689C2B903000000C1FA1FF7F98906C126058B138D04950000000029D0C1E005B90F00000089038B05', 'hex');
    const patchedLast3Buffer =  Buffer.from('00EBAC', 'hex');
    const deltaToLast3Bytes = 0x53;

    return {
        originalStartBuffer,
        originalLast3Buffer,
        patchedStartBuffer,
        patchedLast3Buffer,
        deltaToLast3Bytes
    }
}

module.exports = function init(utils, fileRef) {
    
    function apply(opts) {
        const defs = getByteDefs();
        const exepath = opts.files[fileRef];
        const bytes = fs.readFileSync(exepath);
        
        const idx = utils.subarrayIndex(bytes, defs.originalStartBuffer);
        if(idx==-1) {
            return {
                title: "Original mouse function not found",
                body: "Did you already apply this patch?"
            }
        };

        const idxLast3Bytes = idx+defs.deltaToLast3Bytes;
        
        bytes.set(defs.patchedStartBuffer, idx);
        bytes.set(defs.patchedLast3Buffer, idxLast3Bytes);

        fs.writeFileSync(exepath, bytes);

        return {
            title: "Success",
            body: "Using patched mouse function."
        }
    }

    function undo(opts) {
        const defs = getByteDefs();
        const exepath = opts.files[fileRef];
        const bytes = fs.readFileSync(exepath);
        
        const idx = utils.subarrayIndex(bytes, defs.patchedStartBuffer);
        if(idx==-1) {
            return {
                title: "Patched mouse function not found",
                body: "You probably did not apply the patch yet"
            }
        };

        const idxLast3Bytes = idx+defs.deltaToLast3Bytes;
        
        bytes.set(defs.originalStartBuffer, idx);
        bytes.set(defs.originalLast3Buffer, idxLast3Bytes);

        fs.writeFileSync(exepath, bytes);

        return {
            title: "Success",
            body: "Using original mouse function."
        }
    }

    return {
        "buildmfx": {
            title: "BuildMFX (Mouse Fix)",
            files:[fileRef],
            html: `
            <p>Improves mouse aim for Build Engine games.<br/>
            <u>Replaces old Mouse Code:</u><br/>
            <pre>
            // original MACT386.LIB code (disassembled)
            void CONTROL_GetMouseDelta(int32 *x, int32 *y) {
                MOUSE_GetDelta(x, y);
                // artificial priority for bigger delta axis
                if (abs(*x) <= abs(*y)) {
                    *x /= 3;
                } else {
                    *y /= 3;
                }
                *x = (*x * 32 * CONTROL_MouseSensitivity) >> 15;
                *y = *y * 96;
            }
            </pre>
            <u>With new Mouse Code:</u><br/>
            <pre>
            // new code (after this fix)
            void CONTROL_GetMouseDelta(int32 *x, int32 *y) {
                MOUSE_GetDelta(x, y);
                *x = (*x * 32 * CONTROL_MouseSensitivity) >> 15;
                *y = (*y * 96 * CONTROL_MouseSensitivity) >> 15;
            }
            </pre>
            Credits to <b>-=CHE@TER=-</b> for the fix.
            </p>
            `,
            apply,
            undo
        }
    }
}
