module.exports = function({GameDefsType}) {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const runToolOpts = {"type":"dosbox", "data":{commands, "args":["-machine", "vesa_nolfb"]}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}};
    const beforeRun = [{"cmd":"edit-ini","file":"commit_dat", "edits":[
        {"category":"Commit","entry":"SOCKETNUMBER", "value":"34889"},{"category":"Commit","entry":"LAUNCHNAME", "value":"'TV.EXE'"},{"category":"Commit","entry":"NUMPLAYERS", "value":"GameRoom.NumberOfPlayers"},

        {"category":"Commit","entry":"COMMTYPE", "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]==='serial'?1:3`}, // ipx or serial

        // serial entries:
        {"category":"Commit","entry":"COMPORT", "value":"1"},
        {"category":"Commit","entry":"IRQNUMBER", "unquote":true, "value":"'~'"},
        {"category":"Commit","entry":"UARTADDRESS", "unquote":true, "value":"'~'"},
        {"category":"Commit","entry":"PORTSPEED", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
    ]}];
    
    const files = {
        "setup": {"path":"SETUP.EXE"},
        "tv": {"path":"TV.EXE"},
        "commit": {"path":"COMMIT.EXE"},
        "commit_dat": {"path":"COMMIT.DAT", "optional":true}
    };

    const runnables = {
        "multiplayer": {
            "file":"commit",
            "runTool":runToolOptsMultiplayer,
            "beforeRun":beforeRun
        },
        "settings": {"file":"setup", "runTool":runToolOpts},
        "singleplayer": {"file":"tv", "runTool":runToolOpts}
    };

    return {
        "resourceFolders": ["maps", "mods"],
        "name": "Terminal Velocity",
        "get": [{
            type:"gog",
            prodId:"1861746002",
            nameId:"terminal_velocity_boosted_edition",
            name:"Terminal Velocity™: Boosted Edition"
        }, {
            type:"zoom-platform",
            nameId:"terminal-velocity",
            name:"Terminal Velocity"
        }],
        "executables": {
            "shareware1_2": {
                "name":"Shareware 1.2 (DOS)",
                "files": files,
                "runnables": runnables,
                "parameters": {},
                "resources": {}
            },
            "full": {
                "name":"Full (DOS)",
                "files": files,
                "runnables": runnables,
                "parameters": {},
                "resources": {}
            }
        }
    };
}