module.exports = function({}) {

    const dosboxPreCommands = ['CONFIG -set "core=auto"', 'CONFIG -set "cycles=max"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":"serial"}};

    const runnables = {
        "multiplayer": {"file":"main","runTool":runToolOptsMultiplayer},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const tips = {html: `
    <p>
        <u>The game only works if it's in a root directory called "RETAL".<br/>
        to get around this issue:</u><br/>
        1. Create an empty directory with a file named RETAL.BAT<br/>
        2. Edit the text content of that file to: RETAL\\RETAL.EXE<br/>
        3. Place the game files in a subdirectory called "RETAL".<br/>
        4. Set the root directory (not the "RETAL" dir) in NukemNet.
    </p>

    <h4>Multiplayer Instructions</h4>
    <ol>
       <li>Get to the main menu</li>
       <li>Tap 8: Head to Head</li>
       <li>Tap F2->F1->F4 (Set COM1, Baud 9600)</li>
       <li>Tap F1 to start the game</li>
    </ol>
    
    `};

    return {
        "name": "F29 Retaliator",
        "executables": {
            "full": {
                tips,
                maxPlayers:2,
                "name":"Full (DOS)",
                "files": {
                    "exec": {"glob":["RETAL/RETAL.EXE", "RETAL/X.EXE", "BFILES/RETAL_OR.EXE"]},
                    "main": {"glob":["RETAL.BAT", "RETAL.EXE"]}
                },
                "runnables": runnables,
                parameters: {}
            }
        }
    };
}