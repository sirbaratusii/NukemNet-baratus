module.exports = function({GameDefsType}) {
    const fs = require('fs/promises');
    const path = require('path');

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    // memsize must be set to 16 for gta24.exe to work properly. higher than that makes it crash after the menu.
    const ini = `
[dosbox]
memsize = 16
    `

    const runToolOpts = {"type":"dosbox", "data":{commands, ini, cdInto:true}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, cdInto:true, ini, "net":['ipx', 'serial:9600', 'serial:14400', 'serial:19200']}};

    function calculateRunType(ctx) {
        const gfx = ctx?.GameRoom?.Params.gfx[0];
        if(gfx === 'gta_low_c') return 0;
        if(gfx === 'gta_high_c') return 1;
        if(gfx === 'gta_3dfx') return 2;
        return 0;
    }

    const beforeRun = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {
        await fs.writeFile(path.resolve(ctx.GameDir, 'gtados', 'STARTUP.INI'), `
[dummy]
0
[language]
0
[runtype]
${calculateRunType(ctx)}
[network]
${ctx?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?1:2}
[ipx]
1
[serial]
1
[modem]
1,0000000
[serial-port]
1
[serial-speed]
${ctx?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[1]||19200}
[modem-phone]
0000000
[modem-reset]
ATZ
[modem-dial]
ATDT
[modem-hangup]
+++ATH
[modem-init]
ATZ
[modem-auto]
ATS0=1
`);
    }}];

    function fileResolver(ctx) {
        return ctx?.GameRoom?.Params.gfx[0];
    }

    const runnables = {
        "multiplayer": {
            "file":fileResolver,
            beforeRun,
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"k", "runTool": runToolOpts},
        "singleplayer": {
            "file":fileResolver,
            beforeRun,
            "runTool": runToolOpts}
    };

    return {
        "name": "GTA",
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "files": {
                    "gta_low_c": {"path":"gtados/gta8.exe"},
                    "gta_high_c": {"path":"gtados/gta24.exe"},
                    // "gta_3dfx": {"path":"gtados/gtafx.exe"}, doesn't work on dosbox
                    "k": {"path":"gtados/k.exe"},
                    "gtadata": {"path":"gtadata"},
                    "cfg": {"path":"gtados/STARTUP.INI", "optional":true},
                },
                "runnables": runnables,
                "parameters": {
                    "gfx": {
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "choice",
                        "label": "Graphics",
                        "value":(ctx)=>ctx?.value,
                        "choices": [{
                            "label": "Low Color",
                            "value": "gta_low_c"
                        }, {
                            "label": "High Color",
                            "value": "gta_high_c"
                        }/*, {
                            "label": "3dfx",
                            "value": "gta_3dfx"
                        }*/],
                        syncOnly:true
                    }
                }
            }
        }
    };
}