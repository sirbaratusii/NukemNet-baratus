module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "nk": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":"ipx"}}
        }
    };

    return {
        "resourceFolders": ["maps", "mods"],
        "name": "NetKeen",
        "executables": {
            "netkeen": {
                "name":"NetKeen (DOS)",
                "files": {
                    "netkeen": {"path":"NETKEEN.EXE"},
                    "nklaunch": {"path":"NKLAUNCH.EXE", "optional":true}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"netkeen",
                        "runTool":vars.nk.runToolOptsMultiplayer
                    },
                    "settings": {"file":"netkeen", "runTool":vars.nk.runToolOpts},
                    "singleplayer": {"file":"netkeen", "runTool":vars.nk.runToolOpts}
                },
                "parameters": {
                    "server": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-server'"],
                        "for": "host-only-private"
                    },

                    /* Amount of players, 1-4*/
                    "nodes": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                        "for":"private"
                    },
                    /* Which keen game */
                    "ext": {
                        "modeSupport": ["multiplayer", "singleplayer", "settings"],
                        "type": "choice",
                        "label": "Extension",
                        "value":["'-ext'", "value[0]"],
                        "choices": [
                            {"label": "Vorticons","value": ["ckv"]},
                            {"label": "Keen Dreams","value": ["kdr"]},
                            {"label": "Secret of the Oracle","value": ["ck4"]},
                            {"label": "The Armageddon Machine","value": ["ck5"]},
                            {"label": "Aliens Ate My Babysitter","value": ["ck6"]},
                            {"label": "Gargapplesauce 5","value": ["ga5"]}
                        ]
                    }
                },
                "resources": {}
            }
        }
    };
}