module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands, cdInto:true}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, cdInto:true, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO edit ZAR.CFG

    return {
        "name": "Z.A.R.",
        "executables": {
            "full": {
                "maxPlayers":16,
                midGameJoin: true,
                "mountImg": true,
                "name":"Full (DOS)",
                "files": {
                    "snd": {"path":"SOUND/SETSOUND.EXE"},
                    "main": {"path":"ZAR.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "demo": {
                "maxPlayers":16,
                midGameJoin: true,
                "name":"Demo (DOS)",
                "files": {
                    "snd": {"path":"SOUND/SETSOUND.EXE"},
                    "main": {"path":"ZAR.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}