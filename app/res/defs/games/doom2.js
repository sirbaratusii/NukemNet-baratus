module.exports = function ({GameDefsType, utils}) {

    const common = require('./doom_common.i');

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }

    const EpisodeAndLevels = [{
        "label": "1: Entryway",
        "value": ["-warp", "1"]
    }, {
        "label": "2: Underhalls",
        "value": ["-warp", "2"]
    }, {
        "label": "3: The Gantlet",
        "value": ["-warp", "3"]
    }, {
        "label": "4: The Focus",
        "value": ["-warp", "4"]
    }, {
        "label": "5: The Waste Tunnels",
        "value": ["-warp", "5"]
    }, {
        "label": "6: The Crusher",
        "value": ["-warp", "6"]
    }, {
        "label": "7: Dead Simple",
        "value": ["-warp", "7"]
    }, {
        "label": "8: Tricks and Traps",
        "value": ["-warp", "8"]
    }, {
        "label": "9: The Pit",
        "value": ["-warp", "9"]
    }, {
        "label": "10: Refueling Base",
        "value": ["-warp", "10"]
    }, {
        "label": "11: Circle of Death",
        "value": ["-warp", "11"]
    }, {
        "label": "12: The Factory",
        "value": ["-warp", "12"]
    }, {
        "label": "13: Downtown",
        "value": ["-warp", "13"]
    }, {
        "label": "14: The Inmost Dens",
        "value": ["-warp", "14"]
    }, {
        "label": "15: Industrial Zone",
        "value": ["-warp", "15"]
    }, {
        "label": "16: Suburbs",
        "value": ["-warp", "16"]
    }, {
        "label": "17: Tenements",
        "value": ["-warp", "17"]
    }, {
        "label": "18: The Courtyard",
        "value": ["-warp", "18"]
    }, {
        "label": "19: The Citadel",
        "value": ["-warp", "19"]
    }, {
        "label": "20: Gotcha!",
        "value": ["-warp", "20"]
    }, {
        "label": "21: Nirvana",
        "value": ["-warp", "21"]
    }, {
        "label": "22: The Catacombs",
        "value": ["-warp", "22"]
    }, {
        "label": "23: Barrels o' Fun",
        "value": ["-warp", "23"]
    }, {
        "label": "24: The Chasm",
        "value": ["-warp", "24"]
    }, {
        "label": "25: Bloodfalls",
        "value": ["-warp", "25"]
    }, {
        "label": "26: The Abandoned Mines",
        "value": ["-warp", "26"]
    }, {
        "label": "27: Monster Condo",
        "value": ["-warp", "27"]
    }, {
        "label": "28: The Spirit World",
        "value": ["-warp", "28"]
    }, {
        "label": "29: The Living End",
        "value": ["-warp", "29"]
    }, {
        "label": "30: Icon of Sin",
        "value": ["-warp", "30"]
    }, {
        "label": "31: Wolfenstein",
        "value": ["-warp", "31"]
    }, {
        "label": "32: Grosse",
        "value": ["-warp", "32"]
    }]

    const hoe = {
        "maxPlayers":4,
        "files": {
            "main": {"path":"DOOM2.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "wad": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "file",
                "res":"wads",
                "value": ["'-file'", "value"],
                "label": "WAD File",
                symlink:'forced',
                props: {filters: [{name: 'WAD Files', extensions: ['wad']}]},
                "optional":true
            },
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": EpisodeAndLevels
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "I'm too young to die",
                    "value": "1"
                }, {
                    "label": "Hey, not too rough",
                    "value": "2"
                }, {
                    "label": "Hurt me plenty",
                    "value": "3"
                }, {
                    "label": "Ultra-Violence",
                    "value": "4"
                }, {
                    "label": "Nightmare!",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "fastMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Fast Monsters (3x)",
                "value": "'-fast'"
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    return {
        "name": "Doom 2",
        "get": [{
            type:"gog",
            prodId:"1440161275",
            nameId:"doom_ii",
            name:"Doom II"
        }],
        "executables": {
            "hoe": {
                "name":"Hell on Earth (DOS)",
                ...hoe
            },
        }
    }
}
