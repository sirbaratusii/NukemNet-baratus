module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO edit cfg, set direct conn and player name.
    const tips = {html: `
    <h3>Both players do the following steps:</h3>
    <ol>
        <li>In game menu, select "Modem/Serial"</li>
        <li>Connection: Choose Direct Link</li>
        <li>One player clicks "Dial"</li>
        <li>Other player clicks "Answer"</li>
    </ol>
    `};

    return {
        "name": "The Need for Speed: Special Edition",
        "executables": {
            "full": {
                "maxPlayers":2,
                "mountImg": true,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"NFS.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}