module.exports = function({deepClone, GameDefsType}) {

    const dosboxPreCommands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"'];
    const prepareSetupCfg = [{"cmd":"edit-ini","file":"setup_cfg", "edits":[
        {"category":"Comm Setup","entry":"StartLevel", unquote:true, "value":"parseInt(GameRoom.Params.level[0])"},
        {"category":"Comm Setup","entry":"TimeLimit", unquote:true, "value":"GameRoom.Params.timeLimit[0]>0?GameRoom.Params.timeLimit[0]:null"},
        {"category":"Comm Setup","entry":"NumberPlayers", unquote:true, "value":"GameRoom.NumberOfPlayers"},
        {"category":"Comm Setup","entry":"SocketNumber", unquote:true, "value":"'~'"},
        {"category":"Comm Setup","entry":"ConnectType", "value":`0`},

        // serial entries:
        
        {"category":"Comm Setup","entry":"ComPort", "value":"1"},
        {"category":"Comm Setup","entry":"IrqNumber", "unquote":true, "value":"'~'"},
        {"category":"Comm Setup","entry":"UartAddress", "unquote":true, "value":"'~'"},
        {"category":"Comm Setup","entry":"PortSpeed", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
    ]}];

    const sharewareLevels = [
        {"label": "0 (Coop)","value":               ["0"]},
        {"label": "1 Abu Simbel (Coop)" ,"value":   ["1"]},
        {"label": "2 Dendur (Coop)","value":        ["2"]},
        {"label": "3 Kalabsh (Coop)","value":       ["3"]},
        {"label": "4 El Subua (Coop)","value":      ["4"]}
    ];
    const fullAllLevels = sharewareLevels.concat([
        {"label": "5 El Derr(Coop)","value":            ["5"]},
        {"label": "6 Abu Ghurab (Coop)","value":        ["6"]},
        {"label": "7 Philae (Coop)","value":            ["7"]},
        {"label": "8 El Kab (Coop)","value":            ["8"]},
        {"label": "9 Aswan (Coop)","value":             ["9"]},
        {"label": "10 ??? (Coop)","value":              ["10"]},
        {"label": "11 Qubbet el Hawa (Coop)","value":   ["11"]},
        {"label": "12 Abydos (Coop)","value":           ["12"]},
        {"label": "13 Edufu (Coop)","value":            ["13"]},
        {"label": "14 West Bank (Coop)","value":        ["14"]},
        {"label": "15 Luxor (Coop)","value":            ["15"]},
        {"label": "16 Karnak (Coop)","value":           ["16"]},
        {"label": "17 Saqqara (Coop)","value":          ["17"]},
        {"label": "18 Mitrrahn (Coop)","value":         ["18"]},
        {"label": "19 ???? (Coop)","value":             ["19"]},
        {"label": "20 ??? (Coop)","value":              ["20"]},
        {"label": "21 (Deatmatch)","value":      ["21"]},
        {"label": "22 (Deatmatch)","value":      ["22"]},
        {"label": "23 (Deatmatch)","value":      ["23"]},
        {"label": "24 (Deatmatch)","value":      ["24"]},
        {"label": "25 (Deatmatch)","value":      ["25"]},
        {"label": "26 (Deatmatch)","value":      ["26"]},
        {"label": "27 (Deatmatch)","value":      ["27"]},
        {"label": "28 (Deatmatch)","value":      ["28"]},
        {"label": "29 (Deatmatch)","value":      ["29"]},
        {"label": "30 (Deatmatch)","value":      ["30"]},
        {"label": "31 (Deatmatch)","value":      ["31"]},
        {"label": "32 (Deatmatch)","value":      ["32"]}
    ]);

    const commonDosParams = {
        "network": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'/network'"],
            "for":"private",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]=='ipx'}
        },
        "serial": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'/null'"],
            "for":"private",
            addIf: (obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]=='serial'}
        },
        "level": {
            /* level param is mandatory, or game starts on menu, without ability to start game */
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Level",
            "value":["value[0]"],
            "choices": {},
            "syncOnly": true
        },
        "timeLimit": {
            "modeSupport": ["multiplayer"],
            "syncOnly": true,
            "type": "numrange",
            "min":0,
            "max":240,
            "delta":1,
            "label": "Time Limit (Minutes)",
            "value": "value",
            "optional":true
        },
        "noMonsters": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "No Monsters",
            "value": "'/nocreatures'"
        },
        "record": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "boolean",
            "label": "Record Demo",
            "value": "'/record'"
        }
    };

    let dosFullParams = deepClone(commonDosParams);
    let dosDemoParams = deepClone(commonDosParams);

    /* level param is mandatory for this does game, otherwise it starts on menu, without ability to start game */
    dosFullParams = Object.assign({}, dosFullParams, {
        "level": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Level",
            "value":["value[0]"],
            "choices": fullAllLevels,
            "syncOnly": true
        }
    });

    dosDemoParams = Object.assign({}, dosDemoParams, {
        "level": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Level",
            "value":["value[0]"],
            "choices": sharewareLevels,
            "syncOnly": true
        }
    });

    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{"commands":dosboxPreCommands, "args":["-machine", "vesa_nolfb"]}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}},
            "beforeRun": prepareSetupCfg
        }
    };

    return {
        "resourceFolders": ["maps", "mods"],
        "name": "Powerslave/Exhumed",
        "get": [{
            type:"gog",
            prodId:"2132611980",
            nameId:"powerslave",
            name:"Powerslave"
        }],
        "executables": {
            "exhumed_full": {
                "mountImg": true,
                "name":"Exhumed (DOS)",
                "files": {
                    "setup": {"path":"MENU.EXE"},
                    "main": {"path":"EX.EXE"},
                    "setup_cfg": {"path":"SETUP.CFG", "optional":true}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        "runTool":vars.game.runToolOptsMultiplayer,
                        "beforeRun":vars.game.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": dosFullParams
            },
            "powerslave_full": {
                "mountImg": true,
                "name":"Powerslave (DOS)",
                "files": {
                    "setup": {"path":"MENU.EXE"},
                    "main": {"path":"PS.EXE"},
                    "setup_cfg": {"path":"SETUP.CFG", "optional":true}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        "runTool":vars.game.runToolOptsMultiplayer,
                        "beforeRun":vars.game.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": dosFullParams
            },
            "powerslave_demo": {
                "name":"Powerslave Demo (DOS)",
                "files": {
                    "setup": {"path":"MENU.EXE"},
                    "main": {"path":"PS.EXE"},
                    "setup_cfg": {"path":"SETUP.CFG", "optional":true}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        "runTool":vars.game.runToolOptsMultiplayer,
                        "beforeRun":vars.game.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": dosDemoParams
            }
        }
    };
}