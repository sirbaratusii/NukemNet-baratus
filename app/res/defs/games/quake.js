
module.exports = function({sleep}) {

    const path = require('path');
    const glob = require('glob');
    const fs = require('fs/promises');    

    const dosboxPreCommands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer,
            "waitForFunc": ctx=>new Promise(async resolve=>{
                try {
                    // We need to wait for the game to create a screenshot, then we know the server is ready.
                    const ud1Path = path.resolve(path.join(ctx.GameDir), 'id1');

                    async function getScreenshots() {
                        const result = (await glob.glob(`*.pcx`, {cwd:ud1Path, nocase:true, absolute:true}));
                        return result;
                    }
    
                    const initialScrs = await getScreenshots();
                    const timeoutMS = 1000*12; // 12 seconds
                    const checkIntervalMS = 500;
                    for(let i=0;i<timeoutMS/checkIntervalMS;i++) {
                        await sleep(checkIntervalMS);
                        const newScrs = await getScreenshots();
                        if(newScrs?.length != initialScrs?.length) {
                            newScrs.forEach(scr=>{
                                if(!initialScrs.includes(scr)) {
                                    fs.rm(scr)
                                }
                            });
                            return resolve();
                        }
                    }
                } catch(e) {}
                resolve();
            })
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const sharewareEpisodeOneLevels = [
        {"label": "*Welcome To Quake*","value": ["start"]},
        {"label": "Entrance","value": ["start"]},
        {"label": "*Doomed Dimension*","value": ["e1m1"]},
        {"label": "e1m1 Slipgate Complex","value": ["e1m1"]},
        {"label": "e1m2 Castle of the Damned","value": ["e1m2"]},
        {"label": "e1m3 The Necropolis","value": ["e1m3"]},
        {"label": "e1m4 The Grisly Grotto","value": ["e1m4"]},
        {"label": "e1m5 Gloom Keep","value": ["e1m5"]},
        {"label": "e1m6 The Door To Chthon","value": ["e1m6"]},
        {"label": "e1m7 The House of Chthon","value": ["e1m7"]},
        {"label": "e1m8 Ziggurat Vertigo","value": ["e1m8"]}
    ];

    const fullAllLevels = sharewareEpisodeOneLevels.concat([
        {"label": "*Realm of Black Magic*","value": ["e2m1"]},
        {"label": "e2m1 The Installation","value": ["e2m1"]},
        {"label": "e2m2 Ogre Citadel","value": ["e2m2"]},
        {"label": "e2m3 Crypt of Decay","value": ["e2m3"]},
        {"label": "e2m4 The Ebon Fortress","value": ["e2m4"]},
        {"label": "e2m5 The Wizard's Manse","value": ["e2m5"]},
        {"label": "e2m6 The Dismal Oubliette","value": ["e2m6"]},
        {"label": "e2m7 Underearth","value": ["e2m7"]},

        {"label": "*Netherworld*","value": ["e3m1"]},
        {"label": "e3m1 Termination Central","value": ["e3m1"]},
        {"label": "e3m2 The Vaults of Zin","value": ["e3m2"]},
        {"label": "e3m3 The Tomb of Terror","value": ["e3m3"]},
        {"label": "e3m4 Satan's Dark Delight","value": ["e3m4"]},
        {"label": "e3m5 Wind Tunnels","value": ["e3m5"]},
        {"label": "e3m6 Chambers of Torment","value": ["e3m6"]},
        {"label": "e3m7 The Haunted Halls","value": ["e3m7"]},

        {"label": "*The Elder World*","value": ["e4m1"]},
        {"label": "e4m1 The Sewage System","value": ["e4m1"]},
        {"label": "e4m2 The Tower of Despair","value": ["e4m2"]},
        {"label": "e4m3 The Elder God Shrine","value": ["e4m3"]},
        {"label": "e4m4 The Palace of Hate","value": ["e4m4"]},
        {"label": "e4m5 Hell's Atrium","value": ["e4m5"]},
        {"label": "e4m6 The Pain Maze","value": ["e4m6"]},
        {"label": "e4m7 Azure Agony","value": ["e4m7"]},
        {"label": "e4m8 The Nameless City","value": ["e4m8"]},

        {"label": "*Final Level*","value": ["end"]},
        {"label": "end Shub-Niggurath's Pit","value": ["end"]},

        {"label": "*Deathmatch Arena*","value": ["dm1"]},
        {"label": "dm1 Place of Two Deaths","value": ["dm1"]},
        {"label": "dm2 Claustrophobopolis","value": ["dm2"]},
        {"label": "dm3 The Abandoned Base","value": ["dm3"]},
        {"label": "dm4 The Bad Place","value": ["dm4"]},
        {"label": "dm5 The Cistern","value": ["dm5"]},
        {"label": "dm6 The Dark Zone","value": ["dm6"]}
    ]);

    const commonParams = {

        "name": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'+name'", "MyName"],
            "for":"private"
        },
        "hostname": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+hostname'", "'nukemnet'"],
            "for":"host-only-private"
        },
        "maxplayers": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+maxplayers'", "16"],
            "for":"host-only-private"
        },
        "listen": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+listen'", "'1'"],
            "for":"host-only-private"
        },
        "nocdaudio": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'-nocdaudio'"],
            "addIf": {"hasImgMount":false},
            "for":"private"
        },
        "fraglimit": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "for":"host-only-shared",
            "min":1,
            "max":999,
            "delta":1,
            "label": "Frag Limit",
            "value": ["'+fraglimit'", "value"],
            "optional":true
        },
        "timelimit": { //TODO test
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "for":"host-only-shared",
            "min":1,
            "max":999,
            "delta":1,
            "label": "Time Limit",
            "value": ["'+timelimit'", "value"],
            "optional":true
        },
        "samelevel": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "samelevel",
            "for":"host-only-shared",
            "description": "Prevent Level Advance",
            "value": ["'+samelevel'", "'1'"]
        },
        "gameMode": {
            "modeSupport": ["multiplayer"],
            "for":"host-only-shared",
            "type": "choice",
            "label": "Game Mode",
            "value":["value[0]", "value[1]"],
            "optional":true,
            "choices": [{
                "label": "Deathmatch",
                "value": ["+deathmatch", "1"]
            }, {
                "label": "Coop",
                "value": ["+coop", "1"]
            }]
        },
        "connect": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+connect'", "'nukemnet'"],
            "order":99,
            "for":"client-only-private"
        },
        "scr": { // host takes a screenshot to create a dummy file, indicating that the server is running, so can tell all the clients to launch as well (safer than a constant 5~ seconds delay)
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+screenshot'"],
            "order":10000,
            "for":"host-only-private"
        }
    }

    /*
    quake -nocdaudio  +listen 1
    */

    const swParams = {...commonParams,
        "map_sp": {
            "modeSupport": ["singleplayer"],
            "type": "choice",
            "label": "Map",
            "optional":true,
            "value":["'+map'", "value[0]"],
            "choices": sharewareEpisodeOneLevels
        },
        "map_mp": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Map",
            "value":["'+map'", "value[0]"],
            "for": "host-only-shared",
            "order":99,
            "choices": sharewareEpisodeOneLevels
        }
    };
    
    const fullParams = {...commonParams,
        "map_sp": {
            "modeSupport": ["singleplayer"],
            "type": "choice",
            "label": "Map",
            "optional":true,
            "value":["'+map'", "value[0]"],
            "choices": fullAllLevels
        },
        "map_mp": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Map",
            "for": "host-only-shared",
            "value":["'+map'", "value[0]"],
            "order":99,
            "choices": fullAllLevels
        }
    }

    return {
        "name": "Quake",
        "get": [{
            type:"gog",
            prodId:"1440166133",
            nameId:"quake_the_offering",
            name:"Quake"
        }],
        "executables": {
            "full": {
                midGameJoin: true,
                "mountImg": true,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"quake.exe"}
                },
                "parameters":fullParams,
                "runnables": runnables
            },
            "shareware": {
                midGameJoin: true,
                "mountImg": true,
                "name":"Shareware (DOS)",
                "files": {
                    "main": {"path":"quake.exe"}
                },
                "parameters":swParams,
                "runnables": runnables
            }
        }
    };
}