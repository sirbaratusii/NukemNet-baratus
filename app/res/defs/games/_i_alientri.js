module.exports = function() {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"'];
    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Alien Trilogy",
        "executables": {
            "full": {
                maxPlayers:8,
                "name":"Full (DOS)",
                "mountImg": true,
                midGameJoin: true,
                "files": {
                    "setup": {"path":"SETSOUND.EXE"},
                    "main": {"path":"TRILOGY.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "demo": {
                maxPlayers:8,
                "name":"Demo (DOS)",
                midGameJoin: true,
                "files": {
                    "setup": {"path":"SETSOUND.EXE"},
                    "main": {"path":"TRILOGY.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}