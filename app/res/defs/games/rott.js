module.exports = function ({deepClone, GameDefsType}) {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":GameDefsType.Dosbox_NetTypesAllSpecific}}
        }
    }

    const THB_EpisodeAndLevels = [{
        "label": "1: The HUNT Begins",
        "value": ["1"]
    }, {
        "label": "2: Foggy Mountain",
        "value": ["2"]
    }, {
        "label": "3: The Fourth Door",
        "value": ["3"]
    }, {
        "label": "4: Dark Tunnels",
        "value": ["4"]
    }, {
        "label": "5: Metal Threat",
        "value": ["5"]
    }, {
        "label": "6: Ride 'em, Cowboy!",
        "value": ["6"]
    }, {
        "label": "7: Boom Boom Boom",
        "value": ["7"]
    }, {
        "label": "8: Wall To Wall",
        "value": ["8"]
    }, {
        "label": "9: Too Tall",
        "value": ["9"]
    }, {
        "label": "10: Play Room",
        "value": ["10"]
    }];
    
    
    const DW_EpisodeAndLevels = [{
        "label": "Episode 1: Approach",
        "value": ["1"]
    }, {
        "label": "E1L1: The Thick of it",
        "value": ["1"]
    }, {
        "label": "E1L2: Winding Way",
        "value": ["2"]
    }, {
        "label": "E1L3: Burned and amazed",
        "value": ["3"]
    }, {
        "label": "E1L4: Too much Room",
        "value": ["4"]
    }, {
        "label": "E1L5: Two Key return",
        "value": ["5"]
    }, {
        "label": "E1L6: Spring Surprise",
        "value": ["6"]
    }, {
        "label": "E1L7: General Darian",
        "value": ["7"]
    }, {
        "label": "E1L8: Turn of the Screw",
        "value": ["8"]
    },
    
    {
        "label": "Episode 2: Monastery",
        "value": ["9"]
    }, {
        "label": "E2L1: Into the Castle",
        "value": ["9"]
    }, {
        "label": "E2L2: Great Halls of Fire",
        "value": ["10"]
    }, {
        "label": "E2L3: The Room",
        "value": ["11"]
    }, {
        "label": "E2L4: Spiraling in",
        "value": ["12"]
    }, {
        "label": "E2L5: Rocky Plateau",
        "value": ["13"]
    }, {
        "label": "E2L6: Four Way Chamber",
        "value": ["14"]
    }, {
        "label": "E2L7: Sebastian Krist",
        "value": ["15"]
    }, {
        "label": "E2L8: Elevator Trouble",
        "value": ["16"]
    },

    {
        "label": "Episode 3: Caves below",
        "value": ["17"]
    }, {
        "label": "E3L1: Robotricks",
        "value": ["17"]
    }, {
        "label": "E3L2: Down & Over",
        "value": ["18"]
    }, {
        "label": "E3L3: Dead in five Seconds",
        "value": ["19"]
    }, {
        "label": "E3L4: Clear and present Dangers",
        "value": ["20"]
    }, {
        "label": "E3L5: The angry Quilt",
        "value": ["21"]
    }, {
        "label": "E3L6: Movin' Walls",
        "value": ["22"]
    }, {
        "label": "E3L7: Know thine NME",
        "value": ["23"]
    }, {
        "label": "E3L8: Eight Ways to Hell",
        "value": ["24"]
    },


    {
        "label": "Episode 4: The Slow and the Dead",
        "value": ["25"]
    }, {
        "label": "E4L1: \"Monky\" Business",
        "value": ["25"]
    }, {
        "label": "E4L2: Fire and Brimstone",
        "value": ["26"]
    }, {
        "label": "E4L3: Crushing Defeat",
        "value": ["27"]
    }, {
        "label": "E4L4: Diamonds & Rust",
        "value": ["28"]
    }, {
        "label": "E4L5: Backfire",
        "value": ["29"]
    }, {
        "label": "E4L6: Circles of Fire",
        "value": ["30"]
    }, {
        "label": "E4L7: Lair of El Oscuro",
        "value": ["31"]
    }, {
        "label": "E4L8: Switched around",
        "value": ["32"]
    }, {
        "label": "E4L9: Canyon Chase",
        "value": ["33"]
    }, {
        "label": "E4L10: In the dark Nest",
        "value": ["34"]
    },

    {
        "label": "Misc",
        "value": ["35"]
    }, {
        "label": "1: Dead in two Seconds",
        "value": ["35"]
    }, {
        "label": "2: The Vomitorium",
        "value": ["36"]
    }, {
        "label": "3: This causes an Error! (Joke Level)",
        "value": ["37"]
    }]


    const editRottRott = {"cmd":"edit-space-delimited","file":"rott_rot", "edits":[
        {"entry":"PHONENUMBER", "unquote":true, "value":"'~'"},
        {"entry":"REMOTESOUNDFILE", "unquote":true, "value":"GameRoom?.Params?.rts?.[0]||'~'"},
        {"entry":"GAMELEVELFILE", "unquote":true, "value":"GameRoom?.Params?.smap?.[0]||'~'"},
        {"entry":"COMMBATLEVELFILE", "unquote":true, "value":"GameRoom?.Params?.map?.[0]||'~'"}
    ]};
    
    const commonExecBase = {
        hostInfoOnLaunch: (ctx)=>ctx?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]=='ipx'?'IPX: Starting both dedicated server and client processes to increase netplay performance.':null,
        "files": {
            "main": {"path":"ROTT.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "setup_rot": {"path":"SETUP.ROT", optional:true},
            "rott_rot": {"path":"ROTT.ROT", optional:true},
            "rottipx": {"path":"ROTTIPX.EXE", "runDetachedShell":(obj)=>{return obj?.GameRoom?.ImHost==true}},
            "rottser": {"path":"ROTTSER.EXE"}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'rottser':'rottipx'},
                "runTool":vars.game.runToolOptsMultiplayer,
                "beforeRun":[{"cmd":"edit-space-delimited","file":"setup_rot", "edits":[

                    {"entry":"BAUDRATE", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`},
                    {"entry":"COMPORT", "unquote":true, "value":"1"},
                    {"entry":"IRQ", "unquote":true, "value":"'~'"},
                    {"entry":"UART", "unquote":true, "value":"'~'"},
                    {"entry":"CODENAME", "unquote":true, "value":"MyName"},
                    {"entry":"NETWORKSOCKET", "unquote":true, "value":"'882a'"},
                    {"entry":"NUMPLAYERS", "unquote":true, "value":"GameRoom.NumberOfPlayers"},
                ]},
                editRottRott],

                "hostAlsoRunsAsClient":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]!=='serial'}
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts, "beforeRun":[editRottRott]}
        },
        "parameters": {
            "serverNodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-server'", "'-standalone'", "'-nodes'", `(GameRoom.Params["${GameDefsType.DEDICATEDMODE_PARAM_ID}"] != null && GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]!='serial')? (GameRoom.NumberOfPlayers-1):GameRoom.NumberOfPlayers`],
                "for":"host-only-private",
                "order":-10
            },
            "socket": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-socket'", "34858"],
                "for":"private",
                "order":-9
            },
            // "remoteridicule": {
            //     "modeSupport": ["multiplayer"],
            //     "type": "boolean",
            //     "label": "Allow Remote Ridicule",
            //     "value": "'-remoteridicule'",
            //     "order":-8
            // },
            "map": {
                "modeSupport": ["multiplayer"],
                "type": "file",
                "res":"maps",
                "value": "value",
                "label": "CommBatMap",
                symlink:'forced',
                props: {filters: [{name: 'CommBatMap Files', extensions: ['rtc']}]},
                "optional":true,
                syncOnly:true
            },
            "smap": {
                "modeSupport": ["singleplayer"],
                "type": "file",
                "res":"maps",
                "value": "value",
                "label": "GameMap",
                symlink:'forced',
                props: {filters: [{name: 'GameMap Files', extensions: ['rtl']}]},
                "optional":true,
                syncOnly:true
            },
            "rts": {
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "file",
                "res":"rts",
                "value": "value",
                "label": "Ridicule",
                symlink:'forced',
                props: {filters: [{name: 'RTS Files', extensions: ['rts']}]},
                "optional":true,
                syncOnly:true
            },
            "episodeAndLevel": {
                "modeSupport": ["singleplayer"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["'now'", "'warp'", "value[0]"],
                "optional":true,
                "choices": null,
                "order":-7
            },
            "timelimit": {
                "modeSupport": ["singleplayer"],
                "type": "numrange",
                "min":0,
                "max":9999,
                "delta":1,
                "label": "Timer (Seconds)",
                "value": ["TIMELIMIT", "value"],
                "optional":true,
                "order":-6
            }
        }
    }

    const thb = deepClone(commonExecBase);
    thb.parameters.episodeAndLevel.choices = THB_EpisodeAndLevels;

    const dw = deepClone(commonExecBase);
    dw.parameters.episodeAndLevel.choices = DW_EpisodeAndLevels;

    return {
        "name": "Rise of the Triad",
        "get": [{
            type:"gog",
            prodId:"1207658732",
            nameId:"rise_of_the_triad__dark_war",
            name:"Rise of the Triad: Dark War"
        }, {
            type:"zoom-platform",
            nameId:"rise-of-the-triad-dark-war",
            name:"Rise of the Triad: Dark War: Extreme Edition"
        }, {
            type:"gog",
            prodId:"1722125943",
            nameId:"rise_of_the_triad_ludicrous_edition",
            name:"Rise of the Triad: Ludicrous Edition"
        }],
        "executables": {
            "dw": {
                "name":"Dark War [Full] (DOS)",
                maxPlayers: 11,
                ...dw
            },
            "thb": {
                "name":"The HUNT Begins [Shareware] (DOS)",
                maxPlayers: 5,
                ...thb
            },
        }
    }
}
