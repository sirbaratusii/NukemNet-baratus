
module.exports = function ({}) {

    const common = require('./hl_common.i')();

    const game = 'gearbox';

    return {
        "name": "Half-Life: Opposing Force",
        executables: {
            "compat": {
                name:"Compat",
                "maxPlayers":32,
                midGameJoin: true,
                tips: {html:
                    `<div>You must launch Steam manually before launching the game.</div>
                    <p>
                        Installing addon "Steam Opposing Force Booster Pack" is recommended<br/>
                        https://www.moddb.com/addons/steam-opposing-force-booster-pack
                    </p>
                    `
                },
                "files": {
                    "dedicated": {glob:["hlds.exe", "hlds_run", "hlds"], runDetachedShell:true, optional:true},
                    "main": {glob:["hl.exe", "hl"]},
                    "maindir": {"path":game}
                },
                "runnables": {
                    "multiplayer": {
                        "file":(ctx)=>ctx?.GameRoom?.Params?._dedicated?.[0]?'dedicated':'main',
                        beforeRun: common.writeCommandsCfg
                    }
                },
                parameters: common.getParams(game)
            }
        }
    }
}