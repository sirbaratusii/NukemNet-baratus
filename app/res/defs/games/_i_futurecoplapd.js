module.exports = function() {

    const runnables = {
        "multiplayer": {
            "file":"main",
        },
        "singleplayer": {"file":"main"}
    };

    return {
        "name": "Future Cop: LAPD",
        "executables": {
            "full": {
                "name":"Full",
                "clientInfoOnLaunch": (ctx)=>`Connect to ${ctx.GameRoom.DestIp} (copied to clipboard)`,
                tips: {
                    html: `<div>
                    <b>On Launch, connection details are copied to clipboard</b><br/>
                    <h6>In-Game instructions</h6>

                    <u>Host:</u><br/>
                    1. Host Game<br/>
                    2. Connection type: select Internet TCP/IP Connection For DirectPlay<br/>
                    3. Click "Host", and wait for the other player.<br/>
                    </div>
                    
                    <u>Client:</u><br/>
                    1. Join Game<br/>
                    2. Connection type: select Internet TCP/IP Connection For DirectPlay<br/>
                    3. In the input popup, paste the IP address from the clipboard.<br/>
                    </div>
                    
                    <h6>General Instructions for modern systems</h6>
                    <p>Getting the game to work on modern systems is a bit tricky, bot works even on Windows 11.</p>
                    <p>
                    (1) mount/insert the original game cd.<br/>
                    (2) install using the following installer (select DirectX) https://community.pcgamingwiki.com/files/file/250-future-cop-lapd-installer.<br/>
                    (3) download dgVoodoo from http://dege.freeweb.hu/dgVoodoo2/dgVoodoo2/<br/>
                    (4) copy the dll files from dgVoodoo\\MS\\x86 to your Future Cop LAPD directory.<br/>
                    (5) ready to play
                    </p>
                    `
                },
                networking: {
                    tcpPort: {port:2300, constant:true},
                    udpPort: {port:2350, constant:true},
                },
                clientCopyHostAddressToClipboard:true,
                "files": {
                    "main": {"path":"FCopLAPD.exe"}
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}