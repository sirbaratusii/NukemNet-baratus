module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Jagged Alliance: Deadly Games",
        "get": [{
            type:"gog",
            prodId:"1207658690",
            nameId:"jagged_alliance_deadly_games",
            name:"Jagged Alliance: Deadly Games"
        },{
            type:"zoom-platform",
            nameId:"jagged-alliance-deadly-games",
            name:"Jagged Alliance: Deadly Games"
        }],
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "snd": {"path":"SETSOUND.EXE"},
                    "main": {"path":"DG.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "demo": {
                "maxPlayers":4,
                "name":"Demo (DOS)",
                "files": {
                    "snd": {"path":"SETSOUND.EXE"},
                    "main": {"path":"DGDEMO.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}