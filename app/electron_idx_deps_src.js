
const cachedExports = {
    "@electron/remote/main":require('@electron/remote/main')
}

export function cachedRequire(moduleName) {
    if(cachedExports[moduleName]) {
        return cachedExports[moduleName]
    }
}
